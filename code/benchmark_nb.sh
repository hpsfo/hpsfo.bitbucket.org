#!/bin/bash
myos=`uname -s`

# check whether enable_papi is set
if [ -z "${enable_papi+xxx}" ]; then 
    if [ "$myos" == "Linux" ]; then
        enable_papi="true"
    else
        enable_papi="false"
    fi
fi

# check whether summary is set
if [ -z "${summary+xxx}" ]; then
    summary="true"
fi

# check whether enable-measure-cache is set
if [ -z "${enable_measure_cache+xxx}" ]; then
    enable_measure_cache="false"
fi

for nb in {0,2,4,8} 
#for nb in {8,16,32,64}
do
  echo "====================== Running NB=$nb"
  
  # print header
  echo -e "n \t major \t minor \t rt \t rtse \t flops \t flopsse \t mflops \t mflopsse \t cache_l1_hit_ratio \t cache_l1_hit_ratio_se \t cache_l2_hit_ratio \t cache_l2_hit_ratio_se"

  FILES=`ls -rS ./test/genrmf_data/*_n*.txt | grep -v out_a2_b2_n8_n20.txt`
  for f in $FILES
  do
     ../build/benchmark --kernel=$1 --enable-papi=$enable_papi --workload-type=mincut --mincut-gernm-file=$f --summary=$summary --enable-measure-cache=$enable_measure_cache --triangularize-nb=$nb
  done
done
