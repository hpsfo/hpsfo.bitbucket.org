@echo off
setlocal
set DEFAULT_BUILD_TYPE=Release
set CLEAN_BUILD=false
set CMK_BUILD=false
set _COMPILER=
set CMK_GENERATOR=
set CMK_COMPILER_ARGS=
set CMK_EXTRA_ARGS=
set CMK_BUILD_TYPE=
set CMK_SOURCE_DIR=
set CMK_BUILD_DIR=

:parse_args
if "%1" == "--verbose" (
    shift
    set CMK_EXTRA_ARGS=%CMK_EXTRA_ARGS% -DCMAKE_VERBOSE_MAKEFILE=ON
    goto parse_args
)
if "%1" == "--clean" (
    shift
    set CLEAN_BUILD=true
    goto parse_args
)
if "%1" == "--build" (
    shift
    set CMK_BUILD=true
    goto parse_args
)

rem strip quotes with %~1 (causes problem with if "%1" == "")
if /I "%~1" == "vs2008" (
    shift
    set CMK_GENERATOR=Visual Studio 9 2008
    set _COMPILER=%~1
)
if /I "%~1" == "vs2010" (
    shift
    set CMK_GENERATOR=Visual Studio 10
    set _COMPILER=%~1
)
if /I "%~1" == "mingw" (
    shift
    set CMK_GENERATOR=MinGW Makefiles
    set _COMPILER=%~1
)
if /I "%~1" == "clang" (
    shift
    set CMK_GENERATOR=MinGW Makefiles
    set CMK_COMPILER_ARGS=%CMK_COMPILER_ARGS%-DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++
    set _COMPILER=%~1
)
if "%CMK_GENERATOR%" == "" goto usage

if /I "%~1" == "debug" (
    shift && set CMK_BUILD_TYPE=Debug
)
if /I "%~1" == "release" (
    shift && set CMK_BUILD_TYPE=Release
)
if "%CMK_BUILD_TYPE%" == "" (
    echo Build type not specified, using default value [%DEFAULT_BUILD_TYPE%] && echo.
    set CMK_BUILD_TYPE=%DEFAULT_BUILD_TYPE%
)

set CMK_SOURCE_DIR=%~dp0
set CMK_BUILD_DIR=%CMK_SOURCE_DIR%build_%_COMPILER%
goto build

:debug
echo compiler: "%_COMPILER%"
echo generator: "%CMK_GENERATOR%"
echo compiler args: "%CMK_COMPILER_ARGS%"
echo extra args: "%CMK_EXTRA_ARGS%"
echo build type: "%CMK_BUILD_TYPE%"
echo.
echo clean? %CLEAN_BUILD%
echo build? %CMK_BUILD%
goto end

:build
if "%CLEAN_BUILD%" == "true" (
    if exist "%CMK_BUILD_DIR%" rmdir /S /Q "%CMK_BUILD_DIR%" && echo Removed old build directory && echo.
)

rem Don't pass CMAKE_*_COMPILER flags if build directory already exists:
rem     "You have changed variables that require your cache to be deleted.
rem      Configure will be re-run and you may have to reset some variables."
if exist "%CMK_BUILD_DIR%" (
    set CMK_COMPILER_ARGS=
) else (
    mkdir "%CMK_BUILD_DIR%"
)
pushd "%CMK_BUILD_DIR%"
cmake -G "%CMK_GENERATOR%" -DCMAKE_BUILD_TYPE="%CMK_BUILD_TYPE%" %CMK_COMPILER_ARGS% %CMK_EXTRA_ARGS% "%CMK_SOURCE_DIR%"
if errorlevel 1 goto error
popd

if "%CMK_BUILD%" == "true" (
  cmake --build "%CMK_BUILD_DIR%" --config "%CMK_BUILD_TYPE%"
  if errorlevel 1 goto error
)
goto end

:usage
echo usage: %~n0 [--clean] [--build] [--verbose]
echo             ^<vs2008 ^| vs2010 ^| mingw ^| clang^>
echo             [debug ^| release]
goto end

:error
echo.
echo error: process exited with status %ERRORLEVEL%
goto end

:end
