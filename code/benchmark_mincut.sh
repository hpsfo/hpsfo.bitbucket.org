#!/bin/bash
myos=`uname -s`

# check whether enable_papi is set
if [ -z "${enable_papi+xxx}" ]; then 
    if [ "$myos" == "Linux" ]; then
        enable_papi="true"
    else
        enable_papi="false"
    fi
fi

# check whether summary is set
if [ -z "${summary+xxx}" ]; then
    summary="true"
fi

# check whether inceval is set
if [ -z "${inceval+xxx}" ]; then
    inceval="true"
fi

# check whether enable-measure-cache is set
if [ -z "${enable_measure_cache+xxx}" ]; then
    enable_measure_cache="false"
fi

# print header
echo -e "n \t major \t minor \t rt \t rtse \t flops \t flopsse \t mflops \t mflopsse \t cache_l1_hit_ratio \t cache_l1_hit_ratio_se \t cache_l2_hit_ratio \t cache_l2_hit_ratio_se"

FILES=`ls -rS ./test/genrmf_data/*_n*.txt | grep -v out_a2_b2_n8_n20.txt | grep -v out_a2_b2_n8_n20_unordered.txt`
for f in $FILES
do
    # test the mincut first
    ../build/mincut --kernel=$1 --input-file=$f > tmp.sol 2> /dev/null
    # target solution name
    s=${f%.txt}
    s=${s##*/}".sol"
    if diff tmp.sol ../mincut_solutions/$s >/dev/null ; then
        ../build/benchmark --kernel=$1 --enable-papi=$enable_papi --workload-type=mincut --input-file=$f --summary=$summary --inceval=$inceval --enable-measure-cache=$enable_measure_cache #--num-runs=1 --warm-up-runs=0
    else
        echo "ERROR: mincut produced incorrect result for $f" >&2
    fi    
done
rm tmp.sol
