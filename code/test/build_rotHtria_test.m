
clear all;

% defining 0 and 1 as symbols too, solves the problem
sym_0 = sym('0');
sym_1 = sym('1');

c0 = sym('c0');
c1 = sym('c1');
c2 = sym('c2');
c3 = sym('c3');
c4 = sym('c4');
c5 = sym('c5');
c6 = sym('c6');
c7 = sym('c7');

s0 = sym('s0');
s1 = sym('s1');
s2 = sym('s2');
s3 = sym('s3');
s4 = sym('s4');
s5 = sym('s5');
s6 = sym('s6');
s7 = sym('s7');

% create H orthogonal matrix using the sin and cos symbols
% filling in the first rotation
I=repmat(sym_0,9,9);
for i=1:9
    I(i,i)=sym_1;
end
H = I;
H(1:2,2:3) = [c0 s0; -s0 c0]

% build the 2nd rotation and update H
G = I;
G(2:3,3:4) = [c1 s1; -s1 c1]
H = G*H;

% build the 3rd rotation and update H
G = I;
G(3:4,4:5) = [c2 s2; -s2 c2]
H = G*H;

% build the 4rth rotation and update H
G = I;
G(4:5,5:6) = [c3 s3; -s3 c3]
H = G*H;

% build the 5th rotation and update H
G = I;
G(5:6,6:7) = [c4 s4; -s4 c4]
H = G*H;

% build the 6th rotation and update H
G = I;
G(6:7,7:8) = [c5 s5; -s5 c5]
H = G*H;

% build the 7th rotation and update H
G = I;
G(7:8,8:9) = [c6 s6; -s6 c6]
H = G*H;
    
% generate the C++ code :D
for i=1:9
    for j=1:9
        fprintf('h(%d, %d)=%s;\t', i - 1, j - 1, char(H(i, j)));
    end
    fprintf('\n');
end
