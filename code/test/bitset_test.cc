/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : bitset_test.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 03.10.2012
// Description : Test suite corresponding to the bitset implementation
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <map>

#include "bitset.h"

using namespace std;

TEST(Bitset, GetSetElement)
{
	tbitset bitset(10);
	bitset.set(3);

	ASSERT_FALSE(bitset.test(0));
	ASSERT_FALSE(bitset.test(1));
	ASSERT_FALSE(bitset.test(2));
	ASSERT_TRUE (bitset.test(3));
	ASSERT_FALSE(bitset.test(4));
	ASSERT_FALSE(bitset.test(5));
	ASSERT_FALSE(bitset.test(6));
	ASSERT_FALSE(bitset.test(7));
	ASSERT_FALSE(bitset.test(8));
	ASSERT_FALSE(bitset.test(9));
}

TEST(Bitset, ConstructFromSet)
{
	set<int> existing;
	existing.insert(0);
	existing.insert(3);
	existing.insert(8);

	tbitset bitset(10, existing);

	ASSERT_TRUE (bitset.test(0));
	ASSERT_FALSE(bitset.test(1));
	ASSERT_FALSE(bitset.test(2));
	ASSERT_TRUE (bitset.test(3));
	ASSERT_FALSE(bitset.test(4));
	ASSERT_FALSE(bitset.test(5));
	ASSERT_FALSE(bitset.test(6));
	ASSERT_FALSE(bitset.test(7));
	ASSERT_TRUE (bitset.test(8));
	ASSERT_FALSE(bitset.test(9));
}

TEST(Bitset, Size)
{
	tbitset bitset(10);
	bitset.set(3);
	bitset.set(5);
	ASSERT_EQ(2, bitset.size());
}

TEST(Bitset, UseBitsetAsMapKey)
{
	map<tbitset, double> cache;

	// build set {3, 5, 8, 9}
	tbitset bitset1(10);
	bitset1.set(3);
	bitset1.set(5);
	bitset1.set(8);
	bitset1.set(9);

	// build set {4, 6}
	tbitset bitset2(10);
	bitset2.set(4);
	bitset2.set(6);

	// build set {0, 1}
	tbitset bitset3(10);
	bitset3.set(0);
	bitset3.set(1);

	// build set {0, 1}
	tbitset bitset4(10);
	bitset4.set(0);
	bitset4.set(1);

	// populate the cache
	cache[bitset1] = 0.1;
	cache[bitset2] = 0.2;
	cache[bitset3] = 0.3;
	cache[bitset4] = 0.4;

	// make sure that the cache works
	EXPECT_DOUBLE_EQ(0.1, cache[bitset1]);
	EXPECT_DOUBLE_EQ(0.2, cache[bitset2]);
	EXPECT_DOUBLE_EQ(0.4, cache[bitset3]);
	EXPECT_DOUBLE_EQ(0.4, cache[bitset4]);
}
