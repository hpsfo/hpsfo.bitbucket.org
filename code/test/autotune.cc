//============================================================================
// Name        : autotune.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Creates representative matrix workloads and explores the
//				 different possibilities.
//============================================================================

#include <assert.h>
#include <iostream>
#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string>
#include <time.h>
#include <papi.h>

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/stats.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/variance.hpp>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/config.hpp>
#include <boost/program_options/environment_iterator.hpp>
#include <boost/program_options/eof_iterator.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/version.hpp>

#include <boost/chrono.hpp>

#include "hp_adjlist_bidir.h"
#include "sfo_vector.h"
#include "sfo_matrix.h"
#include "sfo_matrix_tria.h"
#include "sfo_function_iwata.h"
#include "sfo_function_mincut.h"
#include "sfo_function_logdet.h"
#include "sfo_function_corpussel.h"
#include "logdet_moons_generator.h"

using namespace std;
using namespace boost::accumulators;
using namespace boost::chrono;
namespace po = boost::program_options;

static int  WARM_UP_RUNS;
static int  NUM_RUNS;
static int  MATRIX_SIZE;
static bool ENABLE_PAPI;
static bool SUMMARY, INCEVAL;
static bool ENABLE_MEASURE_CACHE;
static string omp_scheduler = "static";
static int n, num_threads = 1, omp_chunk = 70;
static string genrot_type = "cblas_drotg";
static int j_begin, block;
static void (*genrot)(double*, double*, double*, double*, double*);
static long minor_iter_expected, major_iter_expected;
static long minor_iter, major_iter;
static uint64_t flops_count;
static tsfo_matrix_tria<double>* matrix;
static accumulator_set<double, stats<tag::mean, tag::variance> > real_time_acc,
	mflops_acc, l1_hit_ratio_acc, l2_hit_ratio_acc, l3_hit_ratio_acc, tlb_hit_ratio_acc;
static vector<double> real_time_vec, mflops_vec, l1_hit_ratio_vec,
	l2_hit_ratio_vec, l3_hit_ratio_vec, tlb_hit_ratio_vec;
static accumulator_set<long double, stats<tag::mean, tag::variance> > fp_ops_acc;
static vector<long double> fp_ops_vec;

static void measure_cache_hit_ratio(void (*workload)(long&, long&, uint64_t&), int* events,
		accumulator_set<double, stats<tag::mean, tag::variance> >& acc, vector<double>& vec) {
	const int DCA_INDEX  = 0;
	const int DCM_INDEX  = 1;

	int error_code, retval;
	char error_string[PAPI_MAX_STR_LEN + 1];

	const int NUM_EVENTS = 2;
	long long values[NUM_EVENTS];
	int eventset = PAPI_NULL;
	double hit_ratio;

	// ============== setup PAPI second pass ==============
	retval = PAPI_library_init(PAPI_VER_CURRENT);
	if (retval != PAPI_VER_CURRENT && retval > 0) {
		fprintf(stderr, "PAPI_library_init error \n");
		exit(EXIT_FAILURE);
	}

	// try adding new extra events
	error_code = PAPI_create_eventset(&eventset);
	if (error_code != PAPI_OK) {
		PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
		fprintf(stderr, "PAPI_create_eventset error (%d): %s\n", error_code, error_string);
		exit(EXIT_FAILURE);
	}

	error_code = PAPI_add_events(eventset, events, NUM_EVENTS);
	if (error_code != PAPI_OK) {
		PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
		fprintf(stderr, "PAPI_add_events(...) error (%d): %s\n", error_code, error_string);
		exit(EXIT_FAILURE);
	}

	// run benchmarks again
	for (int i = 0; i < NUM_RUNS; ++i) {
		// start measuring
		error_code = PAPI_start_counters(events, NUM_EVENTS);
		if (error_code != PAPI_OK) {
			PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
			fprintf(stderr, "PAPI_start_counters error (%d): %s\n", error_code, error_string);
		}

		// invoke workload
		(*workload)(minor_iter, major_iter, flops_count);

		// get PAPI data
		error_code = PAPI_stop_counters(values, NUM_EVENTS);
		if (error_code != PAPI_OK) {
			PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
			fprintf(stderr, "PAPI_stop_counters error (%d): %s\n", error_code, error_string);
		}

		hit_ratio = 100.0*(((double) values[DCA_INDEX] - (double) values[DCM_INDEX]) / (double) values[DCA_INDEX]);

		if (SUMMARY) {
			// feed the accumulators
			acc(hit_ratio);
		}

		vec.push_back(hit_ratio);

		fprintf(stderr, ".");
		fflush (stdout);
	}

	// ============== shutdown PAPI second pass ==============
	PAPI_shutdown();
}

/**
 * Generic implementation for running a benchmark. Invoking this function will as side effect
 * populate the following accumulators: real_time_acc, fp_ins_acc and mflops_acc
 */
static void run_benchmark(void (*workload)(long&, long&, uint64_t&)) {
	// warm up runs
	for (int i = 0; i < WARM_UP_RUNS; ++i) {
		// invoke workload
		(*workload)(minor_iter, major_iter, flops_count);

		if (i == 0) {
			minor_iter_expected = minor_iter;
			major_iter_expected = major_iter;

		} else {
			if ((minor_iter != minor_iter_expected) || (major_iter != major_iter_expected)) {
				fprintf(stderr, "ERROR: the number of iterations differ %ld!=%ld or %ld!=%ld\n",
					minor_iter, minor_iter_expected, major_iter, major_iter_expected);
			}
		}

		fprintf(stderr, ".");
		fflush (stdout);
	}

	int error_code, retval;
	char error_string[PAPI_MAX_STR_LEN + 1];

	for (int i = 0; i < NUM_RUNS; ++i) {
		long long fp_ops2;
		float real_time2, cpu_time2, mflops2;
		double real_time, cpu_time, mflops;
		long double fp_ops;
		system_clock::time_point start;

		if (ENABLE_PAPI) {
			error_code = PAPI_flops(&real_time2, &cpu_time2, &fp_ops2, &mflops2);
			if (error_code != PAPI_OK) {
				PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
				fprintf(stderr, "PAPI_flops error (%d): %s\n", error_code, error_string);
				exit(EXIT_FAILURE);
			}

		} else {
			// benchmark using high-resolution timer
			start = system_clock::now();
		}

		// invoke workload
		(*workload)(minor_iter, major_iter, flops_count);
		if (i == 0) {
			minor_iter_expected = minor_iter;
			major_iter_expected = major_iter;

		} else {
			if ((minor_iter != minor_iter_expected) || (major_iter != major_iter_expected)) {
				fprintf(stderr, "ERROR: the number of iterations differ %ld!=%ld or %ld!=%ld\n",
					minor_iter, minor_iter_expected, major_iter, major_iter_expected);
				continue;
			}
		}

		// ============== get PAPI data ==============
		if (ENABLE_PAPI) {
			error_code = PAPI_flops(&real_time2, &cpu_time2, &fp_ops2, &mflops2);
			if (error_code != PAPI_OK) {
				PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
				fprintf(stderr, "PAPI_flops error (%d): %s\n", error_code, error_string);
			}

			// save the values
			fp_ops 	  = static_cast<long double>(fp_ops2);
			real_time = real_time2;
			cpu_time  = cpu_time2;
			mflops 	  = mflops2;

			const int NUM_EVENTS = 2;
			long long values[NUM_EVENTS];
			// stop the counters
			error_code = PAPI_stop_counters(values, NUM_EVENTS);
			if (error_code != PAPI_OK) {
				PAPI_perror(error_code, error_string, PAPI_MAX_STR_LEN);
				fprintf(stderr, "PAPI_stop_counters error (%d): %s\n", error_code, error_string);
			}

		} else {
			// use high-resolution timer
			duration<double> sec = system_clock::now() - start;
			real_time = sec.count();
			fp_ops 	  = static_cast<long double>(flops_count);
			mflops 	  = flops_count/(1e6*real_time);
		}

		// coLect resulting data
		real_time_vec.push_back(real_time);
		fp_ops_vec.push_back(fp_ops);
		mflops_vec.push_back(mflops);

		if (SUMMARY) {
			// feed the accumulators
			real_time_acc(real_time);
			fp_ops_acc(fp_ops);
			mflops_acc(mflops);
		}

		fprintf(stderr, ".");
		fflush (stdout);
	}

	// ============== shutdown PAPI first pass ==============
	if (ENABLE_PAPI) {
		// shutdown only once
		PAPI_shutdown();

	}

	// second pass
	if (ENABLE_PAPI && ENABLE_MEASURE_CACHE) {
		const int NUM_EVENTS = 2;
		int events_l1[NUM_EVENTS] = {PAPI_L1_DCA, PAPI_L1_DCM };
		measure_cache_hit_ratio(workload, events_l1, l1_hit_ratio_acc, l1_hit_ratio_vec);
//		int events_l2[NUM_EVENTS] = {PAPI_L2_DCA, PAPI_L2_DCM };
//		measure_cache_hit_ratio(workload, events_l2, l2_hit_ratio_acc, l2_hit_ratio_vec);
//		int events_l3[NUM_EVENTS] = {PAPI_L3_DCA, PAPI_L3_DCM };
//		measure_cache_hit_ratio(workload, events_l3, l3_hit_ratio_acc, l3_hit_ratio_vec);
//		int events_tlb[NUM_EVENTS] = {PAPI_TLB_?, PAPI_TLB_DM };
//		measure_cache_hit_ratio(workload, events_tlb, tlb_hit_ratio_acc, tlb_hit_ratio_vec);
	}

	if (!SUMMARY) {
		if (ENABLE_MEASURE_CACHE) {
			for (int i=0; i < NUM_RUNS; ++i) {
				printf("%s\t%d\t%d\t%d\t%d\t%d\t%e\t%e\t%Le\t%e\t%e\t%e\t%e\t%e\t%e\t%e \n", omp_scheduler.c_str(), omp_chunk, num_threads, num_threads, tsfo_matrix_tria<double>::TRIANGULARIZE_NB, 
					n, real_time_vec[i], 0.0, fp_ops_vec[i], 0.0, mflops_vec[i], 0.0, l1_hit_ratio_vec[i], 0.0, l2_hit_ratio_vec[i], 0.0);
			}
		} else {
			for (int i=0; i < NUM_RUNS; ++i) {
				printf("%s\t%d\t%d\t%d\t%d\t%d\t%e\t%e\t%Le\t%e\t%e\t%e \n", omp_scheduler.c_str(), omp_chunk, num_threads, num_threads, tsfo_matrix_tria<double>::TRIANGULARIZE_NB, 
					n, real_time_vec[i], 0.0, fp_ops_vec[i], 0.0, mflops_vec[i], 0.0);
			}
		}
	}
}

static void matrix_triangularize(long &minor_iter, long &major_iter, uint64_t &flops_count) {
	// execute the matrix triangularization
	matrix->triangularize(j_begin, block, genrot);
	flops_count = matrix->cols()*matrix->cols()/2 - matrix->cols()*j_begin + j_begin*j_begin/2;
}

int main(int argc, char** argv) {
#if !defined(NDEBUG) || defined(DEBUG)
	fprintf(stderr, "Warning: you are running in debug mode - assertions are enabled. \n");

        assert(tsfo_matrix<double>::MAX_M_N == tsfo_vector<double>::MAX_SIZE);
        fprintf(stderr, "SFO_MAX_M_N=%d, set environment 'SFO_MAX_M_N' to change this size\n", tsfo_matrix<double>::MAX_M_N);
#endif

	try {
		// declare supported options
		string kernel, workload, input_file;

		po::options_description desc("Sfo benchmark options");
		desc.add_options()
			("help", "produce help message")
			("warm-up-runs", po::value<int>(&WARM_UP_RUNS)->default_value(3), "warm up runs e.g. 3")
			("num-runs", po::value<int>(&NUM_RUNS)->default_value(10), "number of runs e.g. 10")
			("omp-scheduler", po::value<string>(&omp_scheduler)->default_value(omp_scheduler), "omp scheduler e.g. static, dynamic, guided")
			("omp-chunk", po::value<int>(&omp_chunk)->default_value(omp_chunk), "omp chunk e.g. 70")
			("num-threads", po::value<int>(&num_threads)->default_value(num_threads), "number of threads e.g. 1")
			("n", po::value<int>(&n)->default_value(100), "matrix size e.g. 100")
			("begin", po::value<int>(&j_begin)->default_value(3), "begin column e.g. 3")
			("block", po::value<int>(&block)->default_value(1), "delete column block e.g. 1")
			("genrot-type", po::value<string>(&genrot_type)->default_value(genrot_type), "genrot type e.g. cblas_drotg, dlartg, dlartgp, sqrt")
			("enable-papi", po::value<bool>(&ENABLE_PAPI)->default_value(false), "enable PAPI e.g. true, false")
			("summary", po::value<bool>(&SUMMARY)->default_value(true), "whether to output summarized data or raw observations e.g. true, false")
			("triangularize-nb", po::value<int>(&tsfo_matrix_tria<double>::TRIANGULARIZE_NB)->default_value(tsfo_matrix_tria<double>::TRIANGULARIZE_NB), "triangularize NB block size e.g. 4")
			("enable-measure-cache", po::value<bool>(&ENABLE_MEASURE_CACHE)->default_value(false), "enable measure cache e.g. false")
		;

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if (vm.count("help")) {
			cout << desc << "\n";
			return EXIT_FAILURE;

		} else {

			if (genrot_type == "cblas_drotg") {
				genrot = genrot_low_precision_fast;
			} else
			if (genrot_type == "dlartg") {
				genrot = genrot_high_precision_slow;
			} else
			if (genrot_type == "dlartgp") {
				genrot = genrot_positive_diagonal;
			} else
			if (genrot_type == "sqrt") {
				genrot = genrot_sqrt;
			}

			// initialize random seed
			srand(time(NULL));

			matrix = new tsfo_matrix_tria<double>();
			matrix->random(n, n);

			// run benchmark
			run_benchmark(matrix_triangularize);

			// compute the square root of N once
			double sqrtn = sqrt(NUM_RUNS);

			// print progress to stderr
			fprintf(stderr, "%d,%e,%e,%Le,%Le,%e,%e \n", n,
				mean(real_time_acc), 	sqrt(variance(real_time_acc))/sqrtn,
				mean(fp_ops_acc),    	sqrt(variance(fp_ops_acc))/sqrtn,
				mean(mflops_acc),    	sqrt(variance(mflops_acc))/sqrtn);

			if (SUMMARY) {
				printf("%d\t%e\t%e\t%Le\t%Le\t%e\t%e\t%e\t%e \n", n,
						mean(real_time_acc), 	sqrt(variance(real_time_acc))/sqrtn,
						mean(fp_ops_acc),    	sqrt(variance(fp_ops_acc))/sqrtn,
						mean(mflops_acc),    	sqrt(variance(mflops_acc))/sqrtn,
						mean(l1_hit_ratio_acc), sqrt(variance(l1_hit_ratio_acc))/sqrtn
				);
			}

			delete matrix;
		}
	}
	catch(std::exception& e) {
		cerr << "Exception: " << e.what() << "\n";
		return EXIT_FAILURE;
	}
	catch(...) {
		cerr << "Exception of unknown type!\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
