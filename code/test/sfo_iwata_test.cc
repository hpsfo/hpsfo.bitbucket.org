/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_iwata_test.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Test suite corresponding to the Iwata application
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>

#include "utils.h"
#include "sfo_vector.h"
#include "iwata_sf_context.h"
#include "sfo_kernel_factory.h"

#include "sfo_function_iwata.h"
#include "sfo_iwata_testdata.h"

#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

// ================= Tests Iwata evaluation

static void iwata_eval_test(const IwataEvalTestData* test_data, const size_t num_tests) {
	int n;
	size_t i, j, x_size;
	double expected_result, actual_result;
	const int *x_data;

	for (i = 0; i < num_tests; i++) {
		n = test_data[i].n;
		tiwata_sf_context iwata_sf_context(n);

		x_data = test_data[i].x_data;
		x_size = test_data[i].x_size;
		expected_result = test_data[i].result;

		tsfo_vector<int> vec;
		for (j = 0; j < x_size; j++) {
			vec.append(x_data[j]);
		}

		actual_result = iwata_sf_context.f(vec);
		EXPECT_DOUBLE_EQ(expected_result, actual_result);
	}
}

TEST(IwataEval, Random)
{
	iwata_eval_test(iwata_eval_random_test_data, iwata_eval_num_random_tests);
}

TEST(IwataEval, Exhaustive)
{
	iwata_eval_test(iwata_eval_exhaustive_test_data, iwata_eval_num_exhaustive_tests);
}

TEST(IwataEval, EmptySet)
{
	int n;
	tsfo_vector<double> vec;

	for (n = 0; n < 1000; n++) {
		tiwata_sf_context iwata_sf_context(n);
		EXPECT_DOUBLE_EQ(0.0, iwata_sf_context.f(vec));
	}
}

// ================= Tests Iwata application

static void iwata_test(const IwataMinimizeTestData* test_data, const size_t num_tests) {
	int n;
	size_t i, j, expected_size;
	const int* expected_data;

	for (i = 0; i < num_tests; i++) {
		n = test_data[i].n;
		expected_data = test_data[i].result_data;
		expected_size = test_data[i].result_size;

		int* optimal;
		long optimal_size, major_iter, minor_iter;
		sfo_function_iwata(n, optimal, optimal_size, major_iter, minor_iter);

		EXPECT_EQ(expected_size, optimal_size);
		if (expected_size == optimal_size) {
			for (j = 0; j < expected_size; j++) {
				EXPECT_EQ(expected_data[j], optimal[j]);
			}
		}

		delete[] optimal;
	}
}

TEST(Iwata, HPSFO_Iwata)
{
    // set the kernel
    set_min_norm_point_hpsfo_kernel();
	iwata_test(iwata_min_test_data, iwata_min_num_tests);
}
