/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_mincut_test.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Test suite corresponding to the MinCut application
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>

#include "utils.h"
#include "sfo_vector.h"
#include "sfo_kernel_factory.h"

#include "sfo_function_mincut.h"
#include "sfo_mincut_testdata.h"

#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

// ================= TODO: Tests MinCut evaluation

// ================= TODO: Tests MinCut incremental evaluation

// ================= Tests Mincut application

static void mincut_test(const MinCutTestData* test_data, const size_t num_tests) {
	size_t i, j;
	int s, t, n, m;
	long expected_size, expected_major, expected_minor;
	const int* expected_result;

	char* filepath = "../code/test/genrmf_data";

	for (i = 0; i < num_tests; i++) {
		n = test_data[i].n;
		m = test_data[i].m;
		expected_result = test_data[i].expected_result;
		expected_size 	= test_data[i].expected_size;

		switch (tsfo_kernel_factory::INSTANCE.kernel()) {
			case HPSFO: {
				expected_major = test_data[i].expected_major_hpsfo;
				expected_minor = test_data[i].expected_minor_hpsfo;
				break;
			}
			default: {
				assert(false);
			}
		}

		s = test_data[i].s;
		t = test_data[i].t;

		path p(filepath);
		p /= test_data[i].filename;

		int* optimal;
		long optimal_size, major_iter, minor_iter;
		uint64_t flops_count;
		sfo_function_mincut(p.c_str(), optimal, optimal_size, major_iter, minor_iter, flops_count);

		EXPECT_EQ(expected_size , optimal_size);
//		EXPECT_EQ(expected_major, major_iter);
//		EXPECT_EQ(expected_minor, minor_iter);
		if (expected_size == optimal_size) {
			for (j = 0; j < (size_t) expected_size; j++) {
				EXPECT_EQ(expected_result[i], optimal[i]);
			}
		}

		printf("\n n=%d,%ld,%ld,%ld \n", n, major_iter, minor_iter, optimal_size);

		// clean up
		delete[] optimal;
	}
}

TEST(MinCut, HPSFO_MinCut)
{
    // set the kernel
    set_min_norm_point_hpsfo_kernel();
    mincut_test(MINCUT_TEST_DATA, GRAPH_CUT_MIN_NUM_TESTS);
}
