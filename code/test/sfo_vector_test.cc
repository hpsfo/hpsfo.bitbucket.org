/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : vector_test.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Test suite for the class tsfo_vector
//============================================================================

#include <gtest/gtest.h>
#include <limits.h>
#include <math.h>

#include "sfo_vector.h"
#include "utils.h"

// vector test
class TVectorTest: public ::testing::TestWithParam<int> {
protected:
	TVectorTest() {
		// empty
	}

	virtual void SetUp() {
		// empty
	}

	virtual void TearDown() {
		// empty
	}
};

TEST_F(TVectorTest, Vector_Initialization) {
	// initialize a size 5 vector
	tsfo_vector<double> a(5, 0.0);

	// check that all elements are initialized to zero
	for (int i = 0; i < a.size(); ++i) {
		EXPECT_DOUBLE_EQ(0.0, a[i]);
	}

	// assign one element
	a[2] = 1.5;

	// check that the element was properly assigned
	EXPECT_DOUBLE_EQ(1.5, a[2]);
}

TEST_F(TVectorTest, Vector_DeleteElem) {
	// initialize a size 5 vector
	tsfo_vector<double> a(5, 0.0);

	// check that all elements are initialized incrementally
	for (int i = 0; i < a.size(); ++i) {
		a[i] = i + 1;
	}

	// delete 2nd element
	a.delete_elem(2);

	// check that the element was properly deleted
	EXPECT_EQ(4, a.size());
	EXPECT_DOUBLE_EQ(1.0, a[0]);
	EXPECT_DOUBLE_EQ(2.0, a[1]);
	EXPECT_DOUBLE_EQ(4.0, a[2]);
	EXPECT_DOUBLE_EQ(5.0, a[3]);
}

TEST_F(TVectorTest, Vector_Ln) {
	// initialize a size 5 vector
	tsfo_vector<double> a(3, 10.0);

	// apply the ln transformation
	tsfo_vector<double> b = a.ln();

	EXPECT_DOUBLE_EQ(2.302585092994045, b[0]);
	EXPECT_DOUBLE_EQ(2.302585092994045, b[1]);
	EXPECT_DOUBLE_EQ(2.302585092994045, b[2]);
}
