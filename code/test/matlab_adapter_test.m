% ==================================
% test integration
% ==================================

clear all;

% define the real context
param.n = 28;
param.ground = 1:28;

% define the function handler
F = @(param, A) length(A)*(param.n-length(A))-sum(5*A-2*param.n);

% invoke the HPSFO kernel
subset = hpsfo_matlab(F, param)