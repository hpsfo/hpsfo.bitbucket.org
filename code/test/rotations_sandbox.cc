/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : rotations_sandbox.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 19.07.2012
// Description : Test suite for the linear sort implementation
//============================================================================

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include <mkl_lapack.h>
#include <mkl_lapacke.h>
#include <mkl_cblas.h>

static const int ROWS = 3;
static const int COLS = 3;
static double matrix[ROWS][COLS];

static void print_matrix() {
	int rows = ROWS;
	int cols = COLS;
	printf("matrix of dimensions %dx%d: \n", rows, cols);
	for (int i=0; i < rows; ++i) {
		for (int j=0; j < cols; ++j) {
			printf("%f ", matrix[j][i]);
		}
		printf("\n");
	}
}

int main(int argc, char** argv) {
	// create a simple matrix in "column major" with 1's in the
	// upper triangule and 2s in the band below the diagonal
	// now I want to get rid of the 2s using Givens rotations
	matrix[0][0] = 1; matrix[1][0] = 1; matrix[2][0] = 1;
	matrix[0][1] = 2; matrix[1][1] = 1; matrix[2][1] = 1;
	matrix[0][2] = 0; matrix[1][2] = 2; matrix[2][2] = 1;
	print_matrix();

	double cc[COLS];
	double ss[COLS];

	// create the rotations
	for (int j = 0; j < COLS; j++) {
		double a = matrix[j][j];
		double b = matrix[j][j + 1];
		double c = 0.0;
		double s = 0.0;
		double r = 0.0;
//  	cblas_drotg(&a, &b, &c, &s);
		dlartg(&a, &b, &c, &s, &r);
//		printf("a=%f, b=%f, c[%d]=%f, s[%d]=%f \n", a, b, j, c, j, s);

		for (int jj = j; jj < COLS; ++jj) {
			cc[jj] = c;
			ss[jj] = s;
		}

		// apply the rotations along the diagonal
		char side      = 'L';
		char pivot     = 'V';
		char direct    = 'F';
		lapack_int m   = ROWS - j;
		lapack_int n   = COLS - j;
		lapack_int lda = ROWS;
		dlasr(&side, &pivot, &direct, &m, &n, &cc[j], &ss[j], ((double*) matrix) + j*ROWS + j, &lda);

		print_matrix();
	}

	return 0;
}
