/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_quadpot_test.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 17.10.2012
// Description : Test suite corresponding to the QuadPot application
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>

#include "utils.h"
#include "sfo_vector.h"
#include "quadpot_sf_context.h"
#include "quadpot_sf_inc_context.h"
#include "sfo_kernel_factory.h"

#include "sfo_function_quadpot.h"
#include "quadpot_problem_reader.h"
#include "sfo_quadpot_testdata.h"

#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

static const string DATA_FILEPATH = "../code/test/genquadpot_data/";

// ================= Tests QuadPot evaluation

static void quadpot_eval_test(string* test_data, int num_tests, tsfo_vector<int> (*fnc_set)(int, int), double* expected_data) {
	for (int i=0; i < num_tests; ++i) {
		printf("processing %s \n", test_data[i].c_str());
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		// read the problem
		tquadpot_problem_reader reader;
		file >> reader;

		// setup the quadpot application
		tquadpot_sf_context sf_context(reader.w(), reader.regions());

		// invoke function and check results
		int begin = 0;
		int end	  = reader.n() - 1;
		tsfo_vector<int> set = fnc_set(begin, end);
		double result = sf_context.f(set);

		EXPECT_NEAR(expected_data[i], result, 1e-12);
	}
}

TEST(QuadPotEval, EmptySet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::empty_set, QUADPOT_EXPECTED_EVAL[EMPTY_SET_DATA]);
}

TEST(QuadPotEval, FullSet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::full_set , QUADPOT_EXPECTED_EVAL[FULL_SET_DATA]);
}

TEST(QuadPotEval, FirstSet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::first_set, QUADPOT_EXPECTED_EVAL[FIRST_SET_DATA]);
}

TEST(QuadPotEval, LastSet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::last_set , QUADPOT_EXPECTED_EVAL[LAST_SET_DATA]);
}

TEST(QuadPotEval, EvenSet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::even_set, QUADPOT_EXPECTED_EVAL[EVEN_SET_DATA]);
}

TEST(QuadPotEval, OddSet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::odd_set, QUADPOT_EXPECTED_EVAL[ODD_SET_DATA]);
}

// ================= Tests QuadPot inc evaluation

static void quadpot_inc_eval_test(string* test_data, int num_tests, tsfo_vector<int> (*fnc_set)(int, int), double* expected_data) {
	for (int i=0; i < num_tests; ++i) {
		printf("processing %s \n", test_data[i].c_str());
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		// read the problem
		tquadpot_problem_reader reader;
		file >> reader;

		// setup the quadpot application
		tquadpot_sf_inc_context sf_inc_context(reader.w(), reader.regions());

		// invoke function and check results
		int begin = 0;
		int end	  = reader.n() - 1;
		tsfo_vector<int> set = fnc_set(begin, end);
		for (int j = 0; j < set.size() - 1; ++j) {
			sf_inc_context.f_inc(set[j]);
		}

		double result = sf_inc_context.f_inc(set[set.size() - 1]);

		EXPECT_NEAR(expected_data[i], result, 1e-12);
	}
}

TEST(QuadPotIncEval, FullSet)
{
	quadpot_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::full_set , QUADPOT_EXPECTED_EVAL[FULL_SET_DATA]);
}

TEST(QuadPotIncEval, FirstSet)
{
	quadpot_inc_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::first_set, QUADPOT_EXPECTED_EVAL[FIRST_SET_DATA]);
}

TEST(QuadPotIncEval, LastSet)
{
	quadpot_inc_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::last_set , QUADPOT_EXPECTED_EVAL[LAST_SET_DATA]);
}

TEST(QuadPotIncEval, EvenSet)
{
	quadpot_inc_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::even_set, QUADPOT_EXPECTED_EVAL[EVEN_SET_DATA]);
}

TEST(QuadPotIncEval, OddSet)
{
	quadpot_inc_eval_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS, tset_factory::odd_set, QUADPOT_EXPECTED_EVAL[ODD_SET_DATA]);
}

// ================= Tests QuadPot application

static void quadpot_test(string* test_data, int num_tests) {
	for (int i=0; i < num_tests; ++i) {
		const char* filename = (DATA_FILEPATH + test_data[i]).c_str();

		int* optimal 	 = NULL;
		double* mn_point = NULL;
		long optimal_size, mn_point_size, major_iter, minor_iter;
		uint64_t flops_count;
		sfo_function_quadpot(filename, optimal, optimal_size, mn_point, mn_point_size, major_iter, minor_iter, flops_count);

		printf("\n n=%s,%ld,%ld,%ld \n", test_data[i].c_str(), major_iter, minor_iter, optimal_size);

		// clean up
		delete[] optimal;
		delete[] mn_point;
	}
}

TEST(QuadPot, HPSFO_QuadPot)
{
	// set the kernel
    set_min_norm_point_hpsfo_kernel();
	quadpot_test(QUADPOT_TEST_DATA, QUADPOT_TEST_DATA_NUM_TESTS);
}
