/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_matrix_testdata.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Matrix fixture data
//============================================================================

#ifndef MATRIX_TESTDATA_H_
#define MATRIX_TESTDATA_H_

#ifdef __cplusplus
extern "C" {
#endif

#ifdef _MSC_VER
  #pragma warning(push)
  #pragma warning(disable: 4510)  // default constructor could not be generated
  #pragma warning(disable: 4512)  // assignment operator could not be generated
  #pragma warning(disable: 4610)  // can never be instantiated - user defined constructor required
#endif

#include "sfo_matrix.h"
#include "sfo_vector.h"

typedef struct {
  double * data;
  size_t rows, cols;
} matrix_t;

static const double mmm_test1A_data[] = {
  0.8147, 0.1270, 0.6324,
  0.9058, 0.9134, 0.0975
};
static const matrix_t mmm_test1A = {(double *) mmm_test1A_data, 2, 3};

static const double mmm_test1B_data[] = {
  0.2785, 0.9649, 0.9572, 0.1419,
  0.5469, 0.1576, 0.4854, 0.4218,
  0.9575, 0.9706, 0.8003, 0.9157
};
static const matrix_t mmm_test1B = {(double *) mmm_test1B_data, 3, 4};

static const double mmm_test1_result_data[] = {
  0.90187325, 1.41992667, 1.34758636, 0.74826321,
  0.84516001, 1.11259176, 1.38842537, 0.60308589
};
static const matrix_t mmm_test1_result = {(double *) mmm_test1_result_data, 2, 4};

#ifdef __cplusplus
}
#endif

#endif  // MATRIX_TESTDATA_H_
