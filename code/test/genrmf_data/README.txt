We now include a patched copy of the graph generator.  The changes listed below
are no longer necessary (see {REPO}/code/third_party/genrmf).

http://www.informatik.uni-trier.de/~naeher/Professur/research/generators/maxflow/genrmf/index.html
GENRMF

In 'math_to_gcc.h':
- Comment out srand definition
- Add the following:
    static double drand(void) {
      return (double) rand() / (double) RAND_MAX;
    }

$ ./genrmf -out out_a8_b4_n256.txt -seed 1 -a 8 -b 4 -c1 1 -c2 1
$ ./genrmf -out out_a6_b4_n144.txt -seed 1 -a 6 -b 4 -c1 1 -c2 1
$ ./genrmf -out out_a8_b5_n320.txt -seed 1 -a 8 -b 5 -c1 1 -c2 1
$ ./genrmf -out out_a6_b3_n108.txt -seed 1 -a 6 -b 3 -c1 1 -c2 1
$ ./genrmf -out out_a9_b5_n405.txt -seed 1 -a 9 -b 5 -c1 1 -c2 1
$ ./genrmf -out out_a8_b8_n512.txt -seed 1 -a 8 -b 8 -c1 1 -c2 1
