/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_quadpot_testdata.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 17.10.2012
// Description : Test data for the quadpot test
//============================================================================

#ifndef SFO_QUADPOT_TESTDATA_H_
#define SFO_QUADPOT_TESTDATA_H_

#include "set_factory.h"

static const int QUADPOT_TEST_DATA_NUM_TESTS = 6;
static string QUADPOT_TEST_DATA[] = {
		"synth_5.txt", 	 "synth_32.txt" , "synth_64.txt",
		"synth_128.txt", "synth_256.txt", "synth_512.txt" };

static const int EMPTY_SET_DATA = 0;
static const int FULL_SET_DATA 	= 1;
static const int FIRST_SET_DATA = 2;
static const int LAST_SET_DATA 	= 3;
static const int EVEN_SET_DATA 	= 5;
static const int ODD_SET_DATA 	= 4;

static double QUADPOT_EXPECTED_EVAL[][QUADPOT_TEST_DATA_NUM_TESTS] = {
	{0    , 0.0000000000000000e+00,0.0000000000000000e+00,0.0000000000000000e+00,0.0000000000000000e+00,0.0000000000000000e+00}, // empty set
	{.25  , 0.0000000000000000e+00,0.0000000000000000e+00,0.0000000000000000e+00,0.0000000000000000e+00,0.0000000000000000e+00}, // full set
	{-.5  , 1.2506250000000000e+02,2.8757031250000000e+02,6.2012695312500000e+02,9.9652832031250000e+02,2.1046821289062500e+03}, // first set
	{10.75, 1.1895312500000000e+02,2.8507812500000000e+02,6.4889843750000000e+02,1.1846425781250000e+03,2.4182099609375000e+03}, // last set
	{11   , 1.0000000000000000e+03,4.2027500000000000e+03,1.7030937500000000e+04,7.1137062500000000e+04,2.8193331250000000e+05}, // even set
	{13.25, 9.9800000000000000e+02,4.1912500000000000e+03,1.7017062500000000e+04,7.1096937500000000e+04,2.8192268750000000e+05}  // odd set
};

#endif /* SFO_QUADPOT_TESTDATA_H_ */
