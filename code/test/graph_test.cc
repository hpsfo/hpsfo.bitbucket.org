/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : graph_test.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 19.07.2012
// Description : Test suite for the graph implementation
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>
#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

#include "hp_adjlist_bidir.h"

// graph test
class TGraphTest: public ::testing::TestWithParam<int> {
protected:
	TGraphTest() {
		// empty
	}

	virtual void SetUp() {
		// TODO:
	}

	virtual void TearDown() {
		// empty
	}

	void test_graph(thp_adjlist_bidir& adjlist_bidir) {
		// check base data
		ASSERT_EQ(8, adjlist_bidir.num_vertices());
		ASSERT_EQ(20, adjlist_bidir.num_edges());

		// check out_startpos
		ASSERT_EQ(0, adjlist_bidir.out_startpos()[0]);
		ASSERT_EQ(3, adjlist_bidir.out_startpos()[1]);
		ASSERT_EQ(6, adjlist_bidir.out_startpos()[2]);
		ASSERT_EQ(9, adjlist_bidir.out_startpos()[3]);
		ASSERT_EQ(12, adjlist_bidir.out_startpos()[4]);
		ASSERT_EQ(14, adjlist_bidir.out_startpos()[5]);
		ASSERT_EQ(16, adjlist_bidir.out_startpos()[6]);
		ASSERT_EQ(18, adjlist_bidir.out_startpos()[7]);

		// check out_end_nodes
		ASSERT_EQ(1, adjlist_bidir.out_end_nodes()[0]);
		ASSERT_EQ(2, adjlist_bidir.out_end_nodes()[1]);
		ASSERT_EQ(4, adjlist_bidir.out_end_nodes()[2]);
		ASSERT_EQ(0, adjlist_bidir.out_end_nodes()[3]);
		ASSERT_EQ(3, adjlist_bidir.out_end_nodes()[4]);
		ASSERT_EQ(6, adjlist_bidir.out_end_nodes()[5]);
		ASSERT_EQ(0, adjlist_bidir.out_end_nodes()[6]);
		ASSERT_EQ(3, adjlist_bidir.out_end_nodes()[7]);
		ASSERT_EQ(5, adjlist_bidir.out_end_nodes()[8]);
		ASSERT_EQ(1, adjlist_bidir.out_end_nodes()[9]);
		ASSERT_EQ(2, adjlist_bidir.out_end_nodes()[10]);
		ASSERT_EQ(7, adjlist_bidir.out_end_nodes()[11]);
		ASSERT_EQ(5, adjlist_bidir.out_end_nodes()[12]);
		ASSERT_EQ(6, adjlist_bidir.out_end_nodes()[13]);
		ASSERT_EQ(4, adjlist_bidir.out_end_nodes()[14]);
		ASSERT_EQ(7, adjlist_bidir.out_end_nodes()[15]);
		ASSERT_EQ(4, adjlist_bidir.out_end_nodes()[16]);
		ASSERT_EQ(7, adjlist_bidir.out_end_nodes()[17]);
		ASSERT_EQ(5, adjlist_bidir.out_end_nodes()[18]);
		ASSERT_EQ(6, adjlist_bidir.out_end_nodes()[19]);

		// check out_weights
		ASSERT_EQ(4, adjlist_bidir.out_weights()[0]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[1]);
		ASSERT_EQ(1, adjlist_bidir.out_weights()[2]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[3]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[4]);
		ASSERT_EQ(1, adjlist_bidir.out_weights()[5]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[6]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[7]);
		ASSERT_EQ(1, adjlist_bidir.out_weights()[8]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[9]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[10]);
		ASSERT_EQ(1, adjlist_bidir.out_weights()[11]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[12]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[13]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[14]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[15]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[16]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[17]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[18]);
		ASSERT_EQ(4, adjlist_bidir.out_weights()[19]);

		// check in_startpos
		ASSERT_EQ(0, adjlist_bidir.in_startpos()[0]);
		ASSERT_EQ(2, adjlist_bidir.in_startpos()[1]);
		ASSERT_EQ(4, adjlist_bidir.in_startpos()[2]);
		ASSERT_EQ(6, adjlist_bidir.in_startpos()[3]);
		ASSERT_EQ(8, adjlist_bidir.in_startpos()[4]);
		ASSERT_EQ(11, adjlist_bidir.in_startpos()[5]);
		ASSERT_EQ(14, adjlist_bidir.in_startpos()[6]);
		ASSERT_EQ(17, adjlist_bidir.in_startpos()[7]);

		// check in_end_nodes
		ASSERT_EQ(1, adjlist_bidir.in_end_nodes()[0]);
		ASSERT_EQ(2, adjlist_bidir.in_end_nodes()[1]);
		ASSERT_EQ(0, adjlist_bidir.in_end_nodes()[2]);
		ASSERT_EQ(3, adjlist_bidir.in_end_nodes()[3]);
		ASSERT_EQ(0, adjlist_bidir.in_end_nodes()[4]);
		ASSERT_EQ(3, adjlist_bidir.in_end_nodes()[5]);
		ASSERT_EQ(1, adjlist_bidir.in_end_nodes()[6]);
		ASSERT_EQ(2, adjlist_bidir.in_end_nodes()[7]);
		ASSERT_EQ(0, adjlist_bidir.in_end_nodes()[8]);
		ASSERT_EQ(5, adjlist_bidir.in_end_nodes()[9]);
		ASSERT_EQ(6, adjlist_bidir.in_end_nodes()[10]);
		ASSERT_EQ(2, adjlist_bidir.in_end_nodes()[11]);
		ASSERT_EQ(4, adjlist_bidir.in_end_nodes()[12]);
		ASSERT_EQ(7, adjlist_bidir.in_end_nodes()[13]);
		ASSERT_EQ(1, adjlist_bidir.in_end_nodes()[14]);
		ASSERT_EQ(4, adjlist_bidir.in_end_nodes()[15]);
		ASSERT_EQ(7, adjlist_bidir.in_end_nodes()[16]);
		ASSERT_EQ(3, adjlist_bidir.in_end_nodes()[17]);
		ASSERT_EQ(5, adjlist_bidir.in_end_nodes()[18]);
		ASSERT_EQ(6, adjlist_bidir.in_end_nodes()[19]);

		// check in_weights
		ASSERT_EQ(4, adjlist_bidir.in_weights()[0]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[1]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[2]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[3]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[4]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[5]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[6]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[7]);
		ASSERT_EQ(1, adjlist_bidir.in_weights()[8]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[9]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[10]);
		ASSERT_EQ(1, adjlist_bidir.in_weights()[11]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[12]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[13]);
		ASSERT_EQ(1, adjlist_bidir.in_weights()[14]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[15]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[16]);
		ASSERT_EQ(1, adjlist_bidir.in_weights()[17]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[18]);
		ASSERT_EQ(4, adjlist_bidir.in_weights()[19]);
	}
};

TEST_F(TGraphTest, Ordered_StartNode) {
	char* filepath = "../code/test/genrmf_data";

	path p(filepath);
	p /= "out_a2_b2_n8_n20.txt";

	ifstream is(p.c_str());

	// read the graph
	thp_adjlist_bidir adjlist_bidir;
	is >> adjlist_bidir;

	test_graph(adjlist_bidir);
}

TEST_F(TGraphTest, Unordered_StartNode) {
	char* filepath = "../code/test/genrmf_data";

	path p(filepath);
	p /= "out_a2_b2_n8_n20_unordered.txt";

	ifstream is(p.c_str());

	// read the graph
	thp_adjlist_bidir adjlist_bidir;
	is >> adjlist_bidir;

	test_graph(adjlist_bidir);
}
