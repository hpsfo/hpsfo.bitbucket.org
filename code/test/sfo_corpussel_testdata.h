/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_corpussel_testdata.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 24.07.2012
// Description : Test data for the CorpuSel test suite
//============================================================================

#ifndef SFO_CORPUSSEL_TESTDATA_H_
#define SFO_CORPUSSEL_TESTDATA_H_

#include "set_factory.h"

static const int CORPUSSEL_TEST_DATA_NUM_TESTS = 6;
static string CORPUSSEL_TEST_DATA[] = {
		"uniform-sent.10" , "uniform-sent.100" , "uniform-sent.200", "uniform-sent.400", "uniform-sent.800", "uniform-sent.1600" };

static const int EMPTY_SET_DATA = 0;
static const int FULL_SET_DATA 	= 1;
static const int FIRST_SET_DATA = 2;
static const int LAST_SET_DATA 	= 3;
static const int EVEN_SET_DATA 	= 4;
static const int ODD_SET_DATA 	= 5;

static double CORPUSSEL_EXPECTED_EVAL[][CORPUSSEL_TEST_DATA_NUM_TESTS] = {
	{4.0, 1.000000e+02, 2.000000e+02, 4.000000e+02, 8.000000e+02, 1.600000e+03}, // empty set
	{4.0, 3.030000e+02, 5.360000e+02, 7.540000e+02, 1.105000e+03, 1.781000e+03}, // full set
	{6.0, 1.080000e+02, 2.100000e+02, 4.040000e+02, 8.060000e+02, 1.604000e+03}, // first set
	{4.0, 1.010000e+02, 2.360000e+02, 4.020000e+02, 8.000000e+02, 1.613000e+03}, // last set
	{4.0, 2.300000e+02, 4.400000e+02, 6.420000e+02, 1.131000e+03, 2.007000e+03}, // even set
	{5.0, 2.320000e+02, 4.320000e+02, 7.320000e+02, 1.128000e+03, 1.981000e+03}  // odd set
};

#endif /* SFO_CORPUSSEL_TESTDATA_H_ */
