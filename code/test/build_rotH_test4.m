clear all;

% block size
nb = 4;

% defining 0 and 1 as symbols too, solves the problem
sym_0 = sym('0');
sym_1 = sym('1');

c0  = sym('c0');
c1  = sym('c1');
c2  = sym('c2');
c3  = sym('c3');

s0  = sym('s0');
s1  = sym('s1');
s2  = sym('s2');
s3  = sym('s3');

% create H orthogonal matrix using the sin and cos symbols
% filling in the first rotation
I=repmat(sym_0,(nb+1),(nb+1));
for i=1:(nb+1)
    I(i,i)=sym_1;
end
H = I;
H(1:2,1:2) = [c0 s0; -s0 c0];

G = I;
G(2:3,2:3) = [c1 s1; -s1 c1];
H = G*H;

G = I;
G(3:4,3:4) = [c2 s2; -s2 c2];
H = G*H;

G = I;
G(4:5,4:5) = [c3 s3; -s3 c3];
H = G*H;

% generate the C++ code
for i=1:(nb+1)
    for j=1:(nb+1)
        if H(i, j) ~= sym_0 
            fprintf('gc(%d, %d)=%s;\t', i - 1, j - 1, char(H(i, j)));
        end
    end
    fprintf('\n');
end
