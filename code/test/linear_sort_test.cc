/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : graph_test.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 19.07.2012
// Description : Test suite for the linear sort implementation
//============================================================================

#include <time.h>

#include <gtest/gtest.h>

#include "utils.h"
#include "sfo_vector.h"

// sort test
class AlgoSort: public ::testing::TestWithParam<int> {
protected:
	static const int MAX_SIZE = 20;
	static const int MAX_VALUE = 500;

	// constructor
	AlgoSort() : m_size(0) {
		// empty
	}

	virtual void SetUp() {
		while (m_size == 0) {
			m_size = rand() % MAX_SIZE;
		}
		m_data.resize(m_size);

		for (int i = 0; i < m_size; i++) {
			m_data[i] = (int) (rand() % MAX_VALUE);
		}
	}

	virtual void TearDown() {
		// empty
	}

	int 		 m_size;
	tsfo_vector<int> m_data;
};

TEST_P(AlgoSort, Ascending) {
	tlinear_sort<int> linear_sort(MAX_VALUE + 1);
	linear_sort.sort(m_data, true);

	for (int i = 0; i < m_size - 1; i++) {
		ASSERT_LE(m_data[i], m_data[i + 1]);
	}
}

TEST_P(AlgoSort, Descending) {
	tlinear_sort<int> linear_sort(MAX_VALUE + 1);
	linear_sort.sort(m_data, false);

	for (int i = 0; i < m_size - 1; i++) {
		ASSERT_GE(m_data[i], m_data[i + 1]);
	}
}

INSTANTIATE_TEST_CASE_P(Random, AlgoSort, ::testing::Range(0, 25));

// Use fixed random seed

class RandomSeedEnvironment: public ::testing::Environment {
public:
	virtual void SetUp() {
		// srand((unsigned int) time(NULL));
		srand(0);
	}
};

int main(int argc, char** argv) {
	::testing::AddGlobalTestEnvironment(new RandomSeedEnvironment());
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
