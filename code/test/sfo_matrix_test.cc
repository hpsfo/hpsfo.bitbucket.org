/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : matrix_test.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Test suite for the class tsfo_matrix
//============================================================================

#include <gtest/gtest.h>
#include <limits.h>
#include <math.h>

#include "sfo_vector.h"
#include "sfo_matrix.h"
#include "sfo_matrix_tria.h"
#include "utils.h"
#include "sfo_matrix_testdata.h"

static double EPSILON = 1e-10;

// matrix test
class TMatrixTest: public ::testing::TestWithParam<int> {
protected:
	TMatrixTest() {
		// empty
	}

	virtual void SetUp() {
		// make sure that the delcols update is used
		tsfo_matrix<double>::RECOMPUTE_QR_BEGIN_THRESHOLD = 0.3;
		tsfo_matrix<double>::RECOMPUTE_QR_BLOCK_THRESHOLD = 1.0;
	}

	virtual void TearDown() {
		// empty
	}

	void Validate_With_Direct_Solver(tsfo_matrix<double>& A, const tsfo_vector<double>& b,
			tsfo_vector<double>& x) {
		// delete old possible QR
		A.delete_qr();

		tsfo_vector<double> expected = A.solve(b);

		printf("The full QR matrix is: \n");
		A.qr_print();

		ASSERT_EQ(expected.size(), x.size());
		for (int i = 0; i < expected.size(); ++i) {
			EXPECT_NEAR(expected[i], x[i], EPSILON);
		}
	}
};

TEST_F(TMatrixTest, Initialization) {
	// initialize a 3x3 matrix
	tsfo_matrix<double> A(4, 2, 0.0);

	// check that all elements are initialized to zero
	for (int i = 0; i < A.rows(); ++i) {
		for (int j = 0; j < A.cols(); ++j) {
			EXPECT_DOUBLE_EQ(0.0, A(i, j));
		}
	}
}

TEST_F(TMatrixTest, Constructor_From_DoubleArray) {
	// initialize a 3x3 matrix
	tsfo_matrix<double> A(3, 3, 0.0);
	for (int i=0; i < A.rows(); ++i) {
		for (int j=0; j < A.cols(); ++j) {
			A(i, j) = j*3 + i + 1;
		}
	}

	printf("The matrix A is: \n");
	A.print();

	tsfo_matrix<double> B(A.rows(), A.cols(), A.data());

	printf("The matrix B is: \n");
	B.print();

	// assert both matrix are equal
	ASSERT_TRUE(A == B);
}

TEST_F(TMatrixTest, Reader_and_Writer) {
	// initialize a 3x3 matrix
	tsfo_matrix<double> A(3, 3, 0.0);

	// test
	A(1, 2) = 1.0;
	A(2, 1) = 2.0;
	A(2, 2) = 3.0;

	EXPECT_DOUBLE_EQ(3.0, A(2, 2));

	printf("The modified matrix is: \n");
	A.print();
}

TEST_F(TMatrixTest, Copy) {
	// initialize 2x2 matrix
	tsfo_matrix<double> A(2, 2);
	A(0, 0) = 1.0;
	A(0, 1) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	// copy the matrix
	tsfo_matrix<double> A_copy(A);

	printf("The A_copy is: \n");
	A_copy.print();

	EXPECT_DOUBLE_EQ(A(0, 0), A_copy(0, 0));
	EXPECT_DOUBLE_EQ(A(0, 1), A_copy(0, 1));
	EXPECT_DOUBLE_EQ(A(1, 0), A_copy(1, 0));
	EXPECT_DOUBLE_EQ(A(1, 1), A_copy(1, 1));
}

TEST_F(TMatrixTest, Get_Column) {
	// initialize 3x2 matrix
	tsfo_matrix<double> A(3, 2);
	A(0, 0) = 1.0;
	A(0, 1) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 4.0;
	A(2, 0) = 5.0;
	A(2, 1) = 6.0;

	printf("The matrix A is: \n");
	A.print();

	// extract the column
	tsfo_vector<double> column = A[1];

	printf("The extracted column is: \n");
	column.print();

	EXPECT_DOUBLE_EQ(column[0], A(0, 1));
	EXPECT_DOUBLE_EQ(column[1], A(1, 1));
	EXPECT_DOUBLE_EQ(column[2], A(2, 1));
}

TEST_F(TMatrixTest, Random) {
	tsfo_matrix<double> A;
	A.random(4, 4);

	printf("The matrix A is: \n");
	A.print();

	for (int i = 0; i < A.rows(); ++i) {
		for (int j = 0; j < A.cols(); ++j) {
			EXPECT_LE(A(i, j), 1.0);
		}
	}
}

TEST_F(TMatrixTest, Transpose) {
	// initialize a 3x2 matrix
	tsfo_matrix<double> A(3, 2);
	tsfo_matrix<double> At(0, 0);

	A(0, 0) = 1;
	A(0, 1) = 2;
	A(1, 0) = 3;
	A(1, 1) = 4;
	A(2, 0) = 5;
	A(2, 1) = 6;

	printf("The matrix A is: \n");
	A.print();

	// copy the matrix
	At.transpose(A);

	printf("The A_transpose is: \n");
	At.print();

	ASSERT_EQ(2, At.rows());
	ASSERT_EQ(3, At.cols());

	EXPECT_DOUBLE_EQ(1, At(0, 0));
	EXPECT_DOUBLE_EQ(3, At(0, 1));
	EXPECT_DOUBLE_EQ(5, At(0, 2));
	EXPECT_DOUBLE_EQ(2, At(1, 0));
	EXPECT_DOUBLE_EQ(4, At(1, 1));
	EXPECT_DOUBLE_EQ(6, At(1, 2));
}

TEST_F(TMatrixTest, Diag) {
	// initialize a 3x3 matrix
	tsfo_matrix<double> A(3, 3, 0.0);

	// set the diagonal elements
	A(0, 0) = 1;
	A(1, 1) = 2;
	A(2, 2) = 3;

	printf("The matrix A is: \n");
	A.print();

	// extract the diagonal elements
	tsfo_vector<double> diag = A.diag();

	printf("The diag of A is: \n");
	diag.print();

	ASSERT_EQ(3, diag.size());

	EXPECT_DOUBLE_EQ(1, A(0, 0));
	EXPECT_DOUBLE_EQ(2, A(1, 1));
	EXPECT_DOUBLE_EQ(3, A(2, 2));
}

TEST_F(TMatrixTest, DeleteRowLast) {
	// initialize a 4x2 matrix
	tsfo_matrix<double> A(4, 2);

	A(0, 0) = 1;
	A(0, 1) = 2;
	A(1, 0) = 3;
	A(1, 1) = 4;
	A(2, 0) = 5;
	A(2, 1) = 6;
	A(3, 0) = 7;
	A(3, 1) = 8;

	printf("The matrix A is: \n");
	A.print();

	// copy the matrix
	A.delete_row_last();

	printf("The matrix A after delete is: \n");
	A.print();

	ASSERT_EQ(3, A.rows());
	ASSERT_EQ(2, A.cols());

	EXPECT_DOUBLE_EQ(1, A(0, 0));
	EXPECT_DOUBLE_EQ(2, A(0, 1));
	EXPECT_DOUBLE_EQ(3, A(1, 0));
	EXPECT_DOUBLE_EQ(4, A(1, 1));
	EXPECT_DOUBLE_EQ(5, A(2, 0));
	EXPECT_DOUBLE_EQ(6, A(2, 1));
}

TEST_F(TMatrixTest, Vector_Multiplication) {
	// initialize 2x2 matrix
	tsfo_matrix<double> A(3, 2);
	A(0, 0) = 1.0;
	A(0, 1) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 4.0;
	A(2, 0) = 5.0;
	A(2, 1) = 6.0;

	printf("The matrix A is: \n");
	A.print();

	tsfo_vector<double> b(2);
	b[0] = 1.;
	b[1] = 2.;

	printf("The vector b is: \n");
	b.print();

	tsfo_vector<double> Ab = A * b;
	EXPECT_DOUBLE_EQ( 5.0, Ab[0]);
	EXPECT_DOUBLE_EQ(11.0, Ab[1]);
	EXPECT_DOUBLE_EQ(17.0, Ab[2]);

	printf("The resulting vector Ab is : \n");
	Ab.print();
}

TEST_F(TMatrixTest, Matrix_Multiplication) {
	tsfo_matrix<double> AB(0, 0);

	// initialize two 2x2 matrices
	tsfo_matrix<double> A(2, 2);
	A(0, 0) = 1.0;
	A(0, 1) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	tsfo_matrix<double> B(2, 2);
	B(0, 0) = 1.0;
	B(0, 1) = 2.0;
	B(1, 0) = 3.0;
	B(1, 1) = 4.0;

	printf("The matrix B is: \n");
	B.print();

	A.multiply(B, AB);

	EXPECT_DOUBLE_EQ( 7.0, AB(0, 0));
	EXPECT_DOUBLE_EQ(10.0, AB(0, 1));
	EXPECT_DOUBLE_EQ(15.0, AB(1, 0));
	EXPECT_DOUBLE_EQ(22.0, AB(1, 1));

	printf("The resulting matrix AB is : \n");
	AB.print();
}

TEST_F(TMatrixTest, Cholesky) {
	// initialize a 10x10 matrix
	tsfo_matrix<double> A(10, 10, 0.0);

	// set the upper diagonal elements
    A(0, 0) = 1.0010; A(0, 1) = 0.0000; A(0, 2) = 0.0000; A(0, 3) = 0.0058; A(0, 4) = 0.0000; A(0, 5) = 0.0000; A(0, 6) = 0.0000; A(0, 7) = 0.0000; A(0, 8) = 0.0000; A(0, 9) = 0.0000;
    				  A(1, 1) = 1.0010; A(1, 2) = 0.8416; A(1, 3) = 0.0005; A(1, 4) = 0.0092; A(1, 5) = 0.0016; A(1, 6) = 0.0000; A(1, 7) = 0.0000; A(1, 8) = 0.0000; A(1, 9) = 0.0007;
    				  	  	  	  	    A(2, 2) = 1.0010; A(2, 3) = 0.0001; A(2, 4) = 0.0365; A(2, 5) = 0.0016; A(2, 6) = 0.0000; A(2, 7) = 0.0000; A(2, 8) = 0.0005; A(2, 9) = 0.0009;
    				  	  	  	  	  	  	  	  	  	  A(3, 3) = 1.0010; A(3, 4) = 0.0000; A(3, 5) = 0.0004; A(3, 6) = 0.0000; A(3, 7) = 0.0015; A(3, 8) = 0.0000; A(3, 9) = 0.0000;
    				  	  	  	  	  	  	  	  	  	  	  	  	  	  	A(4, 4) = 1.0010; A(4, 5) = 0.0076; A(4, 6) = 0.0017; A(4, 7) = 0.0000; A(4, 8) = 0.1457; A(4, 9) = 0.0153;
    				  	  	  	  	  	  	  	  	  	  	  	  	  	  					  A(5, 5) = 1.0010; A(5, 6) = 0.0002; A(5, 7) = 0.4026; A(5, 8) = 0.0000; A(5, 9) = 0.8764;
    				  	  	  	  	  	  	  	  	  	  	  	  	  	  					  	  	  	  	  	A(6, 6) = 1.0010; A(6, 7) = 0.0000; A(6, 8) = 0.0003; A(6, 9) = 0.0014;
    				  	  	  	  	  	  	  	  	  	  	  	  	  	  					  	  	  	  	  					  A(7, 7) = 1.0010; A(7, 8) = 0.0000; A(7, 9) = 0.2485;
    				  	  	  	  	  	  	  	  	  	  	  	  	  	  					  	  	  	  	  					  	  	  	  	  	A(8, 8) = 1.0010; A(8, 9) = 0.0000;
    				  	  	  	  	  	  	  	  	  	  	  	  	  	  					  	  	  	  	  					  	  	  	  	  					  A(9, 9) = 1.0010;
	printf("The original matrix A is: \n");
	A.print();

	A.cholesky();

	printf("The cholesky factorization of A is: \n");
	A.print();
}

TEST_F(TMatrixTest, Solve_Direct_2x2) {
	// initialize a 2x2 matrix
	tsfo_matrix<double> A(2, 2);
	A(0, 0) = 31.10;
	A(0, 1) = 6.25;
	A(1, 0) = 6.25;
	A(1, 1) = 6.90;

	printf("The original matrix A is: \n");
	A.print();

	tsfo_vector<double> b(2);
	b[0] = 6.88;
	b[1] = 6.88;

	printf("The vector b is: \n");
	b.print();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	EXPECT_DOUBLE_EQ(0.025477489282306233, x[0]);
	EXPECT_DOUBLE_EQ(0.974024013331244220, x[1]);
}

TEST_F(TMatrixTest, Solve_Direct_3x2) {
	// initialize a 3x2 matrix
	tsfo_matrix<double> A(3, 2);

	A(0, 0) = 0.8147;
	A(1, 0) = 0.9058;
	A(2, 0) = 0.1270;
	A(0, 1) = 0.9134;
	A(1, 1) = 0.6324;
	A(2, 1) = 0.0975;

	printf("The original matrix A is: \n");
	A.print();

	tsfo_vector<double> b(3);
	b[0] = 0.2785;
	b[1] = 0.5469;
	b[2] = 0.9575;

	printf("The vector b is: \n");
	b.print();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	EXPECT_DOUBLE_EQ( 1.29027464127138080, x[0]);
	EXPECT_DOUBLE_EQ(-0.82142736530921623, x[1]);
}

TEST_F(TMatrixTest, Solve_Direct_3x3) {
	// initialize a 3x3 matrix
	tsfo_matrix<double> A(3, 3);
	A(0, 0) = 2.0;
	A(0, 1) = -1.0;
	A(0, 2) = 0.0;
	A(1, 0) = -1.0;
	A(1, 1) = 2.0;
	A(1, 2) = -1.0;
	A(2, 0) = 0.0;
	A(2, 1) = -1.0;
	A(2, 2) = 2.0;

	printf("The original matrix A is: \n");
	A.print();

	tsfo_vector<double> b(3);
	b[0] = 1;
	b[1] = 1;
	b[2] = 1;

	printf("The vector b is: \n");
	b.print();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	EXPECT_DOUBLE_EQ(1.5000, x[0]);
	EXPECT_DOUBLE_EQ(2.0000, x[1]);
	EXPECT_DOUBLE_EQ(1.5000, x[2]);
}

TEST_F(TMatrixTest, QR_Update_AppendColumn_4x2_to_4x3) {
	// test matrix 4x2 to 4x3
	tsfo_matrix<double> A;
	A.random(4, 2);

	printf("The original matrix A is: \n");
	A.print();

	// compute the QR factorization
	A.qr_factorization();

	tsfo_vector<double> a(4);
	a.random();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.append_column(a);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after append is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(4);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_AppendColumn_4x3_to_4x4) {
	// test matrix 4x3 to 4x4
	tsfo_matrix<double> A;
	A.random(4, 3);

	printf("The original matrix A is: \n");
	A.print();

	// compute the QR factorization
	A.qr_factorization();
	printf("The QR factorization is: \n");
	A.qr_print();

	tsfo_vector<double> a(4);
	a[0] = 39;
	a[1] = 77;
	a[2] = 80;
	a[3] = 19;

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.append_column(a);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after append is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(4);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_AppendColumn_3x4_to_3x5) {
	// test matrix 3x4 to 3x5
	tsfo_matrix<double> A;
	A.random(3, 4);

	printf("The original matrix A is: \n");
	A.print();

	// compute the QR factorization
	A.qr_factorization();

	tsfo_vector<double> a(3);
	a.random();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.append_column(a);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after append is: \n");
	A.print();

	// validating here does not make a lot of sense
	// i.e. does not make sense to solve an underdetermined
	// system
}

TEST_F(TMatrixTest, QR_Update_DeleteColumn_4x4_to_4x3_End) {
	tsfo_matrix<double> A;
	A.random(4, 4);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.delete_column(3, 3);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after delete is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(4);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_DeleteColumn_4x4_to_4x2_Middle) {
	tsfo_matrix<double> A;
	A.random(4, 4);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.delete_column(1, 2);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after delete is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(4);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_DeleteColumn_6x4_to_6x3_Middle) {
	// initialize a 6x4 matrix
	tsfo_matrix<double> A;
	A.random(6, 4);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.delete_column(2, 2);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after delete is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(6);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_DeleteColumn_After_DeleteColumn) {
	// initialize a 10x8 matrix
	tsfo_matrix<double> A;
	A.random(10, 8);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());

	// delete 2 columns
	A.delete_column(3, 4);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after the first delete is: \n");
	A.print();

	// delete 3 columns
	A.delete_column(2, 4);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after the second delete is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(10);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_AppendColumn_After_DeleteColumn) {
	// initialize a 10x8 matrix
	tsfo_matrix<double> A;
	A.random(10, 8);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());

	// delete 2 columns
	A.delete_column(3, 4);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after the delete is: \n");
	A.print();

	// generate and append random column
	tsfo_vector<double> a(10);
	a.random();

	A.append_column(a);
	printf("The matrix A after the append is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(10);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, Solve_Direct_With_QR_4x3) {
	tsfo_matrix<double> A(4, 3);

	// initialize a 4x3 matrix
	A(0, 0) = 75;
	A(0, 1) = 4;
	A(0, 2) = 70;
	A(1, 0) = 40;
	A(1, 1) = 28;
	A(1, 2) = 32;
	A(2, 0) = 66;
	A(2, 1) = 5;
	A(2, 2) = 96;
	A(3, 0) = 1.0;
	A(3, 1) = 1.0;
	A(3, 2) = 1.0;

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after append is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(4);
	b[0] = 1;
	b[1] = 1;
	b[2] = 1;
	b[3] = 1;

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	EXPECT_DOUBLE_EQ(0.0091586794285793294, x[0]);
	EXPECT_DOUBLE_EQ(0.0202923386357351170, x[1]);
	EXPECT_DOUBLE_EQ(0.0031392158161981814, x[2]);
}

TEST_F(TMatrixTest, Solve_Forward_3x3) {
	tsfo_matrix<double> A(3, 3, 0.0);

	A(0, 0) = 1;
	A(0, 1) = 2;
	A(0, 2) = 3;
	A(1, 1) = 4;
	A(1, 2) = 5;
	A(2, 2) = 6;

	printf("The original matrix A is: \n");
	A.print();

	// tests the HPSFO special use-case
	tsfo_vector<double>& a3 = A[2];
	a3 = A.solve_forward(a3);

	printf("The matrix A after solve right is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1, A(0, 0));
	EXPECT_DOUBLE_EQ(2, A(0, 1));
	EXPECT_DOUBLE_EQ(3, A(0, 2));
	EXPECT_DOUBLE_EQ(4, A(1, 1));
	EXPECT_DOUBLE_EQ(-2.500000e-01, A(1, 2));
	EXPECT_DOUBLE_EQ(-0.29166666666666669, A(2, 2));
}

TEST_F(TMatrixTest, QR_Update_AppendRow_3x3_to_4x3) {
	tsfo_matrix<double> A;
	A.random(3, 3);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());
	A.append_row_with_value(1.0);
	EXPECT_TRUE(A.has_valid_qr());
	EXPECT_TRUE(A.has_valid_qr_upd_addrow());

	printf("The matrix A after append is: \n");
	A.print();

	printf("The QR after append is: \n");
	A.qr_print();

	// now validate
	tsfo_vector<double> b(4);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, QR_Update_AppendRow_After_DeleteColumn) {
	// initialize a 10x8 matrix
	tsfo_matrix<double> A;
	A.random(10, 8);

	printf("The original matrix A is: \n");
	A.print();

	// compute and print the QR factorization
	A.qr_factorization();

	// make sure we have a valid QR to start with
	EXPECT_TRUE(A.has_valid_qr());

	// delete 2 columns
	A.delete_column(3, 4);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after the delete is: \n");
	A.print();

	A.append_row_with_value(1.0);
	EXPECT_TRUE(A.has_valid_qr());

	printf("The matrix A after the append row is: \n");
	A.print();

	// now validate
	tsfo_vector<double> b(11);
	b.random();

	// solve
	tsfo_vector<double> x = A.solve(b);

	printf("The solution x to the problem is: \n");
	x.print();

	Validate_With_Direct_Solver(A, b, x);
}

TEST_F(TMatrixTest, Matrix_Multiplication_Random) {
	tsfo_matrix<double> C;
	tsfo_matrix<double> A(mmm_test1A.rows, mmm_test1A.cols);
	tsfo_matrix<double> B(mmm_test1B.rows, mmm_test1B.cols);

	for (int i = 0; i < mmm_test1A.rows; i++) {
		for (int j = 0; j < mmm_test1A.cols; j++) {
			A(i, j) = mmm_test1A.data[i * mmm_test1A.cols + j];
		}
	}

	for (int i = 0; i < mmm_test1B.rows; i++) {
		for (int j = 0; j < mmm_test1B.cols; j++) {
			B(i, j) = mmm_test1B.data[i * mmm_test1B.cols + j];
		}
	}

	A.multiply(B, C);
	ASSERT_EQ(C.rows(), mmm_test1A.rows);
	ASSERT_EQ(C.cols(), mmm_test1B.cols);

	for (int i = 0; i < mmm_test1_result.rows; i++) {
		for (int j = 0; j < mmm_test1_result.cols; j++) {
			EXPECT_DOUBLE_EQ(mmm_test1_result.data[i*mmm_test1_result.cols +j], C(i, j));
		}
	}
}

TEST_F(TMatrixTest, Append_Row_With_Value) {
	// initialize two 2x2 matrices
	tsfo_matrix<double> A(2, 2);
	A(0, 0) = 1.0;
	A(0, 1) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	A.append_row_with_value(1.0);

	printf("The resulting matrix with appended row is: ");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(2.0, A(0, 1));
	EXPECT_DOUBLE_EQ(3.0, A(1, 0));
	EXPECT_DOUBLE_EQ(4.0, A(1, 1));
	EXPECT_DOUBLE_EQ(1.0, A(2, 0));
	EXPECT_DOUBLE_EQ(1.0, A(2, 1));
}

TEST_F(TMatrixTest, Append_Column) {
	// initialize 2x2 matrix
	tsfo_matrix<double> A(2, 2);
	A(0, 0) = 1.0;
	A(0, 1) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	tsfo_vector<double> a(2);
	a[0] = 9.0;
	a[1] = 9.0;

	A.append_column(a);

	printf("The resulting matrix with appended column is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(2.0, A(0, 1));
	EXPECT_DOUBLE_EQ(3.0, A(1, 0));
	EXPECT_DOUBLE_EQ(4.0, A(1, 1));
	EXPECT_DOUBLE_EQ(9.0, A(0, 2));
	EXPECT_DOUBLE_EQ(9.0, A(1, 2));
}

TEST_F(TMatrixTest, Delete_Column) {
	// delete from the midle, initialize 2x3 matrix
	tsfo_matrix<double> A(2, 3);
	A(0, 0) = 1.0;
	A(0, 1) = 9.0;
	A(0, 2) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 9.0;
	A(1, 2) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	// check that the column update is consistent
	tsfo_vector<double>& column2 = A[2];
	EXPECT_DOUBLE_EQ(2.0, column2[0]);
	EXPECT_DOUBLE_EQ(4.0, column2[1]);

	A.delete_column(1, 1);

	printf("The resulting matrix with deleted column 1 is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(2.0, A(0, 1));
	EXPECT_DOUBLE_EQ(3.0, A(1, 0));
	EXPECT_DOUBLE_EQ(4.0, A(1, 1));

	// check that the column update is consistent
	tsfo_vector<double>& column1 = A[1];
	EXPECT_DOUBLE_EQ(2.0, column1[0]);
	EXPECT_DOUBLE_EQ(4.0, column1[1]);

	// delete from the end, initialize 2x3 matrix
	A.rows(2);
	A.cols(3);
	A(0, 0) = 1.0;
	A(0, 1) = 9.0;
	A(0, 2) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 9.0;
	A(1, 2) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	A.delete_column(1, 2);

	printf("The resulting matrix with deleted columns 1 and 2 is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(3.0, A(1, 0));
}

TEST_F(TMatrixTest, Delete_Column_with_Gap) {
	// delete from the midle, initialize 2x3 matrix
	tsfo_matrix<double> A(2, 3);
	A(0, 0) = 1.0;
	A(0, 1) = 9.0;
	A(0, 2) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 9.0;
	A(1, 2) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	tsfo_vector<double>& col0a = A[0];
	EXPECT_DOUBLE_EQ(1.0, col0a[0]);
	EXPECT_DOUBLE_EQ(3.0, col0a[1]);

	tsfo_vector<double>& col1a = A[1];
	EXPECT_DOUBLE_EQ(9.0, col1a[0]);
	EXPECT_DOUBLE_EQ(9.0, col1a[1]);

	tsfo_vector<double>& col2a = A[2];
	EXPECT_DOUBLE_EQ(2.0, col2a[0]);
	EXPECT_DOUBLE_EQ(4.0, col2a[1]);

	A.delete_column_with_gap(1, 1);

	printf("The resulting matrix with deleted column 1 is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(2.0, A(0, 1));
	EXPECT_DOUBLE_EQ(3.0, A(1, 0));
	EXPECT_DOUBLE_EQ(4.0, A(1, 1));

	tsfo_vector<double>& col0b = A[0];
	EXPECT_DOUBLE_EQ(1.0, col0b[0]);
	EXPECT_DOUBLE_EQ(3.0, col0b[1]);

	tsfo_vector<double>& col1b = A[1];
	EXPECT_DOUBLE_EQ(2.0, col1b[0]);
	EXPECT_DOUBLE_EQ(4.0, col1b[1]);

	// delete from the end, initialize 2x3 matrix
	A.rows(2);
	A.cols(3);
	A(0, 0) = 1.0;
	A(0, 1) = 9.0;
	A(0, 2) = 2.0;
	A(1, 0) = 3.0;
	A(1, 1) = 9.0;
	A(1, 2) = 4.0;

	printf("The matrix A is: \n");
	A.print();

	A.delete_column_with_gap(1, 2);

	printf("The resulting matrix with deleted columns 1 and 2 is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(3.0, A(1, 0));

	tsfo_vector<double>& col0c = A[0];
	EXPECT_DOUBLE_EQ(1.0, col0c[0]);
	EXPECT_DOUBLE_EQ(3.0, col0c[1]);
}

TEST_F(TMatrixTest, Delete_Column_with_Gap_Triangular) {
	// delete from the midle, initialize 5x5 matrix
	tsfo_matrix_tria<double> A(5, 5, 0.0);
	for (int j = 0, value = 1; j < A.cols(); j++) {
		for (int i = 0; i <= j; i++, value++) {
			A(i, j) = (double) value;
		}
	}

	printf("The matrix A is: \n");
	A.print();

	A.delete_column_with_gap(2, 2, true);

	printf("The resulting matrix with deleted column 2 is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(1.0, A(0, 0));
	EXPECT_DOUBLE_EQ(0.0, A(1, 0));
	EXPECT_DOUBLE_EQ(2.0, A(0, 1));
	EXPECT_DOUBLE_EQ(3.0, A(1, 1));
	EXPECT_DOUBLE_EQ(0.0, A(2, 1));
}

TEST_F(TMatrixTest, Solve_Direct_Border_0x0) {
	// initialize a 0x0 matrix
	tsfo_matrix<double> A;
	printf("The matrix A is: \n");
	A.print();

	tsfo_vector<double> b;
	printf("The vector b is: \n");
	b.print();

	tsfo_vector<double> x = A.solve(b);
	printf("The result vector x is: \n");
	x.print();

	ASSERT_EQ(0, x.size());
}

TEST_F(TMatrixTest, Subspace_Translated) {
	tsfo_matrix<double> S(2, 3);
	S(0, 0) = 1;
	S(0, 1) = 2;
	S(0, 2) = 3;
	S(1, 0) = 1;
	S(1, 1) = 2;
	S(1, 2) = 3;

	printf("The matrix S is: \n");
	S.print();

	tsfo_matrix<double> S0;
	S0.subspace_translated(S);

	printf("The matrix S0 output of subspace translated is: \n");
	S0.print();

	EXPECT_DOUBLE_EQ(1, S0(0, 0));
	EXPECT_DOUBLE_EQ(2, S0(0, 1));
	EXPECT_DOUBLE_EQ(1, S0(1, 0));
	EXPECT_DOUBLE_EQ(2, S0(1, 1));
}

TEST_F(TMatrixTest, Triangularize_NB_0) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 0;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_2x2) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 2;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_4x4) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 4;

	int begin = 2;
	tsfo_matrix_tria<double> r(15, 13, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_7x7) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 7;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_8x8) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 8;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_9x9) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 9;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_10x10) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 10;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_11x11) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 11;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_12x12) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 12;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_13x13) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 13;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_14x14) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 14;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_15x15) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 15;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_NB_16x16) {
	int nb_previous = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 16;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = nb_previous;
}

TEST_F(TMatrixTest, Triangularize_Block_Givens) {
	int previous_nb = tsfo_matrix_tria<double>::TRIANGULARIZE_NB;
	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = 4;

	int begin = 3;
	tsfo_matrix_tria<double> r(20, 17, 0.0);
	for (int j = 0; j < r.cols(); j++) {
		for (int i = 0; i <= j; i++) {
			r(i, j) = 1.0;
		}

		if (j >= begin) {
			r(j + 1, j) = 2.0;
		}
	}

	printf("The matrix r is: \n");
	r.print();

	r.triangularize_block_givens(begin, 1, genrot_high_precision_slow);
	printf("The matrix r after triangularize is: \n");
	r.print();

	EXPECT_TRUE(r.is_triangular());

	tsfo_matrix_tria<double>::TRIANGULARIZE_NB = previous_nb;
}

TEST_F(TMatrixTest, Submatrix) {
	int dim = 5;
	tsfo_matrix<double> A(dim, dim);
	for (int i = 0; i < dim; ++i) {
		for (int j = 0; j < dim; ++j) {
			A(i, j) = j*dim + i + 1;
		}
	}

	printf("The matrix A is: \n");
	A.print();

	// choose the indices we need
	vector<int> b(3);
	b[0] = 0;
	b[1] = 1;
	b[2] = 4;

	// target matrix
	tsfo_matrix<double> B;

	// get the submatrix of A
	A.submatrix(b, B);

	printf("The submatrix B of A is: \n");
	B.print();

	EXPECT_EQ(B.cols(), 3);
	EXPECT_EQ(B.rows(), 3);

	EXPECT_DOUBLE_EQ(B(0, 0), 1.000000e+00);
	EXPECT_DOUBLE_EQ(B(0, 1), 6.000000e+00);
	EXPECT_DOUBLE_EQ(B(0, 2), 2.100000e+01);
	EXPECT_DOUBLE_EQ(B(1, 0), 2.000000e+00);
	EXPECT_DOUBLE_EQ(B(1, 1), 7.000000e+00);
	EXPECT_DOUBLE_EQ(B(1, 2), 2.200000e+01);
	EXPECT_DOUBLE_EQ(B(2, 0), 5.000000e+00);
	EXPECT_DOUBLE_EQ(B(2, 1), 1.000000e+01);
	EXPECT_DOUBLE_EQ(B(2, 2), 2.500000e+01);
}

TEST_F(TMatrixTest, ResetAll) {
	tsfo_matrix<double> A;
	A.random(5, 5);

	printf("The matrix A is: \n");
	A.print();

	// reset the matrix
	A.reset();

	printf("The matrix A after reset ALL is: \n");
	A.print();

	// check that the whole matrix was reset
	for (int i = 0; i < A.rows(); ++i) {
		for (int j = 0; j < A.cols(); ++j) {
			EXPECT_DOUBLE_EQ(A(i, j), 0.0);
		}
	}
}

TEST_F(TMatrixTest, ResetUpper) {
	tsfo_matrix<double> A;
	A.random(5, 5);

	printf("The matrix A is: \n");
	A.print();

	// reset the matrix
	A.reset(tsfo_matrix<double>::UPPER);

	printf("The matrix A after reset UPPER is: \n");
	A.print();

	// check that the whole matrix was reset
	for (int j = 0; j < A.cols(); ++j) {
		for (int i = 0; i < j; ++i) {
			EXPECT_DOUBLE_EQ(A(i, j), 0.0);
		}
	}
}

TEST_F(TMatrixTest, ResetLower) {
	tsfo_matrix<double> A;
	A.random(5, 5);

	printf("The matrix A is: \n");
	A.print();

	// reset the matrix
	A.reset(tsfo_matrix<double>::LOWER);

	printf("The matrix A after reset LOWER is: \n");
	A.print();

	// check that the whole matrix was reset
	for (int j = 0; j < A.cols(); ++j) {
		for (int i = j + 1; i < A.rows(); ++i) {
			EXPECT_DOUBLE_EQ(A(i, j), 0.0);
		}
	}
}

TEST_F(TMatrixTest, ResizePadding) {
	tsfo_matrix<double> A(3, 2, 1.0);

	printf("The original matrix A is: \n");
	A.print();

	// resize with padding
	A.resize(4, 4, 2.0);

	printf("The matrix A after resize with Padding is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(A(0, 0), 1.0);
	EXPECT_DOUBLE_EQ(A(1, 0), 1.0);
	EXPECT_DOUBLE_EQ(A(2, 0), 1.0);
	EXPECT_DOUBLE_EQ(A(0, 1), 1.0);
	EXPECT_DOUBLE_EQ(A(1, 1), 1.0);
	EXPECT_DOUBLE_EQ(A(2, 1), 1.0);
}

TEST_F(TMatrixTest, ResizeTruncate) {
	tsfo_matrix<double> A(3, 2);
	A(0, 0) = 1.0;
	A(1, 0) = 2.0;
	A(2, 0) = 3.0;
	A(0, 1) = 4.0;
	A(1, 1) = 5.0;
	A(2, 1) = 6.0;

	printf("The original matrix A is: \n");
	A.print();

	// resize with truncation
	A.resize(2, 2);

	printf("The matrix A after resize with Truncation is: \n");
	A.print();

	EXPECT_DOUBLE_EQ(A(0, 0), 1.0);
	EXPECT_DOUBLE_EQ(A(1, 0), 2.0);
	EXPECT_DOUBLE_EQ(A(0, 1), 4.0);
	EXPECT_DOUBLE_EQ(A(1, 1), 5.0);
}

TEST_F(TMatrixTest, SubColumn) {
	// TODO:
}

TEST_F(TMatrixTest, CholeskyUpdate) {
	tsfo_matrix<double> sigma5(5, 5, 0.0);

	sigma5(0,0) = 2.6350; sigma5(0,1) = 2.2772; sigma5(0,2) = 2.6283; sigma5(0,3) = 1.7453; sigma5(0,4) = 2.3293;
	sigma5(1,0) = 2.2772; sigma5(1,1) = 3.3730; sigma5(1,2) = 3.3573; sigma5(1,3) = 1.9532; sigma5(1,4) = 3.3334;
	sigma5(2,0) = 2.6283; sigma5(2,1) = 3.3573; sigma5(2,2) = 5.1700; sigma5(2,3) = 2.8389; sigma5(2,4) = 3.6581;
	sigma5(3,0) = 1.7453; sigma5(3,1) = 1.9532; sigma5(3,2) = 2.8389; sigma5(3,3) = 2.3694; sigma5(3,4) = 2.3077;
	sigma5(4,0) = 2.3293; sigma5(4,1) = 3.3334; sigma5(4,2) = 3.6581; sigma5(4,3) = 2.3077; sigma5(4,4) = 4.0751;

	printf("The matrix Sigma5 is: \n");
	sigma5.print();

	tsfo_matrix<double> oldchol;
	tsfo_vector<int> indices(4);
	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 3;
	sigma5.submatrix(indices, oldchol);

	printf("The matrix Sigma4 is: \n");
	oldchol.print();

	// compute Cholesky
	oldchol.cholesky();
	oldchol.reset(tsfo_matrix<double>::LOWER);

	printf("The matrix OldChol is: \n");
	oldchol.print();

	// run the Cholesky update by adding column 4
	int col = 4;
	tsfo_vector<double> xtR;
	sigma5.subcolumn(col, indices, xtR);
	printf("The xtR vector is: \n");
	xtR.print();

	double xtx = sigma5(col, col);
	if (oldchol.is_empty()) {
		oldchol.resize(1, 1, xtx);
		oldchol.cholesky();
	}

	tsfo_vector<double> r = oldchol.solve_forward(xtR);
	printf("The r vector is: \n");
	r.print();

	double rpp = xtx - r.dot_product(r);
	oldchol.append_row_with_value(0);
	r.push_back(sqrt(rpp));
	oldchol.append_column(r);

	printf("The updated Chol is: \n");
	oldchol.print();

	EXPECT_DOUBLE_EQ(oldchol(0, 0), 1.6232683080747925);
	EXPECT_DOUBLE_EQ(oldchol(0, 1), 1.4028488011946558);
	EXPECT_DOUBLE_EQ(oldchol(0, 2), 1.6191408326804471);
	EXPECT_DOUBLE_EQ(oldchol(0, 3), 1.0751765381718923);
	EXPECT_DOUBLE_EQ(oldchol(0, 4), 1.4349445426939715);
	EXPECT_DOUBLE_EQ(oldchol(1, 0), 0     );
	EXPECT_DOUBLE_EQ(oldchol(1, 1), 1.1853333881177552);
	EXPECT_DOUBLE_EQ(oldchol(1, 2), 0.91610532091165731);
	EXPECT_DOUBLE_EQ(oldchol(1, 3), 0.37532890477286029);
	EXPECT_DOUBLE_EQ(oldchol(1, 4), 1.1139395732348818);
	EXPECT_DOUBLE_EQ(oldchol(2, 0), 0     );
	EXPECT_DOUBLE_EQ(oldchol(2, 1), 0     );
	EXPECT_DOUBLE_EQ(oldchol(2, 2), 1.3073385196437139);
	EXPECT_DOUBLE_EQ(oldchol(2, 3), 0.57689492554465505);
	EXPECT_DOUBLE_EQ(oldchol(2, 4), 0.24036370332247389);
	EXPECT_DOUBLE_EQ(oldchol(3, 0), 0     );
	EXPECT_DOUBLE_EQ(oldchol(3, 1), 0     );
	EXPECT_DOUBLE_EQ(oldchol(3, 2), 0     );
	EXPECT_DOUBLE_EQ(oldchol(3, 3), 0.86006736357539904);
	EXPECT_DOUBLE_EQ(oldchol(3, 4), 0.2419845028361543);
	EXPECT_DOUBLE_EQ(oldchol(4, 0), 0     );
	EXPECT_DOUBLE_EQ(oldchol(4, 1), 0     );
	EXPECT_DOUBLE_EQ(oldchol(4, 2), 0     );
	EXPECT_DOUBLE_EQ(oldchol(4, 3), 0     );
	EXPECT_DOUBLE_EQ(oldchol(4, 4), 0.81169056732619693);
}
