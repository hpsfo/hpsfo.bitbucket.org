/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_logdet_test.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Test suite corresponding to the LogDet application
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>

#include "utils.h"
#include "sfo_vector.h"
#include "logdet_sf_context.h"
#include "logdet_sf_inc_context.h"
#include "sfo_kernel_factory.h"

#include "sfo_function_logdet.h"
#include "logdet_moons_generator.h"
#include "sfo_logdet_testdata.h"

#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

static const string DATA_FILEPATH = "../code/test/genmoons_data/";

// ================= Tests LogDet evaluation

static void logdet_eval_test(string* test_data, int num_tests, tsfo_vector<int> (*fnc_set)(int, int), double* expected_data) {
	for (int i=0; i < num_tests; ++i) {
		printf("processing %s \n", test_data[i].c_str());
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		tlogdet_moons_generator generator;
		file >> generator;

		// build context
		int     n      = generator.n();
		double* K_data = generator.K();
		double* s_data = generator.s();
		double  fv     = generator.fv();

		tsfo_matrix<double> K(n, n, K_data);
		tsfo_vector<double> s(n, s_data);

		// setup the logdet application
		tlogdet_sf_context sf_context(K, s, fv);

		// invoke function and check results
		int begin = 0;
		int end	  = n - 1;
		tsfo_vector<int> set = fnc_set(begin, end);
		double result = sf_context.f(set);

//		ASSERT_DOUBLE_EQ(expected_data[i], result);
		EXPECT_NEAR(expected_data[i], result, 1e-12);
	}
}

TEST(LogDetEval, EmptySet)
{
	logdet_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::empty_set, LOGDET_EXPECTED_EVAL[EMPTY_SET_DATA]);
}

TEST(LogDetEval, FullSet)
{
	logdet_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::full_set , LOGDET_EXPECTED_EVAL[FULL_SET_DATA]);
}

TEST(LogDetEval, FirstSet)
{
	logdet_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::first_set, LOGDET_EXPECTED_EVAL[FIRST_SET_DATA]);
}

TEST(LogDetEval, LastSet)
{
	logdet_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::last_set , LOGDET_EXPECTED_EVAL[LAST_SET_DATA]);
}

TEST(LogDetEval, EvenSet)
{
	logdet_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::even_set, LOGDET_EXPECTED_EVAL[EVEN_SET_DATA]);
}

TEST(LogDetEval, OddSet)
{
	logdet_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::odd_set, LOGDET_EXPECTED_EVAL[ODD_SET_DATA]);
}

// ================= Tests LogDet inc evaluation

static void logdet_inc_eval_test(string* test_data, int num_tests, tsfo_vector<int> (*fnc_set)(int, int), double* expected_data) {
	for (int i=0; i < num_tests; ++i) {
		printf("processing %s \n", test_data[i].c_str());
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		tlogdet_moons_generator generator;
		file >> generator;

		// build context
		int     n      = generator.n();
		double* K_data = generator.K();
		double* s_data = generator.s();
		double  fv     = generator.fv();

		tsfo_matrix<double> K(n, n, K_data);
		tsfo_vector<double> s(n, s_data);

		// setup the logdet application
		tlogdet_sf_inc_context sf_inc_context(K, s, fv);

		// invoke function and check results
		int begin = 0;
		int end	  = n - 1;
		tsfo_vector<int> set = fnc_set(begin, end);
		for (int j = 0; j < set.size() - 1; ++j) {
			sf_inc_context.f_inc(set[j]);
		}

//		printf("f_inc(%e)\n", set[set.size() - 1]);
		double result = sf_inc_context.f_inc(set[set.size() - 1]);

//		ASSERT_DOUBLE_EQ(expected_data[i], result);
		EXPECT_NEAR(expected_data[i], result, 1e-12);
	}
}

// EmptySet makes no sense in this case

TEST(LogDetIncEval, FullSet)
{
	logdet_inc_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::full_set , LOGDET_EXPECTED_EVAL[FULL_SET_DATA]);
}

TEST(LogDetIncEval, FirstSet)
{
	logdet_inc_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::first_set, LOGDET_EXPECTED_EVAL[FIRST_SET_DATA]);
}

TEST(LogDetIncEval, LastSet)
{
	logdet_inc_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::last_set , LOGDET_EXPECTED_EVAL[LAST_SET_DATA]);
}

TEST(LogDetIncEval, EvenSet)
{
	logdet_inc_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::even_set, LOGDET_EXPECTED_EVAL[EVEN_SET_DATA]);
}

TEST(LogDetIncEval, OddSet)
{
	logdet_inc_eval_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS, tset_factory::odd_set, LOGDET_EXPECTED_EVAL[ODD_SET_DATA]);
}

// ================= Tests Logdet application

static void logdet_test(string* test_data, int num_tests) {
	for (int i=0; i < num_tests; ++i) {
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		tlogdet_moons_generator generator;
		file >> generator;

		int     n      = generator.n();
		double* K_data = generator.K();
		double* s_data = generator.s();
		double  fv     = generator.fv();

		int* optimal;
		long optimal_size, major_iter, minor_iter;
		uint64_t flops_count;
		sfo_function_logdet(n, K_data, s_data, fv, optimal, optimal_size, major_iter, minor_iter, flops_count);

		int* expected_subset = generator.subset();
		long expected_size = generator.optimal_size();
		EXPECT_EQ(expected_size , optimal_size);
		if (expected_size == optimal_size) {
			for (int j = 0; j < (size_t) expected_size; j++) {
				EXPECT_EQ(expected_subset[i], optimal[i]);
			}
		}
		printf("\n n=%d,%ld,%ld,%ld \n", n, major_iter, minor_iter, optimal_size);

		// clean up
		delete[] optimal;
	}
}

TEST(LogDet, HPSFO_LogDet)
{
	// set the kernel
    set_min_norm_point_hpsfo_kernel();
	logdet_test(LOGDET_TEST_DATA, LOGDET_TEST_DATA_NUM_TESTS);
}
