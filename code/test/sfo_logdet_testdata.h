/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_logdet_testdata.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Test data for the logdet test
//============================================================================

#ifndef SFO_LOGDET_TESTDATA_H_
#define SFO_LOGDET_TESTDATA_H_

#include "set_factory.h"

static const int LOGDET_TEST_DATA_NUM_TESTS = 6;
static string LOGDET_TEST_DATA[] = {
		"logdet_two_moons_n20.txt" , "logdet_two_moons_n108.txt", "logdet_two_moons_n144.txt",
		"logdet_two_moons_n256.txt", "logdet_two_moons_n320.txt", "logdet_two_moons_n404.txt" };

static const int EMPTY_SET_DATA = 0;
static const int FULL_SET_DATA 	= 1;
static const int FIRST_SET_DATA = 2;
static const int LAST_SET_DATA 	= 3;
static const int EVEN_SET_DATA 	= 4;
static const int ODD_SET_DATA 	= 5;

static double LOGDET_EXPECTED_EVAL[][LOGDET_TEST_DATA_NUM_TESTS] = {
	{-3.5527136788005009e-15, -5.6843418860808015e-14, 2.8421709430404007e-13, 4.5474735088646412e-13, 3.4106051316484809e-13, -7.9580786405131221e-13}, // empty set
	{-3.5527136788005009e-15, -5.6843418860808015e-14, 2.8421709430404007e-13, 4.5474735088646412e-13, 3.4106051316484809e-13, -7.9580786405131221e-13}, // full set
	{-5.4235760872956416e+00,  4.1440414026042731e+00, 4.0563977939427787e+00, 3.4491119990943844e+00, 3.2232244969628709e+00,  3.1238393879921205e+00}, // first set
	{-2.8813074237812453e+00,  1.0123742438899512e+00, 3.5342506119959012e+00, 3.5419755771895325e+00, 3.2592258324445993e+00,  3.0941435158498507e+00}, // last set
	{ 1.3861748172090635e+01,  7.3903186014094103e+01, 7.1324421771249263e+01, 1.2603683904759851e+02, 1.3778977102120029e+02,  1.7585397656545479e+02}, // even set
	{-7.9845426119663836e+00,  9.1019660631350462e+01, 1.3640045381411005e+02, 1.4027744630081565e+02, 1.5129881916386489e+02,  1.2513167850345576e+02}  // odd set
};

#endif /* SFO_LOGDET_TESTDATA_H_ */
