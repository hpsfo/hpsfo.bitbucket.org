/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_mincut_testdata.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Test data for the mincut test
//============================================================================

#ifndef SFO_MINCUT_TESTDATA_H_
#define SFO_MINCUT_TESTDATA_H_

#ifdef __cplusplus
extern "C" {
#endif

#define STATIC_SIZE(x) (sizeof((x)) / sizeof((x)[0]))

#ifdef _MSC_VER
  #pragma warning(push)
  #pragma warning(disable: 4510)  // default constructor could not be generated
  #pragma warning(disable: 4512)  // assignment operator could not be generated
  #pragma warning(disable: 4610)  // can never be instantiated - user defined constructor required
#endif

typedef struct {
	const int n;  //number of nodes
	const int m;  //number of edges
	const int s;
	const int t;
	const int* expected_result;
	const long expected_size;
	const long expected_major_krause;
	const long expected_minor_krause;
	const long expected_major_fujishige;
	const long expected_minor_fujishige;
	const long expected_major_hpsfo;
	const long expected_minor_hpsfo;
	const char* filename;
} MinCutTestData;

#ifdef _MSC_VER
  #pragma warning(pop)
#endif

static const int graph_cut_test_n8_result_s1_t8    [] = {2, 3, 4, 5};

static const int graph_cut_test_n108_result_s1_t108[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 1};

static const int graph_cut_test_n144_result_s1_t144[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 1};

static const int graph_cut_test_n256_result_s1_t256[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 1};

static const int graph_cut_test_n320_result_s1_t320[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 1};

static const int graph_cut_test_n405_result_s1_t405[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 1};

static const int graph_cut_test_n512_result_s1_t512[] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 1};

static const MinCutTestData MINCUT_TEST_DATA[] = {
	{  8,  20,  1,   8,     graph_cut_test_n8_result_s1_t8, STATIC_SIZE(    graph_cut_test_n8_result_s1_t8),   3,    3,    3,    3,    3,    3, "out_a2_b2_n8_n20.txt"},
	{108, 432,  1, 108, graph_cut_test_n108_result_s1_t108, STATIC_SIZE(graph_cut_test_n108_result_s1_t108), 143,  181,  140,  177,  140,  177, "out_a6_b3_n108.txt"},
	{144, 588,  1, 144, graph_cut_test_n144_result_s1_t144, STATIC_SIZE(graph_cut_test_n144_result_s1_t144), 197,  254,  191,  242,  191,  242, "out_a6_b4_n144.txt"},
	{256, 1088, 1, 256, graph_cut_test_n256_result_s1_t256, STATIC_SIZE(graph_cut_test_n256_result_s1_t256), 249,  293,  259,  314,  259,  314, "out_a8_b4_n256.txt"},
	{320, 1376, 1, 320, graph_cut_test_n320_result_s1_t320, STATIC_SIZE(graph_cut_test_n320_result_s1_t320), 412,  509,  471,  626,  471,  626, "out_a8_b5_n320.txt"},
	{405, 1764, 1, 405, graph_cut_test_n405_result_s1_t405, STATIC_SIZE(graph_cut_test_n405_result_s1_t405), 716, 1031,  763, 1125,  763, 1125, "out_a9_b5_n405.txt"},
	{512, 2240, 1, 512, graph_cut_test_n512_result_s1_t512, STATIC_SIZE(graph_cut_test_n512_result_s1_t512), 1055, 1604, 974, 1442,  974, 1442, "out_a8_b8_n512.txt"}
};

static const size_t GRAPH_CUT_MIN_NUM_TESTS = STATIC_SIZE(MINCUT_TEST_DATA);

#undef STATIC_SIZE

#ifdef __cplusplus
}
#endif

#endif  // SFO_MINCUT_TESTDATA_H_
