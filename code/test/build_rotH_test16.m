
clear all;

nb = 16;

% defining 0 and 1 as symbols too, solves the problem
sym_0 = sym('0');
sym_1 = sym('1');

c0  = sym('c0');
c1  = sym('c1');
c2  = sym('c2');
c3  = sym('c3');
c4  = sym('c4');
c5  = sym('c5');
c6  = sym('c6');
c7  = sym('c7');
c8  = sym('c8');
c9  = sym('c9');
c10 = sym('c10');
c11 = sym('c11');
c12 = sym('c12');
c13 = sym('c13');
c14 = sym('c14');
c15 = sym('c15');

s0  = sym('s0');
s1  = sym('s1');
s2  = sym('s2');
s3  = sym('s3');
s4  = sym('s4');
s5  = sym('s5');
s6  = sym('s6');
s7  = sym('s7');
s8  = sym('s8');
s9  = sym('s9');
s10 = sym('s10');
s11 = sym('s11');
s12 = sym('s12');
s13 = sym('s13');
s14 = sym('s14');
s15 = sym('s15');

% create H orthogonal matrix using the sin and cos symbols
% filling in the first rotation
I=repmat(sym_0,(nb+1),(nb+1));
for i=1:(nb+1)
    I(i,i)=sym_1;
end
H = I;
H(1:2,1:2) = [c0 s0; -s0 c0];

G = I;
G(2:3,2:3) = [c1 s1; -s1 c1];
H = G*H;

G = I;
G(3:4,3:4) = [c2 s2; -s2 c2];
H = G*H;

G = I;
G(4:5,4:5) = [c3 s3; -s3 c3];
H = G*H;

G = I;
G(5:6,5:6) = [c4 s4; -s4 c4];
H = G*H;

G = I;
G(6:7,6:7) = [c5 s5; -s5 c5];
H = G*H;

G = I;
G(7:8,7:8) = [c6 s6; -s6 c6];
H = G*H;

G = I;
G(8:9,8:9) = [c7 s7; -s7 c7];
H = G*H;

G = I;
G(9:10,9:10) = [c8 s8; -s8 c8];
H = G*H;

G = I;
G(10:11,10:11) = [c9 s9; -s9 c9];
H = G*H;

G = I;
G(11:12,11:12) = [c10 s10; -s10 c10];
H = G*H;

G = I;
G(12:13,12:13) = [c11 s11; -s11 c11];
H = G*H;

G = I;
G(13:14,13:14) = [c12 s12; -s12 c12];
H = G*H;

G = I;
G(14:15,14:15) = [c13 s13; -s13 c13];
H = G*H;

G = I;
G(15:16,15:16) = [c14 s14; -s14 c14];
H = G*H;

G = I;
G(16:17,16:17) = [c15 s15; -s15 c15];
H = G*H;

% generate the C++ code :D
for i=1:(nb+1)
    for j=1:(nb+1)
        fprintf('gc(%d, %d)=%s;\t', i - 1, j - 1, char(H(i, j)));
    end
    fprintf('\n');
end
