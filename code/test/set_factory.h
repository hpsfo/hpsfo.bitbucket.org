/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : set_factory.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 24.07.2012
// Description : Test helper class for generating different set types
//============================================================================

#ifndef SET_FACTORY_H_
#define SET_FACTORY_H_

class tset_factory {
public:

	static tsfo_vector<int> empty_set(int begin, int end);
	static tsfo_vector<int> full_set(int begin, int end);
	static tsfo_vector<int> first_set(int begin, int end);
	static tsfo_vector<int> last_set(int begin, int end);
	static tsfo_vector<int> even_set(int begin, int end);
	static tsfo_vector<int> odd_set(int begin, int end);
};

inline tsfo_vector<int> tset_factory::empty_set(int begin, int end) {
	return tsfo_vector<int>();
}

inline tsfo_vector<int> tset_factory::full_set(int begin, int end) {
	tsfo_vector<int> full_set;
	for (int i = begin; i <= end; ++i) {
		full_set.append(i);
	}

	return full_set;
}

inline tsfo_vector<int> tset_factory::first_set(int begin, int end) {
	tsfo_vector<int> first_set(1, begin);

	return first_set;
}

inline tsfo_vector<int> tset_factory::last_set(int begin, int end) {
	tsfo_vector<int> last_set(1, end);

	return last_set;
}

inline tsfo_vector<int> tset_factory::even_set(int begin, int end) {
	tsfo_vector<int> even_set;
	for (int i = begin + 1; i <= end; i += 2) {
		even_set.append(i);
	}

	return even_set;
}

inline tsfo_vector<int> tset_factory::odd_set(int begin, int end) {
	tsfo_vector<int> odd_set;
	for (int i = begin; i <= end; i += 2) {
		odd_set.append(i);
	}

	return odd_set;
}

#endif /* SET_FACTORY_H_ */
