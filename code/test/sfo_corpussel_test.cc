/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_corpussel_test.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 23.07.2012
// Description : Test suite corresponding to the CorpusSel application
//============================================================================

#include <gtest/gtest.h>
#include <iostream>
#include <fstream>
#include <string>

#include "utils.h"
#include "sfo_vector.h"
#include "corpussel_sf_context.h"
#include "corpussel_sf_inc_context.h"

#include "sfo_function_corpussel.h"
#include "sfo_corpussel_testdata.h"

#include <boost/filesystem.hpp>

using namespace std;
using namespace boost::filesystem;

static const string DATA_FILEPATH = "../code/test/genbipartite_data/";

// ================= Tests CorpusSel evaluation

static void corpussel_eval_test(string* test_data, int num_tests,
		tsfo_vector<int> (*fnc_set)(int, int), double* expected_data) {
	for (int i = 0; i < num_tests; ++i) {
		printf("processing %s \n", test_data[i].c_str());
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		thp_adjlist_bidir graph;
		file >> graph;

		double lambda = 1.0;
		tcorpussel_sf_context sf_context(graph, lambda);
		tsfo_vector<int> ground = sf_context.ground();

		// generate test set
		int begin = ground[0];
		int end = ground[ground.size() - 1];
		tsfo_vector<int> set = fnc_set(begin, end);
		double result = sf_context.f(set);

//		printf("result=%e\n", result);
		ASSERT_DOUBLE_EQ(expected_data[i], result);
	}
}

TEST(CorpusSelEval, EmptySet) {
	corpussel_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, tset_factory::empty_set,
			CORPUSSEL_EXPECTED_EVAL[EMPTY_SET_DATA]);
}

TEST(CorpusSelEval, FullSet) {
	corpussel_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, tset_factory::full_set,
			CORPUSSEL_EXPECTED_EVAL[FULL_SET_DATA]);
}

TEST(CorpusSelEval, FirstSet) {
	corpussel_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, tset_factory::first_set,
			CORPUSSEL_EXPECTED_EVAL[FIRST_SET_DATA]);
}

TEST(CorpusSelEval, LastSet) {
	corpussel_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, tset_factory::last_set,
			CORPUSSEL_EXPECTED_EVAL[LAST_SET_DATA]);
}

TEST(CorpusSelEval, EvenSet) {
	corpussel_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, tset_factory::even_set,
			CORPUSSEL_EXPECTED_EVAL[EVEN_SET_DATA]);
}

TEST(CorpusSelEval, OddSet) {
	corpussel_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, tset_factory::odd_set,
			CORPUSSEL_EXPECTED_EVAL[ODD_SET_DATA]);
}

// ================= Tests CorpusSel inc evaluation

static void corpussel_inc_eval_test(string* test_data, int num_tests,
		tsfo_vector<int> (*fnc_set)(int, int), double* expected_data) {
	for (int i = 0; i < num_tests; ++i) {
		printf("processing %s \n", test_data[i].c_str());
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		thp_adjlist_bidir graph;
		file >> graph;

		double lambda = 1.0;
		tcorpussel_sf_inc_context sf_inc_context(graph, lambda);
		tsfo_vector<int> ground = sf_inc_context.ground();

		// generate test set
		int begin = ground[0];
		int end = ground[ground.size() - 1];
		tsfo_vector<int> set = fnc_set(begin, end);
		for (int j = 0; j < set.size() - 1; ++j) {
			sf_inc_context.f_inc(set[j]);
		}

		double result = sf_inc_context.f_inc(set[set.size() - 1]);

		ASSERT_DOUBLE_EQ(expected_data[i], result);
	}
}

// EmptySet makes no sense in this case

TEST(CorpusSelIncEval, FullSet) {
	corpussel_inc_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS,
			tset_factory::full_set, CORPUSSEL_EXPECTED_EVAL[FULL_SET_DATA]);
}

TEST(CorpusSelIncEval, FirstSet) {
	corpussel_inc_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS,
			tset_factory::first_set, CORPUSSEL_EXPECTED_EVAL[FIRST_SET_DATA]);
}

TEST(CorpusSelIncEval, LastSet) {
	corpussel_inc_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS,
			tset_factory::last_set, CORPUSSEL_EXPECTED_EVAL[LAST_SET_DATA]);
}

TEST(CorpusSelIncEval, EvenSet) {
	corpussel_inc_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS,
			tset_factory::even_set, CORPUSSEL_EXPECTED_EVAL[EVEN_SET_DATA]);
}

TEST(CorpusSelIncEval, OddSet) {
	corpussel_inc_eval_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS,
			tset_factory::odd_set, CORPUSSEL_EXPECTED_EVAL[ODD_SET_DATA]);
}

// ================= Tests CorpusSel application

static void corpussel_test(string* test_data, int num_tests, double* expected_data) {
	for (int i = 0; i < num_tests; ++i) {
		ifstream file((DATA_FILEPATH + test_data[i]).c_str());
		assert(file.is_open());

		thp_adjlist_bidir graph;
		file >> graph;

		// modify the in_weights
		double* in_weights = graph.in_weights();
		for (int i = 0; i < graph.num_edges(); ++i) {
			in_weights[i] = i % 31;
		}

		int n = graph.num_vertices();

		int* optimal;
		long optimal_size, major_iter, minor_iter;
		uint64_t flops_count;

		// TODO: check why HPSFO does not work well for any lambda > 0.0
		double lambda = 0.0;

		sfo_function_corpussel(graph, lambda, optimal, optimal_size, major_iter,
				minor_iter, flops_count);

		printf("\n n=%d,%ld,%ld,%ld \n", n, major_iter, minor_iter, optimal_size);

		// clean up
		delete[] optimal;
	}
}

TEST(CorpusSel, HPSFO_CorpusSel)
{
	// set the kernel
    set_min_norm_point_hpsfo_kernel();
	corpussel_test(CORPUSSEL_TEST_DATA, CORPUSSEL_TEST_DATA_NUM_TESTS, NULL);
}
