#include <stdlib.h>

#include "math_to_gcc.h"

#if defined(_WIN32) || defined(WIN32)
double drand48() {
  return (double) rand() / (double) RAND_MAX;
}

void srand48(long int seed) {
  srand((unsigned int) seed);
}
#endif
