#-------------------------------------------------------------------------------
# HPSFO High-Performance Submodular Function Optimization
# Note that a separate commercial license is available upon request.
# Copyright (C) 2012  Giovanni Azua Garcia
# bravegag@hotmail.com
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
#-------------------------------------------------------------------------------
# Fast Numerical Code (Summer 2011)
# Project: submodular function minimization
# Author: Giovanni Azua Garcia, Andrei Frunza, Zaheer Chothia
#
# Thesis Work (Summer 2012):
# High Performance Minimum-Norm-Point Algorithm for Submodular Function Minimization
# Author: Giovanni Azua Garcia
#-------------------------------------------------------------------------------

Requirements
------------
* C/C++ compiler
  (Tested with gcc 4.x, MinGW and Visual Studio 2010)
* CMake 2.6 or newer
  http://www.cmake.org/cmake/resources/software.html
* Python 2.x
  http://python.org/download/
  (Optional: for running benchmarks and generating plots)

Build Instructions
------------------
The typical workflow is as follows:

  $ mkdir build
  $ cd build
  $ cmake ..
  $ make

Additional commands:
--------------------
  $ cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug ..
  $ cmake -G "Eclipse CDT4 - Unix Makefiles" ..

For Windows you can invoke the batch file 'build.bat' directly.

Use 'make check' to run all tests.
Use 'make cpplint' to run check code style.

Eclipse Project: cmake -G "Eclipse CDT4 - Unix Makefiles"
Xcode Project:  mkdir xcode && cd xcode && cmake -G Xcode ..

To compile with the Intel compiler use:
---------------------------------------
  $ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc -DCMAKE_Fortran_COMPILER=ifort ..

Detect memory errors
--------------------
valgrind --tool=memcheck --leak-check=full --show-reachable=yes --dsymutil=yes --track-origins=yes ./test_matrix
valgrind --show-reachable=yes --dsymutil=yes ./test_matrix

Profile using MKL
--------------------
Included with Intel Composer (bin/loopprofileviewer.jar).
When compiling the code add the flags "/Qprofile-functions /Qprofile-loops:all"
and after running it will produce an XML file which you can open with
the tool.

Choice 1) Compile it separately: icl /fast /Ob2 /fp:fast=2 src/*.cc test/benchmark.cc

Choice 2) Add this to the CMakeLists.txt:

	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} /Qprofile-functions /Qprofile-loops:all")
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Qprofile-functions /Qprofile-loops:all")

Then compile and after run you will find some XML files (e.g. loop_prof_1333620801.xml)

Code Overview
-------------
* 'sfo.h' contains definitions for missing features (e.g. inline, restrict)
* 'util.h' contains memory allocation wrappers, which abort early

Benchmarking notes
------------------
Remember to 'export MKL_NUM_THREADS=1' to limit the number of threads of MKL to one
and 'export OMP_NUM_THREADS=1' to limit the number of threads of OPENMP to one.

Alternatively control the OpenMP scheduler with: export OMP_SCHEDULE=guided,64

Versions
--------
* Baseline (r799) - first correct working version (852)

Compile with user-friendly errors i.e. Clang
--------------------------------------------
cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Debug -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ..

Import external googletest project
----------------------------------
svn propset svn:externals 'googletest http://googletest.googlecode.com/svn/trunk' .

Generating a mincut sample file using genrmf
remember that the n=a*a*b
--------------------------------------------
./genrmf -a 10 -b 40 -c1 1 -c2 10 -seed 0 > ../code/test/genrmf_data/out_a10_b40_n4000.txt

Export the include dirs for the roofline to see
-----------------------------------------------
export CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:/home/bravegag/code/fastcode_project/code/src/api/
