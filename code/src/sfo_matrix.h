/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_matrix.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Include declaration for the tsfo_matrix class.
//============================================================================

#ifndef SFO_MATRIX_H_
#define SFO_MATRIX_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <algorithm>
#include <limits>
#include <deque>
#include <emmintrin.h>

#include "sfo_vector.h"
#include "bufferpool.h"
#include "utils.h"
#include "logger.h"

using namespace std;

/**
 * Concrete definition of matrix in column major ordering that delegates most
 * operations to LAPACK and BLAS functions. This implementation provides support
 * for QR decomposition and fast updates. The following sequence of QR updates
 * are supported:
 *
 * 1) [addcol -> addcol] integrates nicely with the LAPACK compact form.
 * 2) [addcol -> delcol] delcols holds additional Q, and most to date R
 * 3) [delcol -> delcol] delcols holds additional Q, and most to date R
 * 4) [delcol -> addcol] delcols Q must also be applied to the new column
 * 5) [addcol -> addrow] addrows holds additional Q, R is updated in original QR
 * 6) [delcol -> addrow] addrows holds additional Q, R is updated in original QR
 */
template<typename T>
class tsfo_matrix {
protected:
	static const int 	  BLOCK_SIZE 		  	= 64;
	static const int 	  MAX_INT;
	static const double   EPSILON 			 	= 1e-10;
//	static const long 	  MATRIX_POOL_INITIAL  	= 9;  // HPSFO
	static const long 	  MATRIX_POOL_INITIAL  	= 12; // Krause
	static const long 	  MATRIX_BUFFER_SIZE;
	static tbufferpool<T> s_bufferpool;

	// basic matrix structure
	typename sfo_type<T>::aptr32 m_data;
	int			  				 m_rows, m_cols;
	deque<tsfo_vector<T>*>  	 m_cols_vector;

	// cached matrix diagonal
	tsfo_vector<T> 			m_diag;

	// amount by which the matrix has been shifted to the right
	int						m_left_gap;

	typedef typename deque<tsfo_vector<T>*>::iterator tcols_vector_iter;

	// QR decomposition of this matrix
	tsfo_matrix<T>*	  m_QR;
	tsfo_vector<T>*	  m_tau;

	// QR addrow update for this matrix
	tsfo_matrix<T>*	  m_QR_addrow_u;
	tsfo_vector<T>*	  m_tau_addrow_u;
	tsfo_vector<T>*   m_refl_addrow_u;

	// QR delcols update for this matrix
	tsfo_matrix<T>*	  m_QR_delcols_u;
	tsfo_vector<T>*	  m_tau_delcols_u;
	int			  	  m_begin_delcols;
	int			  	  m_block_delcols;

	inline T& elem(int i, int j) {
	  return m_data[m_left_gap + j*m_rows + i];
	}

	inline const T& elem(int i, int j) const {
	  return m_data[m_left_gap + j*m_rows + i];
	}

	void init(tbufferpool<T>& bufferpool);
	void release(tbufferpool<T>& bufferpool);

	void compute_qr_upd_addcol();
	void update_qr_upd_delcols();
	void apply_qr_upd_delcols(T* a);
	void delete_qr_upd_addrow();
 	void delete_qr_upd_delcols();
	void append_row_with_value_qr_upd_only_common(T value);
	void undo_append_row_with_value_qr_upd_only_common();
	void clear_cols_vector();

public:
	// traits
	enum tside {
		UPPER, LOWER, ALL
	};

	// constructors
	tsfo_matrix(int rows = 0, int cols = 0, tbufferpool<T>& bufferpool = tsfo_matrix<T>::s_bufferpool);
	tsfo_matrix(int rows, int cols, T initial, tbufferpool<T>& bufferpool = tsfo_matrix<T>::s_bufferpool);
	tsfo_matrix(const tsfo_matrix<T>& A, tbufferpool<T>& bufferpool = tsfo_matrix<T>::s_bufferpool);
	tsfo_matrix(int rows, int cols, const T* data);

	   // operators
virtual	tsfo_matrix<T>& operator=(const tsfo_matrix<T>& A);
		       bool operator==(const tsfo_matrix<T>& A) const;
	             T& operator()(int i, int j);
	       const T& operator()(int i, int j) const;
	     tsfo_vector<T> operator()(int j) const;
	    tsfo_vector<T>& operator[](int j);
	     tsfo_vector<T> operator*(const tsfo_vector<T>& x) const;

		       void copy_to(tsfo_matrix<T>& B) const;
		  	   void copy_to(tsfo_matrix<T>& B, int rows, int cols) const;
		       void clear();
		       void reset(const tside& side = ALL, T value = 0.0);
		       void resize(int rows, int cols, T initial = 0.0);
		       void reset_cols_vector();
		       bool is_triangular() const;
		       void random(int rows, int cols);
		       void identity();
		       bool is_empty() const;
		       void append_column(const tsfo_vector<T>& a);
		       void delete_column(int begin, int end);
		       void delete_column_with_gap(int begin, int end, bool triangular = false);
		       void delete_row_last();
		       void append_row_with_value(T value);
		       void append_row_with_value_qr_upd_only(T value);
		       void undo_append_row_with_value_qr_upd_only();
        tsfo_matrix<T>& transpose(const tsfo_matrix<T>& S);
        tsfo_matrix<T>& subspace_translated(const tsfo_matrix<T>& S);
        tsfo_matrix<T>& subspace_translated_delta(const tsfo_matrix<T>& S);
	   	       void multiply(const tsfo_matrix<T>& B, tsfo_matrix<T>& C) const;
	     tsfo_vector<T> multiply_triangular(const tsfo_vector<T>& x, const tsfo_matrix<T>::tside side = UPPER) const;
	    tsfo_matrix<T>& cholesky();
	     tsfo_vector<T> solve(const tsfo_vector<T>& b);
	    	   void qr_factorization();
	     tsfo_vector<T> solve_forward(const tsfo_vector<T>& b) const;
	     tsfo_vector<T> solve_backward(const tsfo_vector<T>& b) const;
	    tsfo_vector<T>& diag();
       	   	   void submatrix(const tsfo_vector<int>& indices, tsfo_matrix<T>& B) const;
       	   	   void subcolumn(int col, const tsfo_vector<int>& indices, tsfo_vector<T>& b) const;
	           void print() const;
	           void print(int rows, int cols) const;
	           void print_summary() const;
	           void qr_print() const;
	           bool equals(const tsfo_matrix<T>& B);
	           void delete_qr();
	           bool has_valid_qr() const;
	           bool has_valid_qr_upd_addrow() const;
	           bool has_valid_qr_upd_delcols() const;
	           bool is_squared() const;
	            int rows() const;
	           void rows(int rows);
	            int cols() const;
	           void cols(int cols);
         lapack_int lapack_order() const;
	    CBLAS_ORDER blas_order() const;
	           char char_order() const;
	            int leading_dim() const;
	      inline T* data() { return m_data + m_left_gap; }
    inline const T* data() const { return m_data + m_left_gap; }

	        virtual ~tsfo_matrix();

	static double 	 RECOMPUTE_QR_BEGIN_THRESHOLD;
	static double 	 RECOMPUTE_QR_BLOCK_THRESHOLD;
	static const int MAX_M_N;
};

template<typename T>
const int tsfo_matrix<T>::MAX_M_N = read_env("SFO_MAX_M_N", DEFAULT_MAX_M_N);

template<typename T>
const long tsfo_matrix<T>::MATRIX_POOL_INITIAL;

template<typename T>
const long tsfo_matrix<T>::MATRIX_BUFFER_SIZE = 2*read_env("SFO_MAX_M_N", DEFAULT_MAX_M_N)*read_env("SFO_MAX_M_N", DEFAULT_MAX_M_N);

// static up-front allocation
template<typename T>
tbufferpool<T> tsfo_matrix<T>::s_bufferpool = tbufferpool<T>(MATRIX_POOL_INITIAL,
		MATRIX_BUFFER_SIZE, PAGE_ALIGNED);

template<typename T>
const int tsfo_matrix<T>::MAX_INT = std::numeric_limits<int>::max();

template<typename T>
const int tsfo_matrix<T>::BLOCK_SIZE;

template<typename T>
double tsfo_matrix<T>::RECOMPUTE_QR_BEGIN_THRESHOLD = 0.3;

template<typename T>
double tsfo_matrix<T>::RECOMPUTE_QR_BLOCK_THRESHOLD = 0.6;

#ifdef HAVE_INTEL_MKL
	#include <mkl_lapacke.h>
	#include <mkl_lapack.h>
	#include <mkl_lapacke.h>
	#include <mkl_cblas.h>
	#include <mkl_trans.h>
#endif

inline void F_delcols(lapack_int* m, lapack_int* n, double* A, lapack_int* lda,
		lapack_int* k, lapack_int* p, double* tau, double* work, lapack_int* info) {
	throw "F_delcols is undefined!";
}

inline void F_delcols_appq(lapack_int* m, lapack_int* n, const double* A, lapack_int* lda,
		double* Q, lapack_int* ldq, lapack_int* k, lapack_int* p, double* tau, double* work,
		lapack_int* info) {
	throw "F_delcols_appq is undefined!";
}

inline void F_addrows(lapack_int* m, lapack_int* n, double* A, lapack_int* lda, double* B,
		lapack_int* ldb, double* tau, double* work, lapack_int* lwork, lapack_int* info) {
	throw "F_addrows is undefined!";
}

inline void F_addrows_appq(char* side, char* trans, const lapack_int* m, const lapack_int* n,
		lapack_int* k, const double* v, lapack_int* ldv, double* tau, const double* c, lapack_int* ldc,
		double* d, lapack_int* ldd, double* work, lapack_int* lwork, lapack_int* info) {
	throw "F_addrows_appq is undefined!";
}

template<typename T>
inline lapack_int tsfo_matrix<T>::lapack_order() const {
	return LAPACK_COL_MAJOR;
}

template<typename T>
inline CBLAS_ORDER tsfo_matrix<T>::blas_order() const {
	return CblasColMajor;
}

template<typename T>
inline char tsfo_matrix<T>::char_order() const {
	return 'C';
}

template<typename T>
inline int tsfo_matrix<T>::leading_dim() const {
	return m_rows;
}

/**
 * Assigns A to this object. Performs a full copy of A into this.
 */
template<typename T>
inline tsfo_matrix<T>& tsfo_matrix<T>::operator=(const tsfo_matrix<T>& A) {
	LENTER;

	A.copy_to(*this);

	return *this;
}

// returns true if matrix has valid QR, false otherwise
template<typename T>
inline bool tsfo_matrix<T>::has_valid_qr() const {
	assert((m_QR != NULL && m_tau != NULL) ||
		(m_QR == NULL && m_tau == NULL));

	return m_QR != NULL;
}

// returns true if matrix has valid QR addrow update, false otherwise
template<typename T>
inline bool tsfo_matrix<T>::has_valid_qr_upd_addrow() const {
	assert((m_QR_addrow_u != NULL && m_tau_addrow_u != NULL && m_refl_addrow_u != NULL) ||
			(m_QR_addrow_u == NULL && m_tau_addrow_u == NULL && m_refl_addrow_u == NULL));

	return m_QR_addrow_u != NULL;
}

// returns true if matrix has valid QR delcol update, false otherwise
template<typename T>
inline bool tsfo_matrix<T>::has_valid_qr_upd_delcols() const {
	assert((m_QR_delcols_u != NULL && m_tau_delcols_u != NULL &&
				m_begin_delcols < MAX_INT && m_block_delcols > 0) ||
			(m_QR_delcols_u == NULL && m_tau_delcols_u == NULL &&
				m_begin_delcols == MAX_INT && m_block_delcols == 0));

	return m_QR_delcols_u != NULL;
}

// returns true if the matrix A is squared, false otherwise
template<typename T>
inline bool tsfo_matrix<T>::is_squared() const {
	return m_rows == m_cols;
}

// returns the i, j element of the matrix
template<typename T>
inline T& tsfo_matrix<T>::operator()(int i, int j) {
	return elem(i, j);
}

// returns the i, j element of the matrix
template<typename T>
inline const T& tsfo_matrix<T>::operator()(int i, int j) const {
	return elem(i, j);
}

template<typename T>
inline int tsfo_matrix<T>::rows() const {
	return m_rows;
}

template<typename T>
inline void tsfo_matrix<T>::rows(int rows) {
	m_rows = rows;
}

template<typename T>
inline int tsfo_matrix<T>::cols() const {
	return m_cols;
}

template<typename T>
inline void tsfo_matrix<T>::cols(int cols) {
	m_cols = cols;
}

// reusable init
template<typename T>
inline void tsfo_matrix<T>::init(tbufferpool<T>& bufferpool) {
	m_QR  		   	= NULL;
	m_tau 		   	= NULL;

	m_QR_addrow_u  	= NULL;
	m_tau_addrow_u 	= NULL;
	m_refl_addrow_u = NULL;

	m_QR_delcols_u	= NULL;
	m_tau_delcols_u	= NULL;;
	m_begin_delcols	= MAX_INT;
	m_block_delcols = 0;

	m_left_gap		= 0;

	// allocate the memory from the bufferpool
	m_data = bufferpool.next();

	// initialize cols vector
	assert(m_cols_vector.empty());
	m_cols_vector.insert(m_cols_vector.begin(), m_cols, NULL);
}

// constructor
template<typename T>
inline tsfo_matrix<T>::tsfo_matrix(int rows, int cols, tbufferpool<T>& bufferpool) : m_rows(rows), m_cols(cols) {
	LENTER;

	init(bufferpool);
}

// constructor
template <>
inline tsfo_matrix<double>::tsfo_matrix(int rows, int cols, double initial, tbufferpool<double>& bufferpool) : m_rows(rows), m_cols(cols) {
	LENTER;
	init(bufferpool);

	// initialize to default initial value
	char   *uplo  = "All";
	int 	m     = rows;
	int 	n     = cols;
	double 	alpha = initial;
	double 	beta  = initial;
	int    	lda   = leading_dim();
	DLASET(uplo, &m, &n, &alpha, &beta, data(), &lda);
}

template<typename T>
inline tsfo_matrix<T>::tsfo_matrix(int rows, int cols, T initial, tbufferpool<T>& bufferpool) : m_rows(rows), m_cols(cols) {
	LENTER;
	init(bufferpool);

	// initialize to default initial value
	for (int i = 0; i < m_rows; i++) {
		for (int j = 0; j < m_cols; j++) {
			elem(i, j) = initial;
		}
	}
}

template<typename T>
inline tsfo_matrix<T>::tsfo_matrix(int rows, int cols, const T* data) : m_rows(rows), m_cols(cols) {
	LENTER;
	init(s_bufferpool);

	// initialize using the given data
	for (int i = 0; i < m_rows; i++) {
		for (int j = 0; j < m_cols; j++) {
			elem(i, j) = data[j*m_rows + i];
		}
	}
}

/**
 * Resizes the matrix padding if necessary the extra elements with the given initial value
 */
template<typename T>
inline void tsfo_matrix<T>::resize(int rows, int cols, T initial) {
	LENTER;

	for (int i = m_rows; i < rows; ++i) {
		for (int j = m_cols; j < cols; ++j) {
			elem(i, j) = initial;
		}
	}

	m_rows = rows;
	m_cols = cols;
}

/**
 * Resets the side of the matrix to the given value.
 */
template<typename T>
inline void tsfo_matrix<T>::reset(const tside& side, T value) {
	// reset to value
	switch (side) {
		case ALL: {
			char   *uplo  = "All";
			int 	m     = rows();
			int 	n     = cols();
			double 	alpha = value;
			double 	beta  = value;
			int    	lda   = leading_dim();
			DLASET(uplo, &m, &n, &alpha, &beta, data(), &lda);

			break;
		}
		case LOWER: {
			for (int j = 0; j < cols(); ++j) {
				for (int i = j+1; i < rows(); ++i) {
					elem(i, j) = value;
				}
			}

			break;
		}
		case UPPER: {
			for (int j = 0; j < cols(); ++j) {
				for (int i = 0; i < j; ++i) {
					elem(i, j) = value;
				}
			}

			break;
		}
	}
}

// copy constructor
template<typename T>
inline tsfo_matrix<T>::tsfo_matrix(const tsfo_matrix<T>& A, tbufferpool<T>& bufferpool) : m_rows(A.rows()), m_cols(A.cols()) {
	LENTER;

	m_rows = A.rows();
	m_cols = A.cols();

	init(bufferpool);

	// now copy the data from A to this
	A.copy_to(*this);

	// and also include QR
	if (A.has_valid_qr()) {
		m_QR  = new tsfo_matrix<T>(*A.m_QR);
		m_tau = new tsfo_vector<T>(*A.m_tau);

		if (A.has_valid_qr_upd_addrow()) {
			m_QR_addrow_u  	= new tsfo_matrix<T>(*A.m_QR_addrow_u);
			m_tau_addrow_u 	= new tsfo_vector<T>(*A.m_tau_addrow_u);
			m_refl_addrow_u = new tsfo_vector<T>(*A.m_refl_addrow_u);
		}

		if (A.has_valid_qr_upd_delcols()) {
			m_QR_delcols_u   = new tsfo_matrix<T>(*A.m_QR_delcols_u);
			m_tau_delcols_u  = new tsfo_vector<T>(*A.m_tau_delcols_u);
			m_begin_delcols  = A.m_begin_delcols;
			m_block_delcols  = A.m_block_delcols;
		}
	}
}

/**
 * Returns true if the number of rows and columns is zero, false otherwise
 */
template<typename T>
inline bool tsfo_matrix<T>::is_empty() const {
	return m_rows == 0 && m_cols == 0;
}

/**
 * Logically resets the matrix to zero columns and zero rows,
 * deleting the existing factorizations
 */
template<typename T>
inline void tsfo_matrix<T>::clear() {
	LENTER;

	m_rows = 0;
	m_cols = 0;

	if (has_valid_qr()) {
		delete_qr();
	}

	// clear the cols vector
	clear_cols_vector();
}

/**
 * Returns true if the matrix is triangular, false otherwise
 */
template<typename T>
inline bool tsfo_matrix<T>::is_triangular() const {
	LENTER;

	bool triangular = true;
	for (int j = 0; j < m_cols; ++j) {
		for (int i = j + 1; i < m_rows; ++i) {
			if (elem(i, j) > EPSILON) {
				triangular = false;
				break;
			}
		}
	}

	return triangular;
}

/**
 * Generates a random matrix of rows x cols
 */
template<typename T>
inline void tsfo_matrix<T>::random(int rows, int cols) {
	assert(rows > 0);
	assert(cols > 0);

	m_rows = rows;
	m_cols = cols;

	assert(m_cols_vector.empty());
	m_cols_vector.insert(m_cols_vector.begin(), cols, NULL);

	for (int i = 0; i < rows; ++i) {
		for (int j = 0; j < cols; ++j) {
			elem(i, j) = (double) rand() / (double) RAND_MAX;
		}
	}
}

/**
 * Generates the Identity matrix for the size specified by this matrix
 */
template<typename T>
inline void tsfo_matrix<T>::identity() {
	// initialize the Identity matrix into I the first time
	char   *uplo  = "All";
	int 	m     = m_rows;
	int 	n     = m_cols;
	double 	alpha = 0.0;
	double 	beta  = 1.0;
	int    	lda   = m_rows;
	DLASET(uplo, &m, &n, &alpha, &beta, data(), &lda);
}

/**
 * Reusable release method that will clean up all resources for this matrix
 */
template<typename T>
inline void tsfo_matrix<T>::release(tbufferpool<T>& bufferpool) {
	LENTER;

	if (has_valid_qr()) {
		delete_qr();
	}

	// release the memory back to the bufferpool
	bufferpool.release(m_data);
	m_data = NULL;

	// clear the cols vector
	clear_cols_vector();
}

template<typename T>
inline tsfo_matrix<T>::~tsfo_matrix() {
	LENTER;

	release(tsfo_matrix<T>::s_bufferpool);
}

/**
 * Deletes the QR addrow update data from the matrix A
 */
template<typename T>
inline void tsfo_matrix<T>::delete_qr_upd_addrow() {
	LENTER;

	delete m_QR_addrow_u;   m_QR_addrow_u   = NULL;
	delete m_tau_addrow_u;  m_tau_addrow_u  = NULL;
	delete m_refl_addrow_u;	m_refl_addrow_u = NULL;
}

/**
 * Deletes the QR delcols update data from the matrix A
 */
template<typename T>
inline void tsfo_matrix<T>::delete_qr_upd_delcols() {
	LENTER;

	delete m_QR_delcols_u;  m_QR_delcols_u  = NULL;
	delete m_tau_delcols_u;	m_tau_delcols_u = NULL;
	m_begin_delcols = MAX_INT;
	m_block_delcols = 0;
}

/**
 * Deletes the main QR decomposition data from the matrix A
 */
template<typename T>
inline void tsfo_matrix<T>::delete_qr() {
	LENTER;

	delete_qr_upd_addrow();
	delete_qr_upd_delcols();

	delete m_QR;  m_QR  = NULL;
	delete m_tau; m_tau = NULL;
}

/**
 * Prints the QR structure of a given matrix
 */
template<typename T>
inline void tsfo_matrix<T>::qr_print() const {
	if (has_valid_qr()) {
		printf("QR decomposition: \n");
		m_QR->print();

		printf("TAU coefficients: \n");
		m_tau->print();

		if (has_valid_qr_upd_addrow()) {
			printf("QR addrow update: \n");
			m_QR_addrow_u->print();

			printf("TAU addrow update coefficients: \n");
			m_tau_addrow_u->print();

			printf("Elementary refl. addrow update: \n");
			m_refl_addrow_u->print();
		}

		if (has_valid_qr_upd_delcols()) {
			printf("QR delcols update: \n");
			m_QR_delcols_u->print();

			printf("TAU delcols update coefficients: \n");
			m_tau_delcols_u->print();
		}
	}
}

/**
 * Prints this matrix
 */
template<typename T>
inline void tsfo_matrix<T>::print(int rows, int cols) const {
	assert(rows <= m_rows && cols <= m_cols);
	assert(data() != NULL);

	printf("matrix of dimensions %d x %d \n", rows, cols);
	if (rows > 0 && cols > 0) {
		for (int i = 0; i < rows; ++i) {
			for (int j = 0; j < cols; ++j) {
				printf("%e ", elem(i, j));
			}
			printf("\n");
		}
	}
}

/**
 * Prints this matrix
 */
template<typename T>
inline void tsfo_matrix<T>::print() const {
	print(rows(), cols());
}

/**
 * Prints a summary of this matrix
 */
template<typename T>
inline void tsfo_matrix<T>::print_summary() const {
	printf("main matrix of dimensions %d x %d \n", m_rows, m_cols);

	if (has_valid_qr()) {
		printf("QR matrix of dimensions %d x %d \n", m_QR->m_rows, m_QR->m_cols);

		if (has_valid_qr_upd_delcols()) {
			printf("delcols QR update matrix of dimensions %d x %d \n",
				m_QR_delcols_u->m_rows, m_QR_delcols_u->m_cols);
		}

		if (has_valid_qr_upd_addrow()) {
			printf("addrow QR update matrix of dimensions %d x %d \n",
				m_QR_addrow_u->m_rows, m_QR_addrow_u->m_cols);
		}
	}
}

/**
 * Returns the j-th column of matrix A as a tsfo_vector
 */
template<typename T>
inline tsfo_vector<T>& tsfo_matrix<T>::operator[](int j) {
	assert(0 <= j && j < m_cols);

	if (m_cols_vector[j] == NULL) {
		m_cols_vector[j] = new tsfo_vector<T>(m_rows, data() + m_rows*j);
	}

	return *(m_cols_vector[j]);
}

/**
 * Returns the j-th column of matrix A as a tsfo_vector
 */
template<typename T>
inline tsfo_vector<T> tsfo_matrix<T>::operator()(int j) const {
	return this->operator[](j);
}

/**
 * Efficiently deletes the last row of a matrix
 */
template<typename T>
inline void tsfo_matrix<T>::delete_row_last() {
	LENTER;

	tsfo_matrix<T>& A = *this;

	assert(A.rows() > 0 /* && A.cols() > 0 */);

	const double alpha	     = 1.0;
	const lapack_int src_stride  = A.rows();
	const lapack_int dest_stride = A.rows() - 1;
	mkl_dimatcopy('C', 'N', A.rows(), A.cols(), alpha, A.data(), src_stride, dest_stride);

	// update dimension
	m_rows--;

	if (has_valid_qr()) {
		undo_append_row_with_value_qr_upd_only_common();
	}
}

/**
 * Applies the QR update for the rank-1 update corresponding to one column
 * addition (at the end) to the matrix A. Note it conveniently takes the extra
 * column directly from A's (this) memory rather than allocating extra vectors
 * for that.
 */
template<typename T>
inline void tsfo_matrix<T>::compute_qr_upd_addcol() {
	LENTER;

	assert(has_valid_qr());

	// apply old Q_B' to the new vector a
	int l_order     = lapack_order();
	lapack_int m    = m_rows;
	lapack_int nrhs = 1; // m_cols;
	lapack_int n    = min(m, m_QR->m_cols);
	lapack_int lda  = leading_dim();
	lapack_int ldb  = leading_dim();
	lapack_int info = 0;

	// append column very cheaply
	m_QR->m_cols++;
	int j = m_QR->m_cols - 1;

	lapack_int incx    = 1;
	lapack_int incy    = 1;
	const double *from = data() + m*(m_cols - 1);
	double *to         = m_QR->data() + m*j;
	cblas_dcopy(m, from, incx, to, incy);

	// do not allocate temporary vector, instead use m_QR extra
	// column directly, use an alias for the memory location
	double* u = m_QR->data() + m*j;

	// compute u = Q_B' * a
	info = LAPACKE_dormqr(l_order, 'L', 'T', m, nrhs, n, m_QR->data(), lda,
		m_tau->data(), u, ldb);
	if (info != 0) {
      fprintf(stderr, "LAPACKE_dormqr failed with info = %d\n", info);
	  exit(EXIT_FAILURE);
	}

	// if necesary, annihilate the non-zero elements of the new column
	if (m_QR->m_cols <= m) {
		// update tau
		int minmn = min(m, m_QR->m_cols);
		m_tau->resize(minmn);

		// generate a Householder reflector to annihilate
		// the non-zero elements of u below k
		int k          = n;
		double* alpha  = u + k;
		double* x      = u + k + 1;
		const int incx = 1;
		n              = m - k;
		dlarfp(&n, alpha, x, &incx, (m_tau->data() + k));
	}
}

template<typename T>
inline void tsfo_matrix<T>::update_qr_upd_delcols() {
	LENTER;

	const int j     = m_cols - 1;
	const int begin = m_rows*j;

	// cheaply copy over the updated column and tau from the main QR
	const double *from 	  = m_QR->data() + m_rows*m_block_delcols + begin;
	double *to   	   	  = m_QR_delcols_u->data() + begin;
	const lapack_int incx = 1;
	const lapack_int incy = 1;
	cblas_dcopy(m_rows, from, incx, to, incy);

	// apply delcols Q to this new last column of delcols
	apply_qr_upd_delcols(to);

	// update dimensions of QR delcols
	m_QR_delcols_u->m_cols++;
	int minmn = min(m_QR_delcols_u->rows(), m_QR_delcols_u->cols());
	m_tau_delcols_u->resize(minmn);

	// generate a Householder reflector to annihilate
	// the non-zero elements of u below k
	lapack_int m  	= m_rows;
	double *u     	= to;
	lapack_int lenh = min(m_block_delcols + 1, m - j + 1);
	int k           = j;
	double* alpha   = u + k;
	double* x       = u + k + 1;
	dlarfp(&lenh, alpha, x, &incx, (m_tau_delcols_u->data() + k));
}

/**
 * Append a new column to the given matrix
 */
template<typename T>
inline void tsfo_matrix<T>::append_column(const tsfo_vector<T>& a) {
	LENTER;

	assert(m_rows == a.size());

	m_cols++;
	s_bufferpool.ensure_size(m_rows*m_cols);
	const int j     = m_cols - 1;
	const int begin = m_rows*j;

	// efficiently append column
	const lapack_int incx = 1;
	const lapack_int incy = 1;
	cblas_dcopy(m_rows, a.data(), incx, data() + begin, incy);

	// update cols vector
	m_cols_vector.push_back(NULL);

	if (has_valid_qr()) {
		compute_qr_upd_addcol();

		// update the delcols update
		if (has_valid_qr_upd_delcols()) {
			update_qr_upd_delcols();
		}
	}
}

template<typename T>
inline void tsfo_matrix<T>::undo_append_row_with_value_qr_upd_only() {
	LENTER;

	// update dimension
	m_rows--;

	undo_append_row_with_value_qr_upd_only_common();
}

template<typename T>
inline void tsfo_matrix<T>::undo_append_row_with_value_qr_upd_only_common() {
#ifdef _LOGGING
	printf("tsfo_matrix::undo_append_row_with_value_qr_upd_only_common()\n");
#endif

	// delete QR addrow update
	delete_qr_upd_addrow();
}

/**
 * Appends one row with the given value but only logically, it does not change the
 * original matrix neither the QR delcols and addcol updates. Note that no other QR
 * update should happen after the addrow one.
 */
template<typename T>
inline void tsfo_matrix<T>::append_row_with_value_qr_upd_only(T value) {
	LENTER;

	// update dimensions
	m_rows++;

	append_row_with_value_qr_upd_only_common(value);
}

/**
 * Appends one row with the given value but only logically, it does not change the
 * original matrix neither the QR delcols and addcol updates. Note that no other QR
 * update should happen after the addrow one.
 */
template<typename T>
inline void tsfo_matrix<T>::append_row_with_value_qr_upd_only_common(T value) {
	LENTER;

	assert(rows() > 0 && cols() > 0);
	assert(has_valid_qr());

	assert(!has_valid_qr_upd_addrow());

	lapack_int m    = 1;
	lapack_int n    = m_rows;
	lapack_int lda  = n;
	lapack_int ldb  = m;
	lapack_int info = 0;

	// define the target QR
	tsfo_matrix<T>* QR = has_valid_qr_upd_delcols() ? m_QR_delcols_u : m_QR;

	// initialize QR update matrix
	m_QR_addrow_u  = new tsfo_matrix<T>(QR->rows() + 1, QR->cols());

	// save/copy the latest R, this is where the actual addrow is going to be computed
	const double alpha 			 = 1.0;
	const lapack_int src_stride  = QR->rows();
	const lapack_int dest_stride = m_QR_addrow_u->rows();
	mkl_domatcopy('C', 'N', QR->rows(), QR->cols(), alpha, QR->data(), src_stride,
		m_QR_addrow_u->data(), dest_stride);
	int i = m_QR_addrow_u->rows() - 1;
	for (int j = 0; j < m_QR_addrow_u->cols(); ++j) {
		m_QR_addrow_u->elem(i, j) = 0.0;
	}

	m_refl_addrow_u = new tsfo_vector<T>(n, value);
	m_tau_addrow_u 	= new tsfo_vector<T>(n, 0.0);

	int lwork       = 2*BLOCK_SIZE*n;
	tsfo_vector<double>::s_bufferpool.ensure_size(lwork);
	static tsfo_vector<double> work;

	// update the upper triangular part R of the addrow QR update
	F_addrows(&m, &n, m_QR_addrow_u->data(), &lda, m_refl_addrow_u->data(), &ldb,
		m_tau_addrow_u->data(), work.data(), &lwork, &info);
	if (info != 0) {
		fprintf(stderr, "F_addrows failed with info = %d\n", info);
		exit(EXIT_FAILURE);
	}
}

/**
 * Appends one row with the given value, no other QR update should happen after
 * this one.
 */
template<typename T>
inline void tsfo_matrix<T>::append_row_with_value(T value) {
	LENTER;

	tsfo_matrix<T>& A = *this;

	assert(A.rows() > 0 && A.cols() > 0);

	s_bufferpool.ensure_size((m_rows + 1)*m_cols);
	const double alpha 			 = 1.0;
	const lapack_int src_stride  = A.rows();
	const lapack_int dest_stride = A.rows() + 1;
	mkl_dimatcopy('C', 'N', A.rows(), A.cols(), alpha, A.data(), src_stride, dest_stride);

	// update dimension
	m_rows++;

	// populate the extra row
	int i;
	i = m_rows - 1;
	#pragma simd
	for (int j = 0; j < m_cols; ++j) {
		elem(i, j) = value;
	}

	if (has_valid_qr()) {
		append_row_with_value_qr_upd_only_common(value);
	}
}

template<typename T>
void tsfo_matrix<T>::apply_qr_upd_delcols(T* a) {
	LENTER;

	assert(has_valid_qr_upd_delcols());
	lapack_int m     = (has_valid_qr_upd_addrow()) ? m_rows - 1 : m_rows;
	lapack_int n     = m_cols;
	lapack_int n2    = 1;
	lapack_int lwork = (m - m_begin_delcols)*sizeof(T);
	tsfo_vector<double>::s_bufferpool.ensure_size(lwork);
	static tsfo_vector<double> work;
	int minmn = min(m_QR_delcols_u->rows(), m_QR_delcols_u->cols());
	for (int j = m_begin_delcols; j < minmn; ++j) {
		// how many elements my Householder has
		int n1        	 = min(m_block_delcols + 1, m - j);
		// pointer to the Householder reflector
		const double *v  = m_QR_delcols_u->data() + m*j + j;
		double tau    	 = *(m_tau_delcols_u->data() + j);
		double diag_safe = m_QR_delcols_u->elem(j, j);
		m_QR_delcols_u->elem(j, j) = 1.0;
		dlarfx("L", &n1, &n2, v, &tau, a + j, &n1, work.data());
		m_QR_delcols_u->elem(j, j) = diag_safe;
	}
}

/**
 * Removes the column block specified by begin and end, very efficiently
 * moves data under this column-major representation.
 */
template<typename T>
inline void tsfo_matrix<T>::delete_column(int begin, int end) {
	LENTER;

	assert(begin >= 0 && end >= 0);
	assert(begin <= end);
	assert(end < m_cols);

	int block = end - begin + 1;
	assert(m_cols >= block);

	// optimality
	if (end < (m_cols - 1)) {
		T* from = data() + (m_rows) * begin;
		memmove(from, from + (m_rows) * block, (m_cols - end) * (m_rows) * sizeof(T));
	}
	m_cols -= block;

	// update cols vector
	tcols_vector_iter iter_begin = m_cols_vector.begin() + begin;
	tcols_vector_iter iter_end   = min(m_cols_vector.begin() + end + 1, m_cols_vector.end());
	for (tcols_vector_iter iter  = iter_begin; iter != m_cols_vector.end(); iter++) {
		delete *iter;
		*iter = NULL;
	}
	m_cols_vector.erase(iter_begin, iter_end);
	assert(m_cols_vector.size() == m_cols);

	if (has_valid_qr()) {
		assert(!has_valid_qr_upd_addrow());

		// use delcols update only if the number of flops is lower
		// than the flops corresponding to doing the full QR computation
//		int m = rows();
//		int p = m_block_delcols + block;
//		int n = cols() + p;
//		int k = min(begin, m_begin_delcols);
//		int flops_delcols = 4*(n*p*(n/2 - p - k) + p*p*(p/2 + k)) + p*k*k;
//		int flops_fullqr  = 2*n*n*(m - n/3);

//		if (m_cols == 0 || flops_delcols > flops_fullqr) {

		// The previously computed QR is deleted iff:
		// 1) All columns are deleted from the main matrix
		// 2) Deleting the first column
		// 3) TODO: fine-tune the criteria to delete the existing QR factorization
		//    find a threshold where is optimal to update vs. recompute

		double ratio_begin = (double) begin / (double) m_cols;
		double ratio_block = (double) (m_block_delcols + block) / (double) (m_cols + block);

		if (m_cols == 0 || ratio_begin < RECOMPUTE_QR_BEGIN_THRESHOLD || ratio_block > RECOMPUTE_QR_BLOCK_THRESHOLD) {
			delete_qr();

		} else
		if (end < (m_QR->cols() - 1)) {
			if (has_valid_qr_upd_delcols()) {
				// delete existing QR delcols update
				delete m_QR_delcols_u;  m_QR_delcols_u  = NULL;
				delete m_tau_delcols_u; m_tau_delcols_u = NULL;
			}

			// update begin and block
			if (begin < m_begin_delcols) {
				m_begin_delcols = begin;
			}
			m_block_delcols += block;

			// initialize QR delcols update
			m_QR_delcols_u  = new tsfo_matrix<double>();
			this->copy_to(*m_QR_delcols_u);
			m_tau_delcols_u = new tsfo_vector<double>(*m_tau);

			// compute Q_B' * C and put the result in C
			int l_order      = LAPACK_COL_MAJOR;
			lapack_int m     = m_QR_delcols_u->rows();
			lapack_int n     = m_QR_delcols_u->cols();
			lapack_int minmn = min(m_QR->rows(), m_QR->cols());
			lapack_int lda   = m;
			lapack_int ldc   = m;
			lapack_int info  = 0;

			// Compute A = Q_B' * C
			info = LAPACKE_dormqr(l_order, 'L', 'T', m, n, minmn, m_QR->data(), lda,
				m_tau->data(), m_QR_delcols_u->data(), ldc);
			if (info != 0) {
	          fprintf(stderr, "LAPACKE_dormqr failed with info = %d\n", info);
			  exit(EXIT_FAILURE);
			}

			// TODO: shall the main QR be left untouched? yes it must stay as before,
			// because of two reasons:
			// 1) deletions can be "undone" e.g. whenever another deletion comes along
			//    the current delcols update needs to be recomputed and the original R
			//    present in the main QR should be preserved.
			// 2) On the solve or apply step, we need to apply both Q transformations,
			//    the main Q plus the delcols Q.

			// fortran is 1-based so is not begin but (begin + 1)
			lapack_int k  = m_begin_delcols + 1;
			lapack_int p  = m_block_delcols;
			info          = 0;

			int lwork     = BLOCK_SIZE*(p + 1);
			tsfo_vector<double>::s_bufferpool.ensure_size(lwork);
			static tsfo_vector<double> work;
			F_delcols(&m, &n, m_QR_delcols_u->data(), &lda, &k, &p,
				m_tau_delcols_u->data() + m_begin_delcols, work.data(), &info);
			if (info != 0) {
		      fprintf(stderr, "F_delcols failed with info = %d\n", info);
			  exit(EXIT_FAILURE);
			}

		} else {
			// zero flop case
			// update main QR
			m_QR->delete_column(begin, end);
			int minmn = min(m_QR->rows(), m_QR->cols());
			m_tau->resize(minmn);

			// update delcols QR
			// TODO: test this scenario
			if (has_valid_qr_upd_delcols()) {
				m_QR_delcols_u->delete_column(begin, end);
				int minmn = min(m_QR_delcols_u->rows(), m_QR_delcols_u->cols());
				m_tau_delcols_u->resize(minmn);
			}
		}
	}
}

/**
 * Removes the column block specified by begin and end, very efficiently
 * moves data in column-major representation.
 */
template<typename T>
inline void tsfo_matrix<T>::delete_column_with_gap(int begin, int end, bool triangular) {
	LENTER;

	assert(begin >= 0 && end >= 0);
	assert(begin <= end);
	assert(end < m_cols);

	double ratio = (double) begin / (double) m_cols;
	assert(ratio <= 1.0);

	int block = end - begin + 1;
	assert(m_cols >= block);

	if (ratio > 0.5) {
		if (end < (m_cols - 1)) {
			T* from = data() + m_rows * begin;
			memmove(from, from + m_rows * block, (m_cols - end) * m_rows * sizeof(T));
		}

		// update cols vector
		tcols_vector_iter iter_begin = m_cols_vector.begin() + begin;
		tcols_vector_iter iter_end   = min(m_cols_vector.begin() + end + 1, m_cols_vector.end());
		for (tcols_vector_iter iter  = iter_begin; iter != m_cols_vector.end(); iter++) {
			delete *iter;
			*iter = NULL;
		}
		m_cols_vector.erase(iter_begin, iter_end);

	} else {
		int gap = block * m_rows;
		if (begin > 0) {
			s_bufferpool.ensure_size(gap + m_left_gap + (m_cols - block)*m_rows);
			if (triangular) {
				for (int j = begin; j >= block; --j) {
					int jj = j - block;
					#pragma simd
					for (int i = 0; i < j; ++i) {
						elem(i, j) = elem(i, jj);
					}
				}
			} else {
				memmove(data() + gap, data(), begin * m_rows * sizeof(T));
			}
		}
		m_left_gap += gap;

		// tidy back
		if (m_left_gap >= MAX_M_N*m_rows) {
			memcpy(data() - m_left_gap, data(), (m_cols - block)*m_rows * sizeof(T));
			m_left_gap = 0;
		}

		// update cols vector
		tcols_vector_iter iter_begin = m_cols_vector.begin() + begin;
		tcols_vector_iter iter_end   = min(m_cols_vector.begin() + end + 1, m_cols_vector.end());
		for (tcols_vector_iter iter  = m_cols_vector.begin(); iter != iter_end; iter++) {
			delete *iter;
			*iter = NULL;
		}
		m_cols_vector.erase(iter_begin, iter_end);
	}
	m_cols -= block;
	assert(m_cols_vector.size() == m_cols);
}

/**
 * Populates the target matrix B copying the values from this matrix as
 * source A with the given amount of rows and columns. Note that it doesn't
 * necessarily populate the whole content from Source to Target.
 */
template<typename T>
inline void tsfo_matrix<T>::copy_to(tsfo_matrix<T>& B, int rows, int cols) const {
	LENTER;

	const tsfo_matrix<T>& A = *this;

	assert(A.data() != NULL);
	assert(B.data()  != NULL);
	assert(A.rows() >= rows && A.cols() >= cols);
	assert(B.rows() >= rows && B.cols() >= cols);
	assert(A.rows() > 0 && B.cols() > 0);

	// copy matrices
	const double alpha 	 = 1.0;
	const lapack_int lda = leading_dim();
	const lapack_int ldb = leading_dim();
	mkl_domatcopy(char_order(), 'N', A.rows(), A.cols(), alpha, A.data(), lda, B.data(), ldb);
}

/**
 * Copies the entire matrix from this to B.
 */
template<typename T>
inline void tsfo_matrix<T>::copy_to(tsfo_matrix<T>& B) const {
	LENTER;

	// update the dimensions
	B.rows(m_rows);
	B.cols(m_cols);

	// re-initialize cols vector, overhead
	B.clear_cols_vector();
	B.m_cols_vector.insert(B.m_cols_vector.begin(), m_cols, NULL);

	copy_to(B, m_rows, m_cols);

	if (B.has_valid_qr()) {
		B.delete_qr();
	}
}

template<typename T>
inline void tsfo_matrix<T>::clear_cols_vector() {
	for (tcols_vector_iter iter = m_cols_vector.begin(); iter != m_cols_vector.end(); ++iter) {
		delete *iter;
		*iter = NULL;
	}

	m_cols_vector.clear();
}

/**
 * Resets the cols vector to the new size of the matrix, deleting all
 * previous pointers which became stale after the last operation e.g.
 * when multiple delete_column_with_gap are executed then this method
 * should be invoked.
 */
template<typename T>
inline void tsfo_matrix<T>::reset_cols_vector() {
	for (tcols_vector_iter iter = m_cols_vector.begin(); iter != m_cols_vector.end(); ++iter) {
		delete *iter;
		*iter = NULL;
	}

	tcols_vector_iter begin, end;
	size_t size = m_cols_vector.size();
	if (size > m_cols) {
		begin = m_cols_vector.end() - (size - m_cols);
		end   = m_cols_vector.end();
		m_cols_vector.erase(begin, end);

	} else
	if (size < m_cols) {
		begin = m_cols_vector.end();
		m_cols_vector.insert(begin, m_cols - size, NULL);
	}

	assert(m_cols_vector.size() == m_cols);
}

/**
 * Takes the input matrix and transposes it into this
 */
template<typename T>
inline tsfo_matrix<T>& tsfo_matrix<T>::transpose(const tsfo_matrix<T>& S) {
	LENTER;

	assert(S.rows() > 0 && S.cols() > 0);

	// BLAS-3 transposes the matrix S
	const double alpha 	 = 1.0;
	const lapack_int lda = S.leading_dim();
	const lapack_int ldb = (S.leading_dim() == S.rows()) ? S.cols() : S.rows();
	mkl_domatcopy(char_order(), 'T', S.rows(), S.cols(), alpha, S.data(), lda, data(), ldb);

	// update this matrix dimensions
	m_rows = S.cols();
	m_cols = S.rows();

	return *this;
}

/**
 * Computes and returns the subspace translated of the given matrix.
 */
template<typename T>
inline tsfo_matrix<T>& tsfo_matrix<T>::subspace_translated_delta(const tsfo_matrix<T>& S) {
	LENTER;

	assert(m_rows == S.rows());

	double* elem_j = data() + m_rows*m_cols;

	// add column very cheaply, at this point it is safe to assume that
	// we have enough space in the bufferpool
	m_cols++;

	// copy S.elem(i, S.cols() - 1) -> elem(i, j)
	lapack_int incx = 1;
	lapack_int incy = 1;
	cblas_dcopy(m_rows, S.data() + m_rows*(S.cols() - 1), incx, elem_j, incy);

	// apply one daxpy to compute elem(i, j) -= S.elem(i, 0)
	cblas_daxpy(m_rows, -1.0, S.data(), incx, elem_j, incy);

	// update cols vector
	m_cols_vector.push_back(NULL);

	if (has_valid_qr()) {
		compute_qr_upd_addcol();

		// update the delcols update
		if (has_valid_qr_upd_delcols()) {
			update_qr_upd_delcols();
		}
	}

	return *this;
}

/**
 * Computes and returns the subspace translated of the given matrix.
 */
template<typename T>
inline tsfo_matrix<T>& tsfo_matrix<T>::subspace_translated(const tsfo_matrix<T>& S) {
	LENTER;

	if (S.cols() == 0) {
		m_rows = S.rows();
		m_cols = 0;

	} else {
		m_rows = S.rows();
		m_cols = S.cols() - 1;

		// copy S(:,2:n) over to this
		double alpha 			 	 = 1.0;
		const lapack_int src_stride  = m_rows;
		const lapack_int dest_stride = m_rows;
		double *a       			 = data();
		mkl_domatcopy('C', 'N', m_rows, m_cols, alpha, S.data() + m_rows,
			src_stride, a, dest_stride);

		// ONES needs one-time initialization
		static tsfo_vector<T> ONES(tsfo_vector<T>::VECTOR_BUFFER_SIZE - 1, 1.0);

		CBLAS_ORDER b_order = blas_order();
		lapack_int m 		= m_rows;
		lapack_int n 		= m_cols;
		alpha 				= -1.0;
		const double *x 	= S.data();
		lapack_int incx 	= 1;
		const double *y     = ONES.data();
		lapack_int incy 	= 1;
		lapack_int lda  	= m;
		cblas_dger(b_order, m, n, alpha, x, incx, y, incy, a, lda);
	}

	// initialize cols vector
	assert(m_cols_vector.empty());
	m_cols_vector.insert(m_cols_vector.begin(), m_cols, NULL);

	return *this;
}

/**
 * Computes and stores in-place the Cholesky decomposition of the given matrix.
 * If the current matrix should be preserved then a copy must be made before-hand.
 */
template<typename T>
inline tsfo_matrix<T>& tsfo_matrix<T>::cholesky() {
	LENTER;

	// cholesky only applies to square symmetric SDP matrices
	assert(m_rows == m_cols);

	// TODO: check that the matrix is symmetric and SDP

	lapack_int n = m_cols;
	if (n > 0) {
		lapack_int lda = leading_dim();
		int info = LAPACKE_dpotrf(lapack_order(), 'U', n, data(), lda);
		if (info != 0) {
			fprintf(stderr, "LAPACKE_dpotrf failed with info = %d\n", info);
			exit(EXIT_FAILURE);
		}
	}

	return *this;
}

/**
 * Computes and stores the QR decomposition of the given matrix.
 */
template<typename T>
inline void tsfo_matrix<T>::qr_factorization() {
	LENTER;

	assert(!has_valid_qr());
	assert(!has_valid_qr_upd_addrow());

	// only if valid
	if (m_rows > 0 && m_cols > 0) {
		const int m 	  = m_rows;
		const int n 	  = m_cols;
		const int minmn   = min(m, n);
		const int l_order = lapack_order();
		const int lda 	  = leading_dim();
		int info   		  = 0;

		// initialize QR with dimensions
		m_QR = new tsfo_matrix(*this);

		// initialize tau
		m_tau = new tsfo_vector<T>(minmn);

		// compute the QR decomposition
		// TODO: after moving to LAPACK 3.4.0 use faster version e.g.
		// dgeqrt  (faster blocked implementation that uses the WY representation) or
		// dgeqrt3 (recursive algorithm of Elmroth and Gustavson also uses WY representation)
//		lapack_int jpvt[100] = {0};
//		info = LAPACKE_dgeqpf(l_order, m, n, m_QR->data(), lda, jpvt, m_tau->data());
//		info = LAPACKE_dgeqp3(l_order, m, n, m_QR->data(), lda, jpvt, m_tau->data());
		info = LAPACKE_dgeqrf(l_order, m, n, m_QR->data(), lda, m_tau->data());
		if (info != 0) {
			fprintf(stderr, "LAPACKE_dgeqrf failed with info = %d\n", info);
			exit(EXIT_FAILURE);
		}
	}
}

/**
 * Returns true of matrix A is approximately the same as B, false otherwise.
 */
template<typename T>
inline bool tsfo_matrix<T>::operator==(const tsfo_matrix<T>& A) const {
	const tsfo_matrix<T>& B = *this;

	if ((A.rows() != B.rows()) || (A.cols() != B.cols())) {
		return false;
	}

	for (int i = 0; i < A.rows(); i++) {
		for (int j = 0; j < A.cols(); j++) {
			T diff = A.elem(i, j) - B.elem(i, j);
			if (fabs(diff) > EPSILON) {
				return false;
			}
		}
	}

	return true;
}

/**
 * Computes and returns the multiplication of the given matrix and vector
 */
template<typename T>
inline tsfo_vector<T> tsfo_matrix<T>::operator*(const tsfo_vector<T>& x) const {
	LENTER;

	assert(x.size() == m_cols || x.size() == m_rows);
	CBLAS_TRANSPOSE b_trans = (x.size() == m_cols) ? CblasNoTrans : CblasTrans;

	tsfo_vector<T> y((b_trans == CblasNoTrans) ? m_rows : m_cols, 0.0);
	if (m_rows == 0 || m_cols == 0) {
		return y;
	}

	// y := alpha*A*x + beta*y,
	const CBLAS_ORDER b_order = blas_order();
	const lapack_int m 		  = m_rows;
	const lapack_int n 	      = m_cols;
	const double alpha 		  = 1.0;
	const double beta 		  = 1.0;
	const lapack_int incx 	  = 1;
	const lapack_int lda 	  = leading_dim();
	const lapack_int incy 	  = 1;
	cblas_dgemv(b_order, b_trans, m, n, alpha, data(), lda, x.data(), incx,
		beta, y.data(), incy);

	return y;
}

/**
 * Computes and returns the upper triangular multiplication of the given
 * matrix and vector
 */
template<typename T>
inline tsfo_vector<T> tsfo_matrix<T>::multiply_triangular(const tsfo_vector<T>& x, const tsfo_matrix<T>::tside side) const {
	LENTER;

	assert(x.size() == m_cols || x.size() == m_rows);
	CBLAS_TRANSPOSE b_trans = (x.size() == m_cols) ? CblasNoTrans : CblasTrans;
	if (m_rows == 0 || m_cols == 0) {
		tsfo_vector<T> y((b_trans == CblasNoTrans) ? m_rows : m_cols, 0.0);
		return y;
	}

	tsfo_vector<T> y = x;
	y.resize((b_trans == CblasNoTrans) ? m_rows : m_cols, 0.0);

	// y := alpha*A*x + beta*y,
	const CBLAS_ORDER b_order = blas_order();
	const CBLAS_UPLO  b_uplo  = (side == UPPER) ? CblasUpper : CblasLower;
	const CBLAS_DIAG  b_diag  = CblasNonUnit;
	const lapack_int  n 	  = std::min(m_rows, m_cols);
	const lapack_int  incx 	  = 1;
	const lapack_int  lda 	  = leading_dim();
	cblas_dtrmv(b_order, b_uplo, b_trans, b_diag, n, data(), lda, y.data(), incx);

	return y;
}

/**
 * Solves the linear system Ax=b. Returns a newly allocated vector solution.
 *
 * ?gels - Uses QR or LQ factorization to solve an overdetermined or
 * underdetermined linear system with a full rank matrix
 */
template<typename T>
inline tsfo_vector<T> tsfo_matrix<T>::solve(const tsfo_vector<T>& b) {
	LENTER;

	assert(m_rows == b.size());

	const lapack_int m = m_rows;
	const lapack_int n = m_cols;

	// ensure vector has size MAX(m, n)
	tsfo_vector<T> x;

	if (m_rows > 0 && m_cols > 0) {
		const double alpha        = 1.0;
		const lapack_int nrhs     = 1;
		const lapack_int l_order  = lapack_order();
		const CBLAS_ORDER b_order = blas_order();
		const lapack_int lda      = leading_dim();
		const lapack_int ldc      = leading_dim();
		lapack_int info           = 0;

		// note that x is initially b and x is then overwritten with the actual solution
		b.copy_to(x);

		// and set the right size now
		x.size(n);

		// handle squared matrices first
		if (m == n && !has_valid_qr()) {
			tsfo_matrix LU(*this);

			// TODO: use a bufferpool for temporary work memory
			lapack_int* ipiv = new lapack_int[n];
			info = LAPACKE_dgesv(l_order, n, nrhs, LU.data(), lda, ipiv, x.data(), ldc);
			delete[] ipiv;
			if (info != 0) {
				printf("LAPACKE_dgesv - failed with info = %d\n", info);
				exit(EXIT_FAILURE);
			}

		} else {

			if (!has_valid_qr()) {
				qr_factorization();
			}

			// validate the QR
			int extra_row = (has_valid_qr_upd_addrow()) ? 1 : 0;
			assert(m_rows ==  m_QR->rows() + extra_row);
			assert(m_cols == (m_QR->cols() - m_block_delcols));

			// TODO: figure out the correct assertion
			// test case with and without QR delcols update
//			assert(m_tau->size                    == minmn
//			   || (m_tau->size - m_block_delcols) == minmn
//			);

			// compute c = Q'*b
			const lapack_int minmn_qr = min(m_QR->rows(), m_QR->cols());
			info = LAPACKE_dormqr(l_order, 'L', 'T', m - extra_row, nrhs, minmn_qr,
					m_QR->data(), lda - extra_row, m_tau->data(), x.data(), ldc - extra_row);
			if (info != 0) {
				fprintf(stderr, "LAPACKE_dormqr failed with info = %d\n", info);
				exit(EXIT_FAILURE);
			}

			// optionally apply QR delcols update H_n*..*H1*c
			if (has_valid_qr_upd_delcols()) {
				apply_qr_upd_delcols(x.data());
			}

			// optionally apply QR addrow update H_n*..*H1*c
			if (has_valid_qr_upd_addrow()) {
				lapack_int m_u 	 = 1; // only one row is added
				lapack_int ldq   = m_QR_addrow_u->rows() - m_u;
				lapack_int k   	 = min(m_QR_addrow_u->rows(), m_QR_addrow_u->cols());
				lapack_int lwork = BLOCK_SIZE*n;
				tsfo_vector<double>::s_bufferpool.ensure_size(lwork);
				static tsfo_vector<double> work;
	            F_addrows_appq("L", "N", &m_u, &nrhs, &k, m_refl_addrow_u->data(), &m_u,
	            	m_tau_addrow_u->data(), x.data(), &ldq, (x.data() + ldq), &m_u, work.data(),
	            		&lwork, &info);
				if (info != 0) {
					fprintf(stderr, "addrows_appq failed with info = %d\n", info);
					exit(EXIT_FAILURE);
				}
			}

			// right solve R*x = c
			const double *R = NULL;
			if (has_valid_qr_upd_addrow()) {
				R = m_QR_addrow_u->data();

			} else
			if (has_valid_qr_upd_delcols()) {
				R = m_QR_delcols_u->data();

			} else {
				R = m_QR->data();
			}

			assert(R != NULL);
			cblas_dtrsm(b_order, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, n, 1, alpha,
				R, lda, x.data(), ldc);
		}
	}

	return x;
}

/**
 * Solves the linear system Ax=b using forward substitution.
 * Returns a newly allocated vector solution.
 */
template<typename T>
inline tsfo_vector<T> tsfo_matrix<T>::solve_forward(const tsfo_vector<T>& b) const {
	LENTER;

	const lapack_int m = m_rows;
	const lapack_int n = m_cols;

	// ensure vector has size MAX(m, n)
	tsfo_vector<T> x;

	if (m_rows > 0 && m_cols > 0) {
		const double alpha        = 1.0;
		const lapack_int nrhs     = 1;
		const CBLAS_ORDER b_order = blas_order();
		const lapack_int lda      = leading_dim();
		const lapack_int ldb      = lda; // is_column_major() ? lda : nrhs;

		// note that x is initially b and x is then overwritten with the actual solution
		b.copy_to(x, n);

		cblas_dtrsm(b_order, CblasLeft, CblasUpper, CblasTrans, CblasNonUnit, n, nrhs, alpha,
			data(), lda, x.data(), ldb);
	}

	return x;
}

/**
 * Solves the linear system Ax=b using backward substitution.
 * Returns a newly allocated vector solution.
 */
template<typename T>
inline tsfo_vector<T> tsfo_matrix<T>::solve_backward(const tsfo_vector<T>& b) const {
	LENTER;

	const lapack_int m = m_rows;
	const lapack_int n = m_cols;

	// ensure vector has size MAX(m, n)
	tsfo_vector<T> x;

	if (m_rows > 0 && m_cols > 0) {
		const double alpha        = 1.0;
		const lapack_int nrhs     = 1;
		const CBLAS_ORDER b_order = blas_order();
		const lapack_int lda      = leading_dim();
		const lapack_int ldb      = lda; // is_column_major() ? lda : nrhs;

		// note that x is initially b and x is then overwritten with the actual solution
		b.copy_to(x, n);

		cblas_dtrsm(b_order, CblasLeft, CblasUpper, CblasNoTrans, CblasNonUnit, n, nrhs, alpha,
			data(), lda, x.data(), ldb);
	}

	return x;
}

template<typename T>
inline tsfo_vector<T>& tsfo_matrix<T>::diag() {
	int minmn = min(m_rows, m_cols);
	m_diag.resize(minmn);
	for (int i = 0; i < m_diag.size(); ++i) {
		m_diag[i] = elem(i ,i);
	}

	return m_diag;
}

/**
 * Creates a submatrix choosing only the column/row elements specified in b
 */
template<typename T>
inline void tsfo_matrix<T>::submatrix(const tsfo_vector<int>& indices, tsfo_matrix<T>& B) const {
	LENTER;

	int n = indices.size();

	// resize B
	B.rows(n);
	B.cols(n);
	B.reset_cols_vector();

	if (n > 0) {
		// submatrix
		#pragma simd
		for (int j = 0; j < n; ++j) {
			#pragma simd
			for (int i = 0; i < n; ++i) {
				int idx_i = indices[i];
				int idx_j = indices[j];
				B(i, j) = elem(idx_i, idx_j);
			}
		}
	}
}

/**
 * Creates a subcolumn choosing only the row elements of the given column
 */
template <typename T>
inline void tsfo_matrix<T>::subcolumn(int col, const tsfo_vector<int>& indices, tsfo_vector<T>& b) const {
	LENTER;

	int m = indices.size();

	// resize b
	b.resize(m);

	if (m > 0) {
		// submatrix
		for (int i = 0; i < m; ++i) {
			b[i] = elem(indices[i], col);
		}
	}
}

/**
 * Performs a matrix-matrix multiplication of (C += *this + B) and returns
 * the result in the output reference C.
 */
template<typename T>
inline void tsfo_matrix<T>::multiply(const tsfo_matrix<T>& B, tsfo_matrix<T>& C) const {
	LENTER;

	const tsfo_matrix<T>& A = *this;
	assert(A.cols() == B.rows());

	const int m = A.rows();
	const int n = B.cols();
	const int k = A.cols();

	C.rows(m);
	C.cols(n);
	if (m == 0 || n == 0 || k == 0)
		return;

	// C = alpha*A*B + beta*C
	const CBLAS_ORDER b_order = blas_order();
	const double alpha 	= 1.0;
	const double beta 	= 0.0;
	const MKL_INT lda 	= A.leading_dim();
	const MKL_INT ldb 	= k;
	const MKL_INT ldc 	= A.leading_dim();

	cblas_dgemm(b_order, CblasNoTrans, CblasNoTrans, m, n, k, alpha, A.data(),
		lda, B.data(), ldb, beta, C.data(), ldc);
}

#endif  // SFO_MATRIX_H_
