/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_function_iwata.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 10.05.2012
// Description : Provides the main API entry point for invoking the Iwata
//               application
//============================================================================

#include "sfo_function_iwata.h"
#include "iwata_sf_context.h"
#include "sfo_kernel_factory.h"

/**
 * Setup and run the Iwata optimization
 */
void sfo_function_iwata(int n, int*& optimal, long& optimal_size, long& major_iter, long& minor_iter) {
	// setup the iwata application
	tiwata_sf_context sf_context(n);
	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context);
	sfo_kernel.run();

	major_iter 	 = sfo_kernel.major_iter();
	minor_iter 	 = sfo_kernel.minor_iter();
	optimal_size = sfo_kernel.subset().size();

	// save the solution
	sfo_kernel.subset().copy_to(optimal);
}
