/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_matrix_tria.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Include declaration for the tsfo_matrix_tria class.
//============================================================================

#ifndef SFO_MATRIX_TRIA_H_
#define SFO_MATRIX_TRIA_H_

#include "sfo_matrix.h"

template<typename T>
class tsfo_matrix_tria : public tsfo_matrix<T> {
private:
	static const long MATRIX_POOL_INITIAL  = 4;
	static const long MATRIX_BUFFER_SIZE;
	static tbufferpool<T> s_bufferpool;

	// classic, uses sqrt
	void triangularize0(int begin, int block);

	template<typename GENROT>
	void triangularize2(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize4_direct (int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize4_blas3  (int begin, int block, GENROT genrot);

	template<typename GENROT>
	void triangularize7_direct (int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize8_direct (int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize8_blas3  (int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize9_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize10_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize11_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize12_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize13_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize14_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize15_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize16_direct(int begin, int block, GENROT genrot);
	template<typename GENROT>
	void triangularize16_blas3 (int begin, int block, GENROT genrot);

public:
	// constructors
	tsfo_matrix_tria(int rows = 0, int cols = 0);
	tsfo_matrix_tria(int rows, int cols, T initial);

	// operators
	virtual tsfo_matrix_tria<T>& operator=(const tsfo_matrix<T>& A);
	virtual tsfo_matrix_tria<T>& operator=(const tsfo_matrix_tria<T>& A);

	// triangularization
	template<typename GENROT>
	void triangularize(int begin, int block, GENROT genrot);
	template<typename GENROT>
    void triangularize_givens(int begin, int block, GENROT genrot);
    template<typename GENROT>
    void triangularize_block_givens(int begin, int block, GENROT genrot);
    void triangularize_householder(int begin, int block);

	virtual ~tsfo_matrix_tria();

	static int TRIANGULARIZE_NB;
};

template<typename T>
const long tsfo_matrix_tria<T>::MATRIX_POOL_INITIAL;

template<typename T>
const long tsfo_matrix_tria<T>::MATRIX_BUFFER_SIZE = 2*tsfo_matrix<T>::MAX_M_N*tsfo_matrix<T>::MAX_M_N;

// static up-front allocation
template<typename T>
tbufferpool<T> tsfo_matrix_tria<T>::s_bufferpool = tbufferpool<T>(MATRIX_POOL_INITIAL,
		MATRIX_BUFFER_SIZE, PAGE_ALIGNED);

template<typename T>
int tsfo_matrix_tria<T>::TRIANGULARIZE_NB = 8;


inline void genrot_low_precision_fast(double *x, double *y, double *c, double *s, double *d) {
	cblas_drotg(x, y, c, s);
}

inline void genrot_high_precision_slow(double *x, double *y, double *c, double *s, double *d) {
	dlartg(x, y, c, s, d);
}

inline void genrot_positive_diagonal(double *x, double *y, double *c, double *s, double *d) {
	dlartgp(x, y, c, s, d);
}

inline void genrot_sqrt(double *x, double *y, double *c, double *s, double *d) {
	double h = sqrt((*x)*(*x) + (*y)*(*y));
	*c = (*x) / h;
	*s = (*y) / h;
}

// constructor
template<typename T>
inline tsfo_matrix_tria<T>::tsfo_matrix_tria(int rows, int cols)
	: tsfo_matrix<T>(rows, cols, tsfo_matrix_tria<T>::s_bufferpool) {
	LENTER;
}

// constructor
template <typename T>
inline tsfo_matrix_tria<T>::tsfo_matrix_tria(int rows, int cols, T initial)
	: tsfo_matrix<T>(rows, cols, initial, tsfo_matrix_tria<T>::s_bufferpool) {
	LENTER;
}

// destructor
template<typename T>
inline tsfo_matrix_tria<T>::~tsfo_matrix_tria() {
	LENTER;

	release(tsfo_matrix_tria<T>::s_bufferpool);
}

/**
 * Assigns A to this object. Performs a full copy of A into this.
 */
template<typename T>
inline tsfo_matrix_tria<T>& tsfo_matrix_tria<T>::operator=(const tsfo_matrix<T>& A) {
	LENTER;

	A.copy_to(*this);

	return *this;
}

/**
 * Assigns A to this object. Performs a full copy of A into this.
 */
template<typename T>
inline tsfo_matrix_tria<T>& tsfo_matrix_tria<T>::operator=(const tsfo_matrix_tria<T>& A) {
	LENTER;

	A.copy_to(*this);

	return *this;
}

/**
 * Triangularizes the given matrix after a deletion update.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize(int begin, int block, GENROT genrot) {

	// use blas3 only if the amount of work will payoff
//	bool blas3 = (this->m_cols - begin) >= 50*TRIANGULARIZE_NB;

	switch (TRIANGULARIZE_NB) {
		case 0:  triangularize0(begin, block);
				 break;
		case 2:  triangularize2<GENROT>(begin, block, genrot);
				 break;
		case 4:
//				 if (blas3) {
//					triangularize4_blas3<GENROT>(begin, block, genrot);
//				 } else {
					triangularize4_direct<GENROT>(begin, block, genrot);
//				 }
				 break;
		case 7:
				 triangularize7_direct<GENROT>(begin, block, genrot);
				 break;
		case 8:
//				 if (blas3) {
//					triangularize8_blas3<GENROT>(begin, block, genrot);
//				 } else {
					triangularize8_direct<GENROT>(begin, block, genrot);
//				 }
				 break;
		case 9:
				 triangularize9_direct<GENROT>(begin, block, genrot);
				 break;
		case 10:
				 triangularize10_direct<GENROT>(begin, block, genrot);
				 break;
		case 11:
				 triangularize11_direct<GENROT>(begin, block, genrot);
				 break;
		case 12:
				 triangularize12_direct<GENROT>(begin, block, genrot);
				 break;
		case 13:
				 triangularize13_direct<GENROT>(begin, block, genrot);
				 break;
		case 14:
				 triangularize14_direct<GENROT>(begin, block, genrot);
				 break;
		case 15:
				 triangularize15_direct<GENROT>(begin, block, genrot);
				 break;
		case 16:
//				 if (blas3) {
//					triangularize16_blas3<GENROT>(begin, block, genrot);
//		 	 	 } else {
		 	 		triangularize16_direct<GENROT>(begin, block, genrot);
//		 	 	 }
				 break;
		default: fprintf(stderr, "TRIANGULARIZE_NB=%d not supported.\n", TRIANGULARIZE_NB);
				 assert(false);
				 break;
	}
}

/**
 * Triangularizes the given matrix after a deletion update.
 */
template<typename T>
inline void tsfo_matrix_tria<T>::triangularize0(int begin, int block) {
	LENTER;

	const int m = this->m_rows;
	const int n = this->m_cols;

	tsfo_matrix<double>& r = *this;

	double x00, x01, x10;
	double xx0, xx1;
	double u00, u01, u10;
	double h0;

	int nb = 0;
	assert(nb == TRIANGULARIZE_NB);

	int i = begin - block + 2;
	int j = begin;

	// unblocked step
	for (; i < (n + block); i++, j++) {
		x00 = r(i - 1, j);
		x10 = r(i,     j);
		h0 = sqrt(x00 * x00 + x10 * x10);

		for (int jj = j; jj < n; jj++) {
			xx0  = r(i - 1, jj);
			xx1  = r(i,     jj);

			u00 = ( x00 * xx0 + x10 * xx1) / h0;
			u10 = (-x10 * xx0 + x00 * xx1) / h0;

			r(i - 1, jj) = u00;
			r(i,     jj) = u10;
		}
	}
}

/**
 * Triangularizes the given matrix after a deletion update.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize2(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x00, x01, x10, x11, x21;
	double xx0, xx1, xx2;
	double u00, u01, u10, u11, u21;
	double uu0, uu1, uu2;
	double c0, c1, s0, s1, d;
	int im1, ip1;
	int jp1;

	int nb = 2;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1 = i - 1;
		ip1 = i + 1;

		jp1 = j + 1;

		// make the stencil as easy as possible
		x00 = r(im1, j); x01 = r(im1, jp1);
		x10 = r(i,   j); x11 = r(i,   jp1);
						 x21 = r(ip1, jp1);

		// make nb steps ahead using registers only
		genrot(&x00, &x10, &c0, &s0, &d);
		u00 = c0*x00 + s0*x10;
		u10 = c0*x10 - s0*x00;

		u01 = c0*x01 + s0*x11;
		u11 = c0*x11 - s0*x01;

		x11 = u11;
	    genrot(&x11, &x21, &c1, &s1, &d);
	    u11 = c1*x11 + s1*x21;
	    u21 = c1*x21 - s1*x11;

		r(im1, j) = u00; r(im1, jp1) = u01;
		r(i,   j) = u10; r(i,   jp1) = u11;
						 r(ip1, jp1) = u21;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, uu0, uu1, uu2)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0 = r(i - 1, jj);
			xx1 = r(i    , jj);
			xx2 = r(i + 1, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			r(i - 1, jj) = uu0;
			r(i    , jj) = uu1;
			r(i + 1, jj) = uu2;
		}
	}
}

/**
 * TRANSPOSITION: Triangularizes the given matrix after a deletion update with NB=4.
 */
//template<typename T> template<typename GENROT>
//void tsfo_matrix_tria<T>::triangularize4_direct(int begin, int block, GENROT genrot) {
//	LENTER;
//
//	tsfo_matrix_tria<double>& r = *this;
//
//	const int m = r.rows();
//	const int n = r.cols();
//
//	double x00, x01, x02, x03, x10, x11, x12, x13, x21, x22, x23,
//			x32, x33, x43;
//	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9;
//	double u00, u01, u02, u03, u10, u11, u12, u13, u21, u22, u23,
//			u32, u33, u43;
//	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9;
//	double c0, c1, c2, c3, s0, s1, s2, s3, d;
//	int im1, ip1, ip2, ip3;
//	int jp1, jp2, jp3;
//
//	int nb = 4;
//	assert(nb == TRIANGULARIZE_NB);
//
//	static tsfo_matrix<double> t1;
//	static const double alpha = 1.0;
//
//	int n_iter = (n - begin) % nb == 0
//			   ? (n - begin) / nb
//			   : (n - begin) / nb + 1;
//	int nb_end = begin + n_iter*nb;
//
//	int i = begin - block + 2;
//	int j = begin;
//
//	// blocked step
//	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
//		im1 = i - 1;
//		ip1 = i + 1;
//		ip2 = i + 2;
//		ip3 = i + 3;
//
//		jp1 = j + 1;
//		jp2 = j + 2;
//		jp3 = j + 3;
//
//		// make the stencil as easy as possible
//		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3);
//		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3);
//						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3);
//						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3);
//						   	   	   	   	          	  	  	   x43 = r(ip3, jp3);
//
//		// make nb steps ahead using registers only
//	    genrot(&x00, &x10, &c0, &s0, &d);
//	    u00 = c0*x00 + s0*x10;
//	    u10 = c0*x10 - s0*x00;
//
//	    u01 = c0*x01 + s0*x11;
//	    u11 = c0*x11 - s0*x01;
//
//	    u02 = c0*x02 + s0*x12;
//	    u12 = c0*x12 - s0*x02;
//
//	    u03 = c0*x03 + s0*x13;
//	    u13 = c0*x13 - s0*x03;
//
//		x11 = u11;
//		x12 = u12;
//		x13 = u13;
//	    genrot(&x11, &x21, &c1, &s1, &d);
//	    u11 = c1*x11 + s1*x21;
//	    u21 = c1*x21 - s1*x11;
//
//	    u12 = c1*x12 + s1*x22;
//	    u22 = c1*x22 - s1*x12;
//
//	    u13 = c1*x13 + s1*x23;
//	    u23 = c1*x23 - s1*x13;
//
//		x22 = u22;
//		x23 = u23;
//	    genrot(&x22, &x32, &c2, &s2, &d);
//	    u22 = c2*x22 + s2*x32;
//	    u32 = c2*x32 - s2*x22;
//
//	    u23 = c2*x23 + s2*x33;
//	    u33 = c2*x33 - s2*x23;
//
//		x33 = u33;
//	    genrot(&x33, &x43, &c3, &s3, &d);
//	    u33 = c3*x33 + s3*x43;
//	    u43 = c3*x43 - s3*x33;
//
//		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03;
//		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13;
//						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23;
//						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33;
//						                          	  	  	   r(ip3, jp3) = u43;
//
//		if (n - j - nb > 0) {
//			// create row band temporary working patch
//			int mm = nb + 1;
//			int nn = n - j - nb;
//			t1.rows(nn);
//			t1.cols(mm);
//			lapack_int lda = r .leading_dim();
//			lapack_int ldb = t1.leading_dim();
//			mkl_domatcopy(r.char_order(), 'T', mm, nn, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
//
//			typename sfo_type<T>::aptr32 data = t1.data();
//			#pragma simd
//			for (int jj = 0; jj < t1.rows(); jj++) {
//				xx0 = data[nn*0 + jj];
//				xx1 = data[nn*1 + jj];
//				xx2 = data[nn*2 + jj];
//				xx3 = data[nn*3 + jj];
//				xx4 = data[nn*4 + jj];
//
//				uu0 = c0*xx0 + s0*xx1;
//				uu1 = c0*xx1 - s0*xx0;
//
//				xx1 = uu1;
//				uu1 = c1*xx1 + s1*xx2;
//				uu2 = c1*xx2 - s1*xx1;
//
//				xx2 = uu2;
//				uu2 = c2*xx2 + s2*xx3;
//				uu3 = c2*xx3 - s2*xx2;
//
//				xx3 = uu3;
//				uu3 = c3*xx3 + s3*xx4;
//				uu4 = c3*xx4 - s3*xx3;
//
//				data[nn*0 + jj] = uu0;
//				data[nn*1 + jj] = uu1;
//				data[nn*2 + jj] = uu2;
//				data[nn*3 + jj] = uu3;
//				data[nn*4 + jj] = uu4;
//			}
//
//			// copy patch back
//			lda = t1.leading_dim();
//			ldb = r .leading_dim();
//			mkl_domatcopy(r.char_order(), 'T', nn, mm, alpha, t1.data(), lda, &r(i - 1, j + nb), ldb);
//		}
//	}
//}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=4.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize4_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x00, x01, x02, x03, x10, x11, x12, x13, x21, x22, x23, x32, x33, x43;
	double xx0, xx1, xx2, xx3, xx4;
	double u00, u01, u02, u03, u10, u11, u12, u13, u21, u22, u23, u32, u33, u43;
	double uu0, uu1, uu2, uu3, uu4;
	double c0, c1, c2, c3, s0, s1, s2, s3, d;
	int im1, ip1, ip2, ip3;
	int jp1, jp2, jp3;

	int nb = 4;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1 = i - 1;
		ip1 = i + 1;
		ip2 = i + 2;
		ip3 = i + 3;

		jp1 = j + 1;
		jp2 = j + 2;
		jp3 = j + 3;

		// make the stencil as easy as possible
		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3);
		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3);
						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3);
						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3);
						   	   	   	   	          	  	  	   x43 = r(ip3, jp3);

		// make nb steps ahead using registers only
	    genrot(&x00, &x10, &c0, &s0, &d);
	    u00 = c0*x00 + s0*x10;
	    u10 = c0*x10 - s0*x00;

	    u01 = c0*x01 + s0*x11;
	    u11 = c0*x11 - s0*x01;

	    u02 = c0*x02 + s0*x12;
	    u12 = c0*x12 - s0*x02;

	    u03 = c0*x03 + s0*x13;
	    u13 = c0*x13 - s0*x03;

		x11 = u11;
		x12 = u12;
		x13 = u13;
	    genrot(&x11, &x21, &c1, &s1, &d);
	    u11 = c1*x11 + s1*x21;
	    u21 = c1*x21 - s1*x11;

	    u12 = c1*x12 + s1*x22;
	    u22 = c1*x22 - s1*x12;

	    u13 = c1*x13 + s1*x23;
	    u23 = c1*x23 - s1*x13;

		x22 = u22;
		x23 = u23;
	    genrot(&x22, &x32, &c2, &s2, &d);
	    u22 = c2*x22 + s2*x32;
	    u32 = c2*x32 - s2*x22;

	    u23 = c2*x23 + s2*x33;
	    u33 = c2*x33 - s2*x23;

		x33 = u33;
	    genrot(&x33, &x43, &c3, &s3, &d);
	    u33 = c3*x33 + s3*x43;
	    u43 = c3*x43 - s3*x33;

		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03;
		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13;
						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23;
						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33;
						                          	  	  	   r(ip3, jp3) = u43;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, uu0, uu1, uu2, uu3, uu4)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0 = r(i - 1, jj);
			xx1 = r(i    , jj);
			xx2 = r(i + 1, jj);
			xx3 = r(i + 2, jj);
			xx4 = r(i + 3, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			r(i - 1, jj) = uu0;
			r(i    , jj) = uu1;
			r(i + 1, jj) = uu2;
			r(i + 2, jj) = uu3;
			r(i + 3, jj) = uu4;
		}
	}
}

/**
 * BLAS3 DGEMM: Triangularizes the given matrix after a deletion update with NB=4.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize4_blas3(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x00, x01, x02, x03, x10, x11, x12, x13, x21, x22, x23, x32, x33, x43;
	double xx0, xx1, xx2, xx3, xx4;
	double u00, u01, u02, u03, u10, u11, u12, u13, u21, u22, u23, u32, u33, u43;
	double uu0, uu1, uu2, uu3, uu4;
	double c0, c1, c2, c3, s0, s1, s2, s3, d;
	int im1, ip1, ip2, ip3;
	int jp1, jp2, jp3;

	int nb = 4;
	assert(nb == TRIANGULARIZE_NB);

	static tsfo_matrix<double> gc(nb + 1, nb + 1, 0.0);
	static tsfo_matrix<double> t1(nb + 1, 0);
	static const double alpha = 1.0;
	static const double beta  = 0.0;

	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1 = i - 1;
		ip1 = i + 1;
		ip2 = i + 2;
		ip3 = i + 3;

		jp1 = j + 1;
		jp2 = j + 2;
		jp3 = j + 3;

		// make the stencil as easy as possible
		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3);
		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3);
						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3);
						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3);
						   	   	   	   	          	  	  	   x43 = r(ip3, jp3);

		// make nb steps ahead using registers only
	    genrot(&x00, &x10, &c0, &s0, &d);
	    u00 = c0*x00 + s0*x10;
	    u10 = c0*x10 - s0*x00;

	    u01 = c0*x01 + s0*x11;
	    u11 = c0*x11 - s0*x01;

	    u02 = c0*x02 + s0*x12;
	    u12 = c0*x12 - s0*x02;

	    u03 = c0*x03 + s0*x13;
	    u13 = c0*x13 - s0*x03;

		x11 = u11;
		x12 = u12;
		x13 = u13;
	    genrot(&x11, &x21, &c1, &s1, &d);
	    u11 = c1*x11 + s1*x21;
	    u21 = c1*x21 - s1*x11;

	    u12 = c1*x12 + s1*x22;
	    u22 = c1*x22 - s1*x12;

	    u13 = c1*x13 + s1*x23;
	    u23 = c1*x23 - s1*x13;

		x22 = u22;
		x23 = u23;
	    genrot(&x22, &x32, &c2, &s2, &d);
	    u22 = c2*x22 + s2*x32;
	    u32 = c2*x32 - s2*x22;

	    u23 = c2*x23 + s2*x33;
	    u33 = c2*x33 - s2*x23;

		x33 = u33;
	    genrot(&x33, &x43, &c3, &s3, &d);
	    u33 = c3*x33 + s3*x43;
	    u43 = c3*x43 - s3*x33;

		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03;
		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13;
						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23;
						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33;
						                          	  	  	   r(ip3, jp3) = u43;

		if (n - j - nb > 0) {
			 // build the accumulated Givens orthogonal matrix
			 gc(0, 0)=c0;			gc(0, 1)=s0;
			 gc(1, 0)=-c1*s0;		gc(1, 1)=c0*c1;			gc(1, 2)=s1;
			 gc(2, 0)=c2*s0*s1;		gc(2, 1)=-c0*c2*s1;		gc(2, 2)=c1*c2;		gc(2, 3)=s2;
			 gc(3, 0)=-c3*s0*s1*s2;	gc(3, 1)=c0*c3*s1*s2;	gc(3, 2)=-c1*c3*s2;	gc(3, 3)=c2*c3;	 gc(3, 4)=s3;
			 gc(4, 0)=s0*s1*s2*s3;	gc(4, 1)=-c0*s1*s2*s3;	gc(4, 2)=c1*s2*s3;	gc(4, 3)=-c2*s3; gc(4, 4)=c3;

			// create row band temporary working patch
			{
				const int mm = nb + 1;
				const int nn = n - j - nb;
				t1.cols(nn);
				const lapack_int lda = r.leading_dim();
				const lapack_int ldb = t1.leading_dim();
				mkl_domatcopy(r.char_order(), 'N', mm, nn, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
			}

			// apply the accumulated Givens gc to the matrix row block
			// (excluding the diagonal block) at once using GEMM
			{
				const int mm = gc.rows();
				const int nn = t1.cols();
				assert(nn > 0);
				const int kk = gc.cols();
				const CBLAS_ORDER b_order = r.blas_order();
				const MKL_INT lda 	= gc.leading_dim();
				const MKL_INT ldb 	= t1.leading_dim();
				const MKL_INT ldc 	= r.leading_dim();

				cblas_dgemm(b_order, CblasNoTrans, CblasNoTrans, mm, nn, kk, alpha, gc.data(),
					lda, t1.data(), ldb, beta, &r(i - 1, j + nb), ldc);
			}
		 }
	}
}

/**
 * TRANSPOSITION: Triangularizes the given matrix after a deletion update with NB=8.
 */
//template<typename T>
//inline void tsfo_matrix_tria<T>::triangularize8(int begin, int block) {
//	LENTER;
//
//	tsfo_matrix_tria<double>& r = *this;
//
//	const int m = r.rows();
//	const int n = r.cols();
//
//	double x00, x01, x02, x03, x04, x05, x06, x07, x10, x11, x12, x13, x14, x15, x16, x17, x21, x22, x23, x24, x25, x26, x27,
//			x32, x33, x34, x35, x36, x37, x43, x44, x45, x46, x47, x54, x55, x56, x57, x65, x66, x67, x76, x77, x87;
//	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8;
//	double u00, u01, u02, u03, u04, u05, u06, u07, u10, u11, u12, u13, u14, u15, u16, u17, u21, u22, u23, u24, u25, u26, u27,
//			u32, u33, u34, u35, u36, u37, u43, u44, u45, u46, u47, u54, u55, u56, u57, u65, u66, u67, u76, u77, u87;
//	double c0, c1, c2, c3, c4, c5, c6, c7, s0, s1, s2, s3, s4, s5, s6, s7, d;
//	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8;
//	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7;
//	int jp1, jp2, jp3, jp4, jp5, jp6, jp7;
//
//	int nb = 8;
//	assert(nb == TRIANGULARIZE_NB);
//
//	static tsfo_matrix<double> t1;
//	static const double alpha = 1.0;
//
//	int n_iter = (n - begin) % nb == 0
//			   ? (n - begin) / nb
//			   : (n - begin) / nb + 1;
//	int nb_end = begin + n_iter*nb;
//
//	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));
//
//	int i = begin - block + 2;
//	int j = begin;
//
//	// blocked step
//	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
//		im1 = i - 1;
//		ip1 = i + 1;
//		ip2 = i + 2;
//		ip3 = i + 3;
//		ip4 = i + 4;
//		ip5 = i + 5;
//		ip6 = i + 6;
//		ip7 = i + 7;
//
//		jp1 = j + 1;
//		jp2 = j + 2;
//		jp3 = j + 3;
//		jp4 = j + 4;
//		jp5 = j + 5;
//		jp6 = j + 6;
//		jp7 = j + 7;
//
//		// make the stencil as easy as possible
//		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3); x04 = r(im1, jp4); x05 = r(im1, jp5); x06 = r(im1, jp6); x07 = r(im1, jp7);
//		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3); x14 = r(i  , jp4); x15 = r(i  , jp5); x16 = r(i  , jp6); x17 = r(i  , jp7);
//						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3); x24 = r(ip1, jp4); x25 = r(ip1, jp5); x26 = r(ip1, jp6); x27 = r(ip1, jp7);
//						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3); x34 = r(ip2, jp4); x35 = r(ip2, jp5); x36 = r(ip2, jp6); x37 = r(ip2, jp7);
//						   	   	   	   	          	  	  	   x43 = r(ip3, jp3); x44 = r(ip3, jp4); x45 = r(ip3, jp5); x46 = r(ip3, jp6); x47 = r(ip3, jp7);
//						   	   	   	   	          	  	  	  	  	  	 	 	  x54 = r(ip4, jp4); x55 = r(ip4, jp5); x56 = r(ip4, jp6); x57 = r(ip4, jp7);
//						   	   	   	   	          	  	  	  	  	  	 	 	 	 	         	 x65 = r(ip5, jp5); x66 = r(ip5, jp6); x67 = r(ip5, jp7);
//						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   	x76 = r(ip6, jp6); x77 = r(ip6, jp7);
//						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   x87 = r(ip7, jp7);
//
//		// make nb steps ahead using registers only
//		genrot(&x00, &x10, &c0, &s0, &d);
//		u00 = c0*x00 + s0*x10;
//		u10 = c0*x10 - s0*x00;
//
//		u01 = c0*x01 + s0*x11;
//		u11 = c0*x11 - s0*x01;
//
//		u02 = c0*x02 + s0*x12;
//		u12 = c0*x12 - s0*x02;
//
//		u03 = c0*x03 + s0*x13;
//		u13 = c0*x13 - s0*x03;
//
//		u04 = c0*x04 + s0*x14;
//		u14 = c0*x14 - s0*x04;
//
//		u05 = c0*x05 + s0*x15;
//		u15 = c0*x15 - s0*x05;
//
//		u06 = c0*x06 + s0*x16;
//		u16 = c0*x16 - s0*x06;
//
//		u07 = c0*x07 + s0*x17;
//		u17 = c0*x17 - s0*x07;
//
//		x11 = u11;
//		x12 = u12;
//		x13 = u13;
//		x14 = u14;
//		x15 = u15;
//		x16 = u16;
//		x17 = u17;
//	    genrot(&x11, &x21, &c1, &s1, &d);
//	    u11 = c1*x11 + s1*x21;
//	    u21 = c1*x21 - s1*x11;
//
//	    u12 = c1*x12 + s1*x22;
//	    u22 = c1*x22 - s1*x12;
//
//	    u13 = c1*x13 + s1*x23;
//	    u23 = c1*x23 - s1*x13;
//
//	    u14 = c1*x14 + s1*x24;
//	    u24 = c1*x24 - s1*x14;
//
//	    u15 = c1*x15 + s1*x25;
//	    u25 = c1*x25 - s1*x15;
//
//	    u16 = c1*x16 + s1*x26;
//	    u26 = c1*x26 - s1*x16;
//
//	    u17 = c1*x17 + s1*x27;
//	    u27 = c1*x27 - s1*x17;
//
//		x22 = u22;
//		x23 = u23;
//		x24 = u24;
//		x25 = u25;
//		x26 = u26;
//		x27 = u27;
//	    genrot(&x22, &x32, &c2, &s2, &d);
//	    u22 = c2*x22 + s2*x32;
//	    u32 = c2*x32 - s2*x22;
//
//	    u23 = c2*x23 + s2*x33;
//	    u33 = c2*x33 - s2*x23;
//
//	    u24 = c2*x24 + s2*x34;
//	    u34 = c2*x34 - s2*x24;
//
//	    u25 = c2*x25 + s2*x35;
//	    u35 = c2*x35 - s2*x25;
//
//	    u26 = c2*x26 + s2*x36;
//	    u36 = c2*x36 - s2*x26;
//
//	    u27 = c2*x27 + s2*x37;
//	    u37 = c2*x37 - s2*x27;
//
//		x33 = u33;
//		x34 = u34;
//		x35 = u35;
//		x36 = u36;
//		x37 = u37;
//	    genrot(&x33, &x43, &c3, &s3, &d);
//	    u33 = c3*x33 + s3*x43;
//	    u43 = c3*x43 - s3*x33;
//
//	    u34 = c3*x34 + s3*x44;
//	    u44 = c3*x44 - s3*x34;
//
//	    u35 = c3*x35 + s3*x45;
//	    u45 = c3*x45 - s3*x35;
//
//	    u36 = c3*x36 + s3*x46;
//	    u46 = c3*x46 - s3*x36;
//
//	    u37 = c3*x37 + s3*x47;
//	    u47 = c3*x47 - s3*x37;
//
//		x44 = u44;
//		x45 = u45;
//		x46 = u46;
//		x47 = u47;
//	    genrot(&x44, &x54, &c4, &s4, &d);
//	    u44 = c4*x44 + s4*x54;
//	    u54 = c4*x54 - s4*x44;
//
//	    u45 = c4*x45 + s4*x55;
//	    u55 = c4*x55 - s4*x45;
//
//	    u46 = c4*x46 + s4*x56;
//	    u56 = c4*x56 - s4*x46;
//
//	    u47 = c4*x47 + s4*x57;
//	    u57 = c4*x57 - s4*x47;
//
//		x55 = u55;
//		x56 = u56;
//		x57 = u57;
//	    genrot(&x55, &x65, &c5, &s5, &d);
//	    u55 = c5*x55 + s5*x65;
//	    u65 = c5*x65 - s5*x55;
//
//	    u56 = c5*x56 + s5*x66;
//	    u66 = c5*x66 - s5*x56;
//
//	    u57 = c5*x57 + s5*x67;
//	    u67 = c5*x67 - s5*x57;
//
//		x66 = u66;
//		x67 = u67;
//	    genrot(&x66, &x76, &c6, &s6, &d);
//	    u66 = c6*x66 + s6*x76;
//	    u76 = c6*x76 - s6*x66;
//
//	    u67 = c6*x67 + s6*x77;
//	    u77 = c6*x77 - s6*x67;
//
//		x77 = u77;
//	    genrot(&x77, &x87, &c7, &s7, &d);
//	    u77 = c7*x77 + s7*x87;
//	    u87 = c7*x87 - s7*x77;
//
//		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03; r(im1, jp4) = u04; r(im1, jp5) = u05; r(im1, jp6) = u06; r(im1, jp7) = u07;
//		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13; r(i  , jp4) = u14; r(i  , jp5) = u15; r(i  , jp6) = u16; r(i  , jp7) = u17;
//						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23; r(ip1, jp4) = u24; r(ip1, jp5) = u25; r(ip1, jp6) = u26; r(ip1, jp7) = u27;
//						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33; r(ip2, jp4) = u34; r(ip2, jp5) = u35; r(ip2, jp6) = u36; r(ip2, jp7) = u37;
//						                          	  	  	   r(ip3, jp3) = u43; r(ip3, jp4) = u44; r(ip3, jp5) = u45; r(ip3, jp6) = u46; r(ip3, jp7) = u47;
//						                          	  	  	  	  	  	 	 	  r(ip4, jp4) = u54; r(ip4, jp5) = u55; r(ip4, jp6) = u56; r(ip4, jp7) = u57;
//						                          	  	  	  	  	  	 	 	 	 	 	 		 r(ip5, jp5) = u65; r(ip5, jp6) = u66; r(ip5, jp7) = u67;
//						                          	  	  	  	  	  	 	 	 	 	 	 		 	 	 	 	 	r(ip6, jp6) = u76; r(ip6, jp7) = u77;
//						                          	  	  	  	  	  	 	 	 	 	 	 						   					   r(ip7, jp7) = u87;
//		if (n - j - nb > 0) {
//			// create row band temporary working patch
//			int mm = nb + 1;
//			int nn = n - j - nb;
//			t1.rows(nn);
//			t1.cols(mm);
//			lapack_int lda = r .leading_dim();
//			lapack_int ldb = t1.leading_dim();
//			mkl_domatcopy(r.char_order(), 'T', mm, nn, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
//
//			typename sfo_type<T>::aptr32 data = t1.data();
//			#pragma simd
//			for (int jj = 0; jj < t1.rows(); jj++) {
//				xx0 = data[nn*0 + jj];
//				xx1 = data[nn*1 + jj];
//				xx2 = data[nn*2 + jj];
//				xx3 = data[nn*3 + jj];
//				xx4 = data[nn*4 + jj];
//				xx5 = data[nn*5 + jj];
//				xx6 = data[nn*6 + jj];
//				xx7 = data[nn*7 + jj];
//				xx8 = data[nn*8 + jj];
//
//				uu0 = c0*xx0 + s0*xx1;
//				uu1 = c0*xx1 - s0*xx0;
//
//				xx1 = uu1;
//				uu1 = c1*xx1 + s1*xx2;
//				uu2 = c1*xx2 - s1*xx1;
//
//				xx2 = uu2;
//				uu2 = c2*xx2 + s2*xx3;
//				uu3 = c2*xx3 - s2*xx2;
//
//				xx3 = uu3;
//				uu3 = c3*xx3 + s3*xx4;
//				uu4 = c3*xx4 - s3*xx3;
//
//				xx4 = uu4;
//				uu4 = c4*xx4 + s4*xx5;
//				uu5 = c4*xx5 - s4*xx4;
//
//				xx5  = uu5;
//				uu5 = c5*xx5 + s5*xx6;
//				uu6 = c5*xx6 - s5*xx5;
//
//				xx6  = uu6;
//				uu6 = c6*xx6 + s6*xx7;
//				uu7 = c6*xx7 - s6*xx6;
//
//				xx7  = uu7;
//				uu7 = c7*xx7 + s7*xx8;
//				uu8 = c7*xx8 - s7*xx7;
//
//				data[nn*0 + jj] = uu0;
//				data[nn*1 + jj] = uu1;
//				data[nn*2 + jj] = uu2;
//				data[nn*3 + jj] = uu3;
//				data[nn*4 + jj] = uu4;
//				data[nn*5 + jj] = uu5;
//				data[nn*6 + jj] = uu6;
//				data[nn*7 + jj] = uu7;
//				data[nn*8 + jj] = uu8;
//			}
//
//			// copy patch back
//			lda = t1.leading_dim();
//			ldb = r .leading_dim();
//			mkl_domatcopy(r.char_order(), 'T', nn, mm, alpha, t1.data(), lda, &r(i - 1, j + nb), ldb);
//		}
//	}
//}

/**
 * BLAS3 DGEMM: Triangularizes the given matrix after a deletion update with NB=8.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize8_blas3(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x00, x01, x02, x03, x04, x05, x06, x07, x10, x11, x12, x13, x14, x15, x16, x17, x21, x22, x23, x24, x25, x26, x27,
			x32, x33, x34, x35, x36, x37, x43, x44, x45, x46, x47, x54, x55, x56, x57, x65, x66, x67, x76, x77, x87;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8;
	double u00, u01, u02, u03, u04, u05, u06, u07, u10, u11, u12, u13, u14, u15, u16, u17, u21, u22, u23, u24, u25, u26, u27,
			u32, u33, u34, u35, u36, u37, u43, u44, u45, u46, u47, u54, u55, u56, u57, u65, u66, u67, u76, u77, u87;
	double c0, c1, c2, c3, c4, c5, c6, c7, s0, s1, s2, s3, s4, s5, s6, s7, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7;

	int nb = 8;
	assert(nb == TRIANGULARIZE_NB);

	static tsfo_matrix<double> gc(nb + 1, nb + 1, 0.0);
	static tsfo_matrix<double> t1(nb + 1, 0);
	static const double alpha = 1.0;
	static const double beta  = 0.0;

	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1 = i - 1;
		ip1 = i + 1;
		ip2 = i + 2;
		ip3 = i + 3;
		ip4 = i + 4;
		ip5 = i + 5;
		ip6 = i + 6;
		ip7 = i + 7;

		jp1 = j + 1;
		jp2 = j + 2;
		jp3 = j + 3;
		jp4 = j + 4;
		jp5 = j + 5;
		jp6 = j + 6;
		jp7 = j + 7;

		// make the stencil as easy as possible
		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3); x04 = r(im1, jp4); x05 = r(im1, jp5); x06 = r(im1, jp6); x07 = r(im1, jp7);
		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3); x14 = r(i  , jp4); x15 = r(i  , jp5); x16 = r(i  , jp6); x17 = r(i  , jp7);
						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3); x24 = r(ip1, jp4); x25 = r(ip1, jp5); x26 = r(ip1, jp6); x27 = r(ip1, jp7);
						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3); x34 = r(ip2, jp4); x35 = r(ip2, jp5); x36 = r(ip2, jp6); x37 = r(ip2, jp7);
						   	   	   	   	          	  	  	   x43 = r(ip3, jp3); x44 = r(ip3, jp4); x45 = r(ip3, jp5); x46 = r(ip3, jp6); x47 = r(ip3, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	  x54 = r(ip4, jp4); x55 = r(ip4, jp5); x56 = r(ip4, jp6); x57 = r(ip4, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	         	 x65 = r(ip5, jp5); x66 = r(ip5, jp6); x67 = r(ip5, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   	x76 = r(ip6, jp6); x77 = r(ip6, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   x87 = r(ip7, jp7);

		// make nb steps ahead using registers only
		genrot(&x00, &x10, &c0, &s0, &d);
		u00 = c0*x00 + s0*x10;
		u10 = c0*x10 - s0*x00;

		u01 = c0*x01 + s0*x11;
		u11 = c0*x11 - s0*x01;

		u02 = c0*x02 + s0*x12;
		u12 = c0*x12 - s0*x02;

		u03 = c0*x03 + s0*x13;
		u13 = c0*x13 - s0*x03;

		u04 = c0*x04 + s0*x14;
		u14 = c0*x14 - s0*x04;

		u05 = c0*x05 + s0*x15;
		u15 = c0*x15 - s0*x05;

		u06 = c0*x06 + s0*x16;
		u16 = c0*x16 - s0*x06;

		u07 = c0*x07 + s0*x17;
		u17 = c0*x17 - s0*x07;

		x11 = u11;
		x12 = u12;
		x13 = u13;
		x14 = u14;
		x15 = u15;
		x16 = u16;
		x17 = u17;
	    genrot(&x11, &x21, &c1, &s1, &d);
	    u11 = c1*x11 + s1*x21;
	    u21 = c1*x21 - s1*x11;

	    u12 = c1*x12 + s1*x22;
	    u22 = c1*x22 - s1*x12;

	    u13 = c1*x13 + s1*x23;
	    u23 = c1*x23 - s1*x13;

	    u14 = c1*x14 + s1*x24;
	    u24 = c1*x24 - s1*x14;

	    u15 = c1*x15 + s1*x25;
	    u25 = c1*x25 - s1*x15;

	    u16 = c1*x16 + s1*x26;
	    u26 = c1*x26 - s1*x16;

	    u17 = c1*x17 + s1*x27;
	    u27 = c1*x27 - s1*x17;

		x22 = u22;
		x23 = u23;
		x24 = u24;
		x25 = u25;
		x26 = u26;
		x27 = u27;
	    genrot(&x22, &x32, &c2, &s2, &d);
	    u22 = c2*x22 + s2*x32;
	    u32 = c2*x32 - s2*x22;

	    u23 = c2*x23 + s2*x33;
	    u33 = c2*x33 - s2*x23;

	    u24 = c2*x24 + s2*x34;
	    u34 = c2*x34 - s2*x24;

	    u25 = c2*x25 + s2*x35;
	    u35 = c2*x35 - s2*x25;

	    u26 = c2*x26 + s2*x36;
	    u36 = c2*x36 - s2*x26;

	    u27 = c2*x27 + s2*x37;
	    u37 = c2*x37 - s2*x27;

		x33 = u33;
		x34 = u34;
		x35 = u35;
		x36 = u36;
		x37 = u37;
	    genrot(&x33, &x43, &c3, &s3, &d);
	    u33 = c3*x33 + s3*x43;
	    u43 = c3*x43 - s3*x33;

	    u34 = c3*x34 + s3*x44;
	    u44 = c3*x44 - s3*x34;

	    u35 = c3*x35 + s3*x45;
	    u45 = c3*x45 - s3*x35;

	    u36 = c3*x36 + s3*x46;
	    u46 = c3*x46 - s3*x36;

	    u37 = c3*x37 + s3*x47;
	    u47 = c3*x47 - s3*x37;

		x44 = u44;
		x45 = u45;
		x46 = u46;
		x47 = u47;
	    genrot(&x44, &x54, &c4, &s4, &d);
	    u44 = c4*x44 + s4*x54;
	    u54 = c4*x54 - s4*x44;

	    u45 = c4*x45 + s4*x55;
	    u55 = c4*x55 - s4*x45;

	    u46 = c4*x46 + s4*x56;
	    u56 = c4*x56 - s4*x46;

	    u47 = c4*x47 + s4*x57;
	    u57 = c4*x57 - s4*x47;

		x55 = u55;
		x56 = u56;
		x57 = u57;
	    genrot(&x55, &x65, &c5, &s5, &d);
	    u55 = c5*x55 + s5*x65;
	    u65 = c5*x65 - s5*x55;

	    u56 = c5*x56 + s5*x66;
	    u66 = c5*x66 - s5*x56;

	    u57 = c5*x57 + s5*x67;
	    u67 = c5*x67 - s5*x57;

		x66 = u66;
		x67 = u67;
	    genrot(&x66, &x76, &c6, &s6, &d);
	    u66 = c6*x66 + s6*x76;
	    u76 = c6*x76 - s6*x66;

	    u67 = c6*x67 + s6*x77;
	    u77 = c6*x77 - s6*x67;

		x77 = u77;
	    genrot(&x77, &x87, &c7, &s7, &d);
	    u77 = c7*x77 + s7*x87;
	    u87 = c7*x87 - s7*x77;

		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03; r(im1, jp4) = u04; r(im1, jp5) = u05; r(im1, jp6) = u06; r(im1, jp7) = u07;
		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13; r(i  , jp4) = u14; r(i  , jp5) = u15; r(i  , jp6) = u16; r(i  , jp7) = u17;
						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23; r(ip1, jp4) = u24; r(ip1, jp5) = u25; r(ip1, jp6) = u26; r(ip1, jp7) = u27;
						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33; r(ip2, jp4) = u34; r(ip2, jp5) = u35; r(ip2, jp6) = u36; r(ip2, jp7) = u37;
						                          	  	  	   r(ip3, jp3) = u43; r(ip3, jp4) = u44; r(ip3, jp5) = u45; r(ip3, jp6) = u46; r(ip3, jp7) = u47;
						                          	  	  	  	  	  	 	 	  r(ip4, jp4) = u54; r(ip4, jp5) = u55; r(ip4, jp6) = u56; r(ip4, jp7) = u57;
						                          	  	  	  	  	  	 	 	 	 	 	 		 r(ip5, jp5) = u65; r(ip5, jp6) = u66; r(ip5, jp7) = u67;
						                          	  	  	  	  	  	 	 	 	 	 	 		 	 	 	 	 	r(ip6, jp6) = u76; r(ip6, jp7) = u77;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   r(ip7, jp7) = u87;
		if (n - j - nb > 0) {
            // build the accumulated Givens orthogonal matrix
            gc(0, 0)=c0;						gc(0, 1)=s0;
            gc(1, 0)=-c1*s0;					gc(1, 1)=c0*c1;						gc(1, 2)=s1;
            gc(2, 0)=c2*s0*s1;					gc(2, 1)=-c0*c2*s1;					gc(2, 2)=c1*c2;					gc(2, 3)=s2;
            gc(3, 0)=-c3*s0*s1*s2;				gc(3, 1)=c0*c3*s1*s2;				gc(3, 2)=-c1*c3*s2;				gc(3, 3)=c2*c3;				 gc(3, 4)=s3;
            gc(4, 0)=c4*s0*s1*s2*s3;			gc(4, 1)=-c0*c4*s1*s2*s3;			gc(4, 2)=c1*c4*s2*s3;			gc(4, 3)=-c2*c4*s3;			 gc(4, 4)=c3*c4;			gc(4, 5)=s4;
            gc(5, 0)=-c5*s0*s1*s2*s3*s4;		gc(5, 1)=c0*c5*s1*s2*s3*s4;			gc(5, 2)=-c1*c5*s2*s3*s4;		gc(5, 3)=c2*c5*s3*s4;		 gc(5, 4)=-c3*c5*s4;		gc(5, 5)=c4*c5;			gc(5, 6)=s5;
            gc(6, 0)=c6*s0*s1*s2*s3*s4*s5;		gc(6, 1)=-c0*c6*s1*s2*s3*s4*s5;		gc(6, 2)=c1*c6*s2*s3*s4*s5;		gc(6, 3)=-c2*c6*s3*s4*s5;	 gc(6, 4)=c3*c6*s4*s5;		gc(6, 5)=-c4*c6*s5;		gc(6, 6)=c5*c6;		gc(6, 7)=s6;
            gc(7, 0)=-c7*s0*s1*s2*s3*s4*s5*s6;	gc(7, 1)=c0*c7*s1*s2*s3*s4*s5*s6;	gc(7, 2)=-c1*c7*s2*s3*s4*s5*s6;	gc(7, 3)=c2*c7*s3*s4*s5*s6;	 gc(7, 4)=-c3*c7*s4*s5*s6;	gc(7, 5)=c4*c7*s5*s6;	gc(7, 6)=-c5*c7*s6;	gc(7, 7)=c6*c7;	 gc(7, 8)=s7;
            gc(8, 0)=s0*s1*s2*s3*s4*s5*s6*s7;	gc(8, 1)=-c0*s1*s2*s3*s4*s5*s6*s7;	gc(8, 2)=c1*s2*s3*s4*s5*s6*s7;	gc(8, 3)=-c2*s3*s4*s5*s6*s7; gc(8, 4)=c3*s4*s5*s6*s7;	gc(8, 5)=-c4*s5*s6*s7;	gc(8, 6)=c5*s6*s7;	gc(8, 7)=-c6*s7; gc(8, 8)=c7;

        	// create row band temporary working patch
			{
				const int mm = nb + 1;
				const int nn = n - j - nb;
				t1.cols(nn);
				const lapack_int lda = r.leading_dim();
				const lapack_int ldb = t1.leading_dim();
				mkl_domatcopy(r.char_order(), 'N', mm, nn, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
			}

			// apply the accumulated Givens gc to the matrix row block
			// (excluding the diagonal block) at once using GEMM
			{
				const int mm = gc.rows();
				const int nn = t1.cols();
				assert(nn > 0);
				const int kk = gc.cols();
				const CBLAS_ORDER b_order = r.blas_order();
				const MKL_INT lda 	= gc.leading_dim();
				const MKL_INT ldb 	= t1.leading_dim();
				const MKL_INT ldc 	= r.leading_dim();

				cblas_dgemm(b_order, CblasNoTrans, CblasNoTrans, mm, nn, kk, alpha, gc.data(),
					lda, t1.data(), ldb, beta, &r(i - 1, j + nb), ldc);
			}
        }
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=7.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize7_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x00, x01, x02, x03, x04, x05, x06, x10, x11, x12, x13, x14, x15, x16, x21, x22, x23, x24, x25, x26,
			x32, x33, x34, x35, x36, x43, x44, x45, x46, x54, x55, x56, x65, x66, x76;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8;
	double u00, u01, u02, u03, u04, u05, u06, u10, u11, u12, u13, u14, u15, u16, u21, u22, u23, u24, u25, u26,
			u32, u33, u34, u35, u36, u43, u44, u45, u46, u54, u55, u56, u65, u66, u76;
	double c0, c1, c2, c3, c4, c5, c6, c7, s0, s1, s2, s3, s4, s5, s6, s7, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6;
	int jp1, jp2, jp3, jp4, jp5, jp6;

	int nb = 7;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1 = i - 1;
		ip1 = i + 1;
		ip2 = i + 2;
		ip3 = i + 3;
		ip4 = i + 4;
		ip5 = i + 5;
		ip6 = i + 6;

		jp1 = j + 1;
		jp2 = j + 2;
		jp3 = j + 3;
		jp4 = j + 4;
		jp5 = j + 5;
		jp6 = j + 6;

		// make the stencil as easy as possible
		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3); x04 = r(im1, jp4); x05 = r(im1, jp5); x06 = r(im1, jp6);
		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3); x14 = r(i  , jp4); x15 = r(i  , jp5); x16 = r(i  , jp6);
						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3); x24 = r(ip1, jp4); x25 = r(ip1, jp5); x26 = r(ip1, jp6);
						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3); x34 = r(ip2, jp4); x35 = r(ip2, jp5); x36 = r(ip2, jp6);
						   	   	   	   	          	  	  	   x43 = r(ip3, jp3); x44 = r(ip3, jp4); x45 = r(ip3, jp5); x46 = r(ip3, jp6);
						   	   	   	   	          	  	  	  	  	  	 	 	  x54 = r(ip4, jp4); x55 = r(ip4, jp5); x56 = r(ip4, jp6);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	         	 x65 = r(ip5, jp5); x66 = r(ip5, jp6);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   	x76 = r(ip6, jp6);


		// make nb steps ahead using registers only
		genrot(&x00, &x10, &c0, &s0, &d);
		u00 = c0*x00 + s0*x10;
		u10 = c0*x10 - s0*x00;

		u01 = c0*x01 + s0*x11;
		u11 = c0*x11 - s0*x01;

		u02 = c0*x02 + s0*x12;
		u12 = c0*x12 - s0*x02;

		u03 = c0*x03 + s0*x13;
		u13 = c0*x13 - s0*x03;

		u04 = c0*x04 + s0*x14;
		u14 = c0*x14 - s0*x04;

		u05 = c0*x05 + s0*x15;
		u15 = c0*x15 - s0*x05;

		u06 = c0*x06 + s0*x16;
		u16 = c0*x16 - s0*x06;

		x11 = u11;
		x12 = u12;
		x13 = u13;
		x14 = u14;
		x15 = u15;
		x16 = u16;
	    genrot(&x11, &x21, &c1, &s1, &d);
	    u11 = c1*x11 + s1*x21;
	    u21 = c1*x21 - s1*x11;

	    u12 = c1*x12 + s1*x22;
	    u22 = c1*x22 - s1*x12;

	    u13 = c1*x13 + s1*x23;
	    u23 = c1*x23 - s1*x13;

	    u14 = c1*x14 + s1*x24;
	    u24 = c1*x24 - s1*x14;

	    u15 = c1*x15 + s1*x25;
	    u25 = c1*x25 - s1*x15;

	    u16 = c1*x16 + s1*x26;
	    u26 = c1*x26 - s1*x16;

		x22 = u22;
		x23 = u23;
		x24 = u24;
		x25 = u25;
		x26 = u26;
	    genrot(&x22, &x32, &c2, &s2, &d);
	    u22 = c2*x22 + s2*x32;
	    u32 = c2*x32 - s2*x22;

	    u23 = c2*x23 + s2*x33;
	    u33 = c2*x33 - s2*x23;

	    u24 = c2*x24 + s2*x34;
	    u34 = c2*x34 - s2*x24;

	    u25 = c2*x25 + s2*x35;
	    u35 = c2*x35 - s2*x25;

	    u26 = c2*x26 + s2*x36;
	    u36 = c2*x36 - s2*x26;

		x33 = u33;
		x34 = u34;
		x35 = u35;
		x36 = u36;
	    genrot(&x33, &x43, &c3, &s3, &d);
	    u33 = c3*x33 + s3*x43;
	    u43 = c3*x43 - s3*x33;

	    u34 = c3*x34 + s3*x44;
	    u44 = c3*x44 - s3*x34;

	    u35 = c3*x35 + s3*x45;
	    u45 = c3*x45 - s3*x35;

	    u36 = c3*x36 + s3*x46;
	    u46 = c3*x46 - s3*x36;

		x44 = u44;
		x45 = u45;
		x46 = u46;
	    genrot(&x44, &x54, &c4, &s4, &d);
	    u44 = c4*x44 + s4*x54;
	    u54 = c4*x54 - s4*x44;

	    u45 = c4*x45 + s4*x55;
	    u55 = c4*x55 - s4*x45;

	    u46 = c4*x46 + s4*x56;
	    u56 = c4*x56 - s4*x46;

		x55 = u55;
		x56 = u56;
	    genrot(&x55, &x65, &c5, &s5, &d);
	    u55 = c5*x55 + s5*x65;
	    u65 = c5*x65 - s5*x55;

	    u56 = c5*x56 + s5*x66;
	    u66 = c5*x66 - s5*x56;

		x66 = u66;
	    genrot(&x66, &x76, &c6, &s6, &d);
	    u66 = c6*x66 + s6*x76;
	    u76 = c6*x76 - s6*x66;

		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03; r(im1, jp4) = u04; r(im1, jp5) = u05; r(im1, jp6) = u06;
		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13; r(i  , jp4) = u14; r(i  , jp5) = u15; r(i  , jp6) = u16;
						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23; r(ip1, jp4) = u24; r(ip1, jp5) = u25; r(ip1, jp6) = u26;
						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33; r(ip2, jp4) = u34; r(ip2, jp5) = u35; r(ip2, jp6) = u36;
						                          	  	  	   r(ip3, jp3) = u43; r(ip3, jp4) = u44; r(ip3, jp5) = u45; r(ip3, jp6) = u46;
						                          	  	  	  	  	  	 	 	  r(ip4, jp4) = u54; r(ip4, jp5) = u55; r(ip4, jp6) = u56;
						                          	  	  	  	  	  	 	 	 	 	 	 		 r(ip5, jp5) = u65; r(ip5, jp6) = u66;
						                          	  	  	  	  	  	 	 	 	 	 	 		 	 	 	 	 	r(ip6, jp6) = u76;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0 = r(i - 1, jj);
			xx1 = r(i    , jj);
			xx2 = r(i + 1, jj);
			xx3 = r(i + 2, jj);
			xx4 = r(i + 3, jj);
			xx5 = r(i + 4, jj);
			xx6 = r(i + 5, jj);
			xx7 = r(i + 6, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			r(i - 1, jj) = uu0;
			r(i    , jj) = uu1;
			r(i + 1, jj) = uu2;
			r(i + 2, jj) = uu3;
			r(i + 3, jj) = uu4;
			r(i + 4, jj) = uu5;
			r(i + 5, jj) = uu6;
			r(i + 6, jj) = uu7;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=8.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize8_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x00, x01, x02, x03, x04, x05, x06, x07, x10, x11, x12, x13, x14, x15, x16, x17, x21, x22, x23, x24, x25, x26, x27,
			x32, x33, x34, x35, x36, x37, x43, x44, x45, x46, x47, x54, x55, x56, x57, x65, x66, x67, x76, x77, x87;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8;
	double u00, u01, u02, u03, u04, u05, u06, u07, u10, u11, u12, u13, u14, u15, u16, u17, u21, u22, u23, u24, u25, u26, u27,
			u32, u33, u34, u35, u36, u37, u43, u44, u45, u46, u47, u54, u55, u56, u57, u65, u66, u67, u76, u77, u87;
	double c0, c1, c2, c3, c4, c5, c6, c7, s0, s1, s2, s3, s4, s5, s6, s7, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7;

	int nb = 8;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1 = i - 1;
		ip1 = i + 1;
		ip2 = i + 2;
		ip3 = i + 3;
		ip4 = i + 4;
		ip5 = i + 5;
		ip6 = i + 6;
		ip7 = i + 7;

		jp1 = j + 1;
		jp2 = j + 2;
		jp3 = j + 3;
		jp4 = j + 4;
		jp5 = j + 5;
		jp6 = j + 6;
		jp7 = j + 7;

		// make the stencil as easy as possible
		x00 = r(im1, j); x01 = r(im1, jp1); x02 = r(im1, jp2); x03 = r(im1, jp3); x04 = r(im1, jp4); x05 = r(im1, jp5); x06 = r(im1, jp6); x07 = r(im1, jp7);
		x10 = r(i,   j); x11 = r(i,   jp1); x12 = r(i,   jp2); x13 = r(i,   jp3); x14 = r(i  , jp4); x15 = r(i  , jp5); x16 = r(i  , jp6); x17 = r(i  , jp7);
						 x21 = r(ip1, jp1); x22 = r(ip1, jp2); x23 = r(ip1, jp3); x24 = r(ip1, jp4); x25 = r(ip1, jp5); x26 = r(ip1, jp6); x27 = r(ip1, jp7);
						   	   	   	   	    x32 = r(ip2, jp2); x33 = r(ip2, jp3); x34 = r(ip2, jp4); x35 = r(ip2, jp5); x36 = r(ip2, jp6); x37 = r(ip2, jp7);
						   	   	   	   	          	  	  	   x43 = r(ip3, jp3); x44 = r(ip3, jp4); x45 = r(ip3, jp5); x46 = r(ip3, jp6); x47 = r(ip3, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	  x54 = r(ip4, jp4); x55 = r(ip4, jp5); x56 = r(ip4, jp6); x57 = r(ip4, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	         	 x65 = r(ip5, jp5); x66 = r(ip5, jp6); x67 = r(ip5, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   	x76 = r(ip6, jp6); x77 = r(ip6, jp7);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   x87 = r(ip7, jp7);

		// make nb steps ahead using registers only
		genrot(&x00, &x10, &c0, &s0, &d);
		u00 = c0*x00 + s0*x10;
		u10 = c0*x10 - s0*x00;

		u01 = c0*x01 + s0*x11;
		u11 = c0*x11 - s0*x01;

		u02 = c0*x02 + s0*x12;
		u12 = c0*x12 - s0*x02;

		u03 = c0*x03 + s0*x13;
		u13 = c0*x13 - s0*x03;

		u04 = c0*x04 + s0*x14;
		u14 = c0*x14 - s0*x04;

		u05 = c0*x05 + s0*x15;
		u15 = c0*x15 - s0*x05;

		u06 = c0*x06 + s0*x16;
		u16 = c0*x16 - s0*x06;

		u07 = c0*x07 + s0*x17;
		u17 = c0*x17 - s0*x07;

		x11 = u11;
		x12 = u12;
		x13 = u13;
		x14 = u14;
		x15 = u15;
		x16 = u16;
		x17 = u17;
	    genrot(&x11, &x21, &c1, &s1, &d);
	    u11 = c1*x11 + s1*x21;
	    u21 = c1*x21 - s1*x11;

	    u12 = c1*x12 + s1*x22;
	    u22 = c1*x22 - s1*x12;

	    u13 = c1*x13 + s1*x23;
	    u23 = c1*x23 - s1*x13;

	    u14 = c1*x14 + s1*x24;
	    u24 = c1*x24 - s1*x14;

	    u15 = c1*x15 + s1*x25;
	    u25 = c1*x25 - s1*x15;

	    u16 = c1*x16 + s1*x26;
	    u26 = c1*x26 - s1*x16;

	    u17 = c1*x17 + s1*x27;
	    u27 = c1*x27 - s1*x17;

		x22 = u22;
		x23 = u23;
		x24 = u24;
		x25 = u25;
		x26 = u26;
		x27 = u27;
	    genrot(&x22, &x32, &c2, &s2, &d);
	    u22 = c2*x22 + s2*x32;
	    u32 = c2*x32 - s2*x22;

	    u23 = c2*x23 + s2*x33;
	    u33 = c2*x33 - s2*x23;

	    u24 = c2*x24 + s2*x34;
	    u34 = c2*x34 - s2*x24;

	    u25 = c2*x25 + s2*x35;
	    u35 = c2*x35 - s2*x25;

	    u26 = c2*x26 + s2*x36;
	    u36 = c2*x36 - s2*x26;

	    u27 = c2*x27 + s2*x37;
	    u37 = c2*x37 - s2*x27;

		x33 = u33;
		x34 = u34;
		x35 = u35;
		x36 = u36;
		x37 = u37;
	    genrot(&x33, &x43, &c3, &s3, &d);
	    u33 = c3*x33 + s3*x43;
	    u43 = c3*x43 - s3*x33;

	    u34 = c3*x34 + s3*x44;
	    u44 = c3*x44 - s3*x34;

	    u35 = c3*x35 + s3*x45;
	    u45 = c3*x45 - s3*x35;

	    u36 = c3*x36 + s3*x46;
	    u46 = c3*x46 - s3*x36;

	    u37 = c3*x37 + s3*x47;
	    u47 = c3*x47 - s3*x37;

		x44 = u44;
		x45 = u45;
		x46 = u46;
		x47 = u47;
	    genrot(&x44, &x54, &c4, &s4, &d);
	    u44 = c4*x44 + s4*x54;
	    u54 = c4*x54 - s4*x44;

	    u45 = c4*x45 + s4*x55;
	    u55 = c4*x55 - s4*x45;

	    u46 = c4*x46 + s4*x56;
	    u56 = c4*x56 - s4*x46;

	    u47 = c4*x47 + s4*x57;
	    u57 = c4*x57 - s4*x47;

		x55 = u55;
		x56 = u56;
		x57 = u57;
	    genrot(&x55, &x65, &c5, &s5, &d);
	    u55 = c5*x55 + s5*x65;
	    u65 = c5*x65 - s5*x55;

	    u56 = c5*x56 + s5*x66;
	    u66 = c5*x66 - s5*x56;

	    u57 = c5*x57 + s5*x67;
	    u67 = c5*x67 - s5*x57;

		x66 = u66;
		x67 = u67;
	    genrot(&x66, &x76, &c6, &s6, &d);
	    u66 = c6*x66 + s6*x76;
	    u76 = c6*x76 - s6*x66;

	    u67 = c6*x67 + s6*x77;
	    u77 = c6*x77 - s6*x67;

		x77 = u77;
	    genrot(&x77, &x87, &c7, &s7, &d);
	    u77 = c7*x77 + s7*x87;
	    u87 = c7*x87 - s7*x77;

		r(im1, j) = u00; r(im1, jp1) = u01; r(im1, jp2) = u02; r(im1, jp3) = u03; r(im1, jp4) = u04; r(im1, jp5) = u05; r(im1, jp6) = u06; r(im1, jp7) = u07;
		r(i,   j) = u10; r(i,   jp1) = u11; r(i  , jp2) = u12; r(i  , jp3) = u13; r(i  , jp4) = u14; r(i  , jp5) = u15; r(i  , jp6) = u16; r(i  , jp7) = u17;
						 r(ip1, jp1) = u21; r(ip1, jp2) = u22; r(ip1, jp3) = u23; r(ip1, jp4) = u24; r(ip1, jp5) = u25; r(ip1, jp6) = u26; r(ip1, jp7) = u27;
						                    r(ip2, jp2) = u32; r(ip2, jp3) = u33; r(ip2, jp4) = u34; r(ip2, jp5) = u35; r(ip2, jp6) = u36; r(ip2, jp7) = u37;
						                          	  	  	   r(ip3, jp3) = u43; r(ip3, jp4) = u44; r(ip3, jp5) = u45; r(ip3, jp6) = u46; r(ip3, jp7) = u47;
						                          	  	  	  	  	  	 	 	  r(ip4, jp4) = u54; r(ip4, jp5) = u55; r(ip4, jp6) = u56; r(ip4, jp7) = u57;
						                          	  	  	  	  	  	 	 	 	 	 	 		 r(ip5, jp5) = u65; r(ip5, jp6) = u66; r(ip5, jp7) = u67;
						                          	  	  	  	  	  	 	 	 	 	 	 		 	 	 	 	 	r(ip6, jp6) = u76; r(ip6, jp7) = u77;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   r(ip7, jp7) = u87;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0 = r(i - 1, jj);
			xx1 = r(i    , jj);
			xx2 = r(i + 1, jj);
			xx3 = r(i + 2, jj);
			xx4 = r(i + 3, jj);
			xx5 = r(i + 4, jj);
			xx6 = r(i + 5, jj);
			xx7 = r(i + 6, jj);
			xx8 = r(i + 7, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			r(i - 1, jj) = uu0;
			r(i    , jj) = uu1;
			r(i + 1, jj) = uu2;
			r(i + 2, jj) = uu3;
			r(i + 3, jj) = uu4;
			r(i + 4, jj) = uu5;
			r(i + 5, jj) = uu6;
			r(i + 6, jj) = uu7;
			r(i + 7, jj) = uu8;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=9.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize9_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8;

	int nb = 9;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

		x0707 = u0707;
		x0708 = u0708;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

		x0808 = u0808;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=10.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize10_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9;

	int nb = 10;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

		x0808 = u0808;
		x0809 = u0809;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

		x0909 = u0909;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=11.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize11_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10;

	int nb = 11;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		x0010 = r(im1 , jp10);
		x0110 = r(i   , jp10);
		x0210 = r(ip1 , jp10);
		x0310 = r(ip2 , jp10);
		x0410 = r(ip3 , jp10);
		x0510 = r(ip4 , jp10);
		x0610 = r(ip5 , jp10);
		x0710 = r(ip6 , jp10);
		x0810 = r(ip7 , jp10);
		x0910 = r(ip8 , jp10);
		x1010 = r(ip9 , jp10);
		x1110 = r(ip10, jp10);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

		x0909 = u0909;
		x0910 = u0910;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

		x1010 = u1010;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		r(im1 , jp10) = u0010;
		r(i   , jp10) = u0110;
		r(ip1 , jp10) = u0210;
		r(ip2 , jp10) = u0310;
		r(ip3 , jp10) = u0410;
		r(ip4 , jp10) = u0510;
		r(ip5 , jp10) = u0610;
		r(ip6 , jp10) = u0710;
		r(ip7 , jp10) = u0810;
		r(ip8 , jp10) = u0910;
		r(ip9 , jp10) = u1010;
		r(ip10, jp10) = u1110;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);
			xx11 = r(i + 10, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			xx10 = uu10;
		    uu10 = c10*xx10 + s10*xx11;
		    uu11 = c10*xx11 - s10*xx10;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
			r(i + 10, jj) = uu11;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=12.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize12_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110, x0011, x0111, x0211, x0311, x0411, x0511, x0611, x0711, x0811, x0911, x1011, x1111, x1211;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110, u0011, u0111, u0211, u0311, u0411, u0511, u0611, u0711, u0811, u0911, u1011, u1111, u1211;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10, ip11;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10, jp11;

	int nb = 12;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;
		ip11 = i + 11;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;
		jp11 = j + 11;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		x0010 = r(im1 , jp10);
		x0110 = r(i   , jp10);
		x0210 = r(ip1 , jp10);
		x0310 = r(ip2 , jp10);
		x0410 = r(ip3 , jp10);
		x0510 = r(ip4 , jp10);
		x0610 = r(ip5 , jp10);
		x0710 = r(ip6 , jp10);
		x0810 = r(ip7 , jp10);
		x0910 = r(ip8 , jp10);
		x1010 = r(ip9 , jp10);
		x1110 = r(ip10, jp10);

		x0011 = r(im1 , jp11);
		x0111 = r(i   , jp11);
		x0211 = r(ip1 , jp11);
		x0311 = r(ip2 , jp11);
		x0411 = r(ip3 , jp11);
		x0511 = r(ip4 , jp11);
		x0611 = r(ip5 , jp11);
		x0711 = r(ip6 , jp11);
		x0811 = r(ip7 , jp11);
		x0911 = r(ip8 , jp11);
		x1011 = r(ip9 , jp11);
		x1111 = r(ip10, jp11);
		x1211 = r(ip11, jp11);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		u0011 = c0*x0011 + s0*x0111;
		u0111 = c0*x0111 - s0*x0011;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
		x0111 = u0111;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

	    u0111 = c1*x0111 + s1*x0211;
	    u0211 = c1*x0211 - s1*x0111;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
		x0211 = u0211;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

	    u0211 = c2*x0211 + s2*x0311;
	    u0311 = c2*x0311 - s2*x0211;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
		x0311 = u0311;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

	    u0311 = c3*x0311 + s3*x0411;
	    u0411 = c3*x0411 - s3*x0311;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
		x0411 = u0411;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

	    u0411 = c4*x0411 + s4*x0511;
	    u0511 = c4*x0511 - s4*x0411;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
		x0511 = u0511;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

	    u0511 = c5*x0511 + s5*x0611;
	    u0611 = c5*x0611 - s5*x0511;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
		x0611 = u0611;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

	    u0611 = c6*x0611 + s6*x0711;
	    u0711 = c6*x0711 - s6*x0611;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
		x0711 = u0711;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

	    u0711 = c7*x0711 + s7*x0811;
	    u0811 = c7*x0811 - s7*x0711;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
		x0811 = u0811;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

	    u0811 = c8*x0811 + s8*x0911;
	    u0911 = c8*x0911 - s8*x0811;

		x0909 = u0909;
		x0910 = u0910;
		x0911 = u0911;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

	    u0911 = c9*x0911 + s9*x1011;
	    u1011 = c9*x1011 - s9*x0911;

		x1010 = u1010;
		x1011 = u1011;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

	    u1011 = c10*x1011 + s10*x1111;
	    u1111 = c10*x1111 - s10*x1011;

		x1111 = u1111;
	    genrot(&x1111, &x1211, &c11, &s11, &d);
	    u1111 = c11*x1111 + s11*x1211;
	    u1211 = c11*x1211 - s11*x1111;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		r(im1 , jp10) = u0010;
		r(i   , jp10) = u0110;
		r(ip1 , jp10) = u0210;
		r(ip2 , jp10) = u0310;
		r(ip3 , jp10) = u0410;
		r(ip4 , jp10) = u0510;
		r(ip5 , jp10) = u0610;
		r(ip6 , jp10) = u0710;
		r(ip7 , jp10) = u0810;
		r(ip8 , jp10) = u0910;
		r(ip9 , jp10) = u1010;
		r(ip10, jp10) = u1110;

		r(im1 , jp11) = u0011;
		r(i   , jp11) = u0111;
		r(ip1 , jp11) = u0211;
		r(ip2 , jp11) = u0311;
		r(ip3 , jp11) = u0411;
		r(ip4 , jp11) = u0511;
		r(ip5 , jp11) = u0611;
		r(ip6 , jp11) = u0711;
		r(ip7 , jp11) = u0811;
		r(ip8 , jp11) = u0911;
		r(ip9 , jp11) = u1011;
		r(ip10, jp11) = u1111;
		r(ip11, jp11) = u1211;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);
			xx11 = r(i + 10, jj);
			xx12 = r(i + 11, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			xx10 = uu10;
		    uu10 = c10*xx10 + s10*xx11;
		    uu11 = c10*xx11 - s10*xx10;

			xx11 = uu11;
		    uu11 = c11*xx11 + s11*xx12;
		    uu12 = c11*xx12 - s11*xx11;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
			r(i + 10, jj) = uu11;
			r(i + 11, jj) = uu12;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=13.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize13_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110, x0011, x0111, x0211, x0311, x0411, x0511, x0611, x0711, x0811, x0911, x1011, x1111, x1211,
		   x0012, x0112, x0212, x0312, x0412, x0512, x0612, x0712, x0812, x0912, x1012, x1112, x1212, x1312;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110, u0011, u0111, u0211, u0311, u0411, u0511, u0611, u0711, u0811, u0911, u1011, u1111, u1211,
		   u0012, u0112, u0212, u0312, u0412, u0512, u0612, u0712, u0812, u0912, u1012, u1112, u1212, u1312;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10, ip11, ip12;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10, jp11, jp12;

	int nb = 13;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;
		ip11 = i + 11;
		ip12 = i + 12;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;
		jp11 = j + 11;
		jp12 = j + 12;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		x0010 = r(im1 , jp10);
		x0110 = r(i   , jp10);
		x0210 = r(ip1 , jp10);
		x0310 = r(ip2 , jp10);
		x0410 = r(ip3 , jp10);
		x0510 = r(ip4 , jp10);
		x0610 = r(ip5 , jp10);
		x0710 = r(ip6 , jp10);
		x0810 = r(ip7 , jp10);
		x0910 = r(ip8 , jp10);
		x1010 = r(ip9 , jp10);
		x1110 = r(ip10, jp10);

		x0011 = r(im1 , jp11);
		x0111 = r(i   , jp11);
		x0211 = r(ip1 , jp11);
		x0311 = r(ip2 , jp11);
		x0411 = r(ip3 , jp11);
		x0511 = r(ip4 , jp11);
		x0611 = r(ip5 , jp11);
		x0711 = r(ip6 , jp11);
		x0811 = r(ip7 , jp11);
		x0911 = r(ip8 , jp11);
		x1011 = r(ip9 , jp11);
		x1111 = r(ip10, jp11);
		x1211 = r(ip11, jp11);

		x0012 = r(im1 , jp12);
		x0112 = r(i   , jp12);
		x0212 = r(ip1 , jp12);
		x0312 = r(ip2 , jp12);
		x0412 = r(ip3 , jp12);
		x0512 = r(ip4 , jp12);
		x0612 = r(ip5 , jp12);
		x0712 = r(ip6 , jp12);
		x0812 = r(ip7 , jp12);
		x0912 = r(ip8 , jp12);
		x1012 = r(ip9 , jp12);
		x1112 = r(ip10, jp12);
		x1212 = r(ip11, jp12);
		x1312 = r(ip12, jp12);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		u0011 = c0*x0011 + s0*x0111;
		u0111 = c0*x0111 - s0*x0011;

		u0012 = c0*x0012 + s0*x0112;
		u0112 = c0*x0112 - s0*x0012;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
		x0111 = u0111;
		x0112 = u0112;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

	    u0111 = c1*x0111 + s1*x0211;
	    u0211 = c1*x0211 - s1*x0111;

	    u0112 = c1*x0112 + s1*x0212;
	    u0212 = c1*x0212 - s1*x0112;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
		x0211 = u0211;
		x0212 = u0212;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

	    u0211 = c2*x0211 + s2*x0311;
	    u0311 = c2*x0311 - s2*x0211;

	    u0212 = c2*x0212 + s2*x0312;
	    u0312 = c2*x0312 - s2*x0212;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
		x0311 = u0311;
		x0312 = u0312;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

	    u0311 = c3*x0311 + s3*x0411;
	    u0411 = c3*x0411 - s3*x0311;

	    u0312 = c3*x0312 + s3*x0412;
	    u0412 = c3*x0412 - s3*x0312;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
		x0411 = u0411;
		x0412 = u0412;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

	    u0411 = c4*x0411 + s4*x0511;
	    u0511 = c4*x0511 - s4*x0411;

	    u0412 = c4*x0412 + s4*x0512;
	    u0512 = c4*x0512 - s4*x0412;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
		x0511 = u0511;
		x0512 = u0512;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

	    u0511 = c5*x0511 + s5*x0611;
	    u0611 = c5*x0611 - s5*x0511;

	    u0512 = c5*x0512 + s5*x0612;
	    u0612 = c5*x0612 - s5*x0512;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
		x0611 = u0611;
		x0612 = u0612;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

	    u0611 = c6*x0611 + s6*x0711;
	    u0711 = c6*x0711 - s6*x0611;

	    u0612 = c6*x0612 + s6*x0712;
	    u0712 = c6*x0712 - s6*x0612;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
		x0711 = u0711;
		x0712 = u0712;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

	    u0711 = c7*x0711 + s7*x0811;
	    u0811 = c7*x0811 - s7*x0711;

	    u0712 = c7*x0712 + s7*x0812;
	    u0812 = c7*x0812 - s7*x0712;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
		x0811 = u0811;
		x0812 = u0812;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

	    u0811 = c8*x0811 + s8*x0911;
	    u0911 = c8*x0911 - s8*x0811;

	    u0812 = c8*x0812 + s8*x0912;
	    u0912 = c8*x0912 - s8*x0812;

		x0909 = u0909;
		x0910 = u0910;
		x0911 = u0911;
		x0912 = u0912;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

	    u0911 = c9*x0911 + s9*x1011;
	    u1011 = c9*x1011 - s9*x0911;

	    u0912 = c9*x0912 + s9*x1012;
	    u1012 = c9*x1012 - s9*x0912;

		x1010 = u1010;
		x1011 = u1011;
		x1012 = u1012;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

	    u1011 = c10*x1011 + s10*x1111;
	    u1111 = c10*x1111 - s10*x1011;

	    u1012 = c10*x1012 + s10*x1112;
	    u1112 = c10*x1112 - s10*x1012;

		x1111 = u1111;
		x1112 = u1112;
	    genrot(&x1111, &x1211, &c11, &s11, &d);
	    u1111 = c11*x1111 + s11*x1211;
	    u1211 = c11*x1211 - s11*x1111;

	    u1112 = c11*x1112 + s11*x1212;
	    u1212 = c11*x1212 - s11*x1112;

		x1212 = u1212;
	    genrot(&x1212, &x1312, &c12, &s12, &d);
	    u1212 = c12*x1212 + s12*x1312;
	    u1312 = c12*x1312 - s12*x1212;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		r(im1 , jp10) = u0010;
		r(i   , jp10) = u0110;
		r(ip1 , jp10) = u0210;
		r(ip2 , jp10) = u0310;
		r(ip3 , jp10) = u0410;
		r(ip4 , jp10) = u0510;
		r(ip5 , jp10) = u0610;
		r(ip6 , jp10) = u0710;
		r(ip7 , jp10) = u0810;
		r(ip8 , jp10) = u0910;
		r(ip9 , jp10) = u1010;
		r(ip10, jp10) = u1110;

		r(im1 , jp11) = u0011;
		r(i   , jp11) = u0111;
		r(ip1 , jp11) = u0211;
		r(ip2 , jp11) = u0311;
		r(ip3 , jp11) = u0411;
		r(ip4 , jp11) = u0511;
		r(ip5 , jp11) = u0611;
		r(ip6 , jp11) = u0711;
		r(ip7 , jp11) = u0811;
		r(ip8 , jp11) = u0911;
		r(ip9 , jp11) = u1011;
		r(ip10, jp11) = u1111;
		r(ip11, jp11) = u1211;

		r(im1 , jp12) = u0012;
		r(i   , jp12) = u0112;
		r(ip1 , jp12) = u0212;
		r(ip2 , jp12) = u0312;
		r(ip3 , jp12) = u0412;
		r(ip4 , jp12) = u0512;
		r(ip5 , jp12) = u0612;
		r(ip6 , jp12) = u0712;
		r(ip7 , jp12) = u0812;
		r(ip8 , jp12) = u0912;
		r(ip9 , jp12) = u1012;
		r(ip10, jp12) = u1112;
		r(ip11, jp12) = u1212;
		r(ip12, jp12) = u1312;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);
			xx11 = r(i + 10, jj);
			xx12 = r(i + 11, jj);
			xx13 = r(i + 12, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			xx10 = uu10;
		    uu10 = c10*xx10 + s10*xx11;
		    uu11 = c10*xx11 - s10*xx10;

			xx11 = uu11;
		    uu11 = c11*xx11 + s11*xx12;
		    uu12 = c11*xx12 - s11*xx11;

			xx12 = uu12;
		    uu12 = c12*xx12 + s12*xx13;
		    uu13 = c12*xx13 - s12*xx12;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
			r(i + 10, jj) = uu11;
			r(i + 11, jj) = uu12;
			r(i + 12, jj) = uu13;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=14.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize14_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110, x0011, x0111, x0211, x0311, x0411, x0511, x0611, x0711, x0811, x0911, x1011, x1111, x1211,
		   x0012, x0112, x0212, x0312, x0412, x0512, x0612, x0712, x0812, x0912, x1012, x1112, x1212, x1312, x0013,
		   x0113, x0213, x0313, x0413, x0513, x0613, x0713, x0813, x0913, x1013, x1113, x1213, x1313, x1413;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110, u0011, u0111, u0211, u0311, u0411, u0511, u0611, u0711, u0811, u0911, u1011, u1111, u1211,
		   u0012, u0112, u0212, u0312, u0412, u0512, u0612, u0712, u0812, u0912, u1012, u1112, u1212, u1312, u0013,
		   u0113, u0213, u0313, u0413, u0513, u0613, u0713, u0813, u0913, u1013, u1113, u1213, u1313, u1413,
		   u0015, u1515;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10, ip11, ip12, ip13;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10, jp11, jp12, jp13;

	int nb = 14;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;
		ip11 = i + 11;
		ip12 = i + 12;
		ip13 = i + 13;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;
		jp11 = j + 11;
		jp12 = j + 12;
		jp13 = j + 13;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		x0010 = r(im1 , jp10);
		x0110 = r(i   , jp10);
		x0210 = r(ip1 , jp10);
		x0310 = r(ip2 , jp10);
		x0410 = r(ip3 , jp10);
		x0510 = r(ip4 , jp10);
		x0610 = r(ip5 , jp10);
		x0710 = r(ip6 , jp10);
		x0810 = r(ip7 , jp10);
		x0910 = r(ip8 , jp10);
		x1010 = r(ip9 , jp10);
		x1110 = r(ip10, jp10);

		x0011 = r(im1 , jp11);
		x0111 = r(i   , jp11);
		x0211 = r(ip1 , jp11);
		x0311 = r(ip2 , jp11);
		x0411 = r(ip3 , jp11);
		x0511 = r(ip4 , jp11);
		x0611 = r(ip5 , jp11);
		x0711 = r(ip6 , jp11);
		x0811 = r(ip7 , jp11);
		x0911 = r(ip8 , jp11);
		x1011 = r(ip9 , jp11);
		x1111 = r(ip10, jp11);
		x1211 = r(ip11, jp11);

		x0012 = r(im1 , jp12);
		x0112 = r(i   , jp12);
		x0212 = r(ip1 , jp12);
		x0312 = r(ip2 , jp12);
		x0412 = r(ip3 , jp12);
		x0512 = r(ip4 , jp12);
		x0612 = r(ip5 , jp12);
		x0712 = r(ip6 , jp12);
		x0812 = r(ip7 , jp12);
		x0912 = r(ip8 , jp12);
		x1012 = r(ip9 , jp12);
		x1112 = r(ip10, jp12);
		x1212 = r(ip11, jp12);
		x1312 = r(ip12, jp12);

		x0013 = r(im1 , jp13);
		x0113 = r(i   , jp13);
		x0213 = r(ip1 , jp13);
		x0313 = r(ip2 , jp13);
		x0413 = r(ip3 , jp13);
		x0513 = r(ip4 , jp13);
		x0613 = r(ip5 , jp13);
		x0713 = r(ip6 , jp13);
		x0813 = r(ip7 , jp13);
		x0913 = r(ip8 , jp13);
		x1013 = r(ip9 , jp13);
		x1113 = r(ip10, jp13);
		x1213 = r(ip11, jp13);
		x1313 = r(ip12, jp13);
		x1413 = r(ip13, jp13);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		u0011 = c0*x0011 + s0*x0111;
		u0111 = c0*x0111 - s0*x0011;

		u0012 = c0*x0012 + s0*x0112;
		u0112 = c0*x0112 - s0*x0012;

		u0013 = c0*x0013 + s0*x0113;
		u0113 = c0*x0113 - s0*x0013;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
		x0111 = u0111;
		x0112 = u0112;
		x0113 = u0113;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

	    u0111 = c1*x0111 + s1*x0211;
	    u0211 = c1*x0211 - s1*x0111;

	    u0112 = c1*x0112 + s1*x0212;
	    u0212 = c1*x0212 - s1*x0112;

	    u0113 = c1*x0113 + s1*x0213;
	    u0213 = c1*x0213 - s1*x0113;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
		x0211 = u0211;
		x0212 = u0212;
		x0213 = u0213;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

	    u0211 = c2*x0211 + s2*x0311;
	    u0311 = c2*x0311 - s2*x0211;

	    u0212 = c2*x0212 + s2*x0312;
	    u0312 = c2*x0312 - s2*x0212;

	    u0213 = c2*x0213 + s2*x0313;
	    u0313 = c2*x0313 - s2*x0213;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
		x0311 = u0311;
		x0312 = u0312;
		x0313 = u0313;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

	    u0311 = c3*x0311 + s3*x0411;
	    u0411 = c3*x0411 - s3*x0311;

	    u0312 = c3*x0312 + s3*x0412;
	    u0412 = c3*x0412 - s3*x0312;

	    u0313 = c3*x0313 + s3*x0413;
	    u0413 = c3*x0413 - s3*x0313;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
		x0411 = u0411;
		x0412 = u0412;
		x0413 = u0413;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

	    u0411 = c4*x0411 + s4*x0511;
	    u0511 = c4*x0511 - s4*x0411;

	    u0412 = c4*x0412 + s4*x0512;
	    u0512 = c4*x0512 - s4*x0412;

	    u0413 = c4*x0413 + s4*x0513;
	    u0513 = c4*x0513 - s4*x0413;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
		x0511 = u0511;
		x0512 = u0512;
		x0513 = u0513;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

	    u0511 = c5*x0511 + s5*x0611;
	    u0611 = c5*x0611 - s5*x0511;

	    u0512 = c5*x0512 + s5*x0612;
	    u0612 = c5*x0612 - s5*x0512;

	    u0513 = c5*x0513 + s5*x0613;
	    u0613 = c5*x0613 - s5*x0513;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
		x0611 = u0611;
		x0612 = u0612;
		x0613 = u0613;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

	    u0611 = c6*x0611 + s6*x0711;
	    u0711 = c6*x0711 - s6*x0611;

	    u0612 = c6*x0612 + s6*x0712;
	    u0712 = c6*x0712 - s6*x0612;

	    u0613 = c6*x0613 + s6*x0713;
	    u0713 = c6*x0713 - s6*x0613;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
		x0711 = u0711;
		x0712 = u0712;
		x0713 = u0713;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

	    u0711 = c7*x0711 + s7*x0811;
	    u0811 = c7*x0811 - s7*x0711;

	    u0712 = c7*x0712 + s7*x0812;
	    u0812 = c7*x0812 - s7*x0712;

	    u0713 = c7*x0713 + s7*x0813;
	    u0813 = c7*x0813 - s7*x0713;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
		x0811 = u0811;
		x0812 = u0812;
		x0813 = u0813;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

	    u0811 = c8*x0811 + s8*x0911;
	    u0911 = c8*x0911 - s8*x0811;

	    u0812 = c8*x0812 + s8*x0912;
	    u0912 = c8*x0912 - s8*x0812;

	    u0813 = c8*x0813 + s8*x0913;
	    u0913 = c8*x0913 - s8*x0813;

		x0909 = u0909;
		x0910 = u0910;
		x0911 = u0911;
		x0912 = u0912;
		x0913 = u0913;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

	    u0911 = c9*x0911 + s9*x1011;
	    u1011 = c9*x1011 - s9*x0911;

	    u0912 = c9*x0912 + s9*x1012;
	    u1012 = c9*x1012 - s9*x0912;

	    u0913 = c9*x0913 + s9*x1013;
	    u1013 = c9*x1013 - s9*x0913;

		x1010 = u1010;
		x1011 = u1011;
		x1012 = u1012;
		x1013 = u1013;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

	    u1011 = c10*x1011 + s10*x1111;
	    u1111 = c10*x1111 - s10*x1011;

	    u1012 = c10*x1012 + s10*x1112;
	    u1112 = c10*x1112 - s10*x1012;

	    u1013 = c10*x1013 + s10*x1113;
	    u1113 = c10*x1113 - s10*x1013;

		x1111 = u1111;
		x1112 = u1112;
		x1113 = u1113;
	    genrot(&x1111, &x1211, &c11, &s11, &d);
	    u1111 = c11*x1111 + s11*x1211;
	    u1211 = c11*x1211 - s11*x1111;

	    u1112 = c11*x1112 + s11*x1212;
	    u1212 = c11*x1212 - s11*x1112;

	    u1113 = c11*x1113 + s11*x1213;
	    u1213 = c11*x1213 - s11*x1113;

		x1212 = u1212;
		x1213 = u1213;
	    genrot(&x1212, &x1312, &c12, &s12, &d);
	    u1212 = c12*x1212 + s12*x1312;
	    u1312 = c12*x1312 - s12*x1212;

	    u1213 = c12*x1213 + s12*x1313;
	    u1313 = c12*x1313 - s12*x1213;

		x1313 = u1313;
	    genrot(&x1313, &x1413, &c13, &s13, &d);
	    u1313 = c13*x1313 + s13*x1413;
	    u1413 = c13*x1413 - s13*x1313;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		r(im1 , jp10) = u0010;
		r(i   , jp10) = u0110;
		r(ip1 , jp10) = u0210;
		r(ip2 , jp10) = u0310;
		r(ip3 , jp10) = u0410;
		r(ip4 , jp10) = u0510;
		r(ip5 , jp10) = u0610;
		r(ip6 , jp10) = u0710;
		r(ip7 , jp10) = u0810;
		r(ip8 , jp10) = u0910;
		r(ip9 , jp10) = u1010;
		r(ip10, jp10) = u1110;

		r(im1 , jp11) = u0011;
		r(i   , jp11) = u0111;
		r(ip1 , jp11) = u0211;
		r(ip2 , jp11) = u0311;
		r(ip3 , jp11) = u0411;
		r(ip4 , jp11) = u0511;
		r(ip5 , jp11) = u0611;
		r(ip6 , jp11) = u0711;
		r(ip7 , jp11) = u0811;
		r(ip8 , jp11) = u0911;
		r(ip9 , jp11) = u1011;
		r(ip10, jp11) = u1111;
		r(ip11, jp11) = u1211;

		r(im1 , jp12) = u0012;
		r(i   , jp12) = u0112;
		r(ip1 , jp12) = u0212;
		r(ip2 , jp12) = u0312;
		r(ip3 , jp12) = u0412;
		r(ip4 , jp12) = u0512;
		r(ip5 , jp12) = u0612;
		r(ip6 , jp12) = u0712;
		r(ip7 , jp12) = u0812;
		r(ip8 , jp12) = u0912;
		r(ip9 , jp12) = u1012;
		r(ip10, jp12) = u1112;
		r(ip11, jp12) = u1212;
		r(ip12, jp12) = u1312;

		r(im1 , jp13) = u0013;
		r(i   , jp13) = u0113;
		r(ip1 , jp13) = u0213;
		r(ip2 , jp13) = u0313;
		r(ip3 , jp13) = u0413;
		r(ip4 , jp13) = u0513;
		r(ip5 , jp13) = u0613;
		r(ip6 , jp13) = u0713;
		r(ip7 , jp13) = u0813;
		r(ip8 , jp13) = u0913;
		r(ip9 , jp13) = u1013;
		r(ip10, jp13) = u1113;
		r(ip11, jp13) = u1213;
		r(ip12, jp13) = u1313;
		r(ip13, jp13) = u1413;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);
			xx11 = r(i + 10, jj);
			xx12 = r(i + 11, jj);
			xx13 = r(i + 12, jj);
			xx14 = r(i + 13, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			xx10 = uu10;
		    uu10 = c10*xx10 + s10*xx11;
		    uu11 = c10*xx11 - s10*xx10;

			xx11 = uu11;
		    uu11 = c11*xx11 + s11*xx12;
		    uu12 = c11*xx12 - s11*xx11;

			xx12 = uu12;
		    uu12 = c12*xx12 + s12*xx13;
		    uu13 = c12*xx13 - s12*xx12;

			xx13 = uu13;
		    uu13 = c13*xx13 + s13*xx14;
		    uu14 = c13*xx14 - s13*xx13;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
			r(i + 10, jj) = uu11;
			r(i + 11, jj) = uu12;
			r(i + 12, jj) = uu13;
			r(i + 13, jj) = uu14;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=15.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize15_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110, x0011, x0111, x0211, x0311, x0411, x0511, x0611, x0711, x0811, x0911, x1011, x1111, x1211,
		   x0012, x0112, x0212, x0312, x0412, x0512, x0612, x0712, x0812, x0912, x1012, x1112, x1212, x1312, x0013,
		   x0113, x0213, x0313, x0413, x0513, x0613, x0713, x0813, x0913, x1013, x1113, x1213, x1313, x1413, x0014,
		   x0114, x0214, x0314, x0414, x0514, x0614, x0714, x0814, x0914, x1014, x1114, x1214, x1314, x1414, x1514;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110, u0011, u0111, u0211, u0311, u0411, u0511, u0611, u0711, u0811, u0911, u1011, u1111, u1211,
		   u0012, u0112, u0212, u0312, u0412, u0512, u0612, u0712, u0812, u0912, u1012, u1112, u1212, u1312, u0013,
		   u0113, u0213, u0313, u0413, u0513, u0613, u0713, u0813, u0913, u1013, u1113, u1213, u1313, u1413, u0014,
		   u0114, u0214, u0314, u0414, u0514, u0614, u0714, u0814, u0914, u1014, u1114, u1214, u1314, u1414, u1514,
		   u0015, u1515;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14, uu15;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10, ip11, ip12, ip13, ip14;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10, jp11, jp12, jp13, jp14;

	int nb = 15;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;
		ip11 = i + 11;
		ip12 = i + 12;
		ip13 = i + 13;
		ip14 = i + 14;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;
		jp11 = j + 11;
		jp12 = j + 12;
		jp13 = j + 13;
		jp14 = j + 14;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		x0010 = r(im1 , jp10);
		x0110 = r(i   , jp10);
		x0210 = r(ip1 , jp10);
		x0310 = r(ip2 , jp10);
		x0410 = r(ip3 , jp10);
		x0510 = r(ip4 , jp10);
		x0610 = r(ip5 , jp10);
		x0710 = r(ip6 , jp10);
		x0810 = r(ip7 , jp10);
		x0910 = r(ip8 , jp10);
		x1010 = r(ip9 , jp10);
		x1110 = r(ip10, jp10);

		x0011 = r(im1 , jp11);
		x0111 = r(i   , jp11);
		x0211 = r(ip1 , jp11);
		x0311 = r(ip2 , jp11);
		x0411 = r(ip3 , jp11);
		x0511 = r(ip4 , jp11);
		x0611 = r(ip5 , jp11);
		x0711 = r(ip6 , jp11);
		x0811 = r(ip7 , jp11);
		x0911 = r(ip8 , jp11);
		x1011 = r(ip9 , jp11);
		x1111 = r(ip10, jp11);
		x1211 = r(ip11, jp11);

		x0012 = r(im1 , jp12);
		x0112 = r(i   , jp12);
		x0212 = r(ip1 , jp12);
		x0312 = r(ip2 , jp12);
		x0412 = r(ip3 , jp12);
		x0512 = r(ip4 , jp12);
		x0612 = r(ip5 , jp12);
		x0712 = r(ip6 , jp12);
		x0812 = r(ip7 , jp12);
		x0912 = r(ip8 , jp12);
		x1012 = r(ip9 , jp12);
		x1112 = r(ip10, jp12);
		x1212 = r(ip11, jp12);
		x1312 = r(ip12, jp12);

		x0013 = r(im1 , jp13);
		x0113 = r(i   , jp13);
		x0213 = r(ip1 , jp13);
		x0313 = r(ip2 , jp13);
		x0413 = r(ip3 , jp13);
		x0513 = r(ip4 , jp13);
		x0613 = r(ip5 , jp13);
		x0713 = r(ip6 , jp13);
		x0813 = r(ip7 , jp13);
		x0913 = r(ip8 , jp13);
		x1013 = r(ip9 , jp13);
		x1113 = r(ip10, jp13);
		x1213 = r(ip11, jp13);
		x1313 = r(ip12, jp13);
		x1413 = r(ip13, jp13);

	  	x0014 = r(im1 , jp14);
		x0114 = r(i   , jp14);
		x0214 = r(ip1 , jp14);
		x0314 = r(ip2 , jp14);
		x0414 = r(ip3 , jp14);
		x0514 = r(ip4 , jp14);
		x0614 = r(ip5 , jp14);
		x0714 = r(ip6 , jp14);
		x0814 = r(ip7 , jp14);
		x0914 = r(ip8 , jp14);
		x1014 = r(ip9 , jp14);
		x1114 = r(ip10, jp14);
		x1214 = r(ip11, jp14);
		x1314 = r(ip12, jp14);
		x1414 = r(ip13, jp14);
		x1514 = r(ip14, jp14);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		u0011 = c0*x0011 + s0*x0111;
		u0111 = c0*x0111 - s0*x0011;

		u0012 = c0*x0012 + s0*x0112;
		u0112 = c0*x0112 - s0*x0012;

		u0013 = c0*x0013 + s0*x0113;
		u0113 = c0*x0113 - s0*x0013;

		u0014 = c0*x0014 + s0*x0114;
		u0114 = c0*x0114 - s0*x0014;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
		x0111 = u0111;
		x0112 = u0112;
		x0113 = u0113;
		x0114 = u0114;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

	    u0111 = c1*x0111 + s1*x0211;
	    u0211 = c1*x0211 - s1*x0111;

	    u0112 = c1*x0112 + s1*x0212;
	    u0212 = c1*x0212 - s1*x0112;

	    u0113 = c1*x0113 + s1*x0213;
	    u0213 = c1*x0213 - s1*x0113;

	    u0114 = c1*x0114 + s1*x0214;
	    u0214 = c1*x0214 - s1*x0114;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
		x0211 = u0211;
		x0212 = u0212;
		x0213 = u0213;
		x0214 = u0214;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

	    u0211 = c2*x0211 + s2*x0311;
	    u0311 = c2*x0311 - s2*x0211;

	    u0212 = c2*x0212 + s2*x0312;
	    u0312 = c2*x0312 - s2*x0212;

	    u0213 = c2*x0213 + s2*x0313;
	    u0313 = c2*x0313 - s2*x0213;

	    u0214 = c2*x0214 + s2*x0314;
	    u0314 = c2*x0314 - s2*x0214;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
		x0311 = u0311;
		x0312 = u0312;
		x0313 = u0313;
		x0314 = u0314;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

	    u0311 = c3*x0311 + s3*x0411;
	    u0411 = c3*x0411 - s3*x0311;

	    u0312 = c3*x0312 + s3*x0412;
	    u0412 = c3*x0412 - s3*x0312;

	    u0313 = c3*x0313 + s3*x0413;
	    u0413 = c3*x0413 - s3*x0313;

	    u0314 = c3*x0314 + s3*x0414;
	    u0414 = c3*x0414 - s3*x0314;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
		x0411 = u0411;
		x0412 = u0412;
		x0413 = u0413;
		x0414 = u0414;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

	    u0411 = c4*x0411 + s4*x0511;
	    u0511 = c4*x0511 - s4*x0411;

	    u0412 = c4*x0412 + s4*x0512;
	    u0512 = c4*x0512 - s4*x0412;

	    u0413 = c4*x0413 + s4*x0513;
	    u0513 = c4*x0513 - s4*x0413;

	    u0414 = c4*x0414 + s4*x0514;
	    u0514 = c4*x0514 - s4*x0414;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
		x0511 = u0511;
		x0512 = u0512;
		x0513 = u0513;
		x0514 = u0514;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

	    u0511 = c5*x0511 + s5*x0611;
	    u0611 = c5*x0611 - s5*x0511;

	    u0512 = c5*x0512 + s5*x0612;
	    u0612 = c5*x0612 - s5*x0512;

	    u0513 = c5*x0513 + s5*x0613;
	    u0613 = c5*x0613 - s5*x0513;

	    u0514 = c5*x0514 + s5*x0614;
	    u0614 = c5*x0614 - s5*x0514;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
		x0611 = u0611;
		x0612 = u0612;
		x0613 = u0613;
		x0614 = u0614;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

	    u0611 = c6*x0611 + s6*x0711;
	    u0711 = c6*x0711 - s6*x0611;

	    u0612 = c6*x0612 + s6*x0712;
	    u0712 = c6*x0712 - s6*x0612;

	    u0613 = c6*x0613 + s6*x0713;
	    u0713 = c6*x0713 - s6*x0613;

	    u0614 = c6*x0614 + s6*x0714;
	    u0714 = c6*x0714 - s6*x0614;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
		x0711 = u0711;
		x0712 = u0712;
		x0713 = u0713;
		x0714 = u0714;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

	    u0711 = c7*x0711 + s7*x0811;
	    u0811 = c7*x0811 - s7*x0711;

	    u0712 = c7*x0712 + s7*x0812;
	    u0812 = c7*x0812 - s7*x0712;

	    u0713 = c7*x0713 + s7*x0813;
	    u0813 = c7*x0813 - s7*x0713;

	    u0714 = c7*x0714 + s7*x0814;
	    u0814 = c7*x0814 - s7*x0714;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
		x0811 = u0811;
		x0812 = u0812;
		x0813 = u0813;
		x0814 = u0814;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

	    u0811 = c8*x0811 + s8*x0911;
	    u0911 = c8*x0911 - s8*x0811;

	    u0812 = c8*x0812 + s8*x0912;
	    u0912 = c8*x0912 - s8*x0812;

	    u0813 = c8*x0813 + s8*x0913;
	    u0913 = c8*x0913 - s8*x0813;

	    u0814 = c8*x0814 + s8*x0914;
	    u0914 = c8*x0914 - s8*x0814;

		x0909 = u0909;
		x0910 = u0910;
		x0911 = u0911;
		x0912 = u0912;
		x0913 = u0913;
		x0914 = u0914;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

	    u0911 = c9*x0911 + s9*x1011;
	    u1011 = c9*x1011 - s9*x0911;

	    u0912 = c9*x0912 + s9*x1012;
	    u1012 = c9*x1012 - s9*x0912;

	    u0913 = c9*x0913 + s9*x1013;
	    u1013 = c9*x1013 - s9*x0913;

	    u0914 = c9*x0914 + s9*x1014;
	    u1014 = c9*x1014 - s9*x0914;

		x1010 = u1010;
		x1011 = u1011;
		x1012 = u1012;
		x1013 = u1013;
		x1014 = u1014;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

	    u1011 = c10*x1011 + s10*x1111;
	    u1111 = c10*x1111 - s10*x1011;

	    u1012 = c10*x1012 + s10*x1112;
	    u1112 = c10*x1112 - s10*x1012;

	    u1013 = c10*x1013 + s10*x1113;
	    u1113 = c10*x1113 - s10*x1013;

	    u1014 = c10*x1014 + s10*x1114;
	    u1114 = c10*x1114 - s10*x1014;

		x1111 = u1111;
		x1112 = u1112;
		x1113 = u1113;
		x1114 = u1114;
	    genrot(&x1111, &x1211, &c11, &s11, &d);
	    u1111 = c11*x1111 + s11*x1211;
	    u1211 = c11*x1211 - s11*x1111;

	    u1112 = c11*x1112 + s11*x1212;
	    u1212 = c11*x1212 - s11*x1112;

	    u1113 = c11*x1113 + s11*x1213;
	    u1213 = c11*x1213 - s11*x1113;

	    u1114 = c11*x1114 + s11*x1214;
	    u1214 = c11*x1214 - s11*x1114;

		x1212 = u1212;
		x1213 = u1213;
		x1214 = u1214;
	    genrot(&x1212, &x1312, &c12, &s12, &d);
	    u1212 = c12*x1212 + s12*x1312;
	    u1312 = c12*x1312 - s12*x1212;

	    u1213 = c12*x1213 + s12*x1313;
	    u1313 = c12*x1313 - s12*x1213;

	    u1214 = c12*x1214 + s12*x1314;
	    u1314 = c12*x1314 - s12*x1214;

		x1313 = u1313;
		x1314 = u1314;
	    genrot(&x1313, &x1413, &c13, &s13, &d);
	    u1313 = c13*x1313 + s13*x1413;
	    u1413 = c13*x1413 - s13*x1313;

	    u1314 = c13*x1314 + s13*x1414;
	    u1414 = c13*x1414 - s13*x1314;

		x1414 = u1414;
	    genrot(&x1414, &x1514, &c14, &s14, &d);
	    u1414 = c14*x1414 + s14*x1514;
	    u1514 = c14*x1514 - s14*x1414;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		r(im1 , jp10) = u0010;
		r(i   , jp10) = u0110;
		r(ip1 , jp10) = u0210;
		r(ip2 , jp10) = u0310;
		r(ip3 , jp10) = u0410;
		r(ip4 , jp10) = u0510;
		r(ip5 , jp10) = u0610;
		r(ip6 , jp10) = u0710;
		r(ip7 , jp10) = u0810;
		r(ip8 , jp10) = u0910;
		r(ip9 , jp10) = u1010;
		r(ip10, jp10) = u1110;

		r(im1 , jp11) = u0011;
		r(i   , jp11) = u0111;
		r(ip1 , jp11) = u0211;
		r(ip2 , jp11) = u0311;
		r(ip3 , jp11) = u0411;
		r(ip4 , jp11) = u0511;
		r(ip5 , jp11) = u0611;
		r(ip6 , jp11) = u0711;
		r(ip7 , jp11) = u0811;
		r(ip8 , jp11) = u0911;
		r(ip9 , jp11) = u1011;
		r(ip10, jp11) = u1111;
		r(ip11, jp11) = u1211;

		r(im1 , jp12) = u0012;
		r(i   , jp12) = u0112;
		r(ip1 , jp12) = u0212;
		r(ip2 , jp12) = u0312;
		r(ip3 , jp12) = u0412;
		r(ip4 , jp12) = u0512;
		r(ip5 , jp12) = u0612;
		r(ip6 , jp12) = u0712;
		r(ip7 , jp12) = u0812;
		r(ip8 , jp12) = u0912;
		r(ip9 , jp12) = u1012;
		r(ip10, jp12) = u1112;
		r(ip11, jp12) = u1212;
		r(ip12, jp12) = u1312;

		r(im1 , jp13) = u0013;
		r(i   , jp13) = u0113;
		r(ip1 , jp13) = u0213;
		r(ip2 , jp13) = u0313;
		r(ip3 , jp13) = u0413;
		r(ip4 , jp13) = u0513;
		r(ip5 , jp13) = u0613;
		r(ip6 , jp13) = u0713;
		r(ip7 , jp13) = u0813;
		r(ip8 , jp13) = u0913;
		r(ip9 , jp13) = u1013;
		r(ip10, jp13) = u1113;
		r(ip11, jp13) = u1213;
		r(ip12, jp13) = u1313;
		r(ip13, jp13) = u1413;

		r(im1 , jp14) = u0014;
		r(i   , jp14) = u0114;
		r(ip1 , jp14) = u0214;
		r(ip2 , jp14) = u0314;
		r(ip3 , jp14) = u0414;
		r(ip4 , jp14) = u0514;
		r(ip5 , jp14) = u0614;
		r(ip6 , jp14) = u0714;
		r(ip7 , jp14) = u0814;
		r(ip8 , jp14) = u0914;
		r(ip9 , jp14) = u1014;
		r(ip10, jp14) = u1114;
		r(ip11, jp14) = u1214;
		r(ip12, jp14) = u1314;
		r(ip13, jp14) = u1414;
		r(ip14, jp14) = u1514;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14, uu15)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);
			xx11 = r(i + 10, jj);
			xx12 = r(i + 11, jj);
			xx13 = r(i + 12, jj);
			xx14 = r(i + 13, jj);
			xx15 = r(i + 14, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			xx10 = uu10;
		    uu10 = c10*xx10 + s10*xx11;
		    uu11 = c10*xx11 - s10*xx10;

			xx11 = uu11;
		    uu11 = c11*xx11 + s11*xx12;
		    uu12 = c11*xx12 - s11*xx11;

			xx12 = uu12;
		    uu12 = c12*xx12 + s12*xx13;
		    uu13 = c12*xx13 - s12*xx12;

			xx13 = uu13;
		    uu13 = c13*xx13 + s13*xx14;
		    uu14 = c13*xx14 - s13*xx13;

			xx14 = uu14;
		    uu14 = c14*xx14 + s14*xx15;
		    uu15 = c14*xx15 - s14*xx14;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
			r(i + 10, jj) = uu11;
			r(i + 11, jj) = uu12;
			r(i + 12, jj) = uu13;
			r(i + 13, jj) = uu14;
			r(i + 14, jj) = uu15;
		}
	}
}

/**
 * ORIGINAL Double-Loop OMP: Triangularizes the given matrix after a deletion update with NB=16.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize16_direct(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110, x0011, x0111, x0211, x0311, x0411, x0511, x0611, x0711, x0811, x0911, x1011, x1111, x1211,
		   x0012, x0112, x0212, x0312, x0412, x0512, x0612, x0712, x0812, x0912, x1012, x1112, x1212, x1312, x0013,
		   x0113, x0213, x0313, x0413, x0513, x0613, x0713, x0813, x0913, x1013, x1113, x1213, x1313, x1413, x0014,
		   x0114, x0214, x0314, x0414, x0514, x0614, x0714, x0814, x0914, x1014, x1114, x1214, x1314, x1414, x1514,
		   x0015, x0115, x0215, x0315, x0415, x0515, x0615, x0715, x0815, x0915, x1015, x1115, x1215, x1315, x1415,
		   x1515, x1615;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15, xx16;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110, u0011, u0111, u0211, u0311, u0411, u0511, u0611, u0711, u0811, u0911, u1011, u1111, u1211,
		   u0012, u0112, u0212, u0312, u0412, u0512, u0612, u0712, u0812, u0912, u1012, u1112, u1212, u1312, u0013,
		   u0113, u0213, u0313, u0413, u0513, u0613, u0713, u0813, u0913, u1013, u1113, u1213, u1313, u1413, u0014,
		   u0114, u0214, u0314, u0414, u0514, u0614, u0714, u0814, u0914, u1014, u1114, u1214, u1314, u1414, u1514,
		   u0015, u0115, u0215, u0315, u0415, u0515, u0615, u0715, u0815, u0915, u1015, u1115, u1215, u1315, u1415,
		   u1515, u1615;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14, uu15, uu16;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10, ip11, ip12, ip13, ip14, ip15;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10, jp11, jp12, jp13, jp14, jp15;

	int nb = 16;
	assert(nb == TRIANGULARIZE_NB);
	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;
		ip11 = i + 11;
		ip12 = i + 12;
		ip13 = i + 13;
		ip14 = i + 14;
		ip15 = i + 15;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;
		jp11 = j + 11;
		jp12 = j + 12;
		jp13 = j + 13;
		jp14 = j + 14;
		jp15 = j + 15;

		// make the stencil as easy as possible
		x0000 = r(im1, j);
		x0100 = r(i,   j);

		x0001 = r(im1, jp1);
		x0101 = r(i,   jp1);
		x0201 = r(ip1, jp1);

		x0002 = r(im1, jp2);
		x0102 = r(i,   jp2);
		x0202 = r(ip1, jp2);
		x0302 = r(ip2, jp2);

		x0003 = r(im1, jp3);
		x0103 = r(i,   jp3);
		x0203 = r(ip1, jp3);
		x0303 = r(ip2, jp3);
		x0403 = r(ip3, jp3);

		x0004 = r(im1, jp4);
		x0104 = r(i  , jp4);
		x0204 = r(ip1, jp4);
		x0304 = r(ip2, jp4);
		x0404 = r(ip3, jp4);
		x0504 = r(ip4, jp4);

		x0005 = r(im1, jp5);
		x0105 = r(i  , jp5);
		x0205 = r(ip1, jp5);
		x0305 = r(ip2, jp5);
		x0405 = r(ip3, jp5);
		x0505 = r(ip4, jp5);
		x0605 = r(ip5, jp5);

		x0006 = r(im1, jp6);
		x0106 = r(i  , jp6);
		x0206 = r(ip1, jp6);
		x0306 = r(ip2, jp6);
		x0406 = r(ip3, jp6);
		x0506 = r(ip4, jp6);
		x0606 = r(ip5, jp6);
		x0706 = r(ip6, jp6);

		x0007 = r(im1, jp7);
		x0107 = r(i  , jp7);
		x0207 = r(ip1, jp7);
		x0307 = r(ip2, jp7);
		x0407 = r(ip3, jp7);
		x0507 = r(ip4, jp7);
		x0607 = r(ip5, jp7);
		x0707 = r(ip6, jp7);
		x0807 = r(ip7, jp7);

		x0008 = r(im1, jp8);
		x0108 = r(i  , jp8);
		x0208 = r(ip1, jp8);
		x0308 = r(ip2, jp8);
		x0408 = r(ip3, jp8);
		x0508 = r(ip4, jp8);
		x0608 = r(ip5, jp8);
		x0708 = r(ip6, jp8);
		x0808 = r(ip7, jp8);
		x0908 = r(ip8, jp8);

		x0009 = r(im1, jp9);
		x0109 = r(i  , jp9);
		x0209 = r(ip1, jp9);
		x0309 = r(ip2, jp9);
		x0409 = r(ip3, jp9);
		x0509 = r(ip4, jp9);
		x0609 = r(ip5, jp9);
		x0709 = r(ip6, jp9);
		x0809 = r(ip7, jp9);
		x0909 = r(ip8, jp9);
		x1009 = r(ip9, jp9);

		x0010 = r(im1 , jp10);
		x0110 = r(i   , jp10);
		x0210 = r(ip1 , jp10);
		x0310 = r(ip2 , jp10);
		x0410 = r(ip3 , jp10);
		x0510 = r(ip4 , jp10);
		x0610 = r(ip5 , jp10);
		x0710 = r(ip6 , jp10);
		x0810 = r(ip7 , jp10);
		x0910 = r(ip8 , jp10);
		x1010 = r(ip9 , jp10);
		x1110 = r(ip10, jp10);

		x0011 = r(im1 , jp11);
		x0111 = r(i   , jp11);
		x0211 = r(ip1 , jp11);
		x0311 = r(ip2 , jp11);
		x0411 = r(ip3 , jp11);
		x0511 = r(ip4 , jp11);
		x0611 = r(ip5 , jp11);
		x0711 = r(ip6 , jp11);
		x0811 = r(ip7 , jp11);
		x0911 = r(ip8 , jp11);
		x1011 = r(ip9 , jp11);
		x1111 = r(ip10, jp11);
		x1211 = r(ip11, jp11);

		x0012 = r(im1 , jp12);
		x0112 = r(i   , jp12);
		x0212 = r(ip1 , jp12);
		x0312 = r(ip2 , jp12);
		x0412 = r(ip3 , jp12);
		x0512 = r(ip4 , jp12);
		x0612 = r(ip5 , jp12);
		x0712 = r(ip6 , jp12);
		x0812 = r(ip7 , jp12);
		x0912 = r(ip8 , jp12);
		x1012 = r(ip9 , jp12);
		x1112 = r(ip10, jp12);
		x1212 = r(ip11, jp12);
		x1312 = r(ip12, jp12);

		x0013 = r(im1 , jp13);
		x0113 = r(i   , jp13);
		x0213 = r(ip1 , jp13);
		x0313 = r(ip2 , jp13);
		x0413 = r(ip3 , jp13);
		x0513 = r(ip4 , jp13);
		x0613 = r(ip5 , jp13);
		x0713 = r(ip6 , jp13);
		x0813 = r(ip7 , jp13);
		x0913 = r(ip8 , jp13);
		x1013 = r(ip9 , jp13);
		x1113 = r(ip10, jp13);
		x1213 = r(ip11, jp13);
		x1313 = r(ip12, jp13);
		x1413 = r(ip13, jp13);

	  	x0014 = r(im1 , jp14);
		x0114 = r(i   , jp14);
		x0214 = r(ip1 , jp14);
		x0314 = r(ip2 , jp14);
		x0414 = r(ip3 , jp14);
		x0514 = r(ip4 , jp14);
		x0614 = r(ip5 , jp14);
		x0714 = r(ip6 , jp14);
		x0814 = r(ip7 , jp14);
		x0914 = r(ip8 , jp14);
		x1014 = r(ip9 , jp14);
		x1114 = r(ip10, jp14);
		x1214 = r(ip11, jp14);
		x1314 = r(ip12, jp14);
		x1414 = r(ip13, jp14);
		x1514 = r(ip14, jp14);

		x0015 = r(im1 , jp15);
		x0115 = r(i   , jp15);
		x0215 = r(ip1 , jp15);
		x0315 = r(ip2 , jp15);
		x0415 = r(ip3 , jp15);
		x0515 = r(ip4 , jp15);
		x0615 = r(ip5 , jp15);
		x0715 = r(ip6 , jp15);
		x0815 = r(ip7 , jp15);
		x0915 = r(ip8 , jp15);
		x1015 = r(ip9 , jp15);
		x1115 = r(ip10, jp15);
		x1215 = r(ip11, jp15);
		x1315 = r(ip12, jp15);
		x1415 = r(ip13, jp15);
		x1515 = r(ip14, jp15);
		x1615 = r(ip15, jp15);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		u0011 = c0*x0011 + s0*x0111;
		u0111 = c0*x0111 - s0*x0011;

		u0012 = c0*x0012 + s0*x0112;
		u0112 = c0*x0112 - s0*x0012;

		u0013 = c0*x0013 + s0*x0113;
		u0113 = c0*x0113 - s0*x0013;

		u0014 = c0*x0014 + s0*x0114;
		u0114 = c0*x0114 - s0*x0014;

		u0015 = c0*x0015 + s0*x0115;
		u0115 = c0*x0115 - s0*x0015;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
		x0111 = u0111;
		x0112 = u0112;
		x0113 = u0113;
		x0114 = u0114;
		x0115 = u0115;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

	    u0111 = c1*x0111 + s1*x0211;
	    u0211 = c1*x0211 - s1*x0111;

	    u0112 = c1*x0112 + s1*x0212;
	    u0212 = c1*x0212 - s1*x0112;

	    u0113 = c1*x0113 + s1*x0213;
	    u0213 = c1*x0213 - s1*x0113;

	    u0114 = c1*x0114 + s1*x0214;
	    u0214 = c1*x0214 - s1*x0114;

	    u0115 = c1*x0115 + s1*x0215;
	    u0215 = c1*x0215 - s1*x0115;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
		x0211 = u0211;
		x0212 = u0212;
		x0213 = u0213;
		x0214 = u0214;
		x0215 = u0215;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

	    u0211 = c2*x0211 + s2*x0311;
	    u0311 = c2*x0311 - s2*x0211;

	    u0212 = c2*x0212 + s2*x0312;
	    u0312 = c2*x0312 - s2*x0212;

	    u0213 = c2*x0213 + s2*x0313;
	    u0313 = c2*x0313 - s2*x0213;

	    u0214 = c2*x0214 + s2*x0314;
	    u0314 = c2*x0314 - s2*x0214;

	    u0215 = c2*x0215 + s2*x0315;
	    u0315 = c2*x0315 - s2*x0215;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
		x0311 = u0311;
		x0312 = u0312;
		x0313 = u0313;
		x0314 = u0314;
		x0315 = u0315;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

	    u0311 = c3*x0311 + s3*x0411;
	    u0411 = c3*x0411 - s3*x0311;

	    u0312 = c3*x0312 + s3*x0412;
	    u0412 = c3*x0412 - s3*x0312;

	    u0313 = c3*x0313 + s3*x0413;
	    u0413 = c3*x0413 - s3*x0313;

	    u0314 = c3*x0314 + s3*x0414;
	    u0414 = c3*x0414 - s3*x0314;

	    u0315 = c3*x0315 + s3*x0415;
	    u0415 = c3*x0415 - s3*x0315;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
		x0411 = u0411;
		x0412 = u0412;
		x0413 = u0413;
		x0414 = u0414;
		x0415 = u0415;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

	    u0411 = c4*x0411 + s4*x0511;
	    u0511 = c4*x0511 - s4*x0411;

	    u0412 = c4*x0412 + s4*x0512;
	    u0512 = c4*x0512 - s4*x0412;

	    u0413 = c4*x0413 + s4*x0513;
	    u0513 = c4*x0513 - s4*x0413;

	    u0414 = c4*x0414 + s4*x0514;
	    u0514 = c4*x0514 - s4*x0414;

	    u0415 = c4*x0415 + s4*x0515;
	    u0515 = c4*x0515 - s4*x0415;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
		x0511 = u0511;
		x0512 = u0512;
		x0513 = u0513;
		x0514 = u0514;
		x0515 = u0515;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

	    u0511 = c5*x0511 + s5*x0611;
	    u0611 = c5*x0611 - s5*x0511;

	    u0512 = c5*x0512 + s5*x0612;
	    u0612 = c5*x0612 - s5*x0512;

	    u0513 = c5*x0513 + s5*x0613;
	    u0613 = c5*x0613 - s5*x0513;

	    u0514 = c5*x0514 + s5*x0614;
	    u0614 = c5*x0614 - s5*x0514;

	    u0515 = c5*x0515 + s5*x0615;
	    u0615 = c5*x0615 - s5*x0515;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
		x0611 = u0611;
		x0612 = u0612;
		x0613 = u0613;
		x0614 = u0614;
		x0615 = u0615;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

	    u0611 = c6*x0611 + s6*x0711;
	    u0711 = c6*x0711 - s6*x0611;

	    u0612 = c6*x0612 + s6*x0712;
	    u0712 = c6*x0712 - s6*x0612;

	    u0613 = c6*x0613 + s6*x0713;
	    u0713 = c6*x0713 - s6*x0613;

	    u0614 = c6*x0614 + s6*x0714;
	    u0714 = c6*x0714 - s6*x0614;

	    u0615 = c6*x0615 + s6*x0715;
	    u0715 = c6*x0715 - s6*x0615;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
		x0711 = u0711;
		x0712 = u0712;
		x0713 = u0713;
		x0714 = u0714;
		x0715 = u0715;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

	    u0711 = c7*x0711 + s7*x0811;
	    u0811 = c7*x0811 - s7*x0711;

	    u0712 = c7*x0712 + s7*x0812;
	    u0812 = c7*x0812 - s7*x0712;

	    u0713 = c7*x0713 + s7*x0813;
	    u0813 = c7*x0813 - s7*x0713;

	    u0714 = c7*x0714 + s7*x0814;
	    u0814 = c7*x0814 - s7*x0714;

	    u0715 = c7*x0715 + s7*x0815;
	    u0815 = c7*x0815 - s7*x0715;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
		x0811 = u0811;
		x0812 = u0812;
		x0813 = u0813;
		x0814 = u0814;
		x0815 = u0815;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

	    u0811 = c8*x0811 + s8*x0911;
	    u0911 = c8*x0911 - s8*x0811;

	    u0812 = c8*x0812 + s8*x0912;
	    u0912 = c8*x0912 - s8*x0812;

	    u0813 = c8*x0813 + s8*x0913;
	    u0913 = c8*x0913 - s8*x0813;

	    u0814 = c8*x0814 + s8*x0914;
	    u0914 = c8*x0914 - s8*x0814;

	    u0815 = c8*x0815 + s8*x0915;
	    u0915 = c8*x0915 - s8*x0815;

		x0909 = u0909;
		x0910 = u0910;
		x0911 = u0911;
		x0912 = u0912;
		x0913 = u0913;
		x0914 = u0914;
		x0915 = u0915;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

	    u0911 = c9*x0911 + s9*x1011;
	    u1011 = c9*x1011 - s9*x0911;

	    u0912 = c9*x0912 + s9*x1012;
	    u1012 = c9*x1012 - s9*x0912;

	    u0913 = c9*x0913 + s9*x1013;
	    u1013 = c9*x1013 - s9*x0913;

	    u0914 = c9*x0914 + s9*x1014;
	    u1014 = c9*x1014 - s9*x0914;

	    u0915 = c9*x0915 + s9*x1015;
	    u1015 = c9*x1015 - s9*x0915;

		x1010 = u1010;
		x1011 = u1011;
		x1012 = u1012;
		x1013 = u1013;
		x1014 = u1014;
		x1015 = u1015;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

	    u1011 = c10*x1011 + s10*x1111;
	    u1111 = c10*x1111 - s10*x1011;

	    u1012 = c10*x1012 + s10*x1112;
	    u1112 = c10*x1112 - s10*x1012;

	    u1013 = c10*x1013 + s10*x1113;
	    u1113 = c10*x1113 - s10*x1013;

	    u1014 = c10*x1014 + s10*x1114;
	    u1114 = c10*x1114 - s10*x1014;

	    u1015 = c10*x1015 + s10*x1115;
	    u1115 = c10*x1115 - s10*x1015;

		x1111 = u1111;
		x1112 = u1112;
		x1113 = u1113;
		x1114 = u1114;
		x1115 = u1115;
	    genrot(&x1111, &x1211, &c11, &s11, &d);
	    u1111 = c11*x1111 + s11*x1211;
	    u1211 = c11*x1211 - s11*x1111;

	    u1112 = c11*x1112 + s11*x1212;
	    u1212 = c11*x1212 - s11*x1112;

	    u1113 = c11*x1113 + s11*x1213;
	    u1213 = c11*x1213 - s11*x1113;

	    u1114 = c11*x1114 + s11*x1214;
	    u1214 = c11*x1214 - s11*x1114;

	    u1115 = c11*x1115 + s11*x1215;
	    u1215 = c11*x1215 - s11*x1115;

		x1212 = u1212;
		x1213 = u1213;
		x1214 = u1214;
		x1215 = u1215;
	    genrot(&x1212, &x1312, &c12, &s12, &d);
	    u1212 = c12*x1212 + s12*x1312;
	    u1312 = c12*x1312 - s12*x1212;

	    u1213 = c12*x1213 + s12*x1313;
	    u1313 = c12*x1313 - s12*x1213;

	    u1214 = c12*x1214 + s12*x1314;
	    u1314 = c12*x1314 - s12*x1214;

	    u1215 = c12*x1215 + s12*x1315;
	    u1315 = c12*x1315 - s12*x1215;

		x1313 = u1313;
		x1314 = u1314;
		x1315 = u1315;
	    genrot(&x1313, &x1413, &c13, &s13, &d);
	    u1313 = c13*x1313 + s13*x1413;
	    u1413 = c13*x1413 - s13*x1313;

	    u1314 = c13*x1314 + s13*x1414;
	    u1414 = c13*x1414 - s13*x1314;

	    u1315 = c13*x1315 + s13*x1415;
	    u1415 = c13*x1415 - s13*x1315;

		x1414 = u1414;
		x1415 = u1415;
	    genrot(&x1414, &x1514, &c14, &s14, &d);
	    u1414 = c14*x1414 + s14*x1514;
	    u1514 = c14*x1514 - s14*x1414;

	    u1415 = c14*x1415 + s14*x1515;
	    u1515 = c14*x1515 - s14*x1415;

		x1515 = u1515;
	    genrot(&x1515, &x1615, &c15, &s15, &d);
	    u1515 = c15*x1515 + s15*x1615;
	    u1615 = c15*x1615 - s15*x1515;

		r(im1, j) 	= u0000;
		r(i,   j) 	= u0100;

		r(im1, jp1) = u0001;
		r(i,   jp1) = u0101;
		r(ip1, jp1) = u0201;

		r(im1, jp2) = u0002;
		r(i  , jp2) = u0102;
		r(ip1, jp2) = u0202;
		r(ip2, jp2) = u0302;

		r(im1, jp3) = u0003;
		r(i  , jp3) = u0103;
		r(ip1, jp3) = u0203;
		r(ip2, jp3) = u0303;
		r(ip3, jp3) = u0403;

		r(im1, jp4) = u0004;
		r(i  , jp4) = u0104;
		r(ip1, jp4) = u0204;
		r(ip2, jp4) = u0304;
		r(ip3, jp4) = u0404;
		r(ip4, jp4) = u0504;

		r(im1, jp5) = u0005;
		r(i  , jp5) = u0105;
		r(ip1, jp5) = u0205;
		r(ip2, jp5) = u0305;
		r(ip3, jp5) = u0405;
		r(ip4, jp5) = u0505;
		r(ip5, jp5) = u0605;

		r(im1, jp6) = u0006;
		r(i  , jp6) = u0106;
		r(ip1, jp6) = u0206;
		r(ip2, jp6) = u0306;
		r(ip3, jp6) = u0406;
		r(ip4, jp6) = u0506;
		r(ip5, jp6) = u0606;
		r(ip6, jp6) = u0706;

		r(im1, jp7) = u0007;
		r(i  , jp7) = u0107;
		r(ip1, jp7) = u0207;
		r(ip2, jp7) = u0307;
		r(ip3, jp7) = u0407;
		r(ip4, jp7) = u0507;
		r(ip5, jp7) = u0607;
		r(ip6, jp7) = u0707;
		r(ip7, jp7) = u0807;

		r(im1, jp8) = u0008;
		r(i  , jp8) = u0108;
		r(ip1, jp8) = u0208;
		r(ip2, jp8) = u0308;
		r(ip3, jp8) = u0408;
		r(ip4, jp8) = u0508;
		r(ip5, jp8) = u0608;
		r(ip6, jp8) = u0708;
		r(ip7, jp8) = u0808;
		r(ip8, jp8) = u0908;

		r(im1, jp9) = u0009;
		r(i  , jp9) = u0109;
		r(ip1, jp9) = u0209;
		r(ip2, jp9) = u0309;
		r(ip3, jp9) = u0409;
		r(ip4, jp9) = u0509;
		r(ip5, jp9) = u0609;
		r(ip6, jp9) = u0709;
		r(ip7, jp9) = u0809;
		r(ip8, jp9) = u0909;
		r(ip9, jp9) = u1009;

		r(im1 , jp10) = u0010;
		r(i   , jp10) = u0110;
		r(ip1 , jp10) = u0210;
		r(ip2 , jp10) = u0310;
		r(ip3 , jp10) = u0410;
		r(ip4 , jp10) = u0510;
		r(ip5 , jp10) = u0610;
		r(ip6 , jp10) = u0710;
		r(ip7 , jp10) = u0810;
		r(ip8 , jp10) = u0910;
		r(ip9 , jp10) = u1010;
		r(ip10, jp10) = u1110;

		r(im1 , jp11) = u0011;
		r(i   , jp11) = u0111;
		r(ip1 , jp11) = u0211;
		r(ip2 , jp11) = u0311;
		r(ip3 , jp11) = u0411;
		r(ip4 , jp11) = u0511;
		r(ip5 , jp11) = u0611;
		r(ip6 , jp11) = u0711;
		r(ip7 , jp11) = u0811;
		r(ip8 , jp11) = u0911;
		r(ip9 , jp11) = u1011;
		r(ip10, jp11) = u1111;
		r(ip11, jp11) = u1211;

		r(im1 , jp12) = u0012;
		r(i   , jp12) = u0112;
		r(ip1 , jp12) = u0212;
		r(ip2 , jp12) = u0312;
		r(ip3 , jp12) = u0412;
		r(ip4 , jp12) = u0512;
		r(ip5 , jp12) = u0612;
		r(ip6 , jp12) = u0712;
		r(ip7 , jp12) = u0812;
		r(ip8 , jp12) = u0912;
		r(ip9 , jp12) = u1012;
		r(ip10, jp12) = u1112;
		r(ip11, jp12) = u1212;
		r(ip12, jp12) = u1312;

		r(im1 , jp13) = u0013;
		r(i   , jp13) = u0113;
		r(ip1 , jp13) = u0213;
		r(ip2 , jp13) = u0313;
		r(ip3 , jp13) = u0413;
		r(ip4 , jp13) = u0513;
		r(ip5 , jp13) = u0613;
		r(ip6 , jp13) = u0713;
		r(ip7 , jp13) = u0813;
		r(ip8 , jp13) = u0913;
		r(ip9 , jp13) = u1013;
		r(ip10, jp13) = u1113;
		r(ip11, jp13) = u1213;
		r(ip12, jp13) = u1313;
		r(ip13, jp13) = u1413;

		r(im1 , jp14) = u0014;
		r(i   , jp14) = u0114;
		r(ip1 , jp14) = u0214;
		r(ip2 , jp14) = u0314;
		r(ip3 , jp14) = u0414;
		r(ip4 , jp14) = u0514;
		r(ip5 , jp14) = u0614;
		r(ip6 , jp14) = u0714;
		r(ip7 , jp14) = u0814;
		r(ip8 , jp14) = u0914;
		r(ip9 , jp14) = u1014;
		r(ip10, jp14) = u1114;
		r(ip11, jp14) = u1214;
		r(ip12, jp14) = u1314;
		r(ip13, jp14) = u1414;
		r(ip14, jp14) = u1514;

		r(im1 , jp15) = u0015;
		r(i   , jp15) = u0115;
		r(ip1 , jp15) = u0215;
		r(ip2 , jp15) = u0315;
		r(ip3 , jp15) = u0415;
		r(ip4 , jp15) = u0515;
		r(ip5 , jp15) = u0615;
		r(ip6 , jp15) = u0715;
		r(ip7 , jp15) = u0815;
		r(ip8 , jp15) = u0915;
		r(ip9 , jp15) = u1015;
		r(ip10, jp15) = u1115;
		r(ip11, jp15) = u1215;
		r(ip12, jp15) = u1315;
		r(ip13, jp15) = u1415;
		r(ip14, jp15) = u1515;
		r(ip15, jp15) = u1615;

		#pragma omp parallel for schedule(runtime) \
			private(xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15, xx16, uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14, uu15, uu16)
		for (int jj = (j + nb); jj < n; jj++) {
			xx0  = r(i - 1 , jj);
			xx1  = r(i     , jj);
			xx2  = r(i + 1 , jj);
			xx3  = r(i + 2 , jj);
			xx4  = r(i + 3 , jj);
			xx5  = r(i + 4 , jj);
			xx6  = r(i + 5 , jj);
			xx7  = r(i + 6 , jj);
			xx8  = r(i + 7 , jj);
			xx9  = r(i + 8 , jj);
			xx10 = r(i + 9 , jj);
			xx11 = r(i + 10, jj);
			xx12 = r(i + 11, jj);
			xx13 = r(i + 12, jj);
			xx14 = r(i + 13, jj);
			xx15 = r(i + 14, jj);
			xx16 = r(i + 15, jj);

		    uu0 = c0*xx0 + s0*xx1;
		    uu1 = c0*xx1 - s0*xx0;

			xx1 = uu1;
		    uu1 = c1*xx1 + s1*xx2;
		    uu2 = c1*xx2 - s1*xx1;

			xx2 = uu2;
		    uu2 = c2*xx2 + s2*xx3;
		    uu3 = c2*xx3 - s2*xx2;

			xx3 = uu3;
		    uu3 = c3*xx3 + s3*xx4;
		    uu4 = c3*xx4 - s3*xx3;

			xx4 = uu4;
		    uu4 = c4*xx4 + s4*xx5;
		    uu5 = c4*xx5 - s4*xx4;

			xx5  = uu5;
		    uu5 = c5*xx5 + s5*xx6;
		    uu6 = c5*xx6 - s5*xx5;

			xx6  = uu6;
		    uu6 = c6*xx6 + s6*xx7;
		    uu7 = c6*xx7 - s6*xx6;

			xx7  = uu7;
		    uu7 = c7*xx7 + s7*xx8;
		    uu8 = c7*xx8 - s7*xx7;

			xx8 = uu8;
		    uu8 = c8*xx8 + s8*xx9;
		    uu9 = c8*xx9 - s8*xx8;

			xx9  = uu9;
		    uu9  = c9*xx9 + s9*xx10;
		    uu10 = c9*xx10 - s9*xx9;

			xx10 = uu10;
		    uu10 = c10*xx10 + s10*xx11;
		    uu11 = c10*xx11 - s10*xx10;

			xx11 = uu11;
		    uu11 = c11*xx11 + s11*xx12;
		    uu12 = c11*xx12 - s11*xx11;

			xx12 = uu12;
		    uu12 = c12*xx12 + s12*xx13;
		    uu13 = c12*xx13 - s12*xx12;

			xx13 = uu13;
		    uu13 = c13*xx13 + s13*xx14;
		    uu14 = c13*xx14 - s13*xx13;

			xx14 = uu14;
		    uu14 = c14*xx14 + s14*xx15;
		    uu15 = c14*xx15 - s14*xx14;

			xx15 = uu15;
		    uu15 = c15*xx15 + s15*xx16;
		    uu16 = c15*xx16 - s15*xx15;

			r(i - 1 , jj) = uu0;
			r(i     , jj) = uu1;
			r(i + 1 , jj) = uu2;
			r(i + 2 , jj) = uu3;
			r(i + 3 , jj) = uu4;
			r(i + 4 , jj) = uu5;
			r(i + 5 , jj) = uu6;
			r(i + 6 , jj) = uu7;
			r(i + 7 , jj) = uu8;
			r(i + 8 , jj) = uu9;
			r(i + 9 , jj) = uu10;
			r(i + 10, jj) = uu11;
			r(i + 11, jj) = uu12;
			r(i + 12, jj) = uu13;
			r(i + 13, jj) = uu14;
			r(i + 14, jj) = uu15;
			r(i + 15, jj) = uu16;
		}
	}
}

/**
 * BLAS3 DGEMM: Triangularizes the given matrix after a deletion update with NB=16.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize16_blas3(int begin, int block, GENROT genrot) {
	LENTER;

	tsfo_matrix_tria<double>& r = *this;

	const int m = r.rows();
	const int n = r.cols();

	double x0000, x0001, x0002, x0003, x0004, x0005, x0006, x0007, x0100, x0101, x0102, x0103, x0104, x0105, x0106,
		   x0107, x0201, x0202, x0203, x0204, x0205, x0206, x0207, x0302, x0303, x0304, x0305, x0306, x0307, x0403,
		   x0404, x0405, x0406, x0407, x0504, x0505, x0506, x0507, x0605, x0606, x0607, x0706, x0707, x0807, x0008,
		   x0108, x0208, x0308, x0408, x0508, x0608, x0708, x0808, x0908, x0009, x0109, x0209, x0309, x0409, x0509,
		   x0609, x0709, x0809, x0909, x1009, x0010, x0110, x0210, x0310, x0410, x0510, x0610, x0710, x0810, x0910,
		   x1010, x1110, x0011, x0111, x0211, x0311, x0411, x0511, x0611, x0711, x0811, x0911, x1011, x1111, x1211,
		   x0012, x0112, x0212, x0312, x0412, x0512, x0612, x0712, x0812, x0912, x1012, x1112, x1212, x1312, x0013,
		   x0113, x0213, x0313, x0413, x0513, x0613, x0713, x0813, x0913, x1013, x1113, x1213, x1313, x1413, x0014,
		   x0114, x0214, x0314, x0414, x0514, x0614, x0714, x0814, x0914, x1014, x1114, x1214, x1314, x1414, x1514,
		   x0015, x0115, x0215, x0315, x0415, x0515, x0615, x0715, x0815, x0915, x1015, x1115, x1215, x1315, x1415,
		   x1515, x1615;
	double xx0, xx1, xx2, xx3, xx4, xx5, xx6, xx7, xx8, xx9, xx10, xx11, xx12, xx13, xx14, xx15, xx16;
	double u0000, u0001, u0002, u0003, u0004, u0005, u0006, u0007, u0100, u0101, u0102, u0103, u0104, u0105, u0106,
		   u0107, u0201, u0202, u0203, u0204, u0205, u0206, u0207, u0302, u0303, u0304, u0305, u0306, u0307, u0403,
		   u0404, u0405, u0406, u0407, u0504, u0505, u0506, u0507, u0605, u0606, u0607, u0706, u0707, u0807, u0008,
		   u0108, u0208, u0308, u0408, u0508, u0608, u0708, u0808, u0908, u0009, u0109, u0209, u0309, u0409, u0509,
		   u0609, u0709, u0809, u0909, u1009, u0010, u0110, u0210, u0310, u0410, u0510, u0610, u0710, u0810, u0910,
		   u1010, u1110, u0011, u0111, u0211, u0311, u0411, u0511, u0611, u0711, u0811, u0911, u1011, u1111, u1211,
		   u0012, u0112, u0212, u0312, u0412, u0512, u0612, u0712, u0812, u0912, u1012, u1112, u1212, u1312, u0013,
		   u0113, u0213, u0313, u0413, u0513, u0613, u0713, u0813, u0913, u1013, u1113, u1213, u1313, u1413, u0014,
		   u0114, u0214, u0314, u0414, u0514, u0614, u0714, u0814, u0914, u1014, u1114, u1214, u1314, u1414, u1514,
		   u0015, u0115, u0215, u0315, u0415, u0515, u0615, u0715, u0815, u0915, u1015, u1115, u1215, u1315, u1415,
		   u1515, u1615;
	double c0, c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14, c15, c16,
		   s0, s1, s2, s3, s4, s5, s6, s7, s8, s9, s10, s11, s12, s13, s14, s15, s16, d;
	double uu0, uu1, uu2, uu3, uu4, uu5, uu6, uu7, uu8, uu9, uu10, uu11, uu12, uu13, uu14, uu15, uu16;
	int im1, ip1, ip2, ip3, ip4, ip5, ip6, ip7, ip8, ip9, ip10, ip11, ip12, ip13, ip14, ip15;
	int jp1, jp2, jp3, jp4, jp5, jp6, jp7, jp8, jp9, jp10, jp11, jp12, jp13, jp14, jp15;

	int nb = 16;
	assert(nb == TRIANGULARIZE_NB);

	static tsfo_matrix<double> gc(nb + 1, nb + 1, 0.0);
	static tsfo_matrix<double> t1(nb + 1, 0);
	static const double alpha = 1.0;
	static const double beta  = 0.0;

	int n_iter = (n - begin) % nb == 0
			   ? (n - begin) / nb
			   : (n - begin) / nb + 1;
	int nb_end = begin + n_iter*nb;
	assert(nb_end >= n);

	// avoid uninitialized value accesses by zeroing out the ghost layer
//	memset(r.data() + m*n, 0, m*(nb_end - n + 1)*sizeof(T));

	int i = begin - block + 2;
	int j = begin;

	// blocked step
	for (int k = 0; k < n_iter; ++k, i += nb, j += nb) {
		im1  = i - 1;
		ip1  = i + 1;
		ip2  = i + 2;
		ip3  = i + 3;
		ip4  = i + 4;
		ip5  = i + 5;
		ip6  = i + 6;
		ip7  = i + 7;
		ip8  = i + 8;
		ip9  = i + 9;
		ip10 = i + 10;
		ip11 = i + 11;
		ip12 = i + 12;
		ip13 = i + 13;
		ip14 = i + 14;
		ip15 = i + 15;

		jp1  = j + 1;
		jp2  = j + 2;
		jp3  = j + 3;
		jp4  = j + 4;
		jp5  = j + 5;
		jp6  = j + 6;
		jp7  = j + 7;
		jp8  = j + 8;
		jp9  = j + 9;
		jp10 = j + 10;
		jp11 = j + 11;
		jp12 = j + 12;
		jp13 = j + 13;
		jp14 = j + 14;
		jp15 = j + 15;

		// make the stencil as easy as possible
		x0000 = r(im1, j); x0001 = r(im1, jp1); x0002 = r(im1, jp2); x0003 = r(im1, jp3); x0004 = r(im1, jp4); x0005 = r(im1, jp5); x0006 = r(im1, jp6); x0007 = r(im1, jp7); x0008 = r(im1, jp8); x0009 = r(im1, jp9); x0010 = r(im1 , jp10); x0011 = r(im1 , jp11); x0012 = r(im1 , jp12); x0013 = r(im1 , jp13); x0014 = r(im1 , jp14); x0015 = r(im1 , jp15);
		x0100 = r(i,   j); x0101 = r(i,   jp1); x0102 = r(i,   jp2); x0103 = r(i,   jp3); x0104 = r(i  , jp4); x0105 = r(i  , jp5); x0106 = r(i  , jp6); x0107 = r(i  , jp7); x0108 = r(i  , jp8); x0109 = r(i  , jp9); x0110 = r(i   , jp10); x0111 = r(i   , jp11); x0112 = r(i   , jp12); x0113 = r(i   , jp13); x0114 = r(i   , jp14); x0115 = r(i   , jp15);
						   x0201 = r(ip1, jp1); x0202 = r(ip1, jp2); x0203 = r(ip1, jp3); x0204 = r(ip1, jp4); x0205 = r(ip1, jp5); x0206 = r(ip1, jp6); x0207 = r(ip1, jp7); x0208 = r(ip1, jp8); x0209 = r(ip1, jp9); x0210 = r(ip1 , jp10); x0211 = r(ip1 , jp11); x0212 = r(ip1 , jp12); x0213 = r(ip1 , jp13); x0214 = r(ip1 , jp14); x0215 = r(ip1 , jp15);
						   	   	   	   	        x0302 = r(ip2, jp2); x0303 = r(ip2, jp3); x0304 = r(ip2, jp4); x0305 = r(ip2, jp5); x0306 = r(ip2, jp6); x0307 = r(ip2, jp7); x0308 = r(ip2, jp8); x0309 = r(ip2, jp9); x0310 = r(ip2 , jp10); x0311 = r(ip2 , jp11); x0312 = r(ip2 , jp12); x0313 = r(ip2 , jp13); x0314 = r(ip2 , jp14); x0315 = r(ip2 , jp15);
						   	   	   	   	          	  	  	   	   	 x0403 = r(ip3, jp3); x0404 = r(ip3, jp4); x0405 = r(ip3, jp5); x0406 = r(ip3, jp6); x0407 = r(ip3, jp7); x0408 = r(ip3, jp8); x0409 = r(ip3, jp9); x0410 = r(ip3 , jp10); x0411 = r(ip3 , jp11); x0412 = r(ip3 , jp12); x0413 = r(ip3 , jp13); x0414 = r(ip3 , jp14); x0415 = r(ip3 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	  	  	  x0504 = r(ip4, jp4); x0505 = r(ip4, jp5); x0506 = r(ip4, jp6); x0507 = r(ip4, jp7); x0508 = r(ip4, jp8); x0509 = r(ip4, jp9); x0510 = r(ip4 , jp10); x0511 = r(ip4 , jp11); x0512 = r(ip4 , jp12); x0513 = r(ip4 , jp13); x0514 = r(ip4 , jp14); x0515 = r(ip4 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	         	 	 	   x0605 = r(ip5, jp5); x0606 = r(ip5, jp6); x0607 = r(ip5, jp7); x0608 = r(ip5, jp8); x0609 = r(ip5, jp9); x0610 = r(ip5 , jp10); x0611 = r(ip5 , jp11); x0612 = r(ip5 , jp12); x0613 = r(ip5 , jp13); x0614 = r(ip5 , jp14); x0615 = r(ip5 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   			   	x0706 = r(ip6, jp6); x0707 = r(ip6, jp7); x0708 = r(ip6, jp8); x0709 = r(ip6, jp9); x0710 = r(ip6 , jp10); x0711 = r(ip6 , jp11); x0712 = r(ip6 , jp12); x0713 = r(ip6 , jp13); x0714 = r(ip6 , jp14); x0715 = r(ip6 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	 x0807 = r(ip7, jp7); x0808 = r(ip7, jp8); x0809 = r(ip7, jp9); x0810 = r(ip7 , jp10); x0811 = r(ip7 , jp11); x0812 = r(ip7 , jp12); x0813 = r(ip7 , jp13); x0814 = r(ip7 , jp14); x0815 = r(ip7 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  x0908 = r(ip8, jp8); x0909 = r(ip8, jp9); x0910 = r(ip8 , jp10); x0911 = r(ip8 , jp11); x0912 = r(ip8 , jp12); x0913 = r(ip8 , jp13); x0914 = r(ip8 , jp14); x0915 = r(ip8 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   x1009 = r(ip9, jp9); x1010 = r(ip9 , jp10); x1011 = r(ip9 , jp11); x1012 = r(ip9 , jp12); x1013 = r(ip9 , jp13); x1014 = r(ip9 , jp14); x1015 = r(ip9 , jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   	   	   	   	   	    x1110 = r(ip10, jp10); x1111 = r(ip10, jp11); x1112 = r(ip10, jp12); x1113 = r(ip10, jp13); x1114 = r(ip10, jp14); x1115 = r(ip10, jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   	   	   	   	   	    					   x1211 = r(ip11, jp11); x1212 = r(ip11, jp12); x1213 = r(ip11, jp13); x1214 = r(ip11, jp14); x1215 = r(ip11, jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   	   	   	   	   	    					   	   	   	   	   	   	  x1312 = r(ip12, jp12); x1313 = r(ip12, jp13); x1314 = r(ip12, jp14); x1315 = r(ip12, jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   	   	   	   	   	    					   	   	   	   	   	   	  	  	  	  	  	  	 x1413 = r(ip13, jp13); x1414 = r(ip13, jp14); x1415 = r(ip13, jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   	   	   	   	   	    					   	   	   	   	   	   	  	  	  	  	  	  	 	 	 	 	 	 	x1514 = r(ip14, jp14); x1515 = r(ip14, jp15);
						   	   	   	   	          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	      	  	  	  	  					   	   	   	   	   	    					   	   	   	   	   	   	  	  	  	  	  	  	 	 	 	 	 	 						   x1615 = r(ip15, jp15);

		// make nb steps ahead using registers only
		genrot(&x0000, &x0100, &c0, &s0, &d);
		u0000 = c0*x0000 + s0*x0100;
		u0100 = c0*x0100 - s0*x0000;

		u0001 = c0*x0001 + s0*x0101;
		u0101 = c0*x0101 - s0*x0001;

		u0002 = c0*x0002 + s0*x0102;
		u0102 = c0*x0102 - s0*x0002;

		u0003 = c0*x0003 + s0*x0103;
		u0103 = c0*x0103 - s0*x0003;

		u0004 = c0*x0004 + s0*x0104;
		u0104 = c0*x0104 - s0*x0004;

		u0005 = c0*x0005 + s0*x0105;
		u0105 = c0*x0105 - s0*x0005;

		u0006 = c0*x0006 + s0*x0106;
		u0106 = c0*x0106 - s0*x0006;

		u0007 = c0*x0007 + s0*x0107;
		u0107 = c0*x0107 - s0*x0007;

		u0008 = c0*x0008 + s0*x0108;
		u0108 = c0*x0108 - s0*x0008;

		u0009 = c0*x0009 + s0*x0109;
		u0109 = c0*x0109 - s0*x0009;

		u0010 = c0*x0010 + s0*x0110;
		u0110 = c0*x0110 - s0*x0010;

		u0011 = c0*x0011 + s0*x0111;
		u0111 = c0*x0111 - s0*x0011;

		u0012 = c0*x0012 + s0*x0112;
		u0112 = c0*x0112 - s0*x0012;

		u0013 = c0*x0013 + s0*x0113;
		u0113 = c0*x0113 - s0*x0013;

		u0014 = c0*x0014 + s0*x0114;
		u0114 = c0*x0114 - s0*x0014;

		u0015 = c0*x0015 + s0*x0115;
		u0115 = c0*x0115 - s0*x0015;

		x0101 = u0101;
		x0102 = u0102;
		x0103 = u0103;
		x0104 = u0104;
		x0105 = u0105;
		x0106 = u0106;
		x0107 = u0107;
		x0108 = u0108;
		x0109 = u0109;
		x0110 = u0110;
		x0111 = u0111;
		x0112 = u0112;
		x0113 = u0113;
		x0114 = u0114;
		x0115 = u0115;
	    genrot(&x0101, &x0201, &c1, &s1, &d);
	    u0101 = c1*x0101 + s1*x0201;
	    u0201 = c1*x0201 - s1*x0101;

	    u0102 = c1*x0102 + s1*x0202;
	    u0202 = c1*x0202 - s1*x0102;

	    u0103 = c1*x0103 + s1*x0203;
	    u0203 = c1*x0203 - s1*x0103;

	    u0104 = c1*x0104 + s1*x0204;
	    u0204 = c1*x0204 - s1*x0104;

	    u0105 = c1*x0105 + s1*x0205;
	    u0205 = c1*x0205 - s1*x0105;

	    u0106 = c1*x0106 + s1*x0206;
	    u0206 = c1*x0206 - s1*x0106;

	    u0107 = c1*x0107 + s1*x0207;
	    u0207 = c1*x0207 - s1*x0107;

	    u0108 = c1*x0108 + s1*x0208;
	    u0208 = c1*x0208 - s1*x0108;

	    u0109 = c1*x0109 + s1*x0209;
	    u0209 = c1*x0209 - s1*x0109;

	    u0110 = c1*x0110 + s1*x0210;
	    u0210 = c1*x0210 - s1*x0110;

	    u0111 = c1*x0111 + s1*x0211;
	    u0211 = c1*x0211 - s1*x0111;

	    u0112 = c1*x0112 + s1*x0212;
	    u0212 = c1*x0212 - s1*x0112;

	    u0113 = c1*x0113 + s1*x0213;
	    u0213 = c1*x0213 - s1*x0113;

	    u0114 = c1*x0114 + s1*x0214;
	    u0214 = c1*x0214 - s1*x0114;

	    u0115 = c1*x0115 + s1*x0215;
	    u0215 = c1*x0215 - s1*x0115;

		x0202 = u0202;
		x0203 = u0203;
		x0204 = u0204;
		x0205 = u0205;
		x0206 = u0206;
		x0207 = u0207;
		x0208 = u0208;
		x0209 = u0209;
		x0210 = u0210;
		x0211 = u0211;
		x0212 = u0212;
		x0213 = u0213;
		x0214 = u0214;
		x0215 = u0215;
	    genrot(&x0202, &x0302, &c2, &s2, &d);
	    u0202 = c2*x0202 + s2*x0302;
	    u0302 = c2*x0302 - s2*x0202;

	    u0203 = c2*x0203 + s2*x0303;
	    u0303 = c2*x0303 - s2*x0203;

	    u0204 = c2*x0204 + s2*x0304;
	    u0304 = c2*x0304 - s2*x0204;

	    u0205 = c2*x0205 + s2*x0305;
	    u0305 = c2*x0305 - s2*x0205;

	    u0206 = c2*x0206 + s2*x0306;
	    u0306 = c2*x0306 - s2*x0206;

	    u0207 = c2*x0207 + s2*x0307;
	    u0307 = c2*x0307 - s2*x0207;

	    u0208 = c2*x0208 + s2*x0308;
	    u0308 = c2*x0308 - s2*x0208;

	    u0209 = c2*x0209 + s2*x0309;
	    u0309 = c2*x0309 - s2*x0209;

	    u0210 = c2*x0210 + s2*x0310;
	    u0310 = c2*x0310 - s2*x0210;

	    u0211 = c2*x0211 + s2*x0311;
	    u0311 = c2*x0311 - s2*x0211;

	    u0212 = c2*x0212 + s2*x0312;
	    u0312 = c2*x0312 - s2*x0212;

	    u0213 = c2*x0213 + s2*x0313;
	    u0313 = c2*x0313 - s2*x0213;

	    u0214 = c2*x0214 + s2*x0314;
	    u0314 = c2*x0314 - s2*x0214;

	    u0215 = c2*x0215 + s2*x0315;
	    u0315 = c2*x0315 - s2*x0215;

		x0303 = u0303;
		x0304 = u0304;
		x0305 = u0305;
		x0306 = u0306;
		x0307 = u0307;
		x0308 = u0308;
		x0309 = u0309;
		x0310 = u0310;
		x0311 = u0311;
		x0312 = u0312;
		x0313 = u0313;
		x0314 = u0314;
		x0315 = u0315;
	    genrot(&x0303, &x0403, &c3, &s3, &d);
	    u0303 = c3*x0303 + s3*x0403;
	    u0403 = c3*x0403 - s3*x0303;

	    u0304 = c3*x0304 + s3*x0404;
	    u0404 = c3*x0404 - s3*x0304;

	    u0305 = c3*x0305 + s3*x0405;
	    u0405 = c3*x0405 - s3*x0305;

	    u0306 = c3*x0306 + s3*x0406;
	    u0406 = c3*x0406 - s3*x0306;

	    u0307 = c3*x0307 + s3*x0407;
	    u0407 = c3*x0407 - s3*x0307;

	    u0308 = c3*x0308 + s3*x0408;
	    u0408 = c3*x0408 - s3*x0308;

	    u0309 = c3*x0309 + s3*x0409;
	    u0409 = c3*x0409 - s3*x0309;

	    u0310 = c3*x0310 + s3*x0410;
	    u0410 = c3*x0410 - s3*x0310;

	    u0311 = c3*x0311 + s3*x0411;
	    u0411 = c3*x0411 - s3*x0311;

	    u0312 = c3*x0312 + s3*x0412;
	    u0412 = c3*x0412 - s3*x0312;

	    u0313 = c3*x0313 + s3*x0413;
	    u0413 = c3*x0413 - s3*x0313;

	    u0314 = c3*x0314 + s3*x0414;
	    u0414 = c3*x0414 - s3*x0314;

	    u0315 = c3*x0315 + s3*x0415;
	    u0415 = c3*x0415 - s3*x0315;

		x0404 = u0404;
		x0405 = u0405;
		x0406 = u0406;
		x0407 = u0407;
		x0408 = u0408;
		x0409 = u0409;
		x0410 = u0410;
		x0411 = u0411;
		x0412 = u0412;
		x0413 = u0413;
		x0414 = u0414;
		x0415 = u0415;
	    genrot(&x0404, &x0504, &c4, &s4, &d);
	    u0404 = c4*x0404 + s4*x0504;
	    u0504 = c4*x0504 - s4*x0404;

	    u0405 = c4*x0405 + s4*x0505;
	    u0505 = c4*x0505 - s4*x0405;

	    u0406 = c4*x0406 + s4*x0506;
	    u0506 = c4*x0506 - s4*x0406;

	    u0407 = c4*x0407 + s4*x0507;
	    u0507 = c4*x0507 - s4*x0407;

	    u0408 = c4*x0408 + s4*x0508;
	    u0508 = c4*x0508 - s4*x0408;

	    u0409 = c4*x0409 + s4*x0509;
	    u0509 = c4*x0509 - s4*x0409;

	    u0410 = c4*x0410 + s4*x0510;
	    u0510 = c4*x0510 - s4*x0410;

	    u0411 = c4*x0411 + s4*x0511;
	    u0511 = c4*x0511 - s4*x0411;

	    u0412 = c4*x0412 + s4*x0512;
	    u0512 = c4*x0512 - s4*x0412;

	    u0413 = c4*x0413 + s4*x0513;
	    u0513 = c4*x0513 - s4*x0413;

	    u0414 = c4*x0414 + s4*x0514;
	    u0514 = c4*x0514 - s4*x0414;

	    u0415 = c4*x0415 + s4*x0515;
	    u0515 = c4*x0515 - s4*x0415;

		x0505 = u0505;
		x0506 = u0506;
		x0507 = u0507;
		x0508 = u0508;
		x0509 = u0509;
		x0510 = u0510;
		x0511 = u0511;
		x0512 = u0512;
		x0513 = u0513;
		x0514 = u0514;
		x0515 = u0515;
	    genrot(&x0505, &x0605, &c5, &s5, &d);
	    u0505 = c5*x0505 + s5*x0605;
	    u0605 = c5*x0605 - s5*x0505;

	    u0506 = c5*x0506 + s5*x0606;
	    u0606 = c5*x0606 - s5*x0506;

	    u0507 = c5*x0507 + s5*x0607;
	    u0607 = c5*x0607 - s5*x0507;

	    u0508 = c5*x0508 + s5*x0608;
	    u0608 = c5*x0608 - s5*x0508;

	    u0509 = c5*x0509 + s5*x0609;
	    u0609 = c5*x0609 - s5*x0509;

	    u0510 = c5*x0510 + s5*x0610;
	    u0610 = c5*x0610 - s5*x0510;

	    u0511 = c5*x0511 + s5*x0611;
	    u0611 = c5*x0611 - s5*x0511;

	    u0512 = c5*x0512 + s5*x0612;
	    u0612 = c5*x0612 - s5*x0512;

	    u0513 = c5*x0513 + s5*x0613;
	    u0613 = c5*x0613 - s5*x0513;

	    u0514 = c5*x0514 + s5*x0614;
	    u0614 = c5*x0614 - s5*x0514;

	    u0515 = c5*x0515 + s5*x0615;
	    u0615 = c5*x0615 - s5*x0515;

		x0606 = u0606;
		x0607 = u0607;
		x0608 = u0608;
		x0609 = u0609;
		x0610 = u0610;
		x0611 = u0611;
		x0612 = u0612;
		x0613 = u0613;
		x0614 = u0614;
		x0615 = u0615;
	    genrot(&x0606, &x0706, &c6, &s6, &d);
	    u0606 = c6*x0606 + s6*x0706;
	    u0706 = c6*x0706 - s6*x0606;

	    u0607 = c6*x0607 + s6*x0707;
	    u0707 = c6*x0707 - s6*x0607;

	    u0608 = c6*x0608 + s6*x0708;
	    u0708 = c6*x0708 - s6*x0608;

	    u0609 = c6*x0609 + s6*x0709;
	    u0709 = c6*x0709 - s6*x0609;

	    u0610 = c6*x0610 + s6*x0710;
	    u0710 = c6*x0710 - s6*x0610;

	    u0611 = c6*x0611 + s6*x0711;
	    u0711 = c6*x0711 - s6*x0611;

	    u0612 = c6*x0612 + s6*x0712;
	    u0712 = c6*x0712 - s6*x0612;

	    u0613 = c6*x0613 + s6*x0713;
	    u0713 = c6*x0713 - s6*x0613;

	    u0614 = c6*x0614 + s6*x0714;
	    u0714 = c6*x0714 - s6*x0614;

	    u0615 = c6*x0615 + s6*x0715;
	    u0715 = c6*x0715 - s6*x0615;

		x0707 = u0707;
		x0708 = u0708;
		x0709 = u0709;
		x0710 = u0710;
		x0711 = u0711;
		x0712 = u0712;
		x0713 = u0713;
		x0714 = u0714;
		x0715 = u0715;
	    genrot(&x0707, &x0807, &c7, &s7, &d);
	    u0707 = c7*x0707 + s7*x0807;
	    u0807 = c7*x0807 - s7*x0707;

	    u0708 = c7*x0708 + s7*x0808;
	    u0808 = c7*x0808 - s7*x0708;

	    u0709 = c7*x0709 + s7*x0809;
	    u0809 = c7*x0809 - s7*x0709;

	    u0710 = c7*x0710 + s7*x0810;
	    u0810 = c7*x0810 - s7*x0710;

	    u0711 = c7*x0711 + s7*x0811;
	    u0811 = c7*x0811 - s7*x0711;

	    u0712 = c7*x0712 + s7*x0812;
	    u0812 = c7*x0812 - s7*x0712;

	    u0713 = c7*x0713 + s7*x0813;
	    u0813 = c7*x0813 - s7*x0713;

	    u0714 = c7*x0714 + s7*x0814;
	    u0814 = c7*x0814 - s7*x0714;

	    u0715 = c7*x0715 + s7*x0815;
	    u0815 = c7*x0815 - s7*x0715;

		x0808 = u0808;
		x0809 = u0809;
		x0810 = u0810;
		x0811 = u0811;
		x0812 = u0812;
		x0813 = u0813;
		x0814 = u0814;
		x0815 = u0815;
	    genrot(&x0808, &x0908, &c8, &s8, &d);
	    u0808 = c8*x0808 + s8*x0908;
	    u0908 = c8*x0908 - s8*x0808;

	    u0809 = c8*x0809 + s8*x0909;
	    u0909 = c8*x0909 - s8*x0809;

	    u0810 = c8*x0810 + s8*x0910;
	    u0910 = c8*x0910 - s8*x0810;

	    u0811 = c8*x0811 + s8*x0911;
	    u0911 = c8*x0911 - s8*x0811;

	    u0812 = c8*x0812 + s8*x0912;
	    u0912 = c8*x0912 - s8*x0812;

	    u0813 = c8*x0813 + s8*x0913;
	    u0913 = c8*x0913 - s8*x0813;

	    u0814 = c8*x0814 + s8*x0914;
	    u0914 = c8*x0914 - s8*x0814;

	    u0815 = c8*x0815 + s8*x0915;
	    u0915 = c8*x0915 - s8*x0815;

		x0909 = u0909;
		x0910 = u0910;
		x0911 = u0911;
		x0912 = u0912;
		x0913 = u0913;
		x0914 = u0914;
		x0915 = u0915;
	    genrot(&x0909, &x1009, &c9, &s9, &d);
	    u0909 = c9*x0909 + s9*x1009;
	    u1009 = c9*x1009 - s9*x0909;

	    u0910 = c9*x0910 + s9*x1010;
	    u1010 = c9*x1010 - s9*x0910;

	    u0911 = c9*x0911 + s9*x1011;
	    u1011 = c9*x1011 - s9*x0911;

	    u0912 = c9*x0912 + s9*x1012;
	    u1012 = c9*x1012 - s9*x0912;

	    u0913 = c9*x0913 + s9*x1013;
	    u1013 = c9*x1013 - s9*x0913;

	    u0914 = c9*x0914 + s9*x1014;
	    u1014 = c9*x1014 - s9*x0914;

	    u0915 = c9*x0915 + s9*x1015;
	    u1015 = c9*x1015 - s9*x0915;

		x1010 = u1010;
		x1011 = u1011;
		x1012 = u1012;
		x1013 = u1013;
		x1014 = u1014;
		x1015 = u1015;
	    genrot(&x1010, &x1110, &c10, &s10, &d);
	    u1010 = c10*x1010 + s10*x1110;
	    u1110 = c10*x1110 - s10*x1010;

	    u1011 = c10*x1011 + s10*x1111;
	    u1111 = c10*x1111 - s10*x1011;

	    u1012 = c10*x1012 + s10*x1112;
	    u1112 = c10*x1112 - s10*x1012;

	    u1013 = c10*x1013 + s10*x1113;
	    u1113 = c10*x1113 - s10*x1013;

	    u1014 = c10*x1014 + s10*x1114;
	    u1114 = c10*x1114 - s10*x1014;

	    u1015 = c10*x1015 + s10*x1115;
	    u1115 = c10*x1115 - s10*x1015;

		x1111 = u1111;
		x1112 = u1112;
		x1113 = u1113;
		x1114 = u1114;
		x1115 = u1115;
	    genrot(&x1111, &x1211, &c11, &s11, &d);
	    u1111 = c11*x1111 + s11*x1211;
	    u1211 = c11*x1211 - s11*x1111;

	    u1112 = c11*x1112 + s11*x1212;
	    u1212 = c11*x1212 - s11*x1112;

	    u1113 = c11*x1113 + s11*x1213;
	    u1213 = c11*x1213 - s11*x1113;

	    u1114 = c11*x1114 + s11*x1214;
	    u1214 = c11*x1214 - s11*x1114;

	    u1115 = c11*x1115 + s11*x1215;
	    u1215 = c11*x1215 - s11*x1115;

		x1212 = u1212;
		x1213 = u1213;
		x1214 = u1214;
		x1215 = u1215;
	    genrot(&x1212, &x1312, &c12, &s12, &d);
	    u1212 = c12*x1212 + s12*x1312;
	    u1312 = c12*x1312 - s12*x1212;

	    u1213 = c12*x1213 + s12*x1313;
	    u1313 = c12*x1313 - s12*x1213;

	    u1214 = c12*x1214 + s12*x1314;
	    u1314 = c12*x1314 - s12*x1214;

	    u1215 = c12*x1215 + s12*x1315;
	    u1315 = c12*x1315 - s12*x1215;

		x1313 = u1313;
		x1314 = u1314;
		x1315 = u1315;
	    genrot(&x1313, &x1413, &c13, &s13, &d);
	    u1313 = c13*x1313 + s13*x1413;
	    u1413 = c13*x1413 - s13*x1313;

	    u1314 = c13*x1314 + s13*x1414;
	    u1414 = c13*x1414 - s13*x1314;

	    u1315 = c13*x1315 + s13*x1415;
	    u1415 = c13*x1415 - s13*x1315;

		x1414 = u1414;
		x1415 = u1415;
	    genrot(&x1414, &x1514, &c14, &s14, &d);
	    u1414 = c14*x1414 + s14*x1514;
	    u1514 = c14*x1514 - s14*x1414;

	    u1415 = c14*x1415 + s14*x1515;
	    u1515 = c14*x1515 - s14*x1415;

		x1515 = u1515;
	    genrot(&x1515, &x1615, &c15, &s15, &d);
	    u1515 = c15*x1515 + s15*x1615;
	    u1615 = c15*x1615 - s15*x1515;

		r(im1, j) = u0000; r(im1, jp1) = u0001; r(im1, jp2) = u0002; r(im1, jp3) = u0003; r(im1, jp4) = u0004; r(im1, jp5) = u0005; r(im1, jp6) = u0006; r(im1, jp7) = u0007; r(im1, jp8) = u0008; r(im1, jp9) = u0009; r(im1 , jp10) = u0010; r(im1 , jp11) = u0011; r(im1 , jp12) = u0012; r(im1 , jp13) = u0013; r(im1 , jp14) = u0014; r(im1 , jp15) = u0015;
		r(i,   j) = u0100; r(i,   jp1) = u0101; r(i  , jp2) = u0102; r(i  , jp3) = u0103; r(i  , jp4) = u0104; r(i  , jp5) = u0105; r(i  , jp6) = u0106; r(i  , jp7) = u0107; r(i  , jp8) = u0108; r(i  , jp9) = u0109; r(i   , jp10) = u0110; r(i   , jp11) = u0111; r(i   , jp12) = u0112; r(i   , jp13) = u0113; r(i   , jp14) = u0114; r(i   , jp15) = u0115;
						   r(ip1, jp1) = u0201; r(ip1, jp2) = u0202; r(ip1, jp3) = u0203; r(ip1, jp4) = u0204; r(ip1, jp5) = u0205; r(ip1, jp6) = u0206; r(ip1, jp7) = u0207; r(ip1, jp8) = u0208; r(ip1, jp9) = u0209; r(ip1 , jp10) = u0210; r(ip1 , jp11) = u0211; r(ip1 , jp12) = u0212; r(ip1 , jp13) = u0213; r(ip1 , jp14) = u0214; r(ip1 , jp15) = u0215;
						                    	r(ip2, jp2) = u0302; r(ip2, jp3) = u0303; r(ip2, jp4) = u0304; r(ip2, jp5) = u0305; r(ip2, jp6) = u0306; r(ip2, jp7) = u0307; r(ip2, jp8) = u0308; r(ip2, jp9) = u0309; r(ip2 , jp10) = u0310; r(ip2 , jp11) = u0311; r(ip2 , jp12) = u0312; r(ip2 , jp13) = u0313; r(ip2 , jp14) = u0314; r(ip2 , jp15) = u0315;
						                          	  	  	   	     r(ip3, jp3) = u0403; r(ip3, jp4) = u0404; r(ip3, jp5) = u0405; r(ip3, jp6) = u0406; r(ip3, jp7) = u0407; r(ip3, jp8) = u0408; r(ip3, jp9) = u0409; r(ip3 , jp10) = u0410; r(ip3 , jp11) = u0411; r(ip3 , jp12) = u0412; r(ip3 , jp13) = u0413; r(ip3 , jp14) = u0414; r(ip3 , jp15) = u0415;
						                          	  	  	  	  	  	 	 	  	  	  r(ip4, jp4) = u0504; r(ip4, jp5) = u0505; r(ip4, jp6) = u0506; r(ip4, jp7) = u0507; r(ip4, jp8) = u0508; r(ip4, jp9) = u0509; r(ip4 , jp10) = u0510; r(ip4 , jp11) = u0511; r(ip4 , jp12) = u0512; r(ip4 , jp13) = u0513; r(ip4 , jp14) = u0514; r(ip4 , jp15) = u0515;
						                          	  	  	  	  	  	 	 	 	 	 	 		 	 	   r(ip5, jp5) = u0605; r(ip5, jp6) = u0606; r(ip5, jp7) = u0607; r(ip5, jp8) = u0608; r(ip5, jp9) = u0609; r(ip5 , jp10) = u0610; r(ip5 , jp11) = u0611; r(ip5 , jp12) = u0612; r(ip5 , jp13) = u0613; r(ip5 , jp14) = u0614; r(ip5 , jp15) = u0615;
						                          	  	  	  	  	  	 	 	 	 	 	 		 	 	 	 	 				r(ip6, jp6) = u0706; r(ip6, jp7) = u0707; r(ip6, jp8) = u0708; r(ip6, jp9) = u0709; r(ip6 , jp10) = u0710; r(ip6 , jp11) = u0711; r(ip6 , jp12) = u0712; r(ip6 , jp13) = u0713; r(ip6 , jp14) = u0714; r(ip6 , jp15) = u0715;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	     r(ip7, jp7) = u0807; r(ip7, jp8) = u0808; r(ip7, jp9) = u0809; r(ip7 , jp10) = u0810; r(ip7 , jp11) = u0811; r(ip7 , jp12) = u0812; r(ip7 , jp13) = u0813; r(ip7 , jp14) = u0814; r(ip7 , jp15) = u0815;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  r(ip8, jp8) = u0908; r(ip8, jp9) = u0909; r(ip8 , jp10) = u0910; r(ip8 , jp11) = u0911; r(ip8 , jp12) = u0912; r(ip8 , jp13) = u0913; r(ip8 , jp14) = u0914; r(ip8 , jp15) = u0915;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   r(ip9, jp9) = u1009; r(ip9 , jp10) = u1010; r(ip9 , jp11) = u1011; r(ip9 , jp12) = u1012; r(ip9 , jp13) = u1013; r(ip9 , jp14) = u1014; r(ip9 , jp15) = u1015;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   	   	   	   	   	    r(ip10, jp10) = u1110; r(ip10, jp11) = u1111; r(ip10, jp12) = u1112; r(ip10, jp13) = u1113; r(ip10, jp14) = u1114; r(ip10, jp15) = u1115;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   	   	   	   	   	    					   r(ip11, jp11) = u1211; r(ip11, jp12) = u1212; r(ip11, jp13) = u1213; r(ip11, jp14) = u1214; r(ip11, jp15) = u1215;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   	   	   	   	   	    					   	   	   	   	   	      r(ip12, jp12) = u1312; r(ip12, jp13) = u1313; r(ip12, jp14) = u1314; r(ip12, jp15) = u1315;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   	   	   	   	   	    					   	   	   	   	   	      	  	  	  	  	  	 r(ip13, jp13) = u1413; r(ip13, jp14) = u1414; r(ip13, jp15) = u1415;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   	   	   	   	   	    					   	   	   	   	   	      	  	  	  	  	  	 	 	 	 	 	 	r(ip14, jp14) = u1514; r(ip14, jp15) = u1515;
						                          	  	  	  	  	  	 	 	 	 	 	 						   					   	   	   	   	   	  	  	  	  	  	   	   	   	   	   	   	   	   	   	    					   	   	   	   	   	      	  	  	  	  	  	 	 	 	 	 	 						   r(ip15, jp15) = u1615;
 		if (n - j - nb > 0) {
             // build the accumulated Givens orthogonal matrix
 			gc(0 , 0)=c0;													  gc(0 , 1)=s0;
 			gc(1 , 0)=-c1*s0;												  gc(1 , 1)=c0*c1;													gc(1, 2)=s1;
 			gc(2 , 0)=c2*s0*s1;												  gc(2 , 1)=-c0*c2*s1;												gc(2, 2)=c1*c2;													gc(2, 3)=s2;
 			gc(3 , 0)=-c3*s0*s1*s2;											  gc(3 , 1)=c0*c3*s1*s2;											gc(3, 2)=-c1*c3*s2;												gc(3, 3)=c2*c3;												gc(3, 4)=s3;
 			gc(4 , 0)=c4*s0*s1*s2*s3;										  gc(4 , 1)=-c0*c4*s1*s2*s3;										gc(4, 2)=c1*c4*s2*s3;											gc(4, 3)=-c2*c4*s3;											gc(4, 4)=c3*c4;											gc(4, 5)=s4;
 			gc(5 , 0)=-c5*s0*s1*s2*s3*s4;									  gc(5 , 1)=c0*c5*s1*s2*s3*s4;										gc(5, 2)=-c1*c5*s2*s3*s4;										gc(5, 3)=c2*c5*s3*s4;										gc(5, 4)=-c3*c5*s4;										gc(5, 5)=c4*c5;											gc(5, 6)=s5;
 			gc(6 , 0)=c6*s0*s1*s2*s3*s4*s5;									  gc(6 , 1)=-c0*c6*s1*s2*s3*s4*s5;									gc(6, 2)=c1*c6*s2*s3*s4*s5;										gc(6, 3)=-c2*c6*s3*s4*s5;									gc(6, 4)=c3*c6*s4*s5;									gc(6, 5)=-c4*c6*s5;										gc(6, 6)=c5*c6;										gc(6, 7)=s6;
 			gc(7 , 0)=-c7*s0*s1*s2*s3*s4*s5*s6;								  gc(7 , 1)=c0*c7*s1*s2*s3*s4*s5*s6;								gc(7, 2)=-c1*c7*s2*s3*s4*s5*s6;									gc(7, 3)=c2*c7*s3*s4*s5*s6;									gc(7, 4)=-c3*c7*s4*s5*s6;								gc(7, 5)=c4*c7*s5*s6;									gc(7, 6)=-c5*c7*s6;									gc(7, 7)=c6*c7;									gc(7, 8)=s7;
 			gc(8 , 0)=c8*s0*s1*s2*s3*s4*s5*s6*s7;							  gc(8 , 1)=-c0*c8*s1*s2*s3*s4*s5*s6*s7;							gc(8, 2)=c1*c8*s2*s3*s4*s5*s6*s7;								gc(8, 3)=-c2*c8*s3*s4*s5*s6*s7;								gc(8, 4)=c3*c8*s4*s5*s6*s7;								gc(8, 5)=-c4*c8*s5*s6*s7;								gc(8, 6)=c5*c8*s6*s7;								gc(8, 7)=-c6*c8*s7;								gc(8, 8)=c7*c8;								gc(8, 9)=s8;
 			gc(9 , 0)=-c9*s0*s1*s2*s3*s4*s5*s6*s7*s8;						  gc(9 , 1)=c0*c9*s1*s2*s3*s4*s5*s6*s7*s8;							gc(9, 2)=-c1*c9*s2*s3*s4*s5*s6*s7*s8;							gc(9, 3)=c2*c9*s3*s4*s5*s6*s7*s8;							gc(9, 4)=-c3*c9*s4*s5*s6*s7*s8;							gc(9, 5)=c4*c9*s5*s6*s7*s8;								gc(9, 6)=-c5*c9*s6*s7*s8;							gc(9, 7)=c6*c9*s7*s8;							gc(9, 8)=-c7*c9*s8;							gc(9, 9)=c8*c9;								gc(9, 10)=s9;
 			gc(10, 0)=c10*s0*s1*s2*s3*s4*s5*s6*s7*s8*s9;					  gc(10, 1)=-c0*c10*s1*s2*s3*s4*s5*s6*s7*s8*s9;						gc(10, 2)=c1*c10*s2*s3*s4*s5*s6*s7*s8*s9;						gc(10, 3)=-c2*c10*s3*s4*s5*s6*s7*s8*s9;						gc(10, 4)=c3*c10*s4*s5*s6*s7*s8*s9;						gc(10, 5)=-c4*c10*s5*s6*s7*s8*s9;						gc(10, 6)=c5*c10*s6*s7*s8*s9;						gc(10, 7)=-c6*c10*s7*s8*s9;						gc(10, 8)=c7*c10*s8*s9;						gc(10, 9)=-c8*c10*s9;						gc(10, 10)=c9*c10;						gc(10, 11)=s10;
 			gc(11, 0)=-c11*s0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10;				  gc(11, 1)=c0*c11*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10;					gc(11, 2)=-c1*c11*s2*s3*s4*s5*s6*s7*s8*s9*s10;					gc(11, 3)=c2*c11*s3*s4*s5*s6*s7*s8*s9*s10;					gc(11, 4)=-c3*c11*s4*s5*s6*s7*s8*s9*s10;				gc(11, 5)=c4*c11*s5*s6*s7*s8*s9*s10;					gc(11, 6)=-c5*c11*s6*s7*s8*s9*s10;					gc(11, 7)=c6*c11*s7*s8*s9*s10;					gc(11, 8)=-c7*c11*s8*s9*s10;				gc(11, 9)=c8*c11*s9*s10;					gc(11, 10)=-c9*c11*s10;					gc(11, 11)=c10*c11;					gc(11, 12)=s11;
 			gc(12, 0)=c12*s0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11;			  gc(12, 1)=-c0*c12*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11;				gc(12, 2)=c1*c12*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11;				gc(12, 3)=-c2*c12*s3*s4*s5*s6*s7*s8*s9*s10*s11;				gc(12, 4)=c3*c12*s4*s5*s6*s7*s8*s9*s10*s11;				gc(12, 5)=-c4*c12*s5*s6*s7*s8*s9*s10*s11;				gc(12, 6)=c5*c12*s6*s7*s8*s9*s10*s11;				gc(12, 7)=-c6*c12*s7*s8*s9*s10*s11;				gc(12, 8)=c7*c12*s8*s9*s10*s11;				gc(12, 9)=-c8*c12*s9*s10*s11;				gc(12, 10)=c9*c12*s10*s11;				gc(12, 11)=-c10*c12*s11;			gc(12, 12)=c11*c12;				gc(12, 13)=s12;
 			gc(13, 0)=-c13*s0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12;		  gc(13, 1)=c0*c13*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12;			gc(13, 2)=-c1*c13*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12;			gc(13, 3)=c2*c13*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12;			gc(13, 4)=-c3*c13*s4*s5*s6*s7*s8*s9*s10*s11*s12;		gc(13, 5)=c4*c13*s5*s6*s7*s8*s9*s10*s11*s12;			gc(13, 6)=-c5*c13*s6*s7*s8*s9*s10*s11*s12;			gc(13, 7)=c6*c13*s7*s8*s9*s10*s11*s12;			gc(13, 8)=-c7*c13*s8*s9*s10*s11*s12;		gc(13, 9)=c8*c13*s9*s10*s11*s12;			gc(13, 10)=-c9*c13*s10*s11*s12;			gc(13, 11)=c10*c13*s11*s12;			gc(13, 12)=-c11*c13*s12;		gc(13, 13)=c12*c13;			gc(13, 14)=s13;
 			gc(14, 0)=c14*s0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13;	  gc(14, 1)=-c0*c14*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13;		gc(14, 2)=c1*c14*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13;		gc(14, 3)=-c2*c14*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13;		gc(14, 4)=c3*c14*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13;		gc(14, 5)=-c4*c14*s5*s6*s7*s8*s9*s10*s11*s12*s13;		gc(14, 6)=c5*c14*s6*s7*s8*s9*s10*s11*s12*s13;		gc(14, 7)=-c6*c14*s7*s8*s9*s10*s11*s12*s13;		gc(14, 8)=c7*c14*s8*s9*s10*s11*s12*s13;		gc(14, 9)=-c8*c14*s9*s10*s11*s12*s13;		gc(14, 10)=c9*c14*s10*s11*s12*s13;		gc(14, 11)=-c10*c14*s11*s12*s13;	gc(14, 12)=c11*c14*s12*s13;		gc(14, 13)=-c12*c14*s13;	gc(14, 14)=c13*c14;		gc(14, 15)=s14;
 			gc(15, 0)=-c15*s0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14; gc(15, 1)=c0*c15*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14;	gc(15, 2)=-c1*c15*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14;	gc(15, 3)=c2*c15*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14;	gc(15, 4)=-c3*c15*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14;gc(15, 5)=c4*c15*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14;	gc(15, 6)=-c5*c15*s6*s7*s8*s9*s10*s11*s12*s13*s14;	gc(15, 7)=c6*c15*s7*s8*s9*s10*s11*s12*s13*s14;	gc(15, 8)=-c7*c15*s8*s9*s10*s11*s12*s13*s14;gc(15, 9)=c8*c15*s9*s10*s11*s12*s13*s14;	gc(15, 10)=-c9*c15*s10*s11*s12*s13*s14;	gc(15, 11)=c10*c15*s11*s12*s13*s14;	gc(15, 12)=-c11*c15*s12*s13*s14;gc(15, 13)=c12*c15*s13*s14;	gc(15, 14)=-c13*c15*s14;gc(15, 15)=c14*c15;	gc(15, 16)=s15;
 			gc(16, 0)=s0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;  gc(16, 1)=-c0*s1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 2)=c1*s2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 3)=-c2*s3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 4)=c3*s4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 5)=-c4*s5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 6)=c5*s6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 7)=-c6*s7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 8)=c7*s8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 9)=-c8*s9*s10*s11*s12*s13*s14*s15;	gc(16, 10)=c9*s10*s11*s12*s13*s14*s15;	gc(16, 11)=-c10*s11*s12*s13*s14*s15;gc(16, 12)=c11*s12*s13*s14*s15;	gc(16, 13)=-c12*s13*s14*s15;gc(16, 14)=c13*s14*s15;	gc(16, 15)=-c14*s15;gc(16, 16)=c15;

         	// create row band temporary working patch
 			{
 				const int mm = nb + 1;
 				const int nn = n - j - nb;
 				t1.cols(nn);
 				const lapack_int lda = r.leading_dim();
 				const lapack_int ldb = t1.leading_dim();
 				mkl_domatcopy(r.char_order(), 'N', mm, nn, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
 			}

 			// apply the accumulated Givens gc to the matrix row block
 			// (excluding the diagonal block) at once using GEMM
 			{
 				const int mm = gc.rows();
 				const int nn = t1.cols();
 				assert(nn > 0);
 				const int kk = gc.cols();
 				const CBLAS_ORDER b_order = r.blas_order();
 				const MKL_INT lda 	= gc.leading_dim();
 				const MKL_INT ldb 	= t1.leading_dim();
 				const MKL_INT ldc 	= r.leading_dim();

 				cblas_dgemm(b_order, CblasNoTrans, CblasNoTrans, mm, nn, kk, alpha, gc.data(),
 					lda, t1.data(), ldb, beta, &r(i - 1, j + nb), ldc);
 			}
         }
	}
}

/**
 * Triangularizes the given matrix after a deletion update using Givens rotations.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize_givens(int begin, int block, GENROT genrot) {
	LENTER;

	int k = this->m_cols;

	tsfo_matrix_tria<double>& r = *this;

	tsfo_vector<double>::s_bufferpool.ensure_size(k + 1);
	static tsfo_vector<double> rc;
	static tsfo_vector<double> rs;

	for (int i = begin - block + 2, j = begin; i <= k; i++, j++) {
		double a = r(i - 1, j);
		double b = r(i    , j);
		double d = 0.0;
		genrot(&a, &b, &rc[0], &rs[0], &d);

		for (int jj = 1; jj < (k - j); ++jj) {
			rc[jj] = rc[0];
			rs[jj] = rs[0];
		}

		// apply the rotations along the diagonal
		char side      = 'L';
		char pivot     = 'V';
		char direct    = 'F';
		lapack_int m   = block + 1;
		lapack_int n   = k - j;
		lapack_int lda = r.leading_dim();
		dlasr(&side, &pivot, &direct, &m, &n, rc.data(), rs.data(), &r(i - 1, j), &lda);
	}
}

/**
 * Triangularizes this matrix using accumulated Givens rotations, assumed to be in
 * upper Hessenberg form after a deletion update.
 */
/*
template<typename T>
inline void tsfo_matrix<T>::triangularize_block_givens(int begin, int block) {
	LENTER;

	int nb = TRIANGULARIZE_NB;
	assert(nb == TRIANGULARIZE_NB);
	assert(nb % 2 == 0);

	static tsfo_matrix<double> g1(nb, nb);
	static tsfo_matrix<double> g2(nb, nb);
	static tsfo_matrix<double> gc(nb, nb);
	static tsfo_matrix<double> t1(nb, 0);
	static const double alpha = 1.0;
	static const double beta  = 0.0;

	const int m = m_rows;
	const int n = m_cols;

	int n_iter = (n - begin) % (nb - 1) == 0
			  ? ((n - begin) / (nb - 1))
			  : ((n - begin) / (nb - 1)) + 1;
	int nb_end = begin + n_iter*(nb - 1);

	int i = begin - block + 2;
	int j = begin;

	tsfo_matrix<double>& r = *this;

	double x00, x10, x01, x11, x21;
	double s0, c0, s1, c1, d;
	double xx0, xx1, xx2;
	double u00, u01, u10, u11, u21;
	double uu0, uu1, uu2;

	// blocked
	g1.identity();
	for (; i < nb_end; i += nb, j += nb) {
		// Givens support matrix to accumulate transformations
		g2.identity();

		printf("=============================== \n");
		for (int ii = i - 1, jj = j + 1, ii2 = 0; ii < (i - 2 + nb); ii += 2, ii2 += 2, jj += 2) {
			x00 = r(ii    , jj); x01 = r(ii    , jj + 1);
			x10 = r(ii + 1, jj); x11 = r(ii + 1, jj + 1);
			                     x21 = r(ii + 2, jj + 1);

			// generate and apply rotation
			genrot(&x00, &x10, &c0, &s0, &d);
		    u00 = c0*x00 + s0*x10;
		    u10 = c0*x10 - s0*x00;

		    u01 = c0*x01 + s0*x11;
		    u11 = c0*x11 - s0*x01;

		    // accumulate rotation
			g1(ii2    , ii2)     =  c0;
			g1(ii2 + 1, ii2)     = -s0;
			g1(ii2 + 1, ii2 + 1) =  c0;
			g1(ii2    , ii2 + 1) =  s0;

			// accumulate rotation into gc
			g1.multiply(g2, gc);
			if (ii < (i - 2 + nb)) {
				gc.copy_to(g2, nb, nb);
			}

			printf("g1 is: \n");
			g1.print();

			// restore the identity
			g1(ii2    , ii2)     =  1.0;
			g1(ii2 + 1, ii2)     =  0.0;
			g1(ii2 + 1, ii2 + 1) =  1.0;
			g1(ii2    , ii2 + 1) =  0.0;

			// generate and apply rotation
			x11 = u11;
		    genrot(&x11, &x21, &c1, &s1, &d);
		    u11 = c1*x11 + s1*x21;
		    u21 = c1*x21 - s1*x11;

			if ((ii + 1) < (i - 2 + nb)) {
			    // accumulate rotation
				g1(ii2 + 1, ii2 + 1) =  c1;
				g1(ii2 + 2, ii2 + 1) = -s1;
				g1(ii2 + 2, ii2 + 2) =  c1;
				g1(ii2 + 1, ii2 + 2) =  s1;

				printf("g1 is: \n");
				g1.print();

				// accumulate rotation into gc
				g1.multiply(g2, gc);
				gc.copy_to(g2, nb, nb);

				// restore the identity
				g1(ii2 + 1, ii2 + 1) = 1.0;
				g1(ii2 + 2, ii2 + 1) = 0.0;
				g1(ii2 + 2, ii2 + 2) = 1.0;
				g1(ii2 + 1, ii2 + 2) = 0.0;
			}

			r(ii    , jj) = u00; r(ii    , jj + 1) = u01;
			r(ii + 1, jj) = u10; r(ii + 1, jj + 1) = u11;
			                     r(ii + 2, jj + 1) = u21;

			// apply Givens to trailing columns within the diagonal block
			for (int jj2 = jj + 2; jj2 < (j + nb); ++jj2) {
				xx0 = r(ii    , jj2);
				xx1 = r(ii + 1, jj2);
				xx2 = r(ii + 2, jj2);

			    uu0 = c0*xx0 + s0*xx1;
			    uu1 = c0*xx1 - s0*xx0;

				xx1 = uu1;
			    uu1 = c1*xx1 + s1*xx2;
			    uu2 = c1*xx2 - s1*xx1;

				r(ii    , jj2) = uu0;
				r(ii + 1, jj2) = uu1;
				r(ii + 2, jj2) = uu2;
			}
		}

		printf("g2 is: \n");
		g2.print();

		if (n - j - nb > 0) {
			// create row band temporary working patch
			{
				t1.cols(n - j - nb);
				const lapack_int lda = leading_dim();
				const lapack_int ldb = t1.leading_dim();
				mkl_domatcopy(char_order(), 'N', nb, n - j - nb, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
			}

			// apply the accumulated Givens gc to the matrix row block
			// (excluding the diagonal block) at once using GEMM
			{
				const int m  = gc.rows();
				const int nn = n - j - nb;
				assert(nn > 0);
				const int k = gc.cols();
				const CBLAS_ORDER b_order = blas_order();
				const MKL_INT lda 	= gc.leading_dim();
				const MKL_INT ldb 	= t1.leading_dim();
				const MKL_INT ldc 	= leading_dim();
				cblas_dgemm(b_order, CblasNoTrans, CblasNoTrans, m, nn, k, alpha, gc.data(),
					lda, t1.data(), ldb, beta, &r(i - 1, j + nb), ldc);
			}
		}
	}
}
*/

/**
 * Triangularizes this matrix using accumulated Givens rotations, assumed to be in
 * upper Hessenberg form after a deletion update.
 */
template<typename T> template<typename GENROT>
inline void tsfo_matrix_tria<T>::triangularize_block_givens(int begin, int block, GENROT genrot) {
	LENTER;

	int nb = TRIANGULARIZE_NB;
	assert(nb == TRIANGULARIZE_NB);

	static tsfo_matrix<double> g1(nb, nb);
	static tsfo_matrix<double> g2(nb, nb);
	static tsfo_matrix<double> gc(nb, nb);
	static tsfo_matrix<double> t1(nb, 0);
	static const double alpha = 1.0;
	static const double beta  = 0.0;

	const int m = this->m_rows;
	const int n = this->m_cols;

	int n_iter = (n - begin) % (nb - 1) == 0
			  ? ((n - begin) / (nb - 1))
			  : ((n - begin) / (nb - 1)) + 1;
	int nb_end = begin + n_iter*(nb - 1);

	int i = begin - block + 2;
	int j = begin;

	tsfo_matrix_tria<double>& r = *this;

	double a, b, s, c, d;

	// blocked
	g1.identity();
	for (; i < nb_end; i += (nb - 1), j += (nb - 1)) {
		// Givens support matrix to accumulate transformations
		g2.identity();

//		printf("=============================== \n");
		// unroll first iteration
		{
			a = r(i - 1, j);
			b = r(i    , j);

			// generate a rotation
			genrot(&a, &b, &c, &s, &d);

			g2(0, 0) =  c;
			g2(1, 0) = -s;
			g2(1, 1) =  c;
			g2(0, 1) =  s;

//			printf("g1 is: \n");
//			g2.print();

			// apply Givens to trailing columns within the diagonal block
			for (int jj = i - 1; jj < (i - 1 + nb); ++jj) {
				a = r(i - 1, jj);
				b = r(i    , jj);

				r(i - 1, jj) = c*a + s*b;
				r(i    , jj) = c*b - s*a;
			}
		}

		for (int ii = i, ii2 = 1; ii < (i - 2 + nb); ii++, ii2++) {
			a = r(ii    , ii);
			b = r(ii + 1, ii);

			// generate a rotation
			genrot(&a, &b, &c, &s, &d);

			g1(ii2    , ii2)     =  c;
			g1(ii2 + 1, ii2)     = -s;
			g1(ii2 + 1, ii2 + 1) =  c;
			g1(ii2    , ii2 + 1) =  s;

//			printf("g1 is: \n");
//			g1.print();

			// accumulate rotation into gc
			g1.multiply(g2, gc);
			if (ii < (i - 2 + nb)) {
				gc.copy_to(g2, nb, nb);
			}

			// restore the identity
			g1(ii2    , ii2)     =  1.0;
			g1(ii2 + 1, ii2)     =  0.0;
			g1(ii2 + 1, ii2 + 1) =  1.0;
			g1(ii2    , ii2 + 1) =  0.0;

			// apply Givens to trailing columns within the diagonal block
			for (int jj = ii; jj < (i - 1 + nb); ++jj) {
				a = r(ii    , jj);
				b = r(ii + 1, jj);

				r(ii    , jj) = c*a + s*b;
				r(ii + 1, jj) = c*b - s*a;
			}
		}

//		printf("g2 is: \n");
//		g2.print();

		if (n - j - nb > 0) {
			// create row band temporary working patch
			{
				t1.cols(n - j - nb);
				const lapack_int lda = r.leading_dim();
				const lapack_int ldb = t1.leading_dim();
				mkl_domatcopy(r.char_order(), 'N', nb, n - j - nb, alpha, &r(i - 1, j + nb), lda, t1.data(), ldb);
			}

			// apply the accumulated Givens gc to the matrix row block
			// (excluding the diagonal block) at once using GEMM
			{
				const int m  = gc.rows();
				const int nn = n - j - nb;
				assert(nn > 0);
				const int k = gc.cols();
				const CBLAS_ORDER b_order = r.blas_order();
				const MKL_INT lda 	= gc.leading_dim();
				const MKL_INT ldb 	= t1.leading_dim();
				const MKL_INT ldc 	= r.leading_dim();
				cblas_dgemm(b_order, CblasNoTrans, CblasNoTrans, m, nn, k, alpha, gc.data(),
					lda, t1.data(), ldb, beta, &r(i - 1, j + nb), ldc);
			}
		}
	}
}

/**
 * Triangularizes the given matrix after a deletion update using Householder reflectors.
 */
template<typename T>
inline void tsfo_matrix_tria<T>::triangularize_householder(int begin, int block) {
	LENTER;

	// fortran is 1-based so is not begin but (begin + 1)
	lapack_int m  	= this->rows();
	lapack_int n 	= this->cols();
	lapack_int k  	= begin + 1;
	lapack_int p  	= block;
	lapack_int lda  = m;
	lapack_int info = 0;

	int lwork = tsfo_matrix<T>::BLOCK_SIZE*(p + 1);
	tsfo_vector<double>::s_bufferpool.ensure_size(max(lwork, min(m, n)));
	static tsfo_vector<double> work;
	static tsfo_vector<double> tau;
	F_delcols(&m, &n, this->data(), &lda, &k, &p, tau.data(), work.data(), &info);
	if (info != 0) {
      fprintf(stderr, "F_delcols failed with info = %d\n", info);
	  exit(EXIT_FAILURE);
	}

	// TODO: implement the F_delcols manually
}

#endif  // SFO_MATRIX_TRIA_H_
