/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_vector.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Include declaration for the vector structure
//============================================================================

#ifndef SFO_VECTOR_H_
#define SFO_VECTOR_H_

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <math.h>
#include <limits>
#include <algorithm>
#include <numeric>
#include <utility>
#include <vector>
#include <boost/math/special_functions/fpclassify.hpp>

#include "logger.h"
#include "bufferpool.h"
#include "utils.h"

using namespace std;

// forward declaration
template <typename T>
class tsfo_matrix;

/**
 * Concrete definition of vector that delegates most operations to LAPACK and BLAS
 * functions. This implementation provides support. Furthermore, it guarantees zero
 * memory allocation during algorithm execution time.
 */
template<typename T>
class tsfo_vector {
private:
	static tbufferpool<T> 	 	 s_bufferpool;
//	static const long 		 	 VECTOR_POOL_INITIAL = 31; // HPSFO
	static const long 		 	 VECTOR_POOL_INITIAL = 45; // Krause
	static const long 		 	 VECTOR_BUFFER_SIZE;
	static const double 	 	 EPSILON 		   = 1e-10;
	static const int 		 	 DEFAULT_PRECISION = 20;
	static tlinear_sort<int> 	 s_linear_sort;
	static vector<pair<T, int> > s_pairs;

	typename sfo_type<T>::aptr32 m_data;
	int           				 m_size;
	const bool    				 m_from_pool;

	    void init(int size);
	    void init(int size, T value);
		void release();

   inline T& elem(int i) {
	   return m_data[i];
   }

   inline const T& elem(int i) const {
	   return m_data[i];
   }

   static int sort_index_lcomparator(const pair<T, int>& a, const pair<T, int>& b);
   static int sort_index_gcomparator(const pair<T, int>& a, const pair<T, int>& b);

  template <typename T1>
  friend tsfo_vector<T1> operator*(T1 scalar, const tsfo_vector<T1>& a);
  template <typename T1>
  friend tsfo_vector<T1> operator+(T1 scalar, const tsfo_vector<T1>& a);
  template <typename T1>
  friend tsfo_vector<T1> operator/(T1 scalar, const tsfo_vector<T1>& a);

  friend class tsfo_matrix<T>;

public:

   // stl Container integration
   typedef const T& const_reference;

	// constructors
	tsfo_vector();
	tsfo_vector(int size);
	tsfo_vector(int size, T value);
	tsfo_vector(const tsfo_vector<T>& a);
	tsfo_vector(int size, T* data);

	template<typename T1>
	tsfo_vector(const vector<T1>& a);

	template<typename T1>
	tsfo_vector(const tsfo_vector<T1>& a);

	inline T* data() {
		return m_data;
	}

	inline const T* data() const {
		return m_data;
	}

	// operators
	 	   	   bool operator==(const tsfo_vector<T>& a);
	tsfo_vector<T>& operator=(const tsfo_vector<T>& a);
	         	 T& operator[](int i);
	       const T& operator[](int i) const;
	 tsfo_vector<T> operator*(T scalar) const;
    tsfo_vector<T>& operator*=(T scalar);
	 tsfo_vector<T> operator+(T scalar) const;
    tsfo_vector<T>& operator+=(T scalar);
	 tsfo_vector<T> operator/(T scalar) const;
    tsfo_vector<T>& operator/=(T scalar);
	 tsfo_vector<T> operator/(const tsfo_vector<T>& y) const;
	tsfo_vector<T>& operator/=(const tsfo_vector<T>& y);
	 tsfo_vector<T> operator+(const tsfo_vector<T>& y) const;
	tsfo_vector<T>& operator+=(const tsfo_vector<T>& y);
	 tsfo_vector<T> operator-(const tsfo_vector<T>& y) const;
	tsfo_vector<T>& operator-=(const tsfo_vector<T>& y);

	       bool is_empty() const;
	 	    int size() const;
	 	   void size(int new_size);
	 	   void clear();
	 	   void resize(int new_size, T value = 0);
	 	   void delete_elem(int i);
		   void copy_to(tsfo_vector<T>& b, int n) const;
		   void copy_to(tsfo_vector<T>& b) const;
		   void copy_to(T*& b) const;
		   void seqn();

		    template<typename T1>
		    void copy_to(vector<T1>& b) const;
		    void append(const T& value);
		    void push_back(const T& value);
		    void to_set(tsfo_vector<int>& set) const;
	  	    void random();
	  	    void reset();
	  	       T norm2() const;
	  	       T sum() const;
	  	       T sum(const tsfo_vector<int>& b) const;
 tsfo_vector<T>& ln();
	  	       T min() const;
	  	       T max() const;
	  	       T dot_product(const tsfo_vector<T>& b) const;
	  	       T dot_product(const tsfo_vector<T>& b, int n) const;
	  	    void sort_index(tsfo_vector<int>& result, bool asc = true) const;
     vector<int> sort_index_stable(bool asc = true) const;
 tsfo_vector<T>& linear_combination(const tsfo_vector<T>& x, T a, const tsfo_vector<T>& y, T b);
	   	       T sum_lz() const;
 tsfo_vector<T>& select_lz(const tsfo_vector<T>& x, const tsfo_vector<T>& predicates);
	        void print() const;
	        void print(int size) const;

	          // support for stl iterators
	          T* begin();
	          T* end();

    static void print(const vector<T>& b);
    static void print(const T* b, int size);

	       void characteristic(const tsfo_vector<T>& set, const tsfo_vector<T>& subset);
		virtual ~tsfo_vector();

	static const int MAX_SIZE;
};

template<typename T>
const int tsfo_vector<T>::MAX_SIZE = read_env("SFO_MAX_M_N", DEFAULT_MAX_M_N);

template<typename T>
const long tsfo_vector<T>::VECTOR_POOL_INITIAL;

template<typename T>
const long tsfo_vector<T>::VECTOR_BUFFER_SIZE = read_env("SFO_MAX_M_N", DEFAULT_MAX_M_N)*64;

// static up-front allocation
template<typename T>
tbufferpool<T> tsfo_vector<T>::s_bufferpool = tbufferpool<T>(VECTOR_POOL_INITIAL,
		read_env("SFO_MAX_M_N", DEFAULT_MAX_M_N)*64, AVX_ALIGNED);

template<typename T>
tlinear_sort<int> tsfo_vector<T>::s_linear_sort = tlinear_sort<int>(MAX_SIZE);

template<typename T>
vector<pair<T, int> > tsfo_vector<T>::s_pairs(MAX_SIZE);

#ifdef HAVE_INTEL_MKL
	#include <mkl_lapacke.h>
	#include <mkl_lapack.h>
	#include <mkl_lapacke.h>
	#include <mkl_cblas.h>
	#include <mkl_trans.h>
	#include <mkl_vml_functions.h>
#endif

#include <tbb/parallel_sort.h>

using namespace std;

template<typename T>
inline void tsfo_vector<T>::init(int size) {
	s_bufferpool.ensure_size(size);

	m_size = size;

	// allocate the memory from the bufferpool
	m_data = s_bufferpool.next();
}

template<typename T>
inline void tsfo_vector<T>::init(int size, T value) {
	init(size);

	for (int i = 0; i < size; ++i) {
		elem(i) = value;
	}
}

template<typename T>
inline tsfo_vector<T>::tsfo_vector() : m_from_pool(true) {
	LENTER;

  	init(0);
}

template<typename T>
inline tsfo_vector<T>::tsfo_vector(int size) : m_from_pool(true) {
	LENTER;

	init(size);
}

template<typename T>
inline tsfo_vector<T>::tsfo_vector(int size, T value) : m_from_pool(true) {
	LENTER;

  	init(size, value);
}

template<typename T>
inline tsfo_vector<T>::tsfo_vector(int size, T* data) : m_from_pool(false) {
	LENTER;

	m_size	= size;
	m_data	= data;
}

template<typename T> template<typename T1>
inline tsfo_vector<T>::tsfo_vector(const vector<T1>& a) : m_from_pool(true) {
	LENTER;

  	init(a.size());

	for (int i = 0; i < a.size(); ++i) {
		elem(i) = (T) a[i];
	}
}

template<typename T> template<typename T1>
inline tsfo_vector<T>::tsfo_vector(const tsfo_vector<T1>& a) : m_from_pool(true) {
	LENTER;

  	init(a.size());

	for (int i = 0; i < a.size(); ++i) {
		elem(i) = (T) a[i];
	}
}

template<typename T>
inline T& tsfo_vector<T>::operator[](int i) {
	return this->elem(i);
}

template<typename T>
inline const T& tsfo_vector<T>::operator[](int i) const {
	return this->elem(i);
}

template<typename T>
inline void tsfo_vector<T>::resize(int new_size, T value) {
	LENTER;

	// zero padding to the desired size
	while (m_size < new_size) {
		m_data[m_size++] = value;
	}

	// when new_size < m_size
	m_size = new_size;
}

template<typename T>
inline void tsfo_vector<T>::random() {
	LENTER;

	for (int i = 0; i < size(); ++i) {
		elem(i) = (double) rand() / (double) RAND_MAX;
	}
}

template<typename T>
inline void tsfo_vector<T>::print() const {
	print(m_size);
}

template<typename T>
inline void tsfo_vector<T>::print(int size) const {
	cout << "vector of size: " << size << endl;
	for (int i = 0; i < size; ++i) {
		cout << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << elem(i) << endl;
	}
	cout << endl;
}

template<typename T>
inline T* tsfo_vector<T>::begin() {
	return data();
}

template<typename T>
inline T* tsfo_vector<T>::end() {
	return data() + size();
}

template<typename T>
void tsfo_vector<T>::print(const vector<T>& b) {
	cout << "vector of size: " << b.size() << endl;
	for (int i=0; i < b.size(); ++i) {
		cout << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << b[i] << endl;
	}
	cout << endl;
}

template<typename T>
void tsfo_vector<T>::print(const T* b, int size) {
	cout << "vector of size: " << size << endl;
	for (int i=0; i < size; ++i) {
		cout << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << b[i] << endl;
	}
	cout << endl;
}

template<>
inline void tsfo_vector<int>::print(int size) const {
	cout << "vector of size: " << size << endl;
	for (int i = 0; i < size; ++i) {
		cout << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << elem(i) << endl;
	}
	cout << endl;
}

/**
 * Reusable release method that will clean up all resources for this matrix
 */
template<typename T>
inline void tsfo_vector<T>::release() {
	LENTER;

	// reset the size
	m_size = 0;

	if (m_from_pool) {
		// release the memory back to the bufferpool
		s_bufferpool.release(m_data);
	}
	m_data = NULL;
}

template<typename T>
inline tsfo_vector<T>::~tsfo_vector() {

	// delegate to release
	release();
}

template<typename T>
inline tsfo_vector<T>::tsfo_vector(const tsfo_vector<T>& a) : m_from_pool(true) {
	LENTER;

	init(a.size());

	a.copy_to(*this);
}

template<typename T>
inline bool tsfo_vector<T>::operator==(const tsfo_vector<T>& a) {
	LENTER;

	if (size() != a.size()) {
		return false;
	}

	for (int i = 0; i < size(); i++) {
		T diff = elem(i) - a[i];
		if (fabs(diff) > EPSILON) {
			return false;
		}
	}

	return true;
}

/**
 * Assigns a to this object. Performs a full copy of a into this.
 */
template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator=(const tsfo_vector<T>& a) {
	LENTER;

	a.copy_to(*this);

	return *this;
}

template<typename T>
inline tsfo_vector<T> tsfo_vector<T>::operator/(const tsfo_vector<T>& y) const {
	lapack_int n = std::min(m_size, y.m_size);
	tsfo_vector<T> result(n, 0.0);
	if (y.size() > 0) {
		vdDiv(n, data(), y.data(), result.m_data);
	}

	return result;
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator/=(const tsfo_vector<T>& y) {
	lapack_int n = std::min(m_size, y.m_size);
	if (y.size() > 0) {
		vdDiv(n, data(), y.data(), m_data);
	}

	return *this;
}

template<typename T>
inline tsfo_vector<T> tsfo_vector<T>::operator+(const tsfo_vector<T>& y) const {
	lapack_int n = std::min(m_size, y.m_size);
	tsfo_vector<T> result(n, 0.0);
	if (y.size() > 0) {
		this->copy_to(result);

		double a 		= 1.0;
		lapack_int incx = 1;
		lapack_int incy = 1;
		cblas_daxpy(n, a, y.data(), incy, result.m_data, incx);
	}

	return result;
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator+=(const tsfo_vector<T>& y) {
	lapack_int n = std::min(m_size, y.m_size);
	if (y.size() > 0) {
		double a 		= 1.0;
		lapack_int incx = 1;
		lapack_int incy = 1;
		cblas_daxpy(n, a, y.data(), incy, m_data, incx);
	}

	return *this;
}

template<typename T>
inline tsfo_vector<T> tsfo_vector<T>::operator-(const tsfo_vector<T>& y) const {
	lapack_int n = std::min(m_size, y.size());
	tsfo_vector<T> result(n, 0.0);
	if (y.size() > 0) {
		this->copy_to(result);

		double a 		= -1.0;
		lapack_int incx = 1;
		lapack_int incy = 1;
		cblas_daxpy(n, a, y.m_data, incy, result.m_data, incx);
	}

	return result;
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator-=(const tsfo_vector<T>& y) {
	lapack_int n = std::min(m_size, y.size());
	if (y.size() > 0) {
		double a 		= -1.0;
		lapack_int incx = 1;
		lapack_int incy = 1;
		cblas_daxpy(n, a, y.m_data, incy, m_data, incx);
	}

	return *this;
}

template<typename T>
inline tsfo_vector<T> tsfo_vector<T>::operator*(T scalar) const {
	lapack_int n = m_size;
	tsfo_vector<T> result(n, 0.0);
	if (m_size > 0) {
		// dscal
		lapack_int n 	= m_size;
		lapack_int incx = 1;
		copy_to(result);
		cblas_dscal(n, scalar, result.data(), incx);
	}

	return result;
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator*=(T scalar) {
	lapack_int n = m_size;
	if (m_size > 0) {
		// dscal
		lapack_int n 	= m_size;
		lapack_int incx = 1;
		cblas_dscal(n, scalar, m_data, incx);
	}

	return *this;
}

template<typename T>
inline tsfo_vector<T> tsfo_vector<T>::operator+(T scalar) const {
	tsfo_vector<T> result(size(), 0.0);
	if (size() > 0) {
		for (int i = 0; i < size(); ++i) {
			result[i] += scalar;
		}
	}

	return result;
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator+=(T scalar) {
	if (size() > 0) {
		for (int i = 0; i < size(); ++i) {
			elem(i) += scalar;
		}
	}

	return *this;
}

template<typename T>
inline tsfo_vector<T> tsfo_vector<T>::operator/(T scalar) const {
	lapack_int n = m_size;
	tsfo_vector<T> result(n, 0.0);
	if (m_size > 0) {
		// dscal
		lapack_int n 	= m_size;
		lapack_int incx = 1;
		copy_to(result);
		cblas_dscal(n, 1.0/scalar, result.data(), incx);
	}

	return result;
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::operator/=(T scalar) {
	lapack_int n = m_size;
	if (m_size > 0) {
		// dscal
		lapack_int n 	= m_size;
		lapack_int incx = 1;
		cblas_dscal(n, 1.0/scalar, m_data, incx);
	}

	return *this;
}

template<typename T>
inline int tsfo_vector<T>::size() const {
	return m_size;
}

template<typename T>
inline bool tsfo_vector<T>::is_empty() const {
	return m_size == 0;
}

template<typename T>
inline void tsfo_vector<T>::size(int new_size) {
	m_size = new_size;
}

template<typename T>
inline void tsfo_vector<T>::clear() {
	size(0);
}

template<typename T>
inline void tsfo_vector<T>::delete_elem(int i) {
	assert(i < m_size);

	if (i < (m_size - 1)) {
		memmove(m_data + i, m_data + (i + 1), (m_size - i - 1)*sizeof(T));
	}
	m_size--;
}

template <typename T>
inline void tsfo_vector<T>::seqn() {
	#pragma simd
	for (int i=0; i < size(); ++i) {
		m_data[i] = (T) i;
	}
}

/**
 * Partially copies the vector from this to b.
 */
template<typename T>
inline void tsfo_vector<T>::copy_to(tsfo_vector<T>& b, int n) const {
	LENTER;

	b.m_size = n;

	memcpy(b.data(), data(), n*sizeof(T));
}

/**
 * Partially copies the vector from this to b.
 */
template<>
inline void tsfo_vector<double>::copy_to(tsfo_vector<double>& b, int n) const {
	LENTER;

	b.m_size = n;

	const lapack_int incx = 1;
	const lapack_int incy = 1;
	cblas_dcopy(n, data(), incx, b.data(), incy);
}

/**
 * Copies the entire vector from this to b.
 */
template<typename T>
inline void tsfo_vector<T>::copy_to(tsfo_vector<T>& b) const {
	copy_to(b, m_size);
}

/**
 * Copies the entire vector from this to b.
 */
template<>
inline void tsfo_vector<double>::copy_to(double*& b) const {
	LENTER;

	b = new double[m_size];

	const lapack_int n 	  = m_size;
	const lapack_int incx = 1;
	const lapack_int incy = 1;
	cblas_dcopy(n, data(), incx, b, incy);
}

/**
 * Copies the entire vector from this to b.
 */
template<typename T>
inline void tsfo_vector<T>::copy_to(T*& b) const {
	LENTER;

	b = new T[m_size]();
	#pragma simd
	for (int i = 0; i < m_size; ++i) {
		b[i] = elem(i);
	}
}

/**
 * Copies the entire vector from this to b.
 */
template<typename T> template<typename T1>
inline void tsfo_vector<T>::copy_to(vector<T1>& b) const {
	LENTER;

	b.resize(size());
	for (int i = 0; i < size(); ++i) {
		b[i] = (T1) elem(i);
	}
}

template<typename T>
inline void tsfo_vector<T>::reset() {
	memset(m_data, 0, m_size*sizeof(T));
}

template<typename T>
inline void tsfo_vector<T>::append(const T& value) {
	s_bufferpool.ensure_size(m_size + 1);
	elem(m_size++) = value;
}

template<typename T>
inline void tsfo_vector<T>::push_back(const T& value) {
	append(value);
}

/**
 * Computes the norm 2 of the given vector
 */
template<typename T>
inline T tsfo_vector<T>::norm2() const {
	assert(m_size > 0);

	const lapack_int incx = 1;
	double result = cblas_dnrm2(m_size, data(), incx);

	return result;
}

/**
 * Computes the sum of a vector
 */
template<typename T>
inline T tsfo_vector<T>::sum() const {
	T result = 0.0;
	if (size() > 0) {
		for (int i = 0; i < size(); ++i) {
			result += elem(i);
		}
	}

	return result;
}

/**
 * Computes the sum of a vector
 */
template<typename T>
inline T tsfo_vector<T>::sum(const tsfo_vector<int>& b) const {
	T result = 0.0;
	if (size() > 0) {
		for (int i = 0; i < b.size(); ++i) {
			result += elem(b[i]);
		}
	}

	return result;
}

/**
 * Computes the natural logarithm on the elements of this vector
 */
template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::ln() {
	if (size() > 0) {
//		for (int i = 0; i < size(); ++i) {
//			if (isnan(log(elem(i)))) {
//				printf("log(%e)=nan\n", elem(i));
//			}
//			elem(i) = log(elem(i));
//		}
		vdLn(m_size, data(), data());
	}

	return *this;
}

/**
 * Computes the max of a vector
 */
template<>
inline double tsfo_vector<double>::max() const {
	double result = 0.0;
	if (m_size > 0) {
		const lapack_int n    = m_size;
		const lapack_int incx = 1;
		int i = cblas_idamax(n, data(), incx);
		result = elem(i);
	}
	return result;
}

/**
 * Computes the min of a vector
 */
template<>
inline double tsfo_vector<double>::min() const {
	double result = 0.0;
	if (m_size > 0) {
		const lapack_int n    = m_size;
		const lapack_int incx = 1;
		int i = cblas_idamin(n, data(), incx);
		result = elem(i);
	}
	return result;
}

/**
 * Computes the min of a vector
 */
template<typename T>
inline T tsfo_vector<T>::min() const {
	T result = std::numeric_limits<T>::max();
	#pragma simd
	for (int i = 0; i < m_size; ++i) {
		result = std::min(result, elem(i));
	}

	return result;
}

/**
 * Computes and returns the dot product of two vectors
 */
template<typename T>
inline T tsfo_vector<T>::dot_product(const tsfo_vector& b, int n) const {
	const tsfo_vector<T>& a = *this;
	assert(n <= a.size() && n <= b.size());

	lapack_int incx = 1;
	lapack_int incy = 1;
	double result = cblas_ddot(n, a.data(), incx, b.data(), incy);

	return result;
}

/**
 * Computes and returns the dot product of two vectors
 */
template<typename T>
inline T tsfo_vector<T>::dot_product(const tsfo_vector& b) const {
	const tsfo_vector<T>& a = *this;
	assert(a.size() == b.size());

	return dot_product(b, a.size());
}

/**
 * Returns a vector containing all elements that are less than zero
 */
template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::select_lz(const tsfo_vector<T>& x, const tsfo_vector<T>& predicates) {
	// initialize size to zero
	m_size = 0;
	for (int i = 0; i < predicates.size(); i++) {
		if (predicates.elem(i) < 0) {
			append(x.elem(i));
		}
	}

	return *this;
}

template<typename T>
inline T tsfo_vector<T>::sum_lz() const {
	int i;
	double sum = 0.0;
	for (i = 0; i < size(); i++) {
		if (elem(i) < (T) 0) {
			sum += elem(i);
		}
	}

	return sum;
}

template<typename T>
int tsfo_vector<T>::sort_index_lcomparator(const pair<T, int>& a, const pair<T, int>& b) {
  return a.first < b.first;
}

template<typename T>
int tsfo_vector<T>::sort_index_gcomparator(const pair<T, int>& a, const pair<T, int>& b) {
  return a.first > b.first;
}

// Returns an index vector, such that: a_sorted[i] == a[index_vector[i]]
template<typename T>
inline void tsfo_vector<T>::sort_index(tsfo_vector<int>& result, bool asc) const {
	LENTER;

	for (int i = 0; i < m_size; ++i) {
		s_pairs[i].first  = elem(i);
		s_pairs[i].second = i;
	}

	if (asc) {
		std::sort(s_pairs.begin(), s_pairs.begin() + m_size, tsfo_vector<T>::sort_index_lcomparator);
//		tbb::parallel_sort(s_pairs.begin(), s_pairs.begin() + m_size, tsfo_vector<T>::sort_index_lcomparator);
	} else {
		std::sort(s_pairs.begin(), s_pairs.begin() + m_size, tsfo_vector<T>::sort_index_gcomparator);
//		tbb::parallel_sort(s_pairs.begin(), s_pairs.begin() + m_size, tsfo_vector<T>::sort_index_gcomparator);
	}

	result.resize(m_size);
	for (int i = 0; i < m_size; ++i) {
		result[i] = s_pairs[i].second;
	}
}

// Returns an index vector, such that: a_sorted[i] == a[index_vector[i]]
template<typename T>
inline vector<int> tsfo_vector<T>::sort_index_stable(bool asc) const {
	LENTER;

	for (int i = 0; i < m_size; ++i) {
		s_pairs[i].first  = elem(i);
		s_pairs[i].second = i;
	}

	if (asc) {
		stable_sort(s_pairs.begin(), s_pairs.begin() + m_size, tsfo_vector<T>::sort_index_lcomparator);
	} else {
		stable_sort(s_pairs.begin(), s_pairs.begin() + m_size, tsfo_vector<T>::sort_index_gcomparator);
	}

	vector<int> result(m_size);
	for (int i = 0; i < m_size; ++i) {
		result[i] = s_pairs[i].second;
	}

	return result;
}

/**
 * Returns the same tsfo_vector without duplicates and sorted in ascending order
 */
template<>
inline void tsfo_vector<int>::to_set(tsfo_vector<int>& set) const {
	LENTER;

	set.resize(size());
	if (size() > 0) {
		#pragma simd
		for (int i = 0; i < size(); ++i) {
			set[i] = elem(i);
		}

		bool asc = true;
		s_linear_sort.sort(set, asc);
	}
}

template<typename T>
inline tsfo_vector<T>& tsfo_vector<T>::linear_combination(const tsfo_vector<T>& x, T a, const tsfo_vector<T>& y, T b) {
	LENTER;

	assert(x.size() == y.size());

    // copy only if this is not x
	if (this != &x) {
		*this = x;
	}
	// dscal
	(*this)*= a;

	// axpy
	lapack_int n 	= x.size();
	lapack_int incx = 1;
	lapack_int incy = 1;
	cblas_daxpy(n, b, y.data(), incx, m_data, incy);

	return *this;
}

template<typename T1>
static tsfo_vector<T1> operator*(T1 scalar, const tsfo_vector<T1>& a) {
	return a.operator*(scalar);
}

template<typename T1>
static tsfo_vector<T1> operator+(T1 scalar, const tsfo_vector<T1>& a) {
	return a.operator+(scalar);
}

template<typename T1>
static tsfo_vector<T1> operator/(T1 scalar, const tsfo_vector<T1>& a) {
	return a.operator/(scalar);
}

#endif  // SFO_VECTOR_H_
