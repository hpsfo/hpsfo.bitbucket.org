/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : logdet_moons_generator.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 13.07.2012
// Description : Generates logdet 2-moons problems given a n size
//============================================================================

#include <cmath>
#include <iomanip>

#include "logdet_moons_generator.h"
#include "sfo_matrix.h"
#include "sfo_vector.h"

static const int DEFAULT_PRECISION = 50;

// constructor
tlogdet_moons_generator::tlogdet_moons_generator() {
	m_K 	 = NULL;
	m_s 	 = NULL;
	m_subset = NULL;
}

// generate the problem
void tlogdet_moons_generator::generate(int n1, int n2, double noise, int seed) {
	srand(seed);

	tsfo_vector<double> ran1(n1);
	ran1.random();
	tsfo_vector<double> r1 = 2.0 + noise * ran1;
	tsfo_vector<double> theta1 = -M_PI / 2.0 + M_PI * ran1;

	// TODO:
}

istream& operator>>(istream &is, tlogdet_moons_generator& generator) {
	// read n
	double n;
	is >> setiosflags(ios_base::scientific) >> n;
	generator.m_n = (int) n;

	// read fv
	is >> setiosflags(ios_base::scientific) >> setprecision(DEFAULT_PRECISION) >> generator.m_fv;

	// read s
	generator.m_s = new double[generator.m_n];
	for (int i = 0; i < generator.m_n; ++i) {
		double value;
		is >> setiosflags(ios_base::scientific) >> setprecision(DEFAULT_PRECISION) >> value;
		generator.m_s[i] = value;
	}

	// read K
	generator.m_K = new double[generator.m_n*generator.m_n];
	for (int i = 0; i < generator.m_n; ++i) {
		for (int j=0; j < generator.m_n; ++j) {
			double value;
			is >> setiosflags(ios_base::scientific) >> setprecision(DEFAULT_PRECISION) >> value;
			generator.m_K[i*generator.m_n + j] = value;
		}
	}

	// read subset
	double optimal_size;
	is >> setiosflags(ios_base::scientific) >> optimal_size;
	generator.m_optimal_size = optimal_size;

	generator.m_subset = new int[generator.m_optimal_size];
	for (int i = 0; i < generator.m_optimal_size; ++i) {
		double value;
		is >> setiosflags(ios_base::scientific) >> setprecision(DEFAULT_PRECISION) >> value;
		generator.m_subset[i] = (int) value;
	}

	is >> setiosflags(ios_base::scientific) >> setprecision(DEFAULT_PRECISION) >> generator.m_subopt;
	double major_iter;
	is >> setiosflags(ios_base::scientific) >> major_iter;
	generator.m_major_iter = (long) major_iter;

	double minor_iter;
	is >> setiosflags(ios_base::scientific) >> minor_iter;
	generator.m_minor_iter = (long) minor_iter;

	return is;
}

ostream& operator<<(ostream &os, tlogdet_moons_generator& generator) {
	// write n
	os << setiosflags(ios_base::scientific) << (double) generator.m_n << endl;

	// write fv
	os << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << generator.m_fv << endl;

	// write s
	for (int i=0; i < generator.m_n; ++i) {
		os << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << generator.m_s[i] << endl;
	}

	// write K
	for (int i=0; i < generator.m_n; ++i) {
		for (int j=0; j < generator.m_n; ++j) {
			os << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << generator.m_K[i*generator.m_n + j] << " ";
		}
		os << endl;
	}

	// write subset
	os << setiosflags(ios_base::scientific) << (double) generator.m_optimal_size << endl;
	for (int i=0; i < generator.m_optimal_size; ++i) {
		os << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << (double) generator.m_subset[i] << endl;
	}

	os << setiosflags(ios_base::scientific) << setprecision(DEFAULT_PRECISION) << generator.m_subopt << endl;
	os << setiosflags(ios_base::scientific) << (double) generator.m_major_iter << endl;
	os << setiosflags(ios_base::scientific) << (double) generator.m_minor_iter << endl;

	return os;
}

// destructor
tlogdet_moons_generator::~tlogdet_moons_generator() {
	delete m_K; 	 m_K 	  = NULL;
	delete m_s;		 m_s 	  = NULL;
	delete m_subset; m_subset = NULL;
}
