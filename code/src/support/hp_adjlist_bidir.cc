/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : hp_adjlist_bidir.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 20.07.2012
// Description : Adjacency list class that offers optimized data structure to
//				 support high-performance graph function evaluation.
//============================================================================

#include "hp_adjlist_bidir.h"

// destructor
thp_adjlist_bidir::~thp_adjlist_bidir() {
	delete[] m_in_weights;
	delete[] m_in_startpos;
	delete[] m_in_end_nodes;
	delete[] m_out_weights;
	delete[] m_out_startpos;
	delete[] m_out_end_nodes;
}

// uses boost bgl to read the graph in DIMACS format and converts it
// to the high-performance structure
istream& operator>>(istream &is, thp_adjlist_bidir& adjlist_bidir) {
	tbgl_adjlist& graph = adjlist_bidir.bgl_adjlist();

	// clear the graph
	graph.clear();

	tvertex source, target;
	tvertex_index_map indices = get(vertex_index_t(), graph);
	tedge_capacity_map capacities = get(edge_capacity_t(), graph);
	treverse_edge_map reverses = get(edge_reverse_t(), graph);

	read_dimacs_max_flow(graph, capacities, reverses, source, target, is);
	adjlist_bidir.init(graph, indices[source], indices[target]);

	return is;
}
