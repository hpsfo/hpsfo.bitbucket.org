/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : bitset.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since       : 28.08.2012
// Description : Bitset implementation that provides compact bit set representation
//               this alternative competes with stl and makes possible to allocate the
//               size of the underlying set dynamically.
//============================================================================

#ifndef BITSET_H_
#define BITSET_H_

#include <assert.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <set>
#include <string>

#include "utils.h"

class tbitset {
private:
	typedef uint64_t bitset_data_t;

	size_t			m_capacity;
	size_t			m_size;
	bitset_data_t  *m_data;

	size_t round_up(size_t x, size_t p2) const;

public:
	// constructor
	tbitset(size_t capacity = 0);

	// constructor
	tbitset(size_t capacity, std::set<int> another);

	// copy constructor
	tbitset(const tbitset& another);

		bool operator<(const tbitset& another) const;
	tbitset& operator=(const tbitset& another);
		bool operator==(const tbitset& another) const;

		void reset();
		bool test(size_t i);
		void set(size_t i);
		void unset(size_t i);
	  size_t size();

	 // destructor
	 virtual ~tbitset();
};

#define BITSET_ONE UINT64_C(1)

#define BIT_TEST(x, n)   ((x)  &  (BITSET_ONE << (n)))  // test if n-th bit of x is set
#define BIT_SET(x, n)    ((x) |=  (BITSET_ONE << (n)))  // set n-th bit of x
#define BIT_UNSET(x, n)  ((x) &= ~(BITSET_ONE << (n)))  // unset n-th bit of x
#define BIT_TOGGLE(x, n) ((x) ^=  (BITSET_ONE << (n)))  // toggle n-th bit of x

#define WORD_BITS         (sizeof(bitset_data_t) * 8)
#define WORD_COUNT(bits)  (round_up(bits, WORD_BITS) / WORD_BITS)

// constructor
inline tbitset::tbitset(size_t capacity) {
	m_capacity = capacity;
	m_size	   = 0;
	m_data 	   = new bitset_data_t[WORD_COUNT(capacity)]();
}

// constructor
inline tbitset::tbitset(size_t capacity, std::set<int> another) {
	m_capacity = capacity;
	m_size	   = 0;
	m_data 	   = new bitset_data_t[WORD_COUNT(capacity)]();

	// copy over the existing vector
	for (std::set<int>::iterator iter = another.begin(); iter != another.end(); iter++) {
		set(*iter);
	}
}

// copy constructor
inline tbitset::tbitset(const tbitset& another) {
	m_capacity = another.m_capacity;
	m_size	   = another.m_size;
	int wcount = WORD_COUNT(m_capacity);
	m_data 	   = new bitset_data_t[wcount]();
	for (int i = 0; i < wcount; ++i) {
		m_data[i] = another.m_data[i];
	}
}

inline tbitset& tbitset::operator=(const tbitset& another) {
	m_capacity = another.m_capacity;
	m_size	   = another.m_size;
	delete[] m_data;
	int wcount = WORD_COUNT(m_capacity);
	m_data 	   = new bitset_data_t[wcount]();
	for (int i = 0; i < wcount; ++i) {
		m_data[i] = another.m_data[i];
	}

	return *this;
}

inline bool tbitset::operator==(const tbitset& another) const {
	if (m_capacity != another.m_capacity) {
		return false;
	}

	int wcount = WORD_COUNT(m_capacity);
	for (int i = 0; i < wcount; ++i) {
		if (m_data[i] != another.m_data[i]) {
			return false;
		}
	}

	return true;
}

inline size_t tbitset::size() {
	return m_size;
}

inline void tbitset::reset() {
	assert(m_data != NULL);

	m_size = 0;

	size_t nbytes = WORD_COUNT(m_capacity) * sizeof(*(m_data));
	memset(m_data, 0, nbytes);
}

inline size_t tbitset::round_up(size_t x, size_t p2) const {
	assert(p2 > 0 && (p2 & (p2 - 1)) == 0);  // is power of two

	return (x + (p2 - 1)) & ~(p2 - 1);
}

inline bool tbitset::test(size_t i) {
	assert(i < m_capacity);

	return BIT_TEST(m_data[i / WORD_BITS], i % WORD_BITS) != 0;
}

inline void tbitset::set(size_t i) {
	assert(i < m_capacity);

	if (!test(i)) {
		BIT_SET(m_data[i / WORD_BITS], i % WORD_BITS);
		m_size++;
	}
}

inline void tbitset::unset(size_t i) {
	assert(i < m_capacity);

	if (test(i)) {
		BIT_UNSET(m_data[i / WORD_BITS], i % WORD_BITS);
		m_size--;
	}
}

inline bool tbitset::operator<(const tbitset& another) const {
	assert(m_capacity == another.m_capacity);

	int wcount = WORD_COUNT(m_capacity);
	return std::lexicographical_compare(m_data, &m_data[wcount],
			another.m_data, &another.m_data[wcount]);
}

// destructor
inline tbitset::~tbitset() {
	m_capacity = 0;
	m_size	   = 0;

	delete[] m_data;
	m_data = NULL;
}

#endif // BITSET_H_
