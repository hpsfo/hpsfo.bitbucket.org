/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : utils.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
//               Andrei Frunzaa (funzaa@student.ethz.ch)
//               Zaheer Chothia (zchothia@student.ethz.ch)
// Version     : 1.0
// Description : Aggregates all utility headers
//============================================================================

#ifndef UTILS_H_
#define UTILS_H_

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#if defined( __INTEL_COMPILER )
	template <typename T>
	struct sfo_type {
		typedef T* restrict __attribute__ ((align(32))) aptr32;
	};

#elif defined( __GNUG__ )
	template <typename T>
	struct sfo_type {
		typedef T* __restrict __attribute__ ((aligned(32))) aptr32;
	};
#endif

#include "sfo.h"
#include "linear_sort.h"

// maximum needed for testing
const static int DEFAULT_MAX_M_N = 2000;

template <typename T>
T read_env(const char* name, T def_value);

#endif /* UTILS_H_ */
