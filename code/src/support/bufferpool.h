/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : bufferpool.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 05.04.2012
// Description : Buffer pool for pre-allocating all needed memory
//============================================================================

#ifndef BUFFERPOOL_H_
#define BUFFERPOOL_H_

#include <stdio.h>
#include <stdlib.h>
#include <queue>

using namespace std;

enum talignment {
	PAGE_ALIGNED, SSE_ALIGNED, AVX_ALIGNED, NO_ALIGNMENT
};

template<class T>
class tbufferpool {
private:
	const long 		 m_initial;
	const long 		 m_size;
	const talignment m_alignment;
	vector<T*> 		 m_queue;
	vector<T*>       m_all;

public:
	// constructor
	tbufferpool(long initial, long size, talignment alignment = NO_ALIGNMENT);

	// get next buffer element from the pool
	T* next();

	// release next element from the pool
	void release(T* buffer);

	void ensure_size(long size);

	// destructor
	virtual ~tbufferpool();
};

// constructor
template <typename T>
inline tbufferpool<T>::tbufferpool(long initial, long size, talignment alignment)
	: m_initial(initial), m_size(size), m_alignment(alignment) {
	assert(initial > 0);
	assert(size > 0);

//	printf("initial=%ld, size=%ld, alignment=%d\n", initial, size, alignment);

	switch (m_alignment) {
		case PAGE_ALIGNED: {
			for (long i = 0; i < m_initial; ++i) {
				T* buffer = NULL;
				posix_memalign((void**) &buffer, sysconf(_SC_PAGESIZE), m_size*sizeof(T));
//				memset(buffer, 0, m_size*sizeof(T));
				m_queue.push_back(buffer);
				m_all.push_back(buffer);
			}
			break;
		}
		case SSE_ALIGNED: {
			for (long i = 0; i < m_initial; ++i) {
				T* buffer = NULL;
				posix_memalign((void**) &buffer, 16, m_size*sizeof(T));
//				memset(buffer, 0, m_size*sizeof(T));
				m_queue.push_back(buffer);
				m_all.push_back(buffer);
			}
			break;
		}
		case AVX_ALIGNED: {
			for (long i = 0; i < m_initial; ++i) {
				T* buffer = NULL;
				posix_memalign((void**) &buffer, 32, m_size*sizeof(T));
//				memset(buffer, 0, m_size*sizeof(T));
				m_queue.push_back(buffer);
				m_all.push_back(buffer);
			}
			break;
		}
		case NO_ALIGNMENT:
		default: {
			for (long i = 0; i < m_initial; ++i) {
				T* buffer = new T[m_size]();
				m_queue.push_back(buffer);
				m_all.push_back(buffer);
			}
		}
	}
}

// get next buffer element from the pool
template <typename T>
inline T* tbufferpool<T>::next() {
	// check for pool overflow
	assert(!m_queue.empty());
	if (m_queue.empty()) {
		fprintf(stderr, "Illegal bufferpool state, our bufferpool has %ld buffers only.\n", m_initial);
		exit(EXIT_FAILURE);
	}
	T* next_buffer = m_queue.back();
	m_queue.pop_back();
	return next_buffer;
}

// release next element from the pool
template <typename T>
inline void tbufferpool<T>::release(T* buffer) {
	if (buffer != NULL) {
		m_queue.push_back(buffer);
	}
}

template <typename T>
inline void tbufferpool<T>::ensure_size(long size) {
	assert(size < m_size);
	if (size >= m_size) {
		fprintf(stderr, "Illegal bufferpool state, maximum buffer size is %ld.\n", m_size);
		exit(EXIT_FAILURE);
	}
}

// destructor
template <typename T>
inline tbufferpool<T>::~tbufferpool() {
	m_queue.clear();
	switch (m_alignment) {
		case PAGE_ALIGNED:
		case SSE_ALIGNED:
		case AVX_ALIGNED:
		{
			for (long i = 0; i < m_initial; ++i) {
				free(m_all[i]);
				m_all[i] = NULL;
			}
			break;
		}
		case NO_ALIGNMENT:
		default: {
			for (long i = 0; i < m_initial; ++i) {
				delete[] m_all[i];
				m_all[i] = NULL;
			}
		}
	}
	m_all.clear();
}

#endif /* BUFFERPOOL_H_ */
