/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : linear_sort.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Concrete linear sort O(n) algorithm
//============================================================================

#ifndef LINEAR_SORT_H_
#define LINEAR_SORT_H_

#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "sfo.h"

using namespace std;

template <typename T>
class tsfo_vector;

template <typename T>
class tlinear_sort {
private:
	typename sfo_type<T>::aptr32 m_sorted;
	const size_t 				 m_size;

public:
	// constructor
	tlinear_sort(size_t n);

	// sort method
	void sort(tsfo_vector<T>& a, bool asc);

	// destructor
	virtual ~tlinear_sort();
};

// constructor
template <typename T>
inline tlinear_sort<T>::tlinear_sort(size_t size) : m_size(size) {
	posix_memalign((void**) &m_sorted, 32, m_size*sizeof(T));
}

template <typename T>
inline void tlinear_sort<T>::sort(tsfo_vector<T>& a, bool asc) {
	typename sfo_type<T>::aptr32 data = a.data();
	const size_t 	 		     size = a.size();

	// calculate min/max in one go
	int min  = data[0];
	int max  = data[0];
	#pragma simd
	for (int i = 1; i < (int) size; ++i) {
		int data_i = data[i];

		if (data_i < min) {
			min = data_i;
		}

		if (data_i > max) {
			max = data_i;
		}
	}

	const int last = (max - min);
	if (last >= m_size) {
		printf("maximum exceeds size, increase this size and try again.");
		exit(EXIT_FAILURE);
	}

	// blank sorted
	memset(m_sorted, 0, (last + 1)*sizeof(T));

	#pragma simd
	for (int i = 0; i < (int) size; ++i) {
		int idx = data[i] - min;
		m_sorted[idx]++;
	}

	int i = 0;
	int sorted_j;
	if (asc) {
		#pragma simd
		for (int j = 0; j <= last; ++j) {
			int min_plus_j = min + j;
			sorted_j = m_sorted[j];
			for (int k = 0; k < sorted_j; ++k, ++i) {
				data[i] = min_plus_j + k;
			}
		}
	} else {
		#pragma simd
		for (int j = last; j >= 0; --j) {
			int min_plus_j = min + j;
			sorted_j = m_sorted[j];
			for (int k = (sorted_j - 1); k >= 0; --k, ++i) {
				data[i] = min_plus_j + k;
			}
		}
	}
}

// destructor
template <typename T>
inline tlinear_sort<T>::~tlinear_sort() {
	free(m_sorted);
	m_sorted = NULL;
}

#endif  // LINEAR_SORT_H_
