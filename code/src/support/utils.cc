/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : utils.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Description : Aggregates all utility headers
//============================================================================

#include <cstdlib>
#include <iostream>

#include "utils.h"

using namespace std;

template<>
int read_env(const char* name, int def_value) {
	char *e, *env = getenv(name);
	int result = def_value;
	if (env) {
		result = strtol(env, &e, 0);
		if (*e != 0) {
			cerr << "Environment '" << name << "' unparsable (contains '" << env
					<< "'), aborting.\n";
			exit(-1);
		}
	}

	return result;
}
