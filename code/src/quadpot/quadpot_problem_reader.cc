/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : quadpot_problem_reader.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 16.10.2012
// Description : Parses quadratic potential problem files
//============================================================================

#include <cmath>
#include <iomanip>

#include "quadpot_problem_reader.h"

static const int DEFAULT_PRECISION = 50;

// constructor
tquadpot_problem_reader::tquadpot_problem_reader() {
	m_w 	  = NULL;
	m_regions = NULL;
}

istream& operator>>(istream &is, tquadpot_problem_reader& reader) {
	// read n
	int n;
	is >> n;

	// read weights
	reader.m_w = new vector<double>(n);
	for (int i = 0; i < n; ++i) {
		double value;
		is >> setiosflags(ios_base::scientific) >> setprecision(DEFAULT_PRECISION) >> value;
		(*reader.m_w)[i] = value;
	}

	// read the regions
	int m;
	is >> m;

	reader.m_regions = new vector<set<int> >(m);
	for (int i=0; i < m; ++i) {
		int m_i;
		is >> m_i;

		for (int j=0; j < m_i; ++j) {
			int value;
			is >> value;
			(*reader.m_regions)[i].insert(value);
		}
	}

	return is;
}

// destructor
tquadpot_problem_reader::~tquadpot_problem_reader() {
	delete m_w; 	  m_w 	    = NULL;
	delete m_regions; m_regions = NULL;
}
