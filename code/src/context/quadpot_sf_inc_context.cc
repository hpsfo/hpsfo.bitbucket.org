/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : quadpot_sf_inc_context.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 16.10.2012
// Description : Concrete Mincut incremental context
//============================================================================

#include "utils.h"
#include "sfo_vector.h"
#include "quadpot_sf_inc_context.h"

// constructor
tquadpot_sf_inc_context::tquadpot_sf_inc_context(const vector<double>& w, const vector<set<int> >& regions)
	: tabstract_sf_context(w.size()), tabstract_sf_inc_context(w.size()),
	  	  tquadpot_sf_context(w, regions), m_cardinalities(regions.size(), 0), m_bitset_regions(regions.size()) {
	m_f = 0.0;
	for (int i=0; i < m_bitset_regions.size(); ++i) {
		m_bitset_regions[i] = tbitset(w.size(), regions[i]);
	}
}

// function evaluation
double tquadpot_sf_inc_context::f_inc(int x) {
	m_f += m_w[x];
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count++;
#endif

	for (int j = 0; j < m_regions.size(); ++j) {
		// accumulated intersection, element is part of intersection?
		if (m_bitset_regions[j].test(x)) {
			m_f -= m_cardinalities[j]*(m_bitset_regions[j].size() - m_cardinalities[j]);

			m_cardinalities[j]++;

			m_f += m_cardinalities[j]*(m_bitset_regions[j].size() - m_cardinalities[j]);

//			m_f += m_bitset_regions[j].size() - 2*m_cardinalities[j] - 1;
//			m_cardinalities[j]++;
		}

#ifdef ENABLE_FLOPS_COUNT
		m_flops_count++;
#endif
	}

	return m_f;
}

// function reset
void tquadpot_sf_inc_context::reset() {
	m_f = 0.0;
	m_cardinalities.reset();
}

tquadpot_sf_inc_context::~tquadpot_sf_inc_context() {
	// empty
}
