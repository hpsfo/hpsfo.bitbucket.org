/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : corpussel_sf_inc_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 22.07.2012
// Description : Concrete Corpus Subset selection with incremental evaluation
//============================================================================

#ifndef CORPUSSEL_SF_INC_CONTEXT_H_
#define CORPUSSEL_SF_INC_CONTEXT_H_

#include <set>

#include "hp_adjlist_bidir.h"
#include "bitset.h"
#include "sfo_vector.h"
#include "abstract_sf_inc_context.h"
#include "corpussel_sf_context.h"

class tcorpussel_sf_inc_context : public virtual tabstract_sf_inc_context, public virtual tcorpussel_sf_context {
private:
	tbitset 	m_vocabulary_set;
	double		m_weight_sum;
	double		m_total_weight_sum;

public:
	// constructor
	tcorpussel_sf_inc_context(const thp_adjlist_bidir& graph, double lambda = DEFAULT_LAMBDA);

	// incremental function evaluation
	virtual double f_inc(int x);

	virtual void reset();

	virtual ~tcorpussel_sf_inc_context();
};

#endif /* CORPUSSEL_SF_INC_CONTEXT_H_ */
