/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : abstract_sf_inc_context.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Defines the abstract interface for incremental SF contexts
//============================================================================

#ifndef ABSTRACT_SFO_INCR_CONTEXT_H_
#define ABSTRACT_SFO_INCR_CONTEXT_H_

#include "abstract_sf_context.h"

class tabstract_sf_inc_context : public virtual tabstract_sf_context {
public:
	// default constructor
	tabstract_sf_inc_context(int n);

	// function reset
	virtual void reset() = 0;

	// incremental function evaluation
	virtual double f_inc(int x) = 0;

	// incremental block function evaluation
	virtual double f_inc_block(const tsfo_vector<int>& x);

	// virtual destructor
	virtual ~tabstract_sf_inc_context() {
		// empty
	}
};

// default constructor
inline tabstract_sf_inc_context::tabstract_sf_inc_context(int n) : tabstract_sf_context(n) {
	// empty
}

inline double tabstract_sf_inc_context::f_inc_block(const tsfo_vector<int>& x) {
	double result = 0.0;
	for (int i = 0; i < x.size(); ++i) {
		result = f_inc(x[i]);
	}
	return result;
}

#endif /* ABSTRACT_SFO_INCR_CONTEXT_H_ */
