/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : iwata_sf_context.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete Iwata context
//============================================================================

#include "iwata_sf_context.h"

double tiwata_sf_context::f(const tsfo_vector<int>& x) {
	int size = x.size();
	double sum = 0;

	for (int i = 0; i < x.size(); i++) {
		sum += 5 * (double) x[i];
		sum -= 2 * m_n;
	}

	return size * (m_n - size) - sum;
}

// virtual destructor
tiwata_sf_context::~tiwata_sf_context() {
	// empty
}
