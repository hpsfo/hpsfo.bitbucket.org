/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : abstract_sf_context.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Defines the abstract interface for SFO contexts
//============================================================================

#ifndef ABSTRACT_SFO_CONTEXT_H_
#define ABSTRACT_SFO_CONTEXT_H_

#include "sfo_vector.h"

class tabstract_sf_context {
protected:
	const int  		 m_n;
	uint64_t 	   	 m_flops_count;
	tsfo_vector<int> m_ground;

public:
	// default constructor
	tabstract_sf_context(int n);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x) = 0;

	// generate the ground set
	const tsfo_vector<int>& ground() const;

	// n reader
	int n() const;

	// flops count reader
	uint64_t flops_count() const;

	// post-process the solution
	virtual void postprocess(tsfo_vector<int>& subset);

	// virtual destructor
	virtual ~tabstract_sf_context() {
		// empty
	};
};

// default constructor
inline tabstract_sf_context::tabstract_sf_context(int n) : m_n(n) {
	m_flops_count = 0;
}

// postprocess
inline void tabstract_sf_context::postprocess(tsfo_vector<int>& subset) {
	// empty
}

// n reader
inline int tabstract_sf_context::n() const {
	return m_n;
}

// ground reader
inline const tsfo_vector<int>& tabstract_sf_context::ground() const {
	return m_ground;
}

// flops count reader
inline uint64_t tabstract_sf_context::flops_count() const {
	return m_flops_count;
}

#endif /* ABSTRACT_SFO_CONTEXT_H_ */
