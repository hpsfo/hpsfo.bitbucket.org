/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : corpussel_sf_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 18.07.2012
// Description : Concrete Corpus Subset Selection application context
//============================================================================

#ifndef ICORPUSSEL_CONTEXT_H_
#define ICORPUSSEL_CONTEXT_H_

#include "hp_adjlist_bidir.h"
#include "abstract_sf_context.h"

class tcorpussel_sf_context : public virtual tabstract_sf_context {
protected:
	const thp_adjlist_bidir& m_graph;

	const int     	  m_s, m_t;
	const double  	  m_lambda;

	// temporary reusable vector sets
	tsfo_vector<int>  m_a_set;
	tsfo_vector<int>  m_a_inv_set;

public:
	// constructor
	tcorpussel_sf_context(const thp_adjlist_bidir& graph, double lambda = DEFAULT_LAMBDA);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// post-process the solution
	virtual void postprocess(tsfo_vector<int>& subset);

	// virtual destructor
	virtual ~tcorpussel_sf_context();

	static const double DEFAULT_LAMBDA = 0.8;
};

#endif /* ICORPUSSEL_CONTEXT_H_ */
