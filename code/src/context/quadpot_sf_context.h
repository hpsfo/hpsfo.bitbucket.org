/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : quadpot_sf_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete Iwata context
//============================================================================

#ifndef QUADPOT_CONTEXT_H_
#define QUADPOT_CONTEXT_H_

#include <vector>
#include <set>

#include "abstract_sf_context.h"

using namespace std;

class tquadpot_sf_context : public virtual tabstract_sf_context {
protected:
	const vector<double>& 	 m_w;
	const vector<set<int> >& m_regions;

	// temporary members
	tsfo_vector<int> 	 	 m_sorted_x;

public:
	// constructor
	tquadpot_sf_context(const vector<double>& w, const vector<set<int> >& regions);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// virtual destructor
	virtual ~tquadpot_sf_context();
};

inline tquadpot_sf_context::tquadpot_sf_context(const vector<double>& w, const vector<set<int> >& regions)
	: tabstract_sf_context(w.size()), m_w(w), m_regions(regions) {
	// initialize the ground set
	for (int i = 0; i < m_n; i++) {
		m_ground.append(i);
	}
}

#endif /* QUADPOT_CONTEXT_H_ */
