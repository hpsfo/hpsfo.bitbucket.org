/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : quadpot_sf_context.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete Iwata context
//============================================================================

#include <algorithm>

#include "quadpot_sf_context.h"

double tquadpot_sf_context::f(const tsfo_vector<int>& x) {
	double result = 0.0;

	for (int i = 0; i < x.size(); ++i) {
		result += m_w[x[i]];
	}
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += x.size();
#endif

	x.to_set(m_sorted_x);
	vector<int> output;
	// check the readings
	for (int j = 0; j < m_regions.size(); ++j) {
		const set<int>& s_j = m_regions[j];

		output.clear();
		set_intersection(s_j.begin(), s_j.end(), m_sorted_x.begin(), m_sorted_x.end(), back_inserter(output));
		result += output.size()*(s_j.size() - output.size());
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count++;
#endif
	}

	return result;
}

// virtual destructor
tquadpot_sf_context::~tquadpot_sf_context() {
	// empty
}
