/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : iwata_sf_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete Iwata context
//============================================================================

#ifndef IWATA_CONTEXT_H_
#define IWATA_CONTEXT_H_

#include "abstract_sf_context.h"

class tiwata_sf_context : public tabstract_sf_context {
public:
	// constructor
	tiwata_sf_context(int n);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// virtual destructor
	virtual ~tiwata_sf_context();
};

inline tiwata_sf_context::tiwata_sf_context(int n) : tabstract_sf_context(n) {
	// initialize the ground set
	for (int i = 1; i <= m_n; i++) {
		m_ground.append(i);
	}
}

#endif /* IWATA_CONTEXT_H_ */
