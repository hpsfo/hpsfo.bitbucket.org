/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : icorpussel_sf_context.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 18.07.2012
// Description : Concrete Corpus Subset Selection application context
//============================================================================

#include "corpussel_sf_context.h"

tcorpussel_sf_context::tcorpussel_sf_context(const thp_adjlist_bidir& graph, double lambda) :
	tabstract_sf_context(graph.num_vertices()), m_graph(graph), m_s(graph.source()), m_t(graph.target()), m_lambda(lambda) {
	assert(m_graph.source() == 0);
	assert(m_graph.target() == 1);

	// build the ground set once
	const int* out_startpos  = m_graph.out_startpos();
	const int* out_end_nodes = m_graph.out_end_nodes();

	// compute and cache the ground set, the set of all utterances
	// or outgoing edges from the source
	for (int i = out_startpos[0]; i < out_startpos[1]; i++) {
		m_ground.push_back(out_end_nodes[i]);
	}
}

double tcorpussel_sf_context::f(const tsfo_vector<int>& x) {
	const sfo_type<int>::aptr32 	out_startpos  = m_graph.out_startpos();
	const sfo_type<int>::aptr32		out_end_nodes = m_graph.out_end_nodes();
	const sfo_type<int>::aptr32		in_startpos   = m_graph.in_startpos();
	const sfo_type<double>::aptr32	in_weights    = m_graph.in_weights();

	// compute and evaluate w on the set "V \ X"
	x.to_set(m_a_set);

	// reset and compute the inverse set
	m_a_inv_set.clear();
	assert(m_a_inv_set.size() == 0);
	set_difference(m_ground.begin(), m_ground.end(), m_a_set.begin(), m_a_set.end(),
			back_inserter(m_a_inv_set));

	double w = 0.0;
	for (int i = 0; i < m_a_inv_set.size(); ++i) {
		int a_i = m_a_inv_set[i];

		w += (double) in_weights[in_startpos[a_i]];
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count++;
#endif
	}

	// compute and evaluate Gamma vocabulary size
	set<int> vocabulary;
	int end_idx;
	for (int i = 0; i < m_a_set.size(); ++i) {
		int a_i = m_a_set[i];
		if (a_i < m_graph.num_vertices() - 1) {
			end_idx = out_startpos[a_i + 1];

		} else {
			end_idx = m_graph.num_edges();
		}

		for (int j = out_startpos[a_i]; j < end_idx; ++j) {
			vocabulary.insert(out_end_nodes[j]);
		}
	}

	double gamma = (double) vocabulary.size();

#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += 3;
#endif
	return w + m_lambda*gamma;
}

// post-process the solution
void tcorpussel_sf_context::postprocess(tsfo_vector<int>& subset) {
	for (int i = 0; i < subset.size(); i++) {
		int j = subset[i];
		assert(0 <= j && j < m_n);
		subset[i]++;
	}
}

// virtual destructor
tcorpussel_sf_context::~tcorpussel_sf_context() {
	// empty
}
