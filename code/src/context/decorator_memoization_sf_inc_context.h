/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : decorator_memoization_sf_inc_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 03.10.2012
// Description : Decorator implementation that achieves memoization or caches
// 				 previous function evaluation calls
//============================================================================

#ifndef IDECORATOR_MEMOIZATION_INC_CONTEXT_H_
#define IDECORATOR_MEMOIZATION_INC_CONTEXT_H_

#include "abstract_sf_inc_context.h"
#include "decorator_memoization_sf_context.h"
#include "bitset.h"

class tdecorator_memoization_sf_inc_context : public virtual tabstract_sf_inc_context, public virtual tdecorator_memoization_sf_context {
private:
	tabstract_sf_inc_context& m_inc_component;
	tsfo_vector<int>		  m_pending;

public:
	// constructor
	tdecorator_memoization_sf_inc_context(tabstract_sf_inc_context& sf_inc_context);

	// function evaluation
	virtual double f_inc(int x);

	// reset function
	virtual void reset();

	// virtual destructor
	virtual ~tdecorator_memoization_sf_inc_context();
};

inline tdecorator_memoization_sf_inc_context::tdecorator_memoization_sf_inc_context(tabstract_sf_inc_context& sf_inc_context)
	: tabstract_sf_context(sf_inc_context.n()), tabstract_sf_inc_context(sf_inc_context.n()),
	  	  tdecorator_memoization_sf_context(sf_inc_context), m_inc_component(sf_inc_context) {
	// empty
}

inline double tdecorator_memoization_sf_inc_context::f_inc(int x) {
	// keep track of the complete subset
	m_current_set.set(x);

	// check whether the current subset exist in cache
	if (m_memory.find(m_current_set) == m_memory.end()) {
		// is it an increment of exactly one element only?
		if (m_pending.is_empty()) {
			m_memory[m_current_set] = m_inc_component.f_inc(x);
		} else {
			// otherwise do a smarter block incremental evaluation
			m_pending.push_back(x);
			m_memory[m_current_set] = m_inc_component.f_inc_block(m_pending);
			m_pending.clear();
		}
	} else {
		// accumulate pending incremental updates
		m_pending.push_back(x);
		m_hit_count++;
	}
	m_total_count++;

	return m_memory[m_current_set];
}

inline void tdecorator_memoization_sf_inc_context::reset() {
	m_inc_component.reset();
	m_current_set.reset();
	m_pending.clear();
}

inline tdecorator_memoization_sf_inc_context::~tdecorator_memoization_sf_inc_context() {
	// empty
}

#endif /* IDECORATOR_MEMOIZATION_INC_CONTEXT_H_ */
