/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : mincut_sf_context.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 09.07.2012
// Description : Concrete Mincut context
//============================================================================

#include "utils.h"
#include "sfo_vector.h"
#include "mincut_sf_context.h"

// constructor
tmincut_sf_context::tmincut_sf_context(const thp_adjlist_bidir& graph)
	: tabstract_sf_context(graph.num_vertices()), m_graph(graph), m_s(graph.source()), m_t(graph.target()) {

	m_A = new tsfo_vector<int>();
	m_A->append(m_s);

	// Important: this invocation should only depend on m_graph
	// The other fields of gc_ctx are only initialized below
	m_F_A = actual_f(*m_A);

	// build and cache the ground set once
	for (int i = 0; i < m_n; i++) {
		// corresponds to sfo_setdiff_fast(V, [s, t])
		if (i != m_s && i != m_t) {
			m_ground.push_back(i);
		}
	}
}

// function evaluation
double tmincut_sf_context::f(const tsfo_vector<int>& x) {
	double F_A, F_AB = 0.0;

	F_A = m_F_A;

	tsfo_vector<int> AB = *(m_A);
	for (int i = 0; i < x.size(); i++) {
		AB.append(x[i]);
	}

	F_AB = actual_f(AB);

	return F_AB - F_A;
}

// post-process the solution
void tmincut_sf_context::postprocess(tsfo_vector<int>& subset) {
	for (int i = 0; i < subset.size(); i++) {
		int j = (int) subset[i];
		assert(0 <= j && j < m_n);
		subset[i]++;
	}
}

// destructor
tmincut_sf_context::~tmincut_sf_context() {
	delete m_A;
	m_A = NULL;
}
