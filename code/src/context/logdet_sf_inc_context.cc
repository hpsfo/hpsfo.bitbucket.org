/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : logdet_sf_inc_context.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 02.08.2012
// Description : Concrete Logdet SF incremental context
//============================================================================

#include <algorithm>
#include <boost/math/special_functions/fpclassify.hpp>

#include "logdet_sf_inc_context.h"

// constructor
tlogdet_sf_inc_context::tlogdet_sf_inc_context(const tsfo_matrix<double>& K, const tsfo_vector<double>& s, double fv, double epsilon)
	: tabstract_sf_context(K.cols()), tabstract_sf_inc_context(K.cols()), tlogdet_sf_context(K, s, fv), m_epsilon(epsilon) {

	// copy and compute full cholesky once
	m_Kva_cho_base = m_K;
	m_Kva_cho_base.cholesky();
	m_Kva_cho_base.reset(tsfo_matrix<double>::LOWER);
#ifdef ENABLE_FLOPS_COUNT
	uint64_t kva_cols = m_Kva_cho_base.cols();
	m_flops_count += kva_cols*kva_cols*kva_cols/3;
#endif
	// reset
	m_Kva_cho = m_Kva_cho_base;
	m_va.resize(m_n);
	for (int i = 0; i < m_n; ++i) {
		m_va[i] = i;
	}
}

// function evaluation
double tlogdet_sf_inc_context::f_inc(int x) {
	int col = (int) x;
	assert(0 <= col && col < m_n);

	// use binary search to find the actual j index to delete
	int j = lower_bound(m_va.begin(), m_va.end(), col) - m_va.begin();
	assert(m_va[j] == col);

	// delete the column from m_Kva_cho
//	m_Kva_cho.delete_column(j, j);
	m_Kva_cho.delete_column_with_gap(j, j, true);
	m_Kva_cho.triangularize(j, 1, genrot_positive_diagonal);
#ifdef ENABLE_FLOPS_COUNT
	uint64_t kva_cols = m_Kva_cho.cols();
	m_flops_count += kva_cols*kva_cols/2 - kva_cols*j + j*j/2;
#endif
	m_Kva_cho.delete_row_last();
	m_va.delete_elem(j);

	// append the column to m_Ka_cho
	m_K.subcolumn(col, m_a, m_xtR);
	double xtx = m_K(col, col);
	if (m_Ka_cho.is_empty()) {
		m_Ka_cho.resize(1, 1, xtx);
		m_Ka_cho.cholesky();
#ifdef ENABLE_FLOPS_COUNT
		uint64_t ka_cols = m_Ka_cho.cols();
		m_flops_count += ka_cols*ka_cols*ka_cols/3;
#endif

	} else {
		m_r = m_Ka_cho.solve_forward(m_xtR);
#ifdef ENABLE_FLOPS_COUNT
		uint64_t ka_cols = m_Ka_cho.cols();
		uint64_t ka_rows = m_Ka_cho.rows();
		m_flops_count += ka_rows*ka_cols;
#endif
		double rpp = xtx - m_r.dot_product(m_r);
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += m_r.size() + 1;
#endif

		if (rpp < m_epsilon) {
			// TODO: failed rank
//			printf("failed rank rpp=%e\n", rpp);
//			assert(false);
		}

		m_Ka_cho.append_row_with_value(0);
		m_r.push_back(sqrt(rpp));
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count++;
#endif
		m_Ka_cho.append_column(m_r);
	}

	m_a.push_back(col);

	double result = (m_Ka_cho.diag().ln().sum() + m_Kva_cho.diag().ln().sum())*2.0 - m_fv;
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += 2*m_Ka_cho.cols();
	m_flops_count += 2*m_Kva_cho.cols();
	m_flops_count += 3;
#endif

	result -= m_s.sum(m_a);
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += m_a.size() + 1;
#endif

	return result;
}

inline double tlogdet_sf_inc_context::f_inc_block(const tsfo_vector<int>& x) {
	double result = 0.0;
	if (x.size() <= 3) {
		result = tabstract_sf_inc_context::f_inc_block(x);
	} else {
		for (int i = 0; i < x.size(); ++i) {
			int col = (int) x[i];
			assert(0 <= col && col < m_n);

			// use binary search to find the actual j index to delete
			int j = lower_bound(m_va.begin(), m_va.end(), col) - m_va.begin();
			assert(m_va[j] == col);
			m_va.delete_elem(j);
			m_a.push_back(col);
		}

		m_K.submatrix(m_a , m_Ka_cho);
		m_K.submatrix(m_va, m_Kva_cho);

		m_Ka_cho.cholesky();
		m_Kva_cho.cholesky();
#ifdef ENABLE_FLOPS_COUNT
		uint64_t ka_cols  = m_Ka.cols();
		uint64_t kva_cols = m_Kva.cols();
		m_flops_count += ka_cols*ka_cols*ka_cols/3;
		m_flops_count += kva_cols*kva_cols*kva_cols/3;
#endif

		result = (m_Ka_cho.diag().ln().sum() + m_Kva_cho.diag().ln().sum())*2.0 - m_fv;
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 2*ka_cols;
		m_flops_count += 2*kva_cols;
		m_flops_count += 3;
#endif

		result -= m_s.sum(m_a);
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += m_a.size() + 1;
#endif
	}

	return result;
}

// function reset
void tlogdet_sf_inc_context::reset() {
	// reset
	m_Kva_cho = m_Kva_cho_base;
	m_Ka_cho.clear();

	m_a.clear();
	m_va.resize(m_n);
	for (int i = 0; i < m_n; ++i) {
		m_va[i] = i;
	}
}

// virtual destructor
tlogdet_sf_inc_context::~tlogdet_sf_inc_context() {
	// empty
}
