/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : matlab_sf_adapter.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 02.08.2012
// Description : Matlab adapter to invoke HPSFO from Matlab
//============================================================================

#ifndef MATLAB_SF_ADAPTER_H_
#define MATLAB_SF_ADAPTER_H_

#include "mex.h"

#include "abstract_sf_context.h"

class tmatlab_sf_adapter : public virtual tabstract_sf_context {
private:
	const mxArray* m_f;
	const mxArray* m_param;

	template <typename T>
	T read_param(const mxArray* param, const char* name);

public:
	// constructor
	tmatlab_sf_adapter(const mxArray* f, const mxArray* param);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// virtual destructor
	virtual ~tmatlab_sf_adapter();
};

#endif /* MATLAB_SF_ADAPTER_H_ */
