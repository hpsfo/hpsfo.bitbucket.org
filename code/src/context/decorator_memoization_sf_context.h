/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : decorator_memoization_sf_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 03.10.2012
// Description : Decorator implementation that achieves memoization or caches
// 				 previous function evaluation calls
//============================================================================

#ifndef IDECORATOR_MEMOIZATION_CONTEXT_H_
#define IDECORATOR_MEMOIZATION_CONTEXT_H_

#include "abstract_sf_context.h"
#include "bitset.h"

class tdecorator_memoization_sf_context : public virtual tabstract_sf_context {
private:
	tabstract_sf_context& m_component;

protected:
	map<tbitset, double>  m_memory;
	tbitset				  m_current_set;
	uint64_t			  m_hit_count;
	uint64_t			  m_total_count;

public:
	// constructor
	tdecorator_memoization_sf_context(tabstract_sf_context& sf_context);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// post-process the solution
	virtual void postprocess(tsfo_vector<int>& subset);

	double hit_ratio() const {
		return (double) m_hit_count / (double) m_total_count;
	}

	// virtual destructor
	virtual ~tdecorator_memoization_sf_context();
};

inline tdecorator_memoization_sf_context::tdecorator_memoization_sf_context(tabstract_sf_context& sf_context)
	: tabstract_sf_context(sf_context.n()), m_component(sf_context), m_current_set(sf_context.n()) {
	m_ground      = sf_context.ground();
	m_hit_count   = 0;
	m_total_count = 0;
}

// postprocess
inline void tdecorator_memoization_sf_context::postprocess(tsfo_vector<int>& subset) {
	m_component.postprocess(subset);
}

inline double tdecorator_memoization_sf_context::f(const tsfo_vector<int>& x) {
	m_current_set.reset();
	for (int i = 0; i < x.size(); ++i) {
		m_current_set.set(x[i]);
	}

	if (m_memory.find(m_current_set) == m_memory.end()) {
		m_memory[m_current_set] = m_component.f(x);
	} else {
		m_hit_count++;
	}
	m_total_count++;

	return m_memory[m_current_set];
}

inline tdecorator_memoization_sf_context::~tdecorator_memoization_sf_context() {
	// empty
}

#endif /* IDECORATOR_MEMOIZATION_CONTEXT_H_ */
