/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : logdet_sf_context.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Concrete Logdet SF context
//============================================================================

#include <iostream>
#include <iomanip>

#include "logdet_sf_context.h"

// constructor
tlogdet_sf_context::tlogdet_sf_context(const tsfo_matrix<double>& K, const tsfo_vector<double>& s, double fv)
	: tabstract_sf_context(K.cols()), m_K(K), m_s(s), m_fv(fv) {

	assert(K.is_squared());
	assert(K.cols() == s.size());

	// initialize a sequence from zero to (n - 1)
	for (int i = 0; i < m_n; ++i) {
		m_ground.push_back(i);
	}
}

// function evaluation
double tlogdet_sf_context::f(const tsfo_vector<int>& x) {
	// setup the 'a' and 'va' index sets where va = seq1p - a
	x.to_set(m_a);

	m_va.clear();
	set_difference(m_ground.begin(), m_ground.end(), m_a.begin(), m_a.end(),
			back_inserter(m_va));

	// first step, compute the two matrices Ka and Kva as submatrices of K
	m_K.submatrix(m_a , m_Ka);
	m_K.submatrix(m_va, m_Kva);

	double result = (m_Ka.cholesky().diag().ln().sum() + m_Kva.cholesky().diag().ln().sum())*2.0 - m_fv;
#ifdef ENABLE_FLOPS_COUNT
	uint64_t ka_cols  = m_Ka.cols();
	uint64_t kva_cols = m_Kva.cols();
	m_flops_count += ka_cols*ka_cols*ka_cols/3 + 2*ka_cols;
	m_flops_count += kva_cols*kva_cols*kva_cols/3 + 2*kva_cols;
	m_flops_count += 3;
#endif

	result -= m_s.sum(m_a);
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += m_a.size() + 1;
#endif

	return result;
}

// post-process the solution
void tlogdet_sf_context::postprocess(tsfo_vector<int>& subset) {
	for (int i = 0; i < subset.size(); i++) {
		int j = subset[i];
		assert(0 <= j && j < m_n);
		subset[i]++;
	}
}

// virtual destructor
tlogdet_sf_context::~tlogdet_sf_context() {
	// empty
}
