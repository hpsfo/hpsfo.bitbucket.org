/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : logdet_sf_context.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 11.07.2012
// Description : Concrete Logdet SF context
//============================================================================

#ifndef LOGDET_SF_CONTEXT_H_
#define LOGDET_SF_CONTEXT_H_

#include "sfo_vector.h"
#include "sfo_matrix.h"
#include "abstract_sf_context.h"

class tlogdet_sf_context : public virtual tabstract_sf_context {
private:
	// temporary reusable vector sets
	tsfo_vector<int> 	m_a;
	tsfo_vector<int> 	m_va;

protected:
	// kernel matrix
	const tsfo_matrix<double>&	m_K;
	const tsfo_vector<double>& 	m_s;
	const double		  		m_fv;

	// used internally
	tsfo_matrix<double> m_Ka;
	tsfo_matrix<double> m_Kva;

public:
	// constructor
	tlogdet_sf_context(const tsfo_matrix<double>& K, const tsfo_vector<double>& s, double fv);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// post-process the solution
	virtual void postprocess(tsfo_vector<int>& subset);

	// virtual destructor
	virtual ~tlogdet_sf_context();
};

#endif /* LOGDET_SF_CONTEXT_H_ */
