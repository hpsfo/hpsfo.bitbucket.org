/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : mincut_sf_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 09.07.2012
// Description : Concrete Mincut SF context
//============================================================================

#ifndef MINCUT_CONTEXT_H_
#define MINCUT_CONTEXT_H_

#include "sfo_vector.h"
#include "hp_adjlist_bidir.h"
#include "abstract_sf_context.h"
#include "utils.h"

class tmincut_sf_context : public virtual tabstract_sf_context {
protected:
	const thp_adjlist_bidir& m_graph;

	// 'inherited' from residual_context
	tsfo_vector<int>* m_A;
	double 			  m_F_A;

	const int 		  m_s, m_t;
	double			  m_F_old;

	// temporary reusable vector sets
	tsfo_vector<int>  m_a_set;
	tsfo_vector<int>  m_a_inv_set;

	// wrapped f
	double actual_f(const tsfo_vector<int>& x);

public:
	// constructor
	tmincut_sf_context(const thp_adjlist_bidir& graph);

	// function evaluation
	virtual double f(const tsfo_vector<int>& x);

	// post-process the solution
	virtual void postprocess(tsfo_vector<int>& subset);

	// virtual destructor
	virtual ~tmincut_sf_context();
};

// wrapped f
inline double tmincut_sf_context::actual_f(const tsfo_vector<int>& x) {
	int k, a_i, end_idx;
	double sum = 0.0;

	const sfo_type<int>::aptr32		out_startpos  = m_graph.out_startpos();
	const sfo_type<int>::aptr32	 	out_end_nodes = m_graph.out_end_nodes();
	const sfo_type<double>::aptr32	out_weights   = m_graph.out_weights();
	const sfo_type<int>::aptr32		in_startpos   = m_graph.in_startpos();
	const sfo_type<int>::aptr32		in_end_nodes  = m_graph.in_end_nodes();
	const sfo_type<double>::aptr32	in_weights    = m_graph.in_weights();

	x.to_set(m_a_set);

	assert(x.size() < m_n);
#ifndef NDEBUG
	// neither partition should be empty
	for (int i = 0; i < m_a_set.size(); i++) {
		int j = m_a_set[i];
		assert(0 <= j && j < m_n);
	}
#endif

	// compute the inverse set
	m_a_inv_set.clear();
	assert(m_a_inv_set.size() == 0);
//	set_difference(m_ground_with_st.begin(), m_ground_with_st.end(), m_a_set.begin(), m_a_set.end(),
//			inserter(m_a_inv_set, m_a_inv_set.end()));

	int j = 0;
	for (int i = 0; i < m_n; i++) {
		if (j == m_a_set.size() || m_a_set[j] != i) {
			m_a_inv_set.push_back(i);
		} else {
			j++;
		}
	}

	for (int i = 0; i < m_a_set.size(); i++) {
		a_i = m_a_set[i];
		j = 0;

		//	go over all the edges starting in node i from subset x
		if (a_i < m_n - 1) {
			end_idx = out_startpos[a_i + 1];

		} else {
			end_idx = m_graph.num_edges();
		}

		for (k = out_startpos[a_i]; k < end_idx; k++) {
			//	if the end node of the edge is in V\x add the value to the cut cost
			//	the nodes are sorted ascending in V\x as well as in the end_nodes vector
			while (m_a_inv_set[j] < out_end_nodes[k]) {
				j++;
				if (j == m_a_inv_set.size())
					break;
			}

			if (j == m_a_inv_set.size())
				break;

			//	if the end node of the edge is in V\x add the value to the cut cost
			if (m_a_inv_set[j] == out_end_nodes[k]) {
				sum += out_weights[k];
#ifdef ENABLE_FLOPS_COUNT
				m_flops_count++;
#endif
			}
		}
	}

	return sum;
}

#endif /* MINCUT_CONTEXT_H_ */
