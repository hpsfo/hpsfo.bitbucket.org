/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : mincut_sf_inc_context.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 06.07.2012
// Description : Concrete Mincut incremental context
//============================================================================

#include "utils.h"
#include "sfo_vector.h"
#include "mincut_sf_inc_context.h"

// constructor
tmincut_sf_inc_context::tmincut_sf_inc_context(const thp_adjlist_bidir& graph)
	: tabstract_sf_context(graph.num_vertices()), tabstract_sf_inc_context(graph.num_vertices()),
	  	  tmincut_sf_context(graph), m_current_set((size_t) m_n) {
	// empty
}

// function evaluation
double tmincut_sf_inc_context::f_inc(int x) {
	double F_A = m_F_A;

	int num_vertices = m_graph.num_vertices();

	double F_AB = m_F_old;

	int node = (int) x;
	assert(0 <= node && node < num_vertices);

	m_current_set.set((size_t) node); // add node to our partition

	const sfo_type<int>::aptr32		out_startpos  = m_graph.out_startpos();
	const sfo_type<int>::aptr32		out_end_nodes = m_graph.out_end_nodes();
	const sfo_type<double>::aptr32	out_weights   = m_graph.out_weights();
	const sfo_type<int>::aptr32		in_startpos   = m_graph.in_startpos();
	const sfo_type<int>::aptr32		in_end_nodes  = m_graph.in_end_nodes();
	const sfo_type<double>::aptr32	in_weights    = m_graph.in_weights();

	// add outgoing edges, excluding nodes in the current partition
	// f_new += sum(weights(outgoing(node) \ node_set))
	int node_end, edge_limit;
	double weight;
	int edge_pos = out_startpos[node];
	if (node < num_vertices - 1) {
		edge_limit = out_startpos[node + 1];
	} else {
		edge_limit = m_graph.num_edges();
	}
	while (edge_pos < edge_limit) {
		assert(edge_pos < m_graph.num_edges());
		node_end = out_end_nodes[edge_pos];
		weight = out_weights[edge_pos];
		assert(0 <= node_end && node_end < num_vertices);
		assert(weight > 0);

		if (!m_current_set.test((size_t) node_end)) {
			F_AB += weight;
		}
		edge_pos++;
	}

	// subtract incoming edges into this partition
	// f_new -= sum(weights(incoming(node) intersect current_set))
	edge_pos = in_startpos[node];
	if (node < num_vertices - 1) {
		edge_limit = in_startpos[node + 1];
	} else {
		edge_limit = m_graph.num_edges();
	}
	while (edge_pos < edge_limit) {
		assert(edge_pos < m_graph.num_edges());
		node_end = in_end_nodes[edge_pos];
		weight = in_weights[edge_pos];
		assert(0 <= node_end && node_end < num_vertices);
		assert(weight > 0);

		if (m_current_set.test((size_t) node_end)) {
			F_AB -= weight;
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count++;
#endif
		}
		edge_pos++;
	}

	m_F_old = F_AB;

	return F_AB - F_A;
}

// function reset
void tmincut_sf_inc_context::reset() {
	m_F_old = m_F_A;

	m_current_set.reset();
	for (int i = 0; i < m_A->size(); i++) {
		m_current_set.set((size_t)(*(m_A))[i]);
	}
}

tmincut_sf_inc_context::~tmincut_sf_inc_context() {
	// empty
}
