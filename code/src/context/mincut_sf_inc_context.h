/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : mincut_sf_inc_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete Mincut incremental context
//============================================================================

#ifndef MINCUT_INC_CONTEXT_H_
#define MINCUT_INC_CONTEXT_H_

#include "hp_adjlist_bidir.h"
#include "bitset.h"
#include "sfo_vector.h"
#include "abstract_sf_inc_context.h"
#include "mincut_sf_context.h"

class tmincut_sf_inc_context : public virtual tabstract_sf_inc_context, public virtual tmincut_sf_context {
private:
	// current set partition
	tbitset m_current_set;

public:
	// constructor
	tmincut_sf_inc_context(const thp_adjlist_bidir& graph);

	// function evaluation
	virtual double f_inc(int x);

	// function reset
	virtual void reset();

	// virtual destructor
	virtual ~tmincut_sf_inc_context();
};

#endif /* MINCUT_INC_CONTEXT_H_ */
