/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : logdet_sf_inc_context.h
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 02.08.2012
// Description : Concrete Logdet SF incremental context
//============================================================================

#ifndef LOGDET_SF_INC_CONTEXT_H_
#define LOGDET_SF_INC_CONTEXT_H_

#include "logdet_sf_context.h"
#include "abstract_sf_inc_context.h"
#include "sfo_matrix_tria.h"

class tlogdet_sf_inc_context : public virtual tabstract_sf_inc_context, public virtual tlogdet_sf_context {
private:
	static const double DEFAULT_EPSILON = 1e-10;

	const double 	     	 m_epsilon;
	tsfo_matrix_tria<double> m_Ka_cho;
	tsfo_matrix_tria<double> m_Kva_cho;
	tsfo_matrix_tria<double> m_Kva_cho_base;

	// temporary reusable matrix and vector
	tsfo_vector<double> 	 m_xtR;
	tsfo_vector<double>		 m_r;
	tsfo_vector<int> 		 m_a;
	tsfo_vector<int> 		 m_va;

public:
	// constructor
	tlogdet_sf_inc_context(const tsfo_matrix<double>& K, const tsfo_vector<double>& s, double fv, double epsilon = DEFAULT_EPSILON);

	// incremental function evaluation
	virtual double f_inc(int x);

	// incremental block function evaluation
	virtual double f_inc_block(const tsfo_vector<int>& x);

	// function reset
	virtual void reset();

	// virtual destructor
	virtual ~tlogdet_sf_inc_context();
};

#endif /* LOGDET_SF_INC_CONTEXT_H_ */
