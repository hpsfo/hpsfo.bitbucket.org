/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : corpussel_sf_inc_context.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 22.07.2012
// Description : Concrete Corpus Subset selection with incremental evaluation
//============================================================================

#include "corpussel_sf_inc_context.h"

// constructor
tcorpussel_sf_inc_context::tcorpussel_sf_inc_context(const thp_adjlist_bidir& graph, double lambda)
	: tabstract_sf_context(graph.num_vertices()), tabstract_sf_inc_context(graph.num_vertices()),
	  	  tcorpussel_sf_context(graph, lambda), m_vocabulary_set(m_n - m_ground.size() - 2) {

	// sum the weights of the utterances that are not in the set
	const sfo_type<int>::aptr32		in_startpos = m_graph.in_startpos();
	const sfo_type<double>::aptr32	in_weights  = m_graph.in_weights();
	int n = m_ground.size();
	int ground_0 = m_ground[0];
	m_total_weight_sum = 0.0;
	for (int i = 0; i < n; ++i) {
		int idx = in_startpos[i + ground_0];
		m_total_weight_sum += (double) in_weights[idx];
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count++;
#endif
	}
	m_weight_sum = m_total_weight_sum;
}

// function evaluation
double tcorpussel_sf_inc_context::f_inc(int x) {
	// make sure it is an utterance
	assert(m_ground[0] <= x && x <= m_ground[m_ground.size() - 1]);

	const sfo_type<int>::aptr32		out_startpos  = m_graph.out_startpos();
	const sfo_type<int>::aptr32		out_end_nodes = m_graph.out_end_nodes();
	const sfo_type<int>::aptr32		in_startpos   = m_graph.in_startpos();
	const sfo_type<double>::aptr32	in_weights    = m_graph.in_weights();

	int num_vertices = m_graph.num_vertices();

	// sum the weight of the utterances already in the set
	int idx = in_startpos[x];
	m_weight_sum -= (double) in_weights[idx];
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count++;
#endif

	// update the current vocabulary
	int a_i = x;
	int end_idx;
	if (a_i < num_vertices - 1) {
		end_idx = out_startpos[a_i + 1];
	} else {
		end_idx = m_graph.num_edges();
	}

	int n = m_ground.size();
	for (int j = out_startpos[a_i]; j < end_idx; ++j) {
		int element = out_end_nodes[j] - n - 2;
		m_vocabulary_set.set(element);
	}

	int vocabulary_size = m_vocabulary_set.size();

	double gamma = (double) vocabulary_size;

#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += 3;
#endif
	return m_weight_sum + m_lambda*gamma;
}

// function reset
void tcorpussel_sf_inc_context::reset() {
	// reset the vocabulary set
	m_vocabulary_set.reset();
	m_weight_sum = m_total_weight_sum;
}

tcorpussel_sf_inc_context::~tcorpussel_sf_inc_context() {
	// empty
}
