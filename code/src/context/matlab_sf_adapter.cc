/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : matlab_sf_adapter.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since	   : 02.08.2012
// Description : Matlab adapter to invoke HPSFO from Matlab
//============================================================================

#include <algorithm>

#include "matlab_sf_adapter.h"

using namespace std;

// constructor
tmatlab_sf_adapter::tmatlab_sf_adapter(const mxArray* f, const mxArray* param)
	: tabstract_sf_context(read_param<int>(param, "n")) {
	m_f		= f;
	m_param = param;

	mxArray* ground = mxGetField(m_param, 0, "ground");
//	mexPrintf("'ground' is %p\n", ground);

	if (ground == NULL) {
		mexErrMsgTxt("ERROR: param must include array field 'ground'.");

	} else {
		int m = mxGetM(ground);
		int n = mxGetN(ground);

		if (m > 1 && n > 1 ) {
			mexErrMsgTxt("ERROR: 'ground' should not be a matrix.");

		} else {
			const int size = max(m, n);
			for (int i = 0; i < size; ++i) {
				m_ground.push_back((mxGetPr(ground))[i]);
			}
		}
	}
}

template <typename T>
T tmatlab_sf_adapter::read_param(const mxArray* param, const char* name) {
	mxArray* array = mxGetField(param, 0, name);
	if (array == NULL) {
		char str[100];
		sprintf(str, "ERROR: param must include array field '%s'.", name);
		mexErrMsgTxt(str);
	}

	return (T) mxGetPr(array)[0];
}

// function evaluation
double tmatlab_sf_adapter::f(const tsfo_vector<int>& x) {
	// copy to a MATLAB array
	mxArray *xm = mxCreateDoubleMatrix(x.size(), 1, mxREAL);
	for (int i = 0; i < x.size(); ++i) {
		(mxGetPr(xm))[i] = x[i];
	}

	// invoke the MATLAB callback.
	mxArray *lhs;
	mxArray *rhs[3];
	rhs[0] = const_cast<mxArray*>(m_f);
	rhs[1] = const_cast<mxArray*>(m_param);
	rhs[2] = xm;
	mexCallMATLAB(1, &lhs, 3, rhs, "feval");

	// get return value.
	double result = *mxGetPr(lhs);
//	mexPrintf("Output of the function handle %lf\n", result);

	// clean UP
	mxDestroyArray(lhs);
	mxDestroyArray(xm);

	return result;
}

// destructor
tmatlab_sf_adapter::~tmatlab_sf_adapter() {
	// empty
}
