/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : mincut_main.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Version     : 1.0
// Since       : 25.06.2012
// Description : Main application for solving minimum cut problems. Requires
//               as input a minimum cut problem file in genrmf format.
//============================================================================

#include <assert.h>
#include <iostream>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>

#include <boost/program_options/cmdline.hpp>
#include <boost/program_options/config.hpp>
#include <boost/program_options/environment_iterator.hpp>
#include <boost/program_options/eof_iterator.hpp>
#include <boost/program_options/errors.hpp>
#include <boost/program_options/option.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/positional_options.hpp>
#include <boost/program_options/value_semantic.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/program_options/version.hpp>

#include "hp_adjlist_bidir.h"
#include "sfo_vector.h"
#include "sfo_matrix.h"
#include "sfo_matrix_tria.h"
#include "sfo_function_mincut.h"

using namespace std;
namespace po = boost::program_options;

int main(int argc, char** argv) {
#if !defined(NDEBUG) || defined(DEBUG)
	fprintf(stderr, "Warning: you are running in debug mode - assertions are enabled. \n");
	fprintf(stderr, "SFO_MAX_M_N=%d, set environment 'SFO_MAX_M_N' to change this size\n", tsfo_matrix<double>::MAX_M_N);
#endif

	try {
		// declare supported options
		string kernel, workload, gernmfile;

		po::options_description desc("Mincut main options");
		desc.add_options()
			("help", "produce help message")
			("kernel", po::value<string>(&kernel)->default_value("hpsfo"), "Sfo kernel e.g. krause, fujishige, hpsfo, edmonds-karp")
			("input-file", po::value<string>(&gernmfile), "mincut germf input file")
			("triangularize-nb", po::value<int>(&tsfo_matrix_tria<double>::TRIANGULARIZE_NB)->default_value(tsfo_matrix_tria<double>::TRIANGULARIZE_NB), "triangularize NB block size e.g. 4")
		;

		po::variables_map vm;
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);

		if (vm.count("help")) {
			cout << desc << "\n";
			return EXIT_FAILURE;

		} else {
			if (gernmfile.empty()) {
				cerr << "Input Error: 'input-file' must be provided.\n";
				return EXIT_FAILURE;
			}

			// read the graph
			const char* filename = gernmfile.c_str();
			ifstream is(filename);
			thp_adjlist_bidir adjlist_bidir;
			is >> adjlist_bidir;

			int *optimal = NULL;
			long optimal_size = 0, minor_iter = 0, major_iter = 0;
			uint64_t flops_count = 0;
			int n = adjlist_bidir.num_vertices();

			if (kernel == "edmonds-karp") {
				tbgl_adjlist &graph = adjlist_bidir.bgl_adjlist();
				int source = adjlist_bidir.source();
				int sink   = adjlist_bidir.target();
				edmonds_karp_mincut(graph, source, sink, optimal, optimal_size);

			} else {
				if (kernel == "krause") {
					// set right the kernel
					set_min_norm_point_krause_kernel();

				} else
				if (kernel == "fujishige") {
					// set right the kernel
					set_min_norm_point_fujishige_kernel();

				} else
				if (kernel == "hpsfo") {
					// set right the kernel
					set_min_norm_point_hpsfo_kernel();

				} else {
					cerr << "Input Error: unknown 'kernel' type: " << kernel << "\n";
					return EXIT_FAILURE;
				}

				// invoke the SFO kernel API
				sfo_function_mincut(adjlist_bidir, optimal, optimal_size, major_iter, minor_iter, flops_count);
			}

			// print solution
			fprintf(stderr, "--------------------------\n");
			fprintf(stderr, "n=%d, major=%ld, minor=%ld, optimal size=%ld\n", n, major_iter, minor_iter, optimal_size);
			fprintf(stderr, "--------------------------\n");

			printf("--------------------------\n");
			printf("n=%d, optimal size=%ld\n", n, optimal_size);
			printf("--------------------------\n");
			for (int i=0; i < optimal_size; ++i) {
				printf("%f\n", (double) optimal[i]);
			}
			delete[] optimal;
		}
	}
	catch(std::exception& e) {
		cerr << "Exception: " << e.what() << "\n";
		return EXIT_FAILURE;
	}
	catch(...) {
		cerr << "Exception of unknown type!\n";
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
