/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_function_logdet.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 11.05.2012
// Description : Provides the main API entry point for invoking the log determinant
//               application
//============================================================================

#include "sfo_function_logdet.h"
#include "logdet_sf_context.h"
#include "logdet_sf_inc_context.h"
#include "sfo_kernel_factory.h"
#include "decorator_memoization_sf_inc_context.h"

/**
 * Setup and run the LogDet optimization
 */
void sfo_function_logdet(int n, double* K_data, double* s_data, double fv, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count) {

	tsfo_matrix<double> K(n, n, K_data);
	tsfo_vector<double> s(n, s_data);

	// LogDet requires specific resolution
	double resolution = 0.0;

	// setup the logdet application
	tlogdet_sf_inc_context sf_context(K, s, fv);
	tdecorator_memoization_sf_inc_context decorator(sf_context);

	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(decorator,
			tabstract_sfo_kernel::DEFAULT_EPSILON, resolution);
	sfo_kernel.run();

	major_iter 	 = sfo_kernel.major_iter();
	minor_iter 	 = sfo_kernel.minor_iter();
	flops_count  = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	sf_context.postprocess(subset);

	// save the solution
	optimal_size = subset.size();
	subset.copy_to(optimal);
}

/**
 * Setup and run the LogDet optimization
 */
void sfo_function_logdet_noninc(int n, double* K_data, double* s_data, double fv, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count) {

	tsfo_matrix<double> K(n, n, K_data);
	tsfo_vector<double> s(n, s_data);

	// LogDet requires specific resolution
	double resolution = 0.0;

	// setup the logdet application
	tlogdet_sf_context sf_context(K, s, fv);
	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context,
			tabstract_sfo_kernel::DEFAULT_EPSILON, resolution);
	sfo_kernel.run();

	major_iter 	 = sfo_kernel.major_iter();
	minor_iter 	 = sfo_kernel.minor_iter();
	flops_count  = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	sf_context.postprocess(subset);

	// save the solution
	optimal_size = subset.size();
	subset.copy_to(optimal);
}
