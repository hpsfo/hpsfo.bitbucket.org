/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_function_mincut.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 10.05.2012
// Description : Provides the main API entry point for invoking the Minimum
//               Cut application
//============================================================================

#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <boost/ref.hpp>

#include "sfo_function_mincut.h"
#include "mincut_sf_inc_context.h"
#include "sfo_kernel_factory.h"

/**
 * Setup and run the MinCut optimization
 */
void sfo_function_mincut(const char* filename, int*& optimal, long& optimal_size,
		long& major_iter, long& minor_iter) {
	uint64_t flops_count = 0;
	sfo_function_mincut(filename, optimal, optimal_size, major_iter, minor_iter, flops_count);
}

/**
 * Setup and run the MinCut optimization
 */
void sfo_function_mincut(const char* filename, int*& optimal, long& optimal_size,
		long& major_iter, long& minor_iter, uint64_t& flops_count) {

	// read the graph
	ifstream is(filename);
	thp_adjlist_bidir graph;
	is >> graph;

	sfo_function_mincut(graph, optimal, optimal_size, major_iter, minor_iter, flops_count);
}

/**
 * Setup and run the MinCut optimization
 */
void sfo_function_mincut(thp_adjlist_bidir& graph, int*& optimal, long& optimal_size,
		long& major_iter, long& minor_iter, uint64_t& flops_count) {

	// setup the mincut application
	tmincut_sf_inc_context sf_context(graph);
	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context);
	sfo_kernel.run();

	major_iter  = sfo_kernel.major_iter();
	minor_iter  = sfo_kernel.minor_iter();
	flops_count = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	subset.append(graph.source());
	sf_context.postprocess(subset);

	// save the solution
	optimal_size = subset.size();
	subset.copy_to(optimal);
}

/**
 * Setup and run the MinCut optimization
 */
void sfo_function_mincut_noninc(thp_adjlist_bidir& graph, int*& optimal, long& optimal_size,
		long& major_iter, long& minor_iter, uint64_t& flops_count) {

	// setup the mincut application
	tmincut_sf_context sf_context(graph);

	// create an initial permutation
	tsfo_vector<int> initial_perm(sf_context.ground().size());
	initial_perm.seqn();

	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context,
			tabstract_sfo_kernel::DEFAULT_EPSILON, tabstract_sfo_kernel::DEFAULT_RESOLUTION
				, initial_perm);
	sfo_kernel.run();

	major_iter  = sfo_kernel.major_iter();
	minor_iter  = sfo_kernel.minor_iter();
	flops_count = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	subset.append(graph.source());
	sf_context.postprocess(subset);

	// save the solution
	optimal_size = subset.size();
	subset.copy_to(optimal);
}

/**
 * Setup and run Edmonds-Karp Minimum Cut
 */
void edmonds_karp_mincut(tbgl_adjlist& graph, int source, int sink, int*& optimal, long& optimal_size) {
	// graph capacities
	tedge_capacity_map capacities = get(edge_capacity_t(), graph);

	// graph reverse edges
	treverse_edge_map reverses = get(edge_reverse_t(), graph);

	// graph reverse edges
	tedge_residual_capacity_map residuals = get(edge_residual_capacity_t(), graph);

	// graph vertex colors
	vector<default_color_type> colors(num_vertices(graph));

	// predecesors
	vector<tedge> predecesors(num_vertices(graph));

	// run the maximum flow algorithm
	int maximum_flow = edmonds_karp_max_flow(graph, source, sink, capacities, residuals, reverses,
			&colors[0], &predecesors[0]);

	// contains the solution
	set<int> mincut_nodes;
	tvertex_iterator u_iter, u_end;
	tout_edge_iterator e_iter, e_end;
	for (tie(u_iter, u_end) = vertices(graph); u_iter != u_end; ++u_iter) {
		for (tie(e_iter, e_end) = out_edges(*u_iter, graph); e_iter != e_end; ++e_iter) {
			if (colors[*u_iter] == colors[source] && colors[target(*e_iter, graph)] == colors[sink] && capacities[*e_iter] > 0) {
				mincut_nodes.insert(*u_iter);
				mincut_nodes.insert(target(*e_iter, graph));
			}
		}
	}

	// populate the optimal
	optimal_size = mincut_nodes.size();
	optimal = new int[optimal_size];
	int i = 0;
	for (set<int>::iterator iter = mincut_nodes.begin(); iter != mincut_nodes.end(); ++iter) {
		optimal[i++] = *iter;
	}
}
