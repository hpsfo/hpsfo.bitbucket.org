/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_matlab_adapter.cc
// Author      : Giovanni Azua (azuagarga@student.ethz.ch)
// Since       : 10.07.2012
// Description : Provides the main API entry point for invoking HPSFO
// 				 from Matlab
//============================================================================

#include <iostream>
#include "mex.h"
#include "matrix.h"

#include "matlab_sf_adapter.h"
#include "abstract_sfo_kernel.h"
#include "sfo_kernel_factory.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
	// first argument should be a function handle
	if (!mxIsClass(prhs[0], "function_handle")) {
		mexErrMsgTxt("ERROR: first input argument must be a function handle.");
	}

	// second argument should be a struct 'param' context
    if (!mxIsStruct(prhs[1])) {
       mexErrMsgTxt("ERROR: second argument must be a struct.");
    }

	// setup the mincut application
	tmatlab_sf_adapter sf_context(prhs[0], prhs[1]);
	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context);
	sfo_kernel.run();

	long major_iter  = sfo_kernel.major_iter();
	long minor_iter  = sfo_kernel.minor_iter();
	long flops_count = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	sf_context.postprocess(subset);

	// save the solution
	long optimal_size = subset.size();

	mxArray *result = mxCreateDoubleMatrix(subset.size(), 1, mxREAL);
	for (int i = 0; i < subset.size(); ++i) {
		(mxGetPr(result))[i] = subset[i];
	}
	plhs[0] = result;
}
