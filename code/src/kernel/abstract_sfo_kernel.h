/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : abstract_sfo_kernel.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Defines the abstract interface for SFO kernels
//============================================================================

#ifndef ABSTRACT_SFO_KERNEL_H_
#define ABSTRACT_SFO_KERNEL_H_

#include "abstract_sf_context.h"
#include "abstract_sf_inc_context.h"

class tabstract_sfo_kernel {
private:
	// context
	tabstract_sf_context* 	  m_sf_context;
	tabstract_sf_inc_context* m_sf_inc_context;

	void init();

protected:
	// initial permutation
	tsfo_vector<int> m_initial_perm;

	// ground set (V)
	const tsfo_vector<int>& m_ground;

	const double m_epsilon;
	const double m_resolution;

	// subset of ground set (A)
	tsfo_vector<int> 	m_subset;
	double 				m_subopt;
	tsfo_vector<double> m_x;
	const int 	 		m_n;

	// internally used by the algorithm
	long 				m_major_iter;
	long 				m_minor_iter;
	uint64_t 			m_flops_count;
	double 				m_eval_empty;
	int 				m_number_extreme_points;

public:
	// constructor
	tabstract_sfo_kernel(tabstract_sf_context& sf_context, double epsilon = DEFAULT_EPSILON,
			double resolution = DEFAULT_RESOLUTION, tsfo_vector<int> initial_perm = tsfo_vector<int>());
	tabstract_sfo_kernel(tabstract_sf_inc_context& context, double epsilon = DEFAULT_EPSILON,
			double resolution = DEFAULT_RESOLUTION, tsfo_vector<int> initial_perm = tsfo_vector<int>());

	// run the optimization
	virtual void run() = 0;

	 	 	 	        int n() const;
			   	 	 double epsilon() const;
			   	 	 double resolution() const;
 const tsfo_vector<double>& x() const;
    const tsfo_vector<int>& subset() const;
				 	 double subopt() const;
				   	   long major_iter() const;
			  	   	   long minor_iter() const;
			  	  uint64_t flops_count() const;
	  tabstract_sf_context* current_sf_context();
	  tabstract_sf_context* sf_context();
  tabstract_sf_inc_context* sf_inc_context();

	// virtual destructor
	virtual ~tabstract_sfo_kernel() {
		// empty
	}

	/**
	 * default allowed computational error bounds
	 */
	static const double DEFAULT_EPSILON    = 1e-10;

	/**
	 * Lower bound for nonzero values of |f(X)-f(Y)| for Y \subset X;
	 * default value 1.0 for integer valued submodular function f
	 */
	static const double DEFAULT_RESOLUTION = 1.0;
};

// reusable initialization method
inline void tabstract_sfo_kernel::init() {
	m_subopt  		 = 0.0;
	m_major_iter 	 = 0;
	m_minor_iter 	 = 0;
	m_flops_count 	 = 0;
	m_eval_empty 	 = 0.0;

	// initial permutation
	if (m_initial_perm.is_empty()) {
		m_initial_perm.size(m_ground.size());
		m_initial_perm.seqn();
	}
	assert(m_initial_perm.size() == m_ground.size());
}

// constructor
inline tabstract_sfo_kernel::tabstract_sfo_kernel(tabstract_sf_context& sf_context, double epsilon,
		double resolution, tsfo_vector<int> initial_perm)
	: m_ground(sf_context.ground()), m_x(m_ground.size(), 0.0), m_n(m_ground.size()), m_epsilon(epsilon),
	  	  m_resolution(resolution), m_initial_perm(initial_perm) {
	m_sf_context 	 = &sf_context;
	m_sf_inc_context = NULL;

	init();
}

// constructor
inline tabstract_sfo_kernel::tabstract_sfo_kernel(tabstract_sf_inc_context& sf_inc_context, double epsilon,
		double resolution, tsfo_vector<int> initial_perm)
	: m_ground(sf_inc_context.ground()), m_x(m_ground.size(), 0.0), m_n(m_ground.size()), m_epsilon(epsilon),
	  	  m_resolution(resolution), m_initial_perm(initial_perm) {
	m_sf_context 	 = NULL;
	m_sf_inc_context = &sf_inc_context;

	init();
}

// n reader
inline int tabstract_sfo_kernel::n() const {
	return m_n;
}

// epsilon reader
inline double tabstract_sfo_kernel::epsilon() const {
	return m_epsilon;
}

// resolution reader
inline double tabstract_sfo_kernel::resolution() const {
	return m_resolution;
}

// x reader
inline const tsfo_vector<int>& tabstract_sfo_kernel::subset() const {
	return m_subset;
}

// subset reader
inline const tsfo_vector<double>& tabstract_sfo_kernel::x() const {
	return m_x;
}

// subopt reader
inline double tabstract_sfo_kernel::subopt() const {
	return m_subopt;
}

// major iteration reader
inline long tabstract_sfo_kernel::major_iter() const {
	return m_major_iter;
}

// minor iteration reader
inline long tabstract_sfo_kernel::minor_iter() const {
	return m_minor_iter;
}

// flops count reader
inline 	uint64_t tabstract_sfo_kernel::flops_count() const {
	return m_flops_count;
}

inline tabstract_sf_context* tabstract_sfo_kernel::sf_context() {
	assert(m_sf_context != NULL);

	return m_sf_context;
}

inline tabstract_sf_inc_context* tabstract_sfo_kernel::sf_inc_context() {
	return m_sf_inc_context;
}

inline tabstract_sf_context* tabstract_sfo_kernel::current_sf_context() {
	tabstract_sf_context* current = (m_sf_inc_context != NULL) ? m_sf_inc_context : m_sf_context;
	assert(current != NULL);

	return current;
}

#endif /* ABSTRACT_SFO_KERNEL_H_ */
