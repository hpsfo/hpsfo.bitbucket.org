/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_kernel_factory.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 09.07.2012
// Description : Factory Method for creating SFO kernel instances
//============================================================================

#include "sfo_kernel_factory.h"

using namespace std;
using namespace boost;

tsfo_kernel_factory tsfo_kernel_factory::INSTANCE;

/**
 * Returns the "main" edge just created. This is a helper function to ease the
 * creation of edges. It will set the capacity of the reverse edge to zero.
 * Note! the method returns the edge created so that the associated capacity
 * property does not get lost due to the edge copy instance being destroyed
 * because of going out of scope.
 */
tedge tsfo_kernel_factory::add_edge(tbgl_adjlist& graph, tedge_capacity_map& capacities,
  treverse_edge_map& reverses, set<tedge>& saved_reverses, int begin, int end, int capacity) {
  bool success;
  tedge edge, reverse;

  // add edge
  tie(edge, success)    = boost::add_edge(begin, end, graph);
  assert(success);

  tie(reverse, success) = boost::add_edge(end, begin, graph);
  assert(success);
  saved_reverses.insert(reverse);

  // set the capacities
  capacities[edge]      = capacity;
  capacities[reverse]   = 0;

  // add reverse edge
  reverses[edge]        = reverse;
  reverses[reverse]     = edge;

  return edge;
}

// destructor
tsfo_kernel_factory::~tsfo_kernel_factory() {
	delete m_sfo_kernel;
}
