/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : hpc_min_norm_point_kernel.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete HPC Fujishige SFO kernel
//============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <boost/chrono.hpp>

#include "sfo_vector.h"
#include "sfo_matrix.h"
#include "sfo_matrix_tria.h"
#include "hpsfo_min_norm_point_kernel.h"

using namespace boost::chrono;

void thpsfo_min_norm_point_kernel::run() {
	assert(m_n > 0);

	/**
	 * algorithm's global vectors and matrices
	 */
	const tsfo_vector<double> ZEROS(m_n, 0.0);
	const tsfo_vector<double> ONES (m_n, 1.0);

	double min_F = 0.0, primal, dual, duality_gap;
//	const double MAX_NORM = max_norm();
	const double DUALITY_GAP_EPSILON = epsilon();

	tsfo_vector<double> 	 w;
	tsfo_vector<double> 	 pss;
	tsfo_matrix_tria<double> r (m_n, 1, 0.0);
	tsfo_matrix     <double> ps(m_n, 1, 0.0);

#ifdef ENABLE_SFO_KERNEL_TRACE
	// benchmark using high-resolution timer
	system_clock::time_point glob_start = system_clock::now();
	system_clock::time_point iter_start = system_clock::now();
	system_clock::time_point step_start = system_clock::now();
	duration<double> sec;
	double elapsed_step1 = 0;
	double elapsed_step2 = 0;
	double elapsed_step3 = 0;
	double elapsed_step4 = 0;
	tsfo_vector<int> del_histogram(10, 0);
#endif

	int kx = 0;
	double xxp = 1.0E+10;
	m_x.reset();

	// we need a valid set at this point
	tsfo_vector<int> s = m_initial_perm;

	/**************   step 0 *********************/
	/*
	 * [polyhedron_greedy]
	 */
	double sf_new, sf_previous = 0.0;
	if (sf_inc_context() != NULL) {
		sf_inc_context()->reset();
		for (int i = 0; i < m_n; i++) {
			sf_new = sf_inc_context()->f_inc(m_ground[s[i]]);
			ps(i, 0) = sf_new - sf_previous;
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count++;
#endif
			sf_previous = sf_new;
		}
	} else {
		assert(sf_context() != NULL);

		tsfo_vector<double> x;
		for (int i = 0; i < m_n; i++) {
			x.append(m_ground[s[i]]);
			sf_new = sf_context()->f(x);
			ps(i, 0) = sf_new - sf_previous;
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count++;
#endif
			sf_previous = sf_new;
		}
	}

	double xx = 1.0 + ps[0].dot_product(ps[0]);
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += 2*m_n + 1;
#endif

	pss.append(xx - 1.0);
	r(0, 0) = sqrt(xx);
#ifdef ENABLE_FLOPS_COUNT
	m_flops_count += 2;
#endif

	w.append(1.0);
	int k = 1;

#ifdef ENABLE_SFO_KERNEL_TRACE
	sec = system_clock::now() - step_start;
	elapsed_step1 = sec.count();
	step_start = system_clock::now();
#endif

	/**************   step 1 *********************/
	/******************* (A) ********************/

	while (1) { /* 1000 */
		m_major_iter++;

		m_x = ps*w;
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 2*ps.rows()*ps.cols();
#endif

		/******************* (B) ********************/

		m_x.sort_index(s);

		/*
		 * [polyhedron_greedy] generate a new extreme base
		 */
		ps.append_column(ZEROS);
		sf_previous = 0.0;
		min_F = DBL_MAX;
		if (sf_inc_context() != NULL) {
			sf_inc_context()->reset();
			for (int i = 0; i < m_n; i++) {
				sf_new = sf_inc_context()->f_inc(m_ground[s[i]]);
				ps(s[i], k) = sf_new - sf_previous;
#ifdef ENABLE_FLOPS_COUNT
				m_flops_count++;
#endif
				sf_previous = sf_new;
				min_F  		= min(sf_new, min_F);
			}
		} else {
			assert(sf_context() != NULL);

			tsfo_vector<double> x;
			for (int i = 0; i < m_n; i++) {
				x.append(m_ground[s[i]]);
				sf_new = sf_context()->f(x);
				ps(s[i], k) = sf_new - sf_previous;
#ifdef ENABLE_FLOPS_COUNT
				m_flops_count++;
#endif
				sf_previous = sf_new;
				min_F  		= min(sf_new, min_F);
			}
		}

		m_number_extreme_points++;

		// check for duality gap convergence
		primal 		= min(min_F, 0.0);
		dual 		= m_x.sum_lz();
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += m_x.size();
#endif
		duality_gap = primal - dual;
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 4;
#endif
//		if (duality_gap < sqrt(MAX_NORM / m_n)*epsilon()) {
		if (duality_gap < DUALITY_GAP_EPSILON) {
#ifdef ENABLE_SFO_KERNEL_TRACE
			sec = system_clock::now() - iter_start;
			double real_time = sec.count();
			sec = system_clock::now() - glob_start;
			double glob_time = sec.count();
			tsfo_vector<int> smin(m_n + 1);
			int jmin = size_min_minimizer(smin);
			if (m_major_iter <= SFO_KERNEL_TRACE_STEP) {
				fprintf(stderr, "Iteration\tJmin\tError\tElapsed\tCorral\tDuality Gap\tStep1\tStep2\tStep3\tStep4\tAcc\n");
			}
			fprintf(stderr, "%d\t%d\t%e\t%e\t%d\t%e\t%e\t%e\t%e\t%e\t%e\n", m_major_iter,
					jmin, 0.0, real_time, k, duality_gap, elapsed_step1, elapsed_step2,
						elapsed_step3, elapsed_step4, glob_time);
			printf("==== delete histogram ====\n");
			del_histogram.print();
#endif
			break;
		}

		/******************* (C) ********************/

		xx = m_x.dot_product(m_x);
		const tsfo_vector<double>& psk = ps[k];
		double pp  = psk.dot_product(psk);
		double xpj = psk.dot_product(m_x);
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 6*m_n;
#endif
		pss.append(pp);

#ifdef ENABLE_FLOPS_COUNT
		m_flops_count++;
#endif
		if ((xxp - xpj) <= epsilon()) {
			kx++;
			if (kx >= 10)
				break;
		}
		xxp = xx;

		double ss = max(max(1.0, pp), pss.max());

#ifdef ENABLE_SFO_KERNEL_TRACE
		if (m_major_iter % SFO_KERNEL_TRACE_STEP == 0) {
			sec = system_clock::now() - iter_start;
			double real_time = sec.count();
			sec = system_clock::now() - glob_start;
			double glob_time = sec.count();
			tsfo_vector<int> smin(m_n + 1);
			int jmin = size_min_minimizer(smin);
			if (m_major_iter == SFO_KERNEL_TRACE_STEP) {
				fprintf(stderr, "Iteration\tJmin\tError\tElapsed\tCorral\tDuality Gap\tStep1\tStep2\tStep3\tStep4\tAcc\n");
			}
			fprintf(stderr, "%d\t%d\t%e\t%e\t%d\t%e\t%e\t%e\t%e\t%e\t%e\n", m_major_iter, jmin,
					(xx - xpj)/ss, real_time, k, duality_gap, elapsed_step1, elapsed_step2,
						elapsed_step3, elapsed_step4, glob_time);
			elapsed_step1 = 0;
			elapsed_step2 = 0;
			elapsed_step3 = 0;
			elapsed_step4 = 0;
			iter_start = system_clock::now();
		}
#endif

#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 2;
#endif
		if ((xx - xpj)/ss < epsilon()) {
#ifdef ENABLE_SFO_KERNEL_TRACE
			sec = system_clock::now() - iter_start;
			double real_time = sec.count();
			sec = system_clock::now() - glob_start;
			double glob_time = sec.count();
			tsfo_vector<int> smin(m_n + 1);
			int jmin = size_min_minimizer(smin);
			if (m_major_iter <= SFO_KERNEL_TRACE_STEP) {
				fprintf(stderr, "Iteration\tJmin\tError\tElapsed\tCorral\tDuality Gap\tStep1\tStep2\tStep3\tStep4\tAcc\n");
			}
			fprintf(stderr, "%d\t%d\t%e\t%e\t%d\t%e\t%e\t%e\t%e\t%e\t%e\n", m_major_iter, jmin,
					(xx - xpj)/ss, real_time, k, duality_gap, elapsed_step1, elapsed_step2,
						elapsed_step3, elapsed_step4, glob_time);
			printf("==== delete histogram ====\n");
			del_histogram.print();
#endif
			break;
		}

		if (k == m_n) {
			printf("Trying to augment full-dimensional S!\n");
			break;
		}

		/******************* (D) ********************/
#ifndef NDEBUG
		bool flag;
		for (int i = 0; i < k; ++i) {
			flag = false;
			for (int j = 0; j < m_n; j++) {
#ifdef ENABLE_FLOPS_COUNT
				m_flops_count += 2;
#endif
				double psik = ps(j, i) - ps(j, k);
				if (psik*psik > epsilon()) {
					flag = true;
					break;
				}
			}
			if (!flag)
				break;
		}

		if (!flag) {
			printf("The generated point is in S!\n");
			break;
		}
#endif

		/******************* (E), (F), (G) ********************/
		r.append_column(ONES);

		r[k] += ps*psk;
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 2*ps.rows()*ps.cols() + ps.rows();
#endif

		// updates r for the new ps[k] by solving R^Tx=r[k]
        tsfo_vector<double>& rk = r[k];
		rk = r.solve_forward(rk);
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += r.rows()*r.cols();
#endif

		double rr = rk.dot_product(rk, k);
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 2*k;
#endif

		r(k, k) = sqrt(1.0 + pp - rr);
#ifdef ENABLE_FLOPS_COUNT
		m_flops_count += 3;
#endif

		k++;
		w.append(0.0);

#ifdef ENABLE_SFO_KERNEL_TRACE
		sec = system_clock::now() - step_start;
		elapsed_step2 += sec.count();
#endif

		/**************   step 2 *********************/
		/* [update_xhat] */

		while (1) { /* 2000 */
#ifdef ENABLE_SFO_KERNEL_TRACE
			step_start = system_clock::now();
#endif

			m_minor_iter++;

			// solve R^T\bar{u}=e
			tsfo_vector<double> ubar = r.solve_forward(ONES);
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += r.rows()*r.cols();
#endif

			// solve Ru=\bar{u}
			tsfo_vector<double> u = r.solve_backward(ubar);
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += r.rows()*r.cols();
#endif

			double usum = u.sum();
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += u.size();
#endif

			tsfo_vector<double> mu = u / usum;
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += mu.size();
#endif

#ifdef ENABLE_SFO_KERNEL_TRACE
			sec = system_clock::now() - step_start;
			elapsed_step3 += sec.count();
			step_start = system_clock::now();
#endif

			// check whether new point lies in the relative interior of
			// the convex hull of S
			bool stop_minor = true;
			for (int j = 0; j < k; ++j) {
				if (mu[j] < epsilon()) {
					stop_minor = false;
					break;
				}
			}

			if (stop_minor) {
				w = mu;
				break;
			}

			/**************   step 3 *********************/

			/******************* (A), (B) ********************/
			tsfo_vector<double> lambda_minus_mu = w - mu;
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += lambda_minus_mu.size();
#endif
			tsfo_vector<double> bounds = w / lambda_minus_mu;
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += bounds.size();
#endif
			double beta = 1.0;
			for (int j = 0; j < k; ++j) {
				if (lambda_minus_mu[j] > epsilon()) {
					beta = min(beta, bounds[j]);
				}
			}

			/******************* (C) ********************/
			w.linear_combination(w, 1 - beta, mu, beta);
#ifdef ENABLE_FLOPS_COUNT
			m_flops_count += 2*w.size();
#endif

			for (int j = (k - 1); j >= 0; --j) {
				/******************* (D), (E), (F), (G) ********************/
				if (w[j] < epsilon()) {
					w.delete_elem(j);
					pss.delete_elem(j);
					ps.delete_column_with_gap(j, j);
					r.delete_column_with_gap(j, j, true);
					r.triangularize(j, 1, genrot_high_precision_slow);
					k--;
#ifdef ENABLE_FLOPS_COUNT
					m_flops_count += r.cols()*r.cols()/2 - r.cols()*j + j*j/2;
#endif

#ifdef ENABLE_SFO_KERNEL_TRACE
					// save the delete positions
					double bin = ((double) j / (double) m_n) * del_histogram.size();
					del_histogram[(int) bin]++;
#endif
				}
			}

#ifdef ENABLE_SFO_KERNEL_TRACE
			sec = system_clock::now() - step_start;
			elapsed_step4 += sec.count();
#endif
		}
	}

	// minimum minimizer
	tsfo_vector<int> smin(m_n + 1);
	int jmin = size_min_minimizer(smin);

	// compute the optimal solution function value for the optimal subset
	for (int i = 1; i <= jmin; i++) {
		m_subset.append(m_ground[smin[i] - 1]);
	}

	m_subopt = current_sf_context()->f(m_subset);

#ifdef ENABLE_FLOPS_COUNT
	// update flop count
	m_flops_count += current_sf_context()->flops_count();
#endif
}

// destructor
thpsfo_min_norm_point_kernel::~thpsfo_min_norm_point_kernel() {
	// empty
}
