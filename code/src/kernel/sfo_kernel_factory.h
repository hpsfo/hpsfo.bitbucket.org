/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_kernel_factory.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 09.07.2012
// Description : Factory Method for creating SFO kernel instances
//============================================================================

#ifndef SFO_KERNEL_FACTORY_H_
#define SFO_KERNEL_FACTORY_H_

#include <cassert>
#include <boost/config.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/properties.hpp>
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/graph/push_relabel_max_flow.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <boost/ref.hpp>
#include <iostream>
#include <string>
#include <vector>

#include "hpsfo_min_norm_point_kernel.h"
#include "abstract_sf_context.h"
#include "abstract_sf_inc_context.h"
#include "sfo_function_mincut.h"

enum tkernel {
	HPSFO
};

class tsfo_kernel_factory {
private:
	static const tkernel DEFAULT_KERNEL = HPSFO;

	tkernel   			  m_kernel;
	tabstract_sfo_kernel* m_sfo_kernel;

	// helper add edge function
	tedge add_edge(tbgl_adjlist& graph, tedge_capacity_map& capacities, treverse_edge_map& reverses,
		set<tedge>& saved_reverses, int begin, int end, int capacity);

public:
	// constructor
	tsfo_kernel_factory();

	// kernel reader
	tkernel kernel() const;

	// kernel writter
	void kernel(tkernel kernel);

	// create a new kernel instance
	template<class C>
	tabstract_sfo_kernel& create(C& context, double epsilon = tabstract_sfo_kernel::DEFAULT_EPSILON,
			double resolution = tabstract_sfo_kernel::DEFAULT_RESOLUTION,
				tsfo_vector<int> initial_perm = tsfo_vector<int>());

	// create a new assisted kernel instance
	template<class C>
	tabstract_sfo_kernel& create_assisted(C& context, double epsilon = tabstract_sfo_kernel::DEFAULT_EPSILON,
			double resolution = tabstract_sfo_kernel::DEFAULT_RESOLUTION);

	// destructor
	virtual ~tsfo_kernel_factory();

	static tsfo_kernel_factory INSTANCE;
};

// constructor
inline tsfo_kernel_factory::tsfo_kernel_factory() : m_kernel(DEFAULT_KERNEL) {
	m_sfo_kernel = NULL;
}

// kernel reader
inline tkernel tsfo_kernel_factory::kernel() const {
	return m_kernel;
}

// kernel writter
inline void tsfo_kernel_factory::kernel(tkernel kernel) {
	m_kernel = kernel;
}

// create new instance
template<class C>
inline tabstract_sfo_kernel& tsfo_kernel_factory::create(C& context, double epsilon,
		double resolution, tsfo_vector<int> initial_perm) {
	delete m_sfo_kernel;
	m_sfo_kernel = NULL;

	switch (m_kernel) {
		case HPSFO: {
			m_sfo_kernel = new thpsfo_min_norm_point_kernel(context, epsilon,
					resolution, initial_perm);
			break;
		}
		default: {
			fprintf(stderr, "Unknown kernel type: %d.\n", m_kernel);
			exit(EXIT_FAILURE);
		}
	}

	assert(m_sfo_kernel	!= NULL);
	return *m_sfo_kernel;
}

// create new instance
template<class C>
inline tabstract_sfo_kernel& tsfo_kernel_factory::create_assisted(C& context, double epsilon,
		double resolution) {

	// define the graph
	tsfo_vector<int> ground = context.ground();
	const int n = ground.size();
    const int NUMBER_OF_VERTICES = n + 2;

	tbgl_adjlist graph(NUMBER_OF_VERTICES);

    // location names
    tvertex_name_map names = get(vertex_name_t(), graph);

    // graph capacities
    tedge_capacity_map capacities = get(edge_capacity_t(), graph);

    // graph reverse edges
    treverse_edge_map reverses = get(edge_reverse_t(), graph);

    // graph reverse edges
    tedge_residual_capacity_map residuals = get(edge_residual_capacity_t(), graph);

    // keep track of reverse edges
    set<tedge> saved_reverses;

    // graph vertex colors
    vector<default_color_type> colors(num_vertices(graph));

    // predecesors
    vector<tedge> predecesors(num_vertices(graph));

    // set the source and sink indexes
    int SOURCE_INDEX = n;
    int SINK_INDEX   = n + 1;
	names[SOURCE_INDEX] = "Source";
	names[SINK_INDEX] 	= "Sink";
    int min_ground   	= ground.min();

    // build the graph
    tsfo_vector<int> empty_set;
    double F_empty = context.f(empty_set);

    int inf_capacity = std::numeric_limits<int>::max();

	// connect to Source and Sink
	add_edge(graph, capacities, reverses, saved_reverses, SOURCE_INDEX,   0, inf_capacity);
	add_edge(graph, capacities, reverses, saved_reverses, n - 1, SINK_INDEX, inf_capacity);

    map<int, double> evals;
    for (int i = 0; i < n - 1; ++i) {
    	int begin = ground[i] - min_ground;
    	names[begin] = boost::lexical_cast<string>(begin);

    	double F_i = 0.0;
    	if (evals.find(begin) == evals.end()) {
			tsfo_vector<int> i_set;
			i_set.push_back(ground[i]);
			F_i = context.f(i_set);
			evals[begin] = F_i;

    	} else {
    		F_i = evals[begin];
    	}

    	for (int j = i + 1; j < n; ++j) {
        	int end = ground[j] - min_ground;

        	double F_j = 0.0;
        	if (evals.find(end) == evals.end()) {
            	tsfo_vector<int> j_set;
        		j_set .push_back(ground[j]);
            	F_j = context.f(j_set);
    			evals[end] = F_j;

        	} else {
        		F_j = evals[end];
        	}

        	tsfo_vector<int> ij_set;
    		ij_set.push_back(ground[i]);
    		ij_set.push_back(ground[j]);
        	double F_ij = context.f(ij_set);

        	double F_capacity = F_i + F_j - F_ij - F_empty;
        	int capacity = round(F_capacity);
//        	printf("F_capacity=%e, capacity=%d\n", F_capacity, capacity);

        	add_edge(graph, capacities, reverses, saved_reverses, begin, end, capacity);
//        	add_edge(graph, capacities, reverses, saved_reverses, end, begin, capacity);
    	}
    }

//    // output the graph
//    string file_path = "/Users/bravegag/temp/out.dot";
//    ofstream out(file_path.c_str());
//    write_graphviz(out, graph, make_label_writer(names), make_label_writer(capacities), default_writer());
//
//    assert(false);

    // run BGL's Minimum Cut
    int*  optimal;
    long  optimal_size;
    edmonds_karp_mincut(graph, SOURCE_INDEX, SINK_INDEX, optimal, optimal_size);

	// build the initial permutation
    tsfo_vector<int> initial_perm;
    for (int i = 0; i < optimal_size; ++i) {
		// skip Source and Sink
    	if (optimal[i] == SOURCE_INDEX || optimal[i] == SINK_INDEX) {
			continue;
		}

    	initial_perm.push_back(min_ground + optimal[i]);
    }

    // clean up
    delete[] optimal;

	tsfo_vector<int> inv;
	set_difference(ground.begin(), ground.end(), initial_perm.begin(), initial_perm.end(),
			back_inserter(inv));
	for (int i = 0; i < inv.size(); ++i) {
		initial_perm.push_back(inv[i]);
	}

	return create(context, epsilon, resolution, initial_perm);
}

//template<class C>
//inline tabstract_sfo_kernel& tsfo_kernel_factory::create_assisted(C& context, double epsilon,
//		double resolution) {
//
//	// define the graph
//	tsfo_vector<int> ground = context.ground();
//	const int n 		 = ground.size();
//    const int min_ground = ground.min();
//
//    tsfo_vector<int> empty_set;
//    double F_empty = context.f(empty_set);
//
//    map<int, int> evals;
//    tsfo_vector<double> F_contribs(n, 0.0);
//    for (int i = 0; i < n - 1; ++i) {
//    	int ground_i = ground[i];
//    	int elem_i	 = ground[i] - min_ground;
//
//    	double F_i = 0.0;
//    	if (evals.find(ground_i) == evals.end()) {
//			tsfo_vector<int> i_set;
//			i_set.push_back(ground_i);
//			F_i = context.f(i_set);
//			evals[ground_i] = F_i;
//
//    	} else {
//    		F_i = evals[ground_i];
//    	}
//
//    	for (int j = i + 1; j < n; ++j) {
//        	int ground_j = ground[j];
//        	int elem_j	 = ground[j] - min_ground;
//
//        	double F_j = 0.0;
//        	if (evals.find(ground_j) == evals.end()) {
//            	tsfo_vector<int> j_set;
//        		j_set .push_back(ground_j);
//            	F_j = context.f(j_set);
//    			evals[ground_j] = F_j;
//
//        	} else {
//        		F_j = evals[ground_j];
//        	}
//
//        	tsfo_vector<int> ij_set;
//    		ij_set.push_back(ground_i);
//    		ij_set.push_back(ground_j);
//        	double F_ij = context.f(ij_set);
//
//        	// add the pair
//        	pair<int, int> ground_ij = make_pair(ground_i, ground_j);
//
//        	double F_contrib = round(F_i + F_j - F_ij - F_empty);
//
//        	F_contribs[elem_i] = max(F_contribs[elem_i], F_contrib);
//        	F_contribs[elem_j] = max(F_contribs[elem_j], F_contrib);
//    	}
//    }
//
//    // build the initial permutation
//    bool desc = false;
//    tsfo_vector<int> initial_perm = F_contribs.sort_index(desc);
//
//	return create(context, epsilon, resolution, initial_perm);
//}

#endif /* SFO_KERNEL_FACTORY_H_ */
