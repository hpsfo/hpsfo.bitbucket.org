/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : abstract_sfmin_kernel.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Defines the abstract interface for SFO kernels
//============================================================================

#ifndef ABSTRACT_SFMIN_KERNEL_H_
#define ABSTRACT_SFMIN_KERNEL_H_

#include "abstract_sf_context.h"
#include "abstract_sf_inc_context.h"
#include "abstract_sfo_kernel.h"

class tabstract_sfmin_kernel : public virtual tabstract_sfo_kernel {
protected:
	int    size_min_minimizer(tsfo_vector<int>& smin);
	double max_norm();

public:
	// constructor
	tabstract_sfmin_kernel(tabstract_sf_context& sf_context, double epsilon = DEFAULT_EPSILON,
			double resolution = DEFAULT_RESOLUTION, tsfo_vector<int> initial_perm = tsfo_vector<int>());
	tabstract_sfmin_kernel(tabstract_sf_inc_context& context, double epsilon = DEFAULT_EPSILON,
			double resolution = DEFAULT_RESOLUTION, tsfo_vector<int> initial_perm = tsfo_vector<int>());

	// virtual destructor
	virtual ~tabstract_sfmin_kernel() {
		// empty
	}
};

// constructor
inline tabstract_sfmin_kernel::tabstract_sfmin_kernel(tabstract_sf_context& sf_context, double epsilon,
		double resolution, tsfo_vector<int> initial_perm)
	: tabstract_sfo_kernel(sf_context, epsilon, resolution, initial_perm) {
	// empty
}

// constructor
inline tabstract_sfmin_kernel::tabstract_sfmin_kernel(tabstract_sf_inc_context& sf_inc_context, double epsilon,
		double resolution, tsfo_vector<int> initial_perm)
	: tabstract_sfo_kernel(sf_inc_context, epsilon, resolution, initial_perm) {
	// empty
}

inline double tabstract_sfmin_kernel::max_norm() {
	double result = 0;
	double FV = current_sf_context()->f(m_ground);
	double FVsingletons, Fsingleton;
	for (int i = 0; i < m_ground.size(); ++i) {
		tsfo_vector<double> fvground(m_ground);
		fvground.delete_elem(i);
		tsfo_vector<double> fground;
		fground.append(m_ground[i]);

		FVsingletons = FV - current_sf_context()->f(fvground);
		Fsingleton   = current_sf_context()->f(fground);
		result	 	+= pow(FVsingletons - Fsingleton, 2);
	}

	return result;
}

/**
 * Returns the size of the min minimizer
 */
inline int tabstract_sfmin_kernel::size_min_minimizer(tsfo_vector<int>& smin) {
	int jmin = 0;

	// tolerance for computing minimizers from MNP
	double eps0 = ((double) m_resolution / m_n);

	/* S^- = {u | x(u) < 0} */
	for (int i = 0; i <= m_n; i++) {
		smin[i] = 0;
	}

	double previous = 0.0;
	for (int i = 1, j = 1; i <= m_n; i++) {
		if (m_x[i - 1] < -eps0) {
			smin[j] = i;
			jmin = j;
			j++;
		}
	}

	return jmin;
}

#endif /* ABSTRACT_SFMIN_KERNEL_H_ */
