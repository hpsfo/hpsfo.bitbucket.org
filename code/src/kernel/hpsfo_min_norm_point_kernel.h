/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : hpc_min_norm_point_kernel.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 05.07.2012
// Description : Concrete HPC satoru SFO kernel
//============================================================================

#ifndef HPSFO_MIN_NORM_POINT_KERNEL_H_
#define HPSFO_MIN_NORM_POINT_KERNEL_H_

#include "abstract_sfmin_kernel.h"

class thpsfo_min_norm_point_kernel : public virtual tabstract_sfmin_kernel {
public:
	// constructor
	thpsfo_min_norm_point_kernel(tabstract_sf_context& sf_context, double epsilon = DEFAULT_EPSILON,
			double resolution = DEFAULT_RESOLUTION, tsfo_vector<int> initial_perm = tsfo_vector<int>());
	thpsfo_min_norm_point_kernel(tabstract_sf_inc_context& sf_inc_context, double epsilon = DEFAULT_EPSILON,
			double resolution = DEFAULT_RESOLUTION, tsfo_vector<int> initial_perm = tsfo_vector<int>());

	// run the optimization
	virtual void run();

	// destructor
	virtual ~thpsfo_min_norm_point_kernel();
};

// constructor
inline thpsfo_min_norm_point_kernel::thpsfo_min_norm_point_kernel(tabstract_sf_context& sf_context,
		double epsilon, double resolution, tsfo_vector<int> initial_perm)
	: tabstract_sfo_kernel(sf_context, epsilon, resolution, initial_perm), tabstract_sfmin_kernel(sf_context, epsilon, resolution, initial_perm) {
	//	empty
}

// constructor
inline thpsfo_min_norm_point_kernel::thpsfo_min_norm_point_kernel(tabstract_sf_inc_context& sf_inc_context,
double epsilon, double resolution, tsfo_vector<int> initial_perm)
	: tabstract_sfo_kernel(sf_inc_context, epsilon, resolution, initial_perm), tabstract_sfmin_kernel(sf_inc_context, epsilon, resolution, initial_perm) {
	//	empty
}

#endif /* HPSFO_MIN_NORM_POINT_KERNEL_H_ */
