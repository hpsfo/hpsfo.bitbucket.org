/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_function_quadpot.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 16.10.2012
// Description : Provides the main API entry point for invoking the quadratic
//				 potential app
//============================================================================

#ifndef SFO_FUNCTION_QUADPOT_H_
#define SFO_FUNCTION_QUADPOT_H_

#include <stdint.h>

#include "sfo_kernel_config.h"

void sfo_function_quadpot(const char* filename, int*& optimal, long& optimal_size,
	double*& mn_point, long& mn_point_size, long& major_iter, long& minor_iter, uint64_t& flops_count);

#endif /* SFO_FUNCTION_QUADPOT_H_ */

