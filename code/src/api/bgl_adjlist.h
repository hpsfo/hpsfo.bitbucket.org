/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : bgl_adjlist.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 20.07.2012
// Description : Boost's BGL Adjacency list representation.
//============================================================================

#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/edmonds_karp_max_flow.hpp>
#include <boost/graph/read_dimacs.hpp>

#ifndef BGL_ADJLIST_H_
#define BGL_ADJLIST_H_

using namespace std;
using namespace boost;

typedef adjacency_list_traits<multisetS, vecS, directedS> ttraits;

typedef adjacency_list<multisetS, vecS, directedS,
		// vertex properties
		property<vertex_index_t, int,
	    property<vertex_name_t, string,
		property<vertex_color_t, default_color_type> > >,
		// edge properties
		property<edge_capacity_t, int,
		property<edge_residual_capacity_t, int,
		property<edge_reverse_t, ttraits::edge_descriptor> > >, no_property, vecS> tbgl_adjlist;

typedef graph_traits<tbgl_adjlist>::vertex_descriptor 				tvertex;
typedef graph_traits<tbgl_adjlist>::edge_descriptor					tedge;
typedef property_map<tbgl_adjlist, edge_capacity_t>::type 			tedge_capacity_map;
typedef property_map<tbgl_adjlist, edge_reverse_t>::type 			treverse_edge_map;
typedef property_map<tbgl_adjlist, vertex_name_t>::type            	tvertex_name_map;
typedef property_map<tbgl_adjlist, vertex_color_t>::type 			tvertex_color_map;
typedef property_map<tbgl_adjlist, vertex_index_t>::type 			tvertex_index_map;
typedef graph_traits<tbgl_adjlist>::vertex_iterator 				tvertex_iterator;
typedef graph_traits<tbgl_adjlist>::edge_iterator 					tedge_iterator;
typedef graph_traits<tbgl_adjlist>::out_edge_iterator 				tout_edge_iterator;
typedef graph_traits<tbgl_adjlist>::in_edge_iterator 			   	tin_edge_iterator;
typedef property_map<tbgl_adjlist, edge_residual_capacity_t>::type 	tedge_residual_capacity_map;

#endif /* BGL_ADJLIST_H_ */
