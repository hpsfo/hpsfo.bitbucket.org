/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : logdet_moons_generator.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 13.07.2012
// Description : Generates logdet 2-moons problems given a n size
//============================================================================

#ifndef LOGDET_MOONS_GENERATOR_H_
#define LOGDET_MOONS_GENERATOR_H_

#include <iostream>

using namespace std;

class tlogdet_moons_generator {
private:
	static const double DEFAULT_NOISE = 0.4;
	static const int 	DEFAULT_SEED  = 2;

	int 	m_n;
	double* m_K;
	double*	m_s;
	double 	m_fv;
	int* 	m_subset;
	int 	m_optimal_size;
	double 	m_subopt;
	long	m_major_iter;
	long	m_minor_iter;

	friend istream& operator>>(istream &is, tlogdet_moons_generator& generator);
	friend ostream& operator<<(ostream &os, tlogdet_moons_generator& generator);

public:

	// constructor
	tlogdet_moons_generator();

	// generate the problem
	void generate(int n1, int n2, double noise = DEFAULT_NOISE, int seed = DEFAULT_SEED);

	// n reader
	int n() const;

	// K reader
	double* K();

	// s reader
	double* s();

	// fv reader
	double fv() const;

	// optimal_size reader
	int optimal_size() const;

	// subset reader
	int* subset();

	// subopt reader
	double subopt() const;

	// major_iter reader
	long major_iter() const;

	// major_iter reader
	long minor_iter() const;

	// destructor
	virtual ~tlogdet_moons_generator();
};

// n reader
inline int tlogdet_moons_generator::n() const {
	return m_n;
}

// K reader
inline double* tlogdet_moons_generator::K() {
	return m_K;
}

// s reader
inline double* tlogdet_moons_generator::s() {
	return m_s;
}

// optimal_size reader
inline int tlogdet_moons_generator::optimal_size() const {
	return m_optimal_size;
}

// subset reader
inline int* tlogdet_moons_generator::subset() {
	return m_subset;
}

// fv reader
inline double tlogdet_moons_generator::fv() const {
	return m_fv;
}

// subopt reader
inline double tlogdet_moons_generator::subopt() const {
	return m_subopt;
}

// major_iter reader
inline long tlogdet_moons_generator::major_iter() const {
	return m_major_iter;
}

// minor_iter reader
inline long tlogdet_moons_generator::minor_iter() const {
	return m_minor_iter;
}

istream& operator>>(istream &is, tlogdet_moons_generator& generator);

ostream& operator<<(ostream &os, tlogdet_moons_generator& generator);

#endif /* LOGDET_MOONS_GENERATOR_H_ */
