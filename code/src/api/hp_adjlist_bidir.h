/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : hp_adjlist_bidir.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 20.07.2012
// Description : Adjacency list class that offers optimized data structure
//  			 for fast performance graph function evaluation.
//============================================================================

#ifndef HP_ADJLIST_BIDIR_H_
#define HP_ADJLIST_BIDIR_H_

#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

#include "bgl_adjlist.h"

using namespace std;
using namespace boost;

/**
 * High-performance bidirectional adjacency list implementation tailored for
 * fast submodular function evaluation for different applications. The representation
 * requires bidirectionality to allow incremental function evaluation of graph-based
 * applications.
 */
class thp_adjlist_bidir {
private:
	int 		 m_num_vertices;
	int			 m_num_edges;
	int 		 m_source, m_target;
    int    		*m_in_startpos;
    int    		*m_in_end_nodes;
    double 		*m_in_weights;
    int    		*m_out_startpos;
    int    		*m_out_end_nodes;
    double 		*m_out_weights;

    // a copy of the loaded BGL is maintained
	tbgl_adjlist m_bgl_adjlist;

    friend istream& operator>>(istream &is, thp_adjlist_bidir& adjlist_bidir);

    // common reusable init methods
    void init(const thp_adjlist_bidir& another);
    void init(tbgl_adjlist& bgl_adjlist_bidir, int source, int target);

public:
    // constructors
    thp_adjlist_bidir();
    thp_adjlist_bidir(const thp_adjlist_bidir& another);
    thp_adjlist_bidir(tbgl_adjlist& bgl_adjlist_bidir, int source, int target);
    thp_adjlist_bidir(int num_vertices, int num_edges, int *in_startpos, int *in_end_nodes, double *in_weights, int *out_startpos, int *out_end_nodes, double *out_weights);

    thp_adjlist_bidir& operator=(const thp_adjlist_bidir& another);

    	  // readers
	 	   int num_edges() const;
	 	   int num_vertices() const;
	 	   int source() const;
	 	   int target() const;
	      int* in_end_nodes() const;
		  int* in_startpos() const;
	   double* in_weights() const;
     	  int* out_end_nodes() const;
	 	  int* out_startpos() const;
	   double* out_weights() const;
 tbgl_adjlist& bgl_adjlist();

    // destructor
    virtual ~thp_adjlist_bidir();
};

// common reusable init method
inline void thp_adjlist_bidir::init(const thp_adjlist_bidir& another) {
	assert(m_num_vertices == 0);
	assert(m_num_edges == 0);
	assert(m_source == 0);
	assert(m_target == 0);
	assert(m_in_startpos == NULL);
	assert(m_in_end_nodes == NULL);
	assert(m_in_weights == NULL);
	assert(m_out_startpos == NULL);
	assert(m_out_end_nodes == NULL);
	assert(m_out_weights == NULL);

	m_num_vertices	  = another.m_num_vertices;
	m_num_edges	  = another.m_num_edges;
	m_source	  = another.m_source;
	m_target	  = another.m_target;
	m_in_startpos = new int[m_num_vertices];
	memcpy(m_in_startpos, another.m_in_startpos, m_num_vertices*sizeof(int));

	m_in_end_nodes = new int[m_num_edges];
	memcpy(m_in_end_nodes, another.m_in_end_nodes, m_num_edges*sizeof(int));

	m_in_weights = new double[m_num_edges];
	memcpy(m_in_weights, another.m_in_weights, m_num_edges*sizeof(double));

	m_out_startpos = new int[m_num_vertices];
	memcpy(m_out_startpos, another.m_out_startpos, m_num_vertices*sizeof(int));

    m_out_end_nodes	= new int[m_num_edges];
	memcpy(m_out_end_nodes, another.m_out_end_nodes, m_num_edges*sizeof(int));

    m_out_weights = new double[m_num_edges];
	memcpy(m_out_weights, another.m_out_weights, m_num_edges*sizeof(double));
}

// common reusable init method
inline void thp_adjlist_bidir::init(tbgl_adjlist& bgl_adjlist_bidir, int source, int target) {
	assert(m_num_vertices == 0);
	assert(m_num_edges == 0);
	assert(m_source == 0);
	assert(m_target == 0);
	assert(m_in_startpos == NULL);
	assert(m_in_end_nodes == NULL);
	assert(m_in_weights == NULL);
	assert(m_out_startpos == NULL);
	assert(m_out_end_nodes == NULL);
	assert(m_out_weights == NULL);

	m_num_vertices = boost::num_vertices(bgl_adjlist_bidir);
	m_num_edges	   = boost::num_edges(bgl_adjlist_bidir) / 2;
	m_source	   = source;
	m_target	   = target;

	assert(m_source == 0);
//	assert(m_target == (m_num_vertices - 1));

	m_in_startpos 	= new int[m_num_vertices];
	m_in_end_nodes 	= new int[m_num_edges];
	m_in_weights 	= new double[m_num_edges];
	m_out_startpos 	= new int[m_num_vertices];
    m_out_end_nodes	= new int[m_num_edges];
    m_out_weights 	= new double[m_num_edges];

	tvertex_index_map indices = get(vertex_index, bgl_adjlist_bidir);
	tedge_capacity_map capacities = get(edge_capacity, bgl_adjlist_bidir);
	treverse_edge_map rev_edges = get(edge_reverse, bgl_adjlist_bidir);

	// NOTE: parallel edges are allowed

	// while populating the outgoing edges record the information
	// needed to easily build the incoming edges
	map<int, multiset<int> > in_edges;
	map<pair<int, int>, double> in_capacities;

	// populate the outgoing edges
	tedge_iterator ei, ei_end;
	int out_pos = 0, current = 0;
	m_out_startpos[0] = 0;
	for (tie(ei, ei_end) = boost::edges(bgl_adjlist_bidir); ei != ei_end; ++ei) {
		int begin = indices[boost::source(*ei, bgl_adjlist_bidir)];
		int end   = indices[boost::target(*ei, bgl_adjlist_bidir)];
		int capacity = capacities[*ei];

		// reverse edge are set to have capacity zero
		if (capacity == 0) {
			continue;
		}

//		printf("current=%d, edge %d -> %d\n", current, begin, end);

//		if (in_edges[end].find(begin) != in_edges[end].end()) {
//			printf("duplicate edge %d -> %d\n", begin, end);
//			continue;
//		}

		in_edges[end].insert(begin);
		in_capacities[make_pair(end, begin)] = capacity;

		m_out_end_nodes[out_pos] = end;
		m_out_weights  [out_pos] = capacity;
		while (current < begin) {
			m_out_startpos[++current] = out_pos;
//			printf("m_out_startpos[%d]=%d\n", current, out_pos);
		}
		out_pos++;
	}

	// populate the incoming edges
	int in_pos = 0;
	for (int i = 0; i < m_num_vertices; ++i) {
		const multiset<int>& in_edges_i = in_edges[i];
		int in_edges_i_size = in_edges_i.size();
		m_in_startpos[i] 	= in_pos;

		int end = i;
		for (multiset<int>::const_iterator iter = in_edges_i.begin(); iter != in_edges_i.end(); ++iter, ++in_pos) {
			int begin = *iter;
			m_in_end_nodes[in_pos] = *iter;
			m_in_weights  [in_pos] = in_capacities[make_pair(end, begin)];
		}
	}

	assert(out_pos == in_pos);
	if (in_pos != m_num_edges) {
		printf("discrepancy: in_pos=%d != num_edges=%d\n", in_pos, m_num_edges);
	}
	assert(in_pos  == m_num_edges);
}

// default constructor
inline thp_adjlist_bidir::thp_adjlist_bidir() {
	m_num_vertices	= 0;
	m_num_edges	 	= 0;
	m_source		= 0;
	m_target		= 0;
	m_in_startpos	= NULL;
    m_in_end_nodes	= NULL;
    m_in_weights	= NULL;
    m_out_startpos	= NULL;
    m_out_end_nodes	= NULL;
    m_out_weights	= NULL;
}

// copy constructor
inline thp_adjlist_bidir::thp_adjlist_bidir(const thp_adjlist_bidir& another) {
	init(another);
}

// constructor
inline thp_adjlist_bidir::thp_adjlist_bidir(int num_vertices, int num_edges, int *in_startpos, int *in_end_nodes, double *in_weights, int *out_startpos, int *out_end_nodes, double *out_weights) {
	m_num_vertices	= num_vertices;
	m_num_edges	 	= num_edges;
    m_in_startpos	= in_startpos;
    m_in_end_nodes	= in_end_nodes;
    m_in_weights	= in_weights;
    m_out_startpos	= out_startpos;
    m_out_end_nodes	= out_end_nodes;
    m_out_weights	= out_weights;
}

inline thp_adjlist_bidir& thp_adjlist_bidir::operator=(const thp_adjlist_bidir& another) {
	init(another);

	return *this;
}

// source reader
inline int thp_adjlist_bidir::source() const {
	return m_source;
}

// target reader
inline int thp_adjlist_bidir::target() const {
	return m_target;
}

// in_end_nodes reader
inline int* thp_adjlist_bidir::in_end_nodes() const {
	return m_in_end_nodes;
}

// in_startpos reader
inline int* thp_adjlist_bidir::in_startpos() const {
	return m_in_startpos;
}

// in_weights reader
inline double* thp_adjlist_bidir::in_weights() const {
	return m_in_weights;
}

// num_edges reader
inline int thp_adjlist_bidir::num_edges() const {
	return m_num_edges;
}

// num_vertices reader
inline int thp_adjlist_bidir::num_vertices() const {
	return m_num_vertices;
}

// out_end_nodes reader
inline int* thp_adjlist_bidir::out_end_nodes() const {
	return m_out_end_nodes;
}

// out_startpos reader
inline int* thp_adjlist_bidir::out_startpos() const {
	return m_out_startpos;
}

// out_weights reader
inline double* thp_adjlist_bidir::out_weights() const {
	return m_out_weights;
}

// bgl_adjlist reader
inline tbgl_adjlist& thp_adjlist_bidir::bgl_adjlist() {
	return m_bgl_adjlist;
}

istream& operator>>(istream &is, thp_adjlist_bidir& adjlist_bidir);

#endif /* HP_ADJLIST_BIDIR_H_ */
