/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_kernel_config.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 14.05.2012
// Description : Configuration settings e.g. switching the Sfo kernels
//============================================================================

#ifndef SFO_KERNEL_CONFIG_H_
#define SFO_KERNEL_CONFIG_H_

/**
 * Enables Krause's kernel (default)
 */
void set_min_norm_point_krause_kernel();

/**
 * Enables Fujishige's kernel
 */
void set_min_norm_point_fujishige_kernel();

/**
 * Enables HPSFO's kernel
 */
void set_min_norm_point_hpsfo_kernel();

/**
 * Enables Iwata's kernel
 */
void set_min_norm_point_iwata_kernel();

#endif /* SFO_KERNEL_CONFIG_H_ */
