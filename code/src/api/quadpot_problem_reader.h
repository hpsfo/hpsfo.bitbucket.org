/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : quapot_problem_reader.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since	   : 13.07.2012
// Description : Reads quadpot problem files
//============================================================================

#ifndef QUADPOT_PROBLEM_READER_H_
#define QUADPOT_PROBLEM_READER_H_

#include <iostream>
#include <vector>
#include <set>

using namespace std;

class tquadpot_problem_reader {
private:
	vector<double>* 	m_w;
	vector<set<int> >*	m_regions;

	friend istream& operator>>(istream &is, tquadpot_problem_reader& reader);

public:

	// constructor
	tquadpot_problem_reader();

	// n reader
	int n() const;

	// w reader
	const vector<double>& w() const;

	// regions reader
	const vector<set<int> >& regions() const;

	// destructor
	virtual ~tquadpot_problem_reader();
};

// n reader
inline int tquadpot_problem_reader::n() const {
	return (*m_w).size();
}

// w reader
inline const vector<double>& tquadpot_problem_reader::w() const {
	return *m_w;
}

// regions reader
inline const vector<set<int> >& tquadpot_problem_reader::regions() const {
	return *m_regions;
}

istream& operator>>(istream &is, tquadpot_problem_reader& reader);

#endif /* QUADPOT_PROBLEM_READER_H_ */
