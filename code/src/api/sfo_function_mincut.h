/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_function_mincut.h
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 10.05.2012
// Description : Provides the main API entry point for invoking the Minimum
//               Cut application
//============================================================================

#ifndef SFO_FUNCTION_MINCUT_H_
#define SFO_FUNCTION_MINCUT_H_

#include "hp_adjlist_bidir.h"
#include "sfo_kernel_config.h"

void sfo_function_mincut(const char* filename, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter);

void sfo_function_mincut(const char* filepath, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count);

void sfo_function_mincut(thp_adjlist_bidir& graph, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count);

void sfo_function_mincut_noninc(thp_adjlist_bidir& graph, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count);

void edmonds_karp_mincut(tbgl_adjlist& graph, int source, int sink, int*& optimal,
		long& optimal_size);

#endif /* SFO_FUNCTION_MINCUT_H_ */
