/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_function_corpussel.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 18.07.2012
// Description : Provides the main API entry point for invoking the Corpus Subset
// 				 selection application
//============================================================================

#include "sfo_function_corpussel.h"
#include "corpussel_sf_inc_context.h"
#include "sfo_kernel_factory.h"

void sfo_function_corpussel(const char* filename, double lambda, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter) {
	uint64_t flops_count = 0;
	sfo_function_corpussel(filename, lambda, optimal, optimal_size, major_iter,
			minor_iter, flops_count);
}

void sfo_function_corpussel(const char* filename, double lambda, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count) {

	// read the graph
	ifstream is(filename);
	thp_adjlist_bidir graph;
	is >> graph;

	sfo_function_corpussel(graph, lambda, optimal, optimal_size, major_iter,
			minor_iter, flops_count);
}

void sfo_function_corpussel(thp_adjlist_bidir& graph, double lambda, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count) {

	// CorpusSel requires specific resolution
	double resolution = 0.0;

	// setup the mincut application
	tcorpussel_sf_inc_context sf_context(graph, lambda);
	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context,
			tabstract_sfo_kernel::DEFAULT_EPSILON, resolution);
	sfo_kernel.run();

	major_iter  = sfo_kernel.major_iter();
	minor_iter  = sfo_kernel.minor_iter();
	flops_count = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	subset.append(graph.source());
	sf_context.postprocess(subset);

	// save the solution
	optimal_size = subset.size();
	subset.copy_to(optimal);
}

void sfo_function_corpussel_noninc(thp_adjlist_bidir& graph, double lambda, int*& optimal,
		long& optimal_size, long& major_iter, long& minor_iter, uint64_t& flops_count) {

	// CorpusSel requires specific resolution
	double resolution = 0.0;

	// setup the mincut application
	tcorpussel_sf_context sf_context(graph, lambda);
	tabstract_sfo_kernel& sfo_kernel = tsfo_kernel_factory::INSTANCE.create(sf_context,
			tabstract_sfo_kernel::DEFAULT_EPSILON, resolution);
	sfo_kernel.run();

	major_iter  = sfo_kernel.major_iter();
	minor_iter  = sfo_kernel.minor_iter();
	flops_count = sfo_kernel.flops_count();

	// retrieve and post-process the solution
	tsfo_vector<int> subset = sfo_kernel.subset();
	subset.append(graph.source());
	sf_context.postprocess(subset);

	// save the solution
	optimal_size = subset.size();
	subset.copy_to(optimal);
}
