/**
 * HPSFO High-Performance Submodular Function Optimization
 * Note that a separate commercial license is available upon request.
 * Copyright (C) 2012  Giovanni Azua Garcia
 * bravegag@hotmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */
//============================================================================
// Name        : sfo_kernel_config.cc
// Author      : Giovanni Azua  (azuagarga@student.ethz.ch)
// Since       : 14.05.2012
// Description : Configuration settings e.g. switching the Sfo kernels
//============================================================================

#include "sfo_kernel_config.h"
#include "sfo_kernel_factory.h"

/**
 * Enables Krause's kernel (default)
 */
void set_min_norm_point_krause_kernel() {
//	tsfo_kernel_factory::INSTANCE.kernel(KRAUSE);
}

/**
 * Enables Fujishige's kernel
 */
void set_min_norm_point_fujishige_kernel() {
//	tsfo_kernel_factory::INSTANCE.kernel(FUJISHIGE);
}

/**
 * Enables HPSFO kernel (default)
 */
void set_min_norm_point_hpsfo_kernel() {
	tsfo_kernel_factory::INSTANCE.kernel(HPSFO);
}

/**
 * Enables Iwata kernel
 */
void set_min_norm_point_iwata_kernel() {
//	tsfo_kernel_factory::INSTANCE.kernel(IWATA);
}
