#!/bin/bash
myos=`uname -s`

# check whether enable_papi is set
if [ -z "${enable_papi+xxx}" ]; then 
    if [ "$myos" == "Linux" ]; then
        enable_papi="true"
    else
        enable_papi="false"
    fi
fi

# check whether summary is set
if [ -z "${summary+xxx}" ]; then
    summary="true"
fi

# check whether enable-measure-cache is set
if [ -z "${enable_measure_cache+xxx}" ]; then
    enable_measure_cache="false"
fi

# print header
echo -e "openmp_scheduler \t openmp_chunk \t omp_num_threads \t mkl_num_threads \t nb_block_size \t n \t rt \t flops \t mflops \t l1_dcm \t l2_dcm \t l3_tcm "

for n_size in {200,500,1000,2000,5000,10000}
do
	for openmp_scheduler in {"static","dynamic","guided"}
	do
		for openmp_chunk in {64,70,80,96,128,144}
		do
			for num_threads in {1,2,3,4,5,6} 
			do
				for nb_block_size in {4,8,9,10,11,12,13,14,15,16}
				do
					export MKL_NUM_THREADS=$num_threads
					export OMP_NUM_THREADS=$num_threads
					export OMP_SCHEDULE="$openmp_scheduler,$openmp_chunk"
					../build/autotune --n=$n_size --genrot-type=dlartg --enable-papi=$enable_papi --summary=$summary --enable-measure-cache=$enable_measure_cache --triangularize-nb=$nb_block_size --omp-scheduler=$openmp_scheduler --omp-chunk=$openmp_chunk --num-threads=$num_threads
				done
			done
		done 
	done
done
